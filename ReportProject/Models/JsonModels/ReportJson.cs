﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class ReportJson
    {
        public int salonId { get; set; }
        public string salonName { get; set; }
        public int? salonOrder { get; set; }
        public int? salonCity { get; set; }
        public double? numberOfTurns { get; set; }
        public double? totalServiceProfit { get; set; }
        public double? totalProductProfit { get; set; }
        public double? totalSales { get; set; }
        public double? totalTransactionPerCus { get; set; }
        public double? totalServicePerCus { get; set; }
        public double? totalProductPerCus { get; set; }
        public double? totalStaffSalary { get; set; }
        public double? salaryServicePerServiceIncome { get; set; }
        public double? totalPayOffKCS { get; set; }
        public double? totalSMSExpenes { get; set; }
        public double? totalProductCapital { get; set; }
        public double? totalProdutPrice { get; set; }
        public double? tax { get; set; }
        public double? totalDailyCostInventory { get; set; }
        public string rank { get; set; }
        public double? electricityAndWaterBill { get; set; }
        public double? rentWithTax { get; set; }
        public double? capitalSpending { get; set; }
        public double? mktExpense { get; set; }
        public double? shippingExpend { get; set; }
        public double? internetAndPhoneBill { get; set; }
        public double? socialInsuranceAndFixedCost { get; set; }
        public double? incomeTaxes { get; set; }
        public double? salonUnplannedSpending { get; set; }
        public double? officeRentAndSeviceCost { get; set; }
        public double? officeStaffSalary { get; set; }
        public double? salesSalary { get; set; }
        public double? itSalary { get; set; }
        public double? officeStaffSocialInsurance { get; set; }
        public double? unplannedSpending { get; set; }
        public double? directFee { get; set; }
        public double? salonFee { get; set; }
        public double? manageFee { get; set; }
        public double? totalIncomeAfterTax { get; set; }
        public double? compensation { get; set; }
        public double? otherIncome { get; set; }
        // add 20190226
        public double smDistributionToday { get; set; }
        public double totalProductShineMember { get; set; }
        public double totalShineMemberDistribution { get; set; }
        public double totalNetRevenue { get; set; }
        public double totalSecurityCheckOutSalary { get; set; }
    }
}
