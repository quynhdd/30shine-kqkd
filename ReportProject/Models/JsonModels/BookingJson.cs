﻿using ReportProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class BookingJson
    {
        public class BookingReal
        {
            public int Id { get; set; }
            public int? HourId { get; set; }
            public int? SalonId { get; set; }
            public int? StylistId { get; set; }
            public DateTime? DatedBook { get; set; }
            public int? BookingTempId { get; set; }
            public bool? IsBookOnline { get; set; }
            public bool? IsBookStylist { get; set; }
            public DateTime? CreatedDate { get; set; }
            public int? IsDelete { get; set; }
            public int? Os { get; set; }
            public int? HourSubId { get; set; }

        }
        public class BookingTempReal
        {
            public int Id { get; set; }
            public bool? IsCall { get; set; }
            public DateTime? IsCallTime { get; set; }
            public DateTime? IsCallTimeModified { get; set; }
            
        }
    }
}
