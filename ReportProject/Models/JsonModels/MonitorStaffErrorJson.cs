﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class MonitorStaffErrorJson
    {
        public class MonitorStaffErrorReal
        {
            public int Id { get; set; }
            public int? SalonId { get; set; }
            public bool IsCommit { get; set; }
            public int StatusError { get; set; }           
            public DateTime CreateDate { get; set; }
        }
    }
}
