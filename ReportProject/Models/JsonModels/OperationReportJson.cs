﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class OperationReportJsonOutPut
    {
        public List<ObjServiceFollow> totalServiceFollow { get; set; }
        public List<OperationReportJsonReal> dataBodyReport { get; set; }
        //public List<ObjHeaderServiceFollow> headerServiceFollow { get; set; }
    }
    public class OperationReportJsonReal
    {
        public int salonId { get; set; } // id salon
        public string salonName { get; set; } // ten salon
        public double totalIncome { get; set; } // tong doanh thu
        public double totalServiceInCome { get; set; }// tong doanh thu dich vu
        public double totalProductIncome { get; set; } // tong doanh thu my pham
        public double worktimeStylist { get; set; } // số Giờ làm việc
        public int numberOfTurns { get; set; }// so luong khach
        public int numberCustomerOld { get; set; } // so luong khach cu
        public int numberBillNotImg { get; set; }// so luong bill khong anh
        public int numberBill1Star { get; set; } // so luong bill 1 sao
        public int numberBill2Star { get; set; } // so luong bill 2 sao
        public int numberBill3Star { get; set; } // so luong bill 3 sao
        public int numberBill4Star { get; set; } // so luong bill 4 sao
        public int numberBill5Star { get; set; } // so luong bill 5 sao
        public int totalStarNumber { get; set; } // tong so sao danhg gia
        public int numberErrorMonitoring { get; set; } // so luong loi giam sat
        public int numberStylistTimekeeping { get; set; } // so luong stylist cham cong
        public int numberSkinnerTimekeeping { get; set; } // so luong skinner cham cong
        public int numberStylist { get; set; }// so luong stylist chinh thuc
        public int numberCanleBooking { get; set; } // so luong huy book
        public int numberDeleteBooking { get; set; } // so luong xóa book
        public int overtimeHoursStylist { get; set; }  // Số giờ tăng ca của stylist
        public int numberSlotBooking { get; set; } // Tổng số slot được booking
        public int numberBooking { get; set; } // Khách đặt trước online
        public int numberBookingBefor { get; set; } // Tổng số bill đặt trước hoàn thành  bill service
        public int numberBillWaited { get; set; } // bill chờ lâu
        //public List<ObjServiceFollow> DataServiceFollow { get; set; }
        public int totalShineCombo { get; set; } // tong so luong billShinecombo
        public int totalKidCombo { get; set; } // tong so luong KidCombo
        public int totalProtein { get; set; } // tong so luong PROTEIN
        public int totalMask { get; set; } // tong so luong Tay da chet
        public int totalExFoliation { get; set; } // tong so luong dap mat na
        public int totalGroupUonDuoi { get; set; } // tong so luong group uon + duoi
        public int totalGroupColorCombo { get; set; } // tong so luong group Color ComBo ( Nhuộm )
        public int? salonOrder { get; set; } // sắp xếp salon

    }

    public class ObjServiceFollow
    {
        public int? id { get; set; }
        public string serviceName { get; set; }
        public int totalServiceNumber { get; set; }
    }
    public class ObjHeaderServiceFollow
    {
        public int id { get; set; }
        public string serviceName { get; set; }
    }

    public class CustomerJson
    {
        [JsonIgnore]
        public int SalonId { get; set; }
        public int? TotalBill { get; set; }
        public int? OldCustomer { get; set; }
        public int? NewCustomer { get; set; }
        public int? BookInAdvance { get; set; }
        public int? CancelBook { get; set; }
        public int? BookAll { get; set; }
    }

    public class ShineMemberJson
    {
        [JsonIgnore]
        public int SalonId { get; set; }
        public int? TotalBuyShineMember { get; set; }
        public int? BillNewCustomer { get; set; }
        public int? BillOldCustomer { get; set; }
        public int? ShineMemberComeBack { get; set; }
        public int? TotalIncomePerShineMember { get; set; }

    }

    public class ProductivityJson
    {
        [JsonIgnore]
        public int SalonId { get; set; }
        public int TotalSalon { get; set; }
        public int TotalDay { get; set; }
        public decimal? TotalStylistWorkTime { get; set; }
        public decimal? TotalSkinnerWorkTime { get; set; }
    }

    public class QualityJson
    {
        [JsonIgnore]
        public int SalonId { get; set; }
        public decimal? TNDV { get; set; }
        public int? AvgRating { get; set; }
        public int? Mark1and2 { get; set; }
        public int? SpecialCustomer { get; set; }
        public int? StaffError { get; set; }
        public int? Wait { get; set; }
        public int? BillWithoutImage { get; set; }
    }
    public class Quality
    {
        [JsonIgnore]
        public int SalonId { get; set; }
        public decimal? TNDV { get; set; }
        public int? AvgRating { get; set; }
        public int? Mark1and2 { get; set; }
        public int? SpecialCustomer { get; set; }
        public int? StaffError { get; set; }
        public int? Wait { get; set; }
        public int? BillWithoutImage { get; set; }
        public int? ErrorSCSC { get; set; }
        public int? ErrorCurling { get; set; }
        public float? ErrorMissImage { get; set; }
        public int? Feedback { get; set; }
    }
    public class RevenueJson
    {
        [JsonIgnore]
        public int SalonId { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public decimal Income { get; set; }
        public int Type { get; set; }
        public int BillCount { get; set; }
        [JsonIgnore]
        public int TotalBill { get; set; }
        public int Quantity { get; set; }
    }


    public class TotalIncomeJson
    {
        [JsonIgnore]
        public int SalonId { get; set; }
        public int TotalIncome { get; set; }
    }

    public class LNGJson
    {
        [JsonIgnore]
        public int SalonId { get; set; }
        public double? DirectFee { get; set; }
        public double? SalonFee { get; set; }
        public double? ManageFee { get; set; }
    }

    public class OperationError
    {
        [JsonIgnore]
        public int SalonId { get; set; }
        public double? DirectFee { get; set; }
        public double? SalonFee { get; set; }
        public double? ManageFee { get; set; }
    }

    public class DataReportJson
    {
        public string ShortName { get; set; }
        public int SalonId { get; set; }
        public CustomerJson Customer { get; set; }
        public ShineMemberJson ShineMember { get; set; }
        public ProductivityJson Productivity { get; set; }
        public Quality Quality { get; set; }
        public TotalRevenueJson Revenue { get; set; }
        public LNGJson LNG { get; set; }
    }

    public class RevenueStatisticJson
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public decimal Income { get; set; }
        public int Type { get; set; }
        public int BillCount { get; set; }
        public int Quantity { get; set; }
    }

    public class SaleConfigJson
    {
        public byte? SalonType { get; set; }
        public object SaleConfig { get; set; }
    }

    public class TotalRevenueJson
    {
        public int TotalRevenue { get; set; }
        public int TotalBill { get; set; }
        public List<RevenueJson> ListService { get; set; }
    }


}
