﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class OperationReportV3Json
    {
        public class DataRevenueJson
        {
            public int SalonId { get; set; }
            public TotalRevenueJson Revenue { get; set; }
        }

        public class DataProductivityJson
        {
            public int SalonId { get; set; }
            public ProductivityJson Productivity { get; set; }
        }

        public class DataCustomerInfoJson
        {
            public int SalonId { get; set; }
            public CustomerJson Customer { get; set; }
        }

        public class DataShineMemberJson
        {
            public int SalonId { get; set; }
            public ShineMemberJson ShineMember { get; set; }
        }

        public class DataQualityJson
        {
            public int SalonId { get; set; }
            public Quality Quality { get; set; }
        }
    }
}
