﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class PostEmailJson
    {
        public List<string> to { get; set; }
        public string subject { get; set; }
        public string message { get; set; }
    }
}