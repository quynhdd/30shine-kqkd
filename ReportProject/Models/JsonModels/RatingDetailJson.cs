﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class RatingDetailJson
    {
        public class RatingDetailReal
        {
            public int Id { get; set; }
            public int? BillId { get; set; }
            public int? RatingValue { get; set; }
            public int? StarNumber { get; set; }
            public DateTime? CreatedTime { get; set; }
            public DateTime? ModifiedTime { get; set; }
        }
    }
}
