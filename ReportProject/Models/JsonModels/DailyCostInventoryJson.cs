﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class DailyCostInventoryJson
    {
        public class Obj
        {
            public byte status { get; set; }
            public string message { get; set; }
            public List<Data> data { get; set; }
        }


        public class Data
        {
            public int salonId { get; set; }
            public int id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public int? price { get; set; }
            public int? cost { get; set; }
            public double quantityOld { get; set; }
            public double quantityNew { get; set; }
            public double quantityLevelOfUse { get; set; }
            public double quantityReceived { get; set; }
            public double totalMoney { get; set; }
        }
    }
}
