﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class BillServiceJoin
    {
        public class BillServiceJoinFlowProductJson
        {
            public int? salonId { get; set; }
            public double totalProductIncome { get; set; }
            public bool? isPayByCard { get; set; }
        }

        public class BillServiceJoinFlowServiceJson
        {
            public int billId { get; set; }
            public int? salonId { get; set; }
            public int? serviceId { get; set; }
            public int? quantity { get; set; }
            public double totalServiceIncome { get; set; }
            public int? mark { get; set; }
            public int? voucherPercent { get; set; }
            public bool? isPayByCard { get; set; }
            public DateTime? CompleteBillTime { get; set; }

        }
        public class BillserviceJoinRatingDetail
        {
            public int billId { get; set; }
            public int? salonId { get; set; }
            public int? StarNumber { get; set; }
        }

        public class ObjectFlow
        {
            public List<BillServiceJoinFlowProductJson> flowProduct { get; set; }
            public List<BillServiceJoinFlowServiceJson> flowService { get; set; }
        }

        public class ObjectReport
        {
            public List<BillServiceHis> billServices { get; set; }
            public List<FlowService> flowServices { get; set; }
            public List<FlowProduct> flowProducts { get; set; }
            public List<TblSalon> salons { get; set; }
        }
    }
}
