﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class PostDailyReportJson
    {
        public string reportDate { get; set; }
        public int salonId { get; set; }
        public string salonShortName { get; set; }
        public string electricityAndWaterBill { get; set; }
        public string rentWithTax { get; set; }
        public string capitalSpending { get; set; }
        public string advertisementExpend { get; set; }
        public string shippingExpend { get; set; }
        public string internetAndPhoneBill { get; set; }
        public string socialInsuranceAndFixedCost { get; set; }
        public string incomeTaxes { get; set; }
        public string salonUnplannedSpending { get; set; }
        public string thaiHaRentAndSeviceCost { get; set; }
        public string officeStaffSalary { get; set; }
        public string salesSalary { get; set; }
        public string itSalary { get; set; }
        public string officeStaffSocialInsurance { get; set; }
        public string unplannedSpending { get; set; }
        public string dailyCostInventory { get; set; }
        public string compensation { get; set; }
        public string otherIncome { get; set; }
        public string smDistribution { get; set; }
        public string payOffKcs { get; set; }
        public string smsExpenses { get; set; }
        public string tax { get; set; }
        public string securityCheckoutSalary { get; set; }
    }
}
