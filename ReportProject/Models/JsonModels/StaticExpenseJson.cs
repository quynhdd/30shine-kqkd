﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class StaticExpenseJson
    { 

        public class Obj
        {
            public int cityId { get; set; }
            public string cityName { get; set; }
            public ReportJson[] salon { get; set; }

        }
    }
}
