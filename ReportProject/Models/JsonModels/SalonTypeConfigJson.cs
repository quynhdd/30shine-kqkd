﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class SalonTypeConfigJson
    {
        public class InputSalonTypeConfigJson
        {
            public int UserId { get; set; }
            public List<InputSalonType> data { get; set; }
        }

        public class InputSalonType
        {
            public int SalonId { get; set; }
            public int SalonType { get; set; }
        }

        public class OutputServiceSalonTypeConfig
        {
            public int SalonId { get; set; }
            public int Departmentd { get; set; }
            public int ServiceId { get; set; }
            public double ServiceCoefficient { get; set; }
            public double ServiceBonus { get; set; }
            public double CoefficientOvertimeHour { get; set; }
            public double CoefficientOvertimeDay { get; set; }
        }
    }
}
