﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class RankReportJson
    {
        public class RankReportBCKQKDJson
        {
            public string rank { get; set; }
            public string colorRank { get; set; }
        }
    }
}
