﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class StaffJson
    {
        public class InforStaff
        {
            public int Id { get; set; }
            public int? Type { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }            
            public string Fullname { get; set; }           
            public byte? IsDelete { get; set; }
            public int? SalonId { get; set; }
            public int? SkillLevel { get; set; }
            public DateTime? DateJoin { get; set; }
            public byte? Active { get; set; }
            public DateTime? NgayTinhThamNien { get; set; }
        }
    }
}
