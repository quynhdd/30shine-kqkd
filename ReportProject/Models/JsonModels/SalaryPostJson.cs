﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class SalaryPostJson
    {
        public string timeFrom { get; set; }
        public string timeTo { get; set; }
        public int salonId { get; set; }
        public int departmentId { get; set; }
        public int staffId { get; set; }
        public int active { get; set; }
    }
}
