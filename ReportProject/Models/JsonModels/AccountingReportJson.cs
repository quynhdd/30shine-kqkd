﻿using ReportProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class AccountingReportJson
    {
        public class GetAccountingReportReturnJson
        {
            public int salonId { get; set; }
            public string salonName { get; set; }
            public double totalIncome { get; set; }
            public double totalServiceIncome { get; set; }
            public double totalProductIncome { get; set; }
            public int shineCombo { get; set; }
            public double sendCompanyAccount { get; set; }
            public double sendPersonalAccount { get; set; }
            public double swipeCardMoney { get; set; }
            //public double billShineComboFree { get; set; }
            public double shineComboFreeIncome { get; set; }
            public int revokeNumberRHL4 { get; set; }
            public int shineComboFreeNumber { get; set; }
        }

        public class StaffBillServiceDetailJson
        {
            public int billId { get; set; }
            public int staffId { get; set; }
        }

        public class AccountingTaxReportJson
        {
            public Header header { get; set; }
            public ObjData total { get; set; }
            public List<ObjData> data { get; set; }
        }

        public class Header
        {
            public string salonName { get; set; }
            public string id { get; set; }
            public string staffName { get; set; }
            public string level { get; set; }
            public string departmentName { get; set; }
            public string totalSalary { get; set; }
            public string fixSalaryByDay { get; set; }
            public string totalServiceTurnoverBonusAll { get; set; }
            public string totalServiceTurnoverBonus { get; set; }
            public string totalServiceTurnoverHoursOvertimeBonus { get; set; }
            public string totalServiceTurnoverDayOvertimeBonus { get; set; }
            public string workDay { get; set; }
            public string totalServiceRatedVerySatisfied { get; set; }
            public HeaderShineCombo shineComBo { get; set; }
            public List<HeaderOtherService> service { get; set; }

        }

        public class HeaderShineCombo
        {
            public string serviceName { get; set; }
            public SubHeaderShineCombo subHeader { get; set; }
        }

        public class SubHeaderShineCombo
        {
            public string totalNumberOfShineCombo { get; set; }
            public string totalServiceRatedVerySatisfiedOld { get; set; }
            public string totalServiceRatedVerySatisfiedNew { get; set; }
            public string verySatisfied5Hour { get; set; }
            public string verySatisfied5Day { get; set; }
            public string verySatisfied4 { get; set; }
            public string verySatisfied3 { get; set; }
            public string verySatisfied3Discount { get; set; }
            public string satisfied { get; set; }
            public string notSatisfied { get; set; }
        }

        public class HeaderOtherService
        {
            public string serviceName { get; set; }
            public SubHeaderOtherService subHeader { get; set; }
        }

        public class SubHeaderOtherService
        {
            public string totalServiceNumber { get; set; }
            public string verySatisfied5Hour { get; set; }
            public string verySatisfied5Day { get; set; }
            public string verySatisfied3 { get; set; }
            public string satisfied { get; set; }
            public string notSatisfied { get; set; }
        }

        public class ObjData
        {
            public string salonName { get; set; }
            public int id { get; set; }
            public string staffName { get; set; }
            public string level { get; set; }
            public string departmentName { get; set; }
            public double totalSalary { get; set; }
            public double? fixSalaryByDay { get; set; }
            public int totalServiceTurnoverBonusAll { get; set; }
            public int totalServiceTurnoverBonus { get; set; }
            public int totalServiceTurnoverHoursOvertimeBonus { get; set; }
            public int totalServiceTurnoverDayOvertimeBonus { get; set; }
            public int workDay { get; set; }
            public double totalServiceRatedVerySatisfied { get; set; }
            public ObjDataShineCombo dataShineCombo { get; set; }
            public List<ObjDataOtherService> dataOtherService { get; set; }
        }

        public class ObjDataShineCombo
        {
            public string serviceName { get; set; }
            public int totalNumberOfShineCombo { get; set; }
            public int totalServiceRatedVerySatisfiedOld { get; set; }
            public int totalServiceRatedVerySatisfiedNew { get; set; }
            public double verySatisfied5Hour { get; set; }
            public double verySatisfied5Day { get; set; }
            public double verySatisfied4 { get; set; }
            public double verySatisfied3 { get; set; }
            public double satisfied { get; set; }
            public double notSatisfied { get; set; }
            public double verySatisfied3Discount { get; set; }
            public ObjDataShineCombo()
            {
                serviceName = Constant.SHINE_COMBO_100K;
            }

        }

        public class ObjDataOtherService
        {
            public string serviceName { get; set; }
            public int totalServiceNumber { get; set; }
            public double verySatisfied5Hour { get; set; }
            public double verySatisfied5Day { get; set; }
            public double verySatisfied3 { get; set; }
            public double satisfied { get; set; }
            public double notSatisfied { get; set; }
        }


        public class StaffJoinTimeKeepingJson
        {
            public int staffId { get; set; }
            public string staffName { get; set; }
            public int? salonId { get; set; }
            public int? type { get; set; }
            public DateTime? workDate { get; set; }
        }

        public class CountData
        {
            public int countService { get; set; }
            public ObjData data { get; set; }
        }

        public class StaffInfo
        {
            public int? StaffId { get; set; }
            public string StaffName { get; set; }
            public string StaffType { get; set; }
            public double? FixSalary { get; set; }
            //public string LevelName { get; set; }
            public DateTime? WorkDate { get; set; }
            //public DateTime CreateDate { get; set; }
            public double? BehaveSalary { get; set; }
        }

        public class BillServiceDetails
        {
            public int BillId { get; set; }
            public int StaffId { get; set; }
            public int OvertimeStatusValue { get; set; }
            public double ServiceIncomeBonus { get; set; }
            public double RatingMark { get; set; }
            public int ServiceId { get; set; }
            public int? VoucherPercent { get; set; }
            public double Coeficient { get; set; }
            public int Quantity { get; set; }
            public double ConventionPoint { get; set; }
            public DateTime? CompleteBillTime { get; set; }
            public double? ServiceBonus { get; set; }

        }


    }
}
