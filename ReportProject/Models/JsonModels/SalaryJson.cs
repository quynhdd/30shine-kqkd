﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class SalaryJson
    {
        public class Obj
        {
            public byte status { get; set; }
            public string message { get; set; }
            public List<Output_ListSalaryByStaff> data { get; set; }
        }
        public class Output_ListSalaryByStaff
        {
            /// <summary>
            /// lấy thông tin nhân viên
            /// </summary>
            public int Id { get; set; }
            public int? SalonId { get; set; }
            public string FullName { get; set; }
            public string NameCMT { get; set; }
            public string NumberCMT { get; set; }
            public int? DepartmentId { get; set; }
            public string NameType { get; set; }
            public string SkillName { get; set; }
            public string NganHang_TenTK { get; set; }
            public string NganHang_SoTK { get; set; }
            public string NumberInsurrance { get; set; }
            public string MST { get; set; }
            /// <summary>
            /// workday
            /// </summary>
            public int Workday { get; set; }
            /// <summary>
            /// lương hỗ trợ
            /// </summary>
            public double? FixSalaryOscillation { get; set; }
            /// <summary>
            /// get lương cơ bản
            /// </summary>
            public double? FixedSalaryConfig { get; set; }
            /// <summary>
            /// Lấy bản ghi lương của nhân viên
            /// </summary>
            //public Models.Entity.SalaryIncome SalaryIncome { get; set; }
            public double? FixedSalary { get; set; }
            public double? AllowanceSalary { get; set; }
            public double? OvertimeSalary { get; set; }
            public double? ProductSalary { get; set; }
            public double? ServiceSalary { get; set; }
            public double? TotalIncome { get; set; }
            public double? GrandTotalIncome { get; set; }
            public double? BehaveSalary { get; set; }
            /// <summary>
            /// Get số lần thưởng phạt
            /// </summary>
            public List<Output_SalaryIncomeChange_V1> salaryIncomeChange { get; set; }
            /// <summary>
            /// get data 30shinecare
            /// </summary>
            public List<Output_VoucherWaitTime> voucherWaitTime { get; set; }
        }

        public class Output_SalaryIncomeChange_V1
        {
            public string description { get; set; }
            public string changedType { get; set; }
            public int? point { get; set; }
        }

        public class Output_VoucherWaitTime
        {
            public double checkinmoney { get; set; }
            public double skinnermoney { get; set; }
            public double stylistmoney { get; set; }
        }
    }
}
