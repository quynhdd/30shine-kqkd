﻿using ReportProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class BillserviceJson
    {
        public class BillserviceReal
        {
            public int Id { get; set; }
            public int? StaffHairdresserId { get; set; }
            public int? ServiceId { get; set; }
            public string ProductIds { get; set; }
            public int? TotalMoney { get; set; }
            public int? StaffHairMassageId { get; set; }
            public int? Mark { get; set; }
            public string ServiceIds { get; set; }
            public string Images { get; set; }
            public int? SalonId { get; set; }
            public int? SellerId { get; set; }
            public byte? Pending { get; set; }
            public int? ReceptionId { get; set; }
            public DateTime? CompleteBillTime { get; set; }
            public int? CustomerId { get; set; }
            public bool? IsPayByCard { get; set; }
            public DateTime? InProcedureTime { get; set; }
            public DateTime? InProcedureTimeModifed { get; set; }
            public int? BookingId { get; set; }
            public DateTime? UploadImageTime { get; set; }
            public int? CheckoutId { get; set; }
            public DateTime? CreatedDate { get; set; }
            public int? CustomerUsedNumber { get; set; }
        }
        
    }
}
