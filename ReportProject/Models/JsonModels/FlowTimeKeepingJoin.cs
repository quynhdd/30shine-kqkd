﻿using ReportProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class FlowTimeKeepingJoin
    {
        public class FlowTimeKeepingJoinWorkTime
        {
            public int Id { get; set; }
            public int StaffId { get; set; }
            public int SalonId { get; set; }
            public int? staffType { get; set; }            
            public DateTime? WorkDate { get; set; }
            public int? WorkHour { get; set; }
            public int? WorkTimeId { get; set; }
            public int HourIdNumber { get; set; }
            public int? TotalHourFrame { get; set; }
            public TimeSpan? StrartTime { get; set; }
            public TimeSpan? EnadTime { get; set; }
            public string HourIds { get; set; }

        }
    }
}
