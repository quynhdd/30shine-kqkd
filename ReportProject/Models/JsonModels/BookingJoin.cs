﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Models.JsonModels
{
    public class BookingJoin
    {
        public class BookingJoinBillService
        {
            public int Id { get; set; }
            public int? BillId { get; set; }
            public int? SalonId { get; set; }
            public bool? IsBookOnline { get; set; }
            //public DateTime? CreatedDateBooking { get; set; }
            //public DateTime? CreatedDateBillservice { get; set; }
        }
    }
}
