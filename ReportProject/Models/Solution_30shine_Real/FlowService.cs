﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class FlowService
    {
        public int Id { get; set; }
        public int? BillId { get; set; }
        public int? ServiceId { get; set; }
        public int? Price { get; set; }
        public DateTime? CreatedDate { get; set; }
        public byte? IsDelete { get; set; }
        public int? Quantity { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? SalonId { get; set; }
        public int? SellerId { get; set; }
        public int? VoucherPercent { get; set; }
        public byte? CoefficientRating { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
