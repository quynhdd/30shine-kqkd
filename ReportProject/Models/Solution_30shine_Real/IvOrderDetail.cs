﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class IvOrderDetail
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int? QuantityType1 { get; set; }
        public int? QuantityType2 { get; set; }
        public int? QuantityType3 { get; set; }
        public int? QuantityType4 { get; set; }
        public int? QuantityType5 { get; set; }
        public int? QuantitySuggest { get; set; }
        public int? QuantityOrder { get; set; }
        public int? QuantityExport { get; set; }
        public int? QuantityImport { get; set; }
        public int? Cost { get; set; }
        public bool? IsAuto { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }
    }
}
