﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class Stylist4MenCredits
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsDelete { get; set; }
        public bool? Publish { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
