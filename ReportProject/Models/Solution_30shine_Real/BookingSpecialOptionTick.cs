﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class BookingSpecialOptionTick
    {
        public int Id { get; set; }
        public int? BookingId { get; set; }
        public string ConfigKey { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDelete { get; set; }
    }
}
