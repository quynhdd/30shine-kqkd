﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class TblPermissionDefaultPage
    {
        public int Id { get; set; }
        public int? MenuId { get; set; }
        public string MenuLink { get; set; }
        public int? PId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
