﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class StaticExpense
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public string ShortName { get; set; }
        public double? TotalSales { get; set; }
        public double? TotalServiceProfit { get; set; }
        public double? TotalCosmeticProfit { get; set; }
        public double? ProductSalary { get; set; }
        public double? TotalProductPrice { get; set; }
        public double? TotalStaffSalary { get; set; }
        public double? TotalProductUsed { get; set; }
        public double? OtherExpense { get; set; }
        public double? Mktexpense { get; set; }
        public double? Itexpense { get; set; }
        public double? DayExpense { get; set; }
        public double? Lngall30shine { get; set; }
        public DateTime? WorkDate { get; set; }
        public DateTime? CreatedTime { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public double? TotalIncomeAfterTax { get; set; }
        public double? NumberOfTurns { get; set; }
        public double? TotalTransactionPerCus { get; set; }
        public double? TotalServicePerCus { get; set; }
        public double? TotalProductPerCus { get; set; }
        public double? DirectFee { get; set; }
        public double? SalaryServicePerServiceIncome { get; set; }
        public double? TotalProductCapital { get; set; }
        public double? TotalPayOffKcs { get; set; }
        public double? TotalDailyCostInventory { get; set; }
        public double? ElectricityAndWaterBill { get; set; }
        public double? SalonFee { get; set; }
        public double? RentWithTax { get; set; }
        public double? CapitalSpending { get; set; }
        public double? TotalSmsexpenses { get; set; }
        public double? ShippingExpend { get; set; }
        public double? InternetAndPhoneBill { get; set; }
        public double? SocialInsuranceAndFixedCost { get; set; }
        public double? Tax { get; set; }
        public double? IncomeTaxes { get; set; }
        public double? SalonUnplannedSpending { get; set; }
        public double? ManageFee { get; set; }
        public double? OfficeRentAndServiceCose { get; set; }
        public double? OfficeStaffSalary { get; set; }
        public double? SalesSalary { get; set; }
        public double? OfficeStaffSocialInsurance { get; set; }
        public double? UnplannedSpending { get; set; }
        public DateTime? ModifyTime { get; set; }
        public double? Compensation { get; set; }
        public double? TotalOtherIncome { get; set; }
        public double TotalSmdistributionToday { get; set; }
        public double TotalProductSm { get; set; }
        public double TotalSmdistributon { get; set; }
        public double TotalNetRevenue { get; set; }
        public double TotalSecurityCheckoutSalary { get; set; }
    }
}
