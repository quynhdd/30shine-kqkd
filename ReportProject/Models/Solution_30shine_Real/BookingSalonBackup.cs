﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class BookingSalonBackup
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public int? SalonBackupId { get; set; }
        public bool IsDelete { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
