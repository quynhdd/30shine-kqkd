﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class CustomerHairSkinHistory
    {
        public int Id { get; set; }
        public string HairAttIds { get; set; }
        public string SkinAttIds { get; set; }
        public int? CustomerId { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreateDate { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
