﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class StyleMasterLog
    {
        public int Id { get; set; }
        public int? StyleMasterId { get; set; }
        public string PostNumber { get; set; }
        public int? StaffId { get; set; }
        public DateTime? PostedDate { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
