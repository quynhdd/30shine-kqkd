﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class ApiSlide
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Des { get; set; }
        public string Url { get; set; }
        public string Img { get; set; }
        public bool? Publish { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreatedTime { get; set; }
        public int? CategoryId { get; set; }
        public int? Order { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
