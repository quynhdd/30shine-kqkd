﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class ImageData
    {
        public int Id { get; set; }
        public int? Objid { get; set; }
        public string Images { get; set; }
        public string ImageBefore { get; set; }
        public string ImageAfter { get; set; }
        public string ImageCheckBefore { get; set; }
        public string ImageCheckAfter { get; set; }
        public string Slugkey { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDelete { get; set; }
    }
}
