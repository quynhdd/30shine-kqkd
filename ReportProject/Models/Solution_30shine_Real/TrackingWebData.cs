﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class TrackingWebData
    {
        public int Id { get; set; }
        public int? EventId { get; set; }
        public string Value { get; set; }
        public DateTime? CreatedTime { get; set; }
        public string TokenKey { get; set; }
        public string Meta { get; set; }
        public int? VersionId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
