﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class CustomerSurveyResultDetail
    {
        public int Id { get; set; }
        public int SurveyResultId { get; set; }
        public int QuestionId { get; set; }
        public int? AnswerId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsDelete { get; set; }
        public bool IsMultipleChoice { get; set; }
        public string Answer { get; set; }
    }
}
