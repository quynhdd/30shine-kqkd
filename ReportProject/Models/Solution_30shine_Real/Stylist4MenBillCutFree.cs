﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class Stylist4MenBillCutFree
    {
        public int Id { get; set; }
        public int? StudentId { get; set; }
        public int? CustomerId { get; set; }
        public string Images { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public int? ClassId { get; set; }
        public string ImageStatusId { get; set; }
        public string ImageChecked1 { get; set; }
        public string ImageChecked2 { get; set; }
        public string ImageChecked3 { get; set; }
        public string ImageChecked4 { get; set; }
        public string ErrorNote { get; set; }
        public string NoteByStylist { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public DateTime? UploadImageTime { get; set; }
        public int? HairStyleId { get; set; }
    }
}
