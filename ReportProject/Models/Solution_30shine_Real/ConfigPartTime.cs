﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class ConfigPartTime
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public double? CoefficientStylist { get; set; }
        public double? CoefficinetSkinner { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public double? CoefficientSalon { get; set; }
    }
}
