﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class IvOrder
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int InventoryOrderId { get; set; }
        public int InventoryPartnerId { get; set; }
        public string Note { get; set; }
        public int? OrderType { get; set; }
        public int? Status { get; set; }
        public bool? IsAuto { get; set; }
        public int? CosmeticType { get; set; }
        public int? UserIdOrder { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UserIdReview { get; set; }
        public DateTime? ReviewedDate { get; set; }
        public int? UserIdReceive { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }
    }
}
