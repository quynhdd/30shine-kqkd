﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class TblSalon
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Images { get; set; }
        public int? ManagerId { get; set; }
        public string Gmap { get; set; }
        public int? Order { get; set; }
        public bool? Publish { get; set; }
        public byte? IsDelete { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ManagerName { get; set; }
        public string Fanpage { get; set; }
        public string FanpageId { get; set; }
        public double? PercentCusNew { get; set; }
        public double? PercentCusOld { get; set; }
        public double? PercentSlot { get; set; }
        public string ShortName { get; set; }
        public bool? IsSalonHoiQuan { get; set; }
        public string SmsSignature { get; set; }
        public string Mktsorder { get; set; }
        public string ImgMap { get; set; }
        public bool? SalonOtp { get; set; }
        public bool? IsPartTimeHours { get; set; }
        public bool? IsPartTimeRating { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public bool? IsPartTimeSalon { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public DateTime? OpeningDate { get; set; }
        public string Maps { get; set; }
        public string Ward { get; set; }
        public int? RegionId { get; set; }
    }
}
