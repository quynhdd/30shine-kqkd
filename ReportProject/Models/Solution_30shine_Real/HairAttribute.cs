﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class HairAttribute
    {
        public int Id { get; set; }
        public string HairTypeName { get; set; }
        public int? ParentId { get; set; }
        public bool? IsDelete { get; set; }
        public bool? Status { get; set; }
        public int? Order { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
