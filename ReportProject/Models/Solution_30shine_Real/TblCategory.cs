﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class TblCategory
    {
        public int Id { get; set; }
        public int? Pid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Meta { get; set; }
        public bool? Publish { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? CreatedTime { get; set; }
        public byte? IsDelete { get; set; }
        public int? Order { get; set; }
        public string Images { get; set; }
        public string Thumb { get; set; }
        public int? OnWebId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
