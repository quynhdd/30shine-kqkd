﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class TimekeepingConfig
    {
        public int Id { get; set; }
        public byte? SalonType { get; set; }
        public byte? DayOfWeek { get; set; }
        public int? MinWorkHour { get; set; }
        public int? MaxWorkHour { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDelete { get; set; }
    }
}
