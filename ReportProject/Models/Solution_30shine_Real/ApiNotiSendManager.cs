﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class ApiNotiSendManager
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string DeviceRegistrationToken { get; set; }
        public DateTime? SendTime { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
