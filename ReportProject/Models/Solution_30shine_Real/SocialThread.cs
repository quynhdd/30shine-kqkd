﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class SocialThread
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte? Publish { get; set; }
        public DateTime? CreatedDate { get; set; }
        public byte? IsDelete { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? StType { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
