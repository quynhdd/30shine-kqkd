﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class BackupTableServiceSalonConfig
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public int? ServiceId { get; set; }
        public int? DepartmentId { get; set; }
        public double? ServiceCoefficient { get; set; }
        public double? ServiceBonus { get; set; }
        public double? CoefficientOvertimeHour { get; set; }
        public double CoefficientOvertimeDay { get; set; }
        public DateTime BackupDate { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsPublish { get; set; }
        public bool? IsDelete { get; set; }
    }
}
