﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class BillService
    {
        public int Id { get; set; }
        public string CustomerCode { get; set; }
        public int? StaffHairdresserId { get; set; }
        public int? ServiceId { get; set; }
        public string ProductIds { get; set; }
        public int? TotalMoney { get; set; }
        public byte? Vote { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? StaffHairMassageId { get; set; }
        public int? StaffMakeBill { get; set; }
        public string Note { get; set; }
        public int? Mark { get; set; }
        public byte? IsDelete { get; set; }
        public string ServiceIds { get; set; }
        public string Images { get; set; }
        public string ImagesRating { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? SalonId { get; set; }
        public int? SellerId { get; set; }
        public bool? CustomerNoInfo { get; set; }
        public byte? Pending { get; set; }
        public string BillCode { get; set; }
        public string PdfbillCode { get; set; }
        public bool? ServiceError { get; set; }
        public bool? IsCusFamiliar { get; set; }
        public int? TimesUsedService { get; set; }
        public int? TeamId { get; set; }
        public bool? IsBooking { get; set; }
        public bool? IsBooked { get; set; }
        public DateTime? AppointmentTime { get; set; }
        public DateTime? ShampooTime0 { get; set; }
        public DateTime? ShampooTime1 { get; set; }
        public DateTime? HairCutTime0 { get; set; }
        public DateTime? HairCutTime1 { get; set; }
        public DateTime? FinishTime { get; set; }
        public byte? Status { get; set; }
        public int? ReceptionId { get; set; }
        public DateTime? CompleteBillTime { get; set; }
        public string CustomerCode1 { get; set; }
        public int? CustomerId { get; set; }
        public bool? Vipcard { get; set; }
        public string VipcardGive { get; set; }
        public string VipcardUse { get; set; }
        public bool? IsOnline { get; set; }
        public int? PayMethodId { get; set; }
        public bool? Paid { get; set; }
        public int? BillStatusId { get; set; }
        public int? FeeCod { get; set; }
        public int? FeeExtra { get; set; }
        public bool? IsPayByCard { get; set; }
        public bool? IsX2 { get; set; }
        public DateTime? InProcedureTime { get; set; }
        public DateTime? InProcedureTimeModifed { get; set; }
        public int? BookingId { get; set; }
        public string ImageStatusId { get; set; }
        public string ImageChecked1 { get; set; }
        public string ImageChecked2 { get; set; }
        public string ImageChecked3 { get; set; }
        public string ImageChecked4 { get; set; }
        public string ErrorNote { get; set; }
        public string Hcitem { get; set; }
        public string NoteByStylist { get; set; }
        public int? EstimateTimeCut { get; set; }
        public DateTime? UploadImageTime { get; set; }
        public DateTime? EstimateTimeCutTime { get; set; }
        public bool? IsSulphite { get; set; }
        public int? CheckoutId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public int? CustomerUsedNumber { get; set; }
        public string QueueCheckoutInfo { get; set; }
        public int? SecurityId { get; set; }
        public int CustomerConfirm { get; set; }
        public bool? IsImages { get; set; }
        public int? MoneyPrePaid { get; set; }
        public int? MoneyDeductions { get; set; }
        public int? MoneyInBill { get; set; }
    }
}
