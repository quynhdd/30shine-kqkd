﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class OrderRecruitingStaff
    {
        public int Id { get; set; }
        public int? RegionId { get; set; }
        public int? SalonId { get; set; }
        public int? DepartmentId { get; set; }
        public int? CreatorId { get; set; }
        public int? StaffId { get; set; }
        public DateTime? StaffDate { get; set; }
        public int? Type { get; set; }
        public int? Quantity { get; set; }
        public DateTime? Needed { get; set; }
        public DateTime? Completed { get; set; }
        public int? Status { get; set; }
        public int? ReasonRevoke { get; set; }
        public int? CandidateId { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }
        public bool? HavingTry { get; set; }
    }
}
