﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class DcustomerInfo
    {
        public int CustomerId { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerName { get; set; }
        public bool? CustomerGender { get; set; }
        public bool CustomerIsMarry { get; set; }
        public double CustomerScore { get; set; }
        public DateTime? CustomerBirthday { get; set; }
        public int? CustomerCareerId { get; set; }
    }
}
