﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class Call
    {
        public int Id { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? DeleteTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Receiver { get; set; }
        public int? SenderId { get; set; }
        public int? ReceiverId { get; set; }
        public int? Seq { get; set; }
        public string Sender { get; set; }
        public DateTime? StartTime { get; set; }
        public bool? Status { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int? Length { get; set; }
        public bool? IsDelete { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public string Salon { get; set; }
        public int? SalonId { get; set; }
        public bool? IsAutoGenerate { get; set; }
        public int? Type { get; set; }
        public int? SubStatus { get; set; }
        public string Note { get; set; }
    }
}
