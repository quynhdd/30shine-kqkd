﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class StaffAutoLevelLog
    {
        public int Id { get; set; }
        public int? StaffId { get; set; }
        public int? SalonId { get; set; }
        public int? StaffTypeId { get; set; }
        public int? SkillLevelId { get; set; }
        public int? SkillLevelupId { get; set; }
        public int? LevelupId { get; set; }
        public DateTime? SDate { get; set; }
        public double? Point { get; set; }
        public double? Kcs { get; set; }
        public int? Warning { get; set; }
        public string TslDanhGia { get; set; }
        public bool? StatusSkillLevel { get; set; }
        public int? Approval { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifieldTime { get; set; }
        public bool? IsCondition { get; set; }
        public bool? IsDelete { get; set; }
        public bool? IsLevelUp { get; set; }
        public bool? IsApproval { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
