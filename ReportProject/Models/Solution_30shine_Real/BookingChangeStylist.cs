﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class BookingChangeStylist
    {
        public int Id { get; set; }
        public int? BookingTmpId { get; set; }
        public int? StylistCurrentId { get; set; }
        public int? StylistChangeId { get; set; }
        public string CustomerPhone { get; set; }
        public int? HourId { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDelete { get; set; }
        public bool? Publish { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
