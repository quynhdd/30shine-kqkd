﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class SmBillTemp
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public int? CustomerId { get; set; }
        public int? BookingId { get; set; }
        public int? StaffHairdresserId { get; set; }
        public int? StaffHairMassageId { get; set; }
        public int? ReceptionId { get; set; }
        public int? SellerId { get; set; }
        public string ServiceIds { get; set; }
        public string ProductIds { get; set; }
        public int? TotalMoney { get; set; }
        public int? Mark { get; set; }
        public string Images { get; set; }
        public string Note { get; set; }
        public DateTime? InProcedureTime { get; set; }
        public DateTime? InProcedureTimeModifed { get; set; }
        public byte? IsDelete { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? CompleteBillTime { get; set; }
        public string BillCode { get; set; }
        public string PdfbillCode { get; set; }
        public int? TeamId { get; set; }
        public byte? Status { get; set; }
        public bool? Vipcard { get; set; }
        public string VipcardGive { get; set; }
        public string VipcardUse { get; set; }
        public bool? IsOnline { get; set; }
        public int? PayMethodId { get; set; }
        public bool? Paid { get; set; }
        public int? BillStatusId { get; set; }
        public int? FeeCod { get; set; }
        public int? FeeExtra { get; set; }
        public bool? IsPayByCard { get; set; }
        public bool? IsX2 { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Hcitem { get; set; }
        public int? BillServiceId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
