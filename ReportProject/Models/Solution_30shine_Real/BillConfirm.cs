﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class BillConfirm
    {
        public int Id { get; set; }
        public int? BillId { get; set; }
        public string ServiceIds { get; set; }
        public string ProductIds { get; set; }
        public long? SentTime { get; set; }
        public int? TotalMoney { get; set; }
        public int CustomerConfirm { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
