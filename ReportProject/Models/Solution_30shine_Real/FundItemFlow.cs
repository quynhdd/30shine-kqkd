﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class FundItemFlow
    {
        public int Id { get; set; }
        public int? ImportId { get; set; }
        public int? ItemId { get; set; }
        public string ItemTitle { get; set; }
        public int? ItemMoney { get; set; }
        public DateTime? CreatedTime { get; set; }
        public bool? IsDelete { get; set; }
        public int? SourceId { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
