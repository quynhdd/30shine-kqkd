﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class SkinAttributeProduct
    {
        public int Id { get; set; }
        public int? SkinAttributeId { get; set; }
        public string ProductId { get; set; }
        public bool? IsDelete { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
