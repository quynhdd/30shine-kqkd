﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class ProductUsedStatistic
    {
        public int Id { get; set; }
        public int? StaffId { get; set; }
        public int? ServiceId { get; set; }
        public int? ProductId { get; set; }
        public double? QuantifyDefault { get; set; }
        public double? Quantify { get; set; }
        public string SlugKey { get; set; }
        public int? OrderNumber { get; set; }
        public DateTime? WorkDate { get; set; }
        public int? Status { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
    }
}
