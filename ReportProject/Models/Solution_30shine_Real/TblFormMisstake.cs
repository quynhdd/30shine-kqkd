﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class TblFormMisstake
    {
        public int Id { get; set; }
        public string Formality { get; set; }
        public string Description { get; set; }
        public bool? IsDelete { get; set; }
        public bool? IsPuslish { get; set; }
        public string Slug { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
