﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class KcsCheck3S
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public int? UserId { get; set; }
        public string Images { get; set; }
        public string Note { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDelete { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
