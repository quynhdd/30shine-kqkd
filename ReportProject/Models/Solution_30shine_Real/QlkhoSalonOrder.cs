﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class QlkhoSalonOrder
    {
        public int Id { get; set; }
        public string BillCode { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDelete { get; set; }
        public int? SalonId { get; set; }
        public int? StatusId { get; set; }
        public string NoteSalon { get; set; }
        public string NoteKho { get; set; }
        public string ProductIds { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? ExportTime { get; set; }
        public DateTime? ReceivedTime { get; set; }
        public int? Order { get; set; }
        public int? CheckCosmetic { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
