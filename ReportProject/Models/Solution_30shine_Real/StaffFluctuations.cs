﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class StaffFluctuations
    {
        public int Id { get; set; }
        public int? StaffId { get; set; }
        public string StaffName { get; set; }
        public byte? StaffType { get; set; }
        public int? SalonCurrentId { get; set; }
        public int? SalonChangeId { get; set; }
        public byte? StatusId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public int? NumberTransfer { get; set; }
        public int? NumberReceive { get; set; }
        public string Description { get; set; }
        public bool? IsDetele { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
