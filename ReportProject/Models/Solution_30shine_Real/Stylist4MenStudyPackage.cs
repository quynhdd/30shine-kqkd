﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class Stylist4MenStudyPackage
    {
        public int Id { get; set; }
        public string NamePackage { get; set; }
        public int? Price { get; set; }
        public bool? Publish { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiledTime { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
