﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class Stylist4MenPointClubs
    {
        public int Id { get; set; }
        public int? StaffId { get; set; }
        public int? PointTheory { get; set; }
        public int? PointVote { get; set; }
        public string Images { get; set; }
        public bool? IsDelete { get; set; }
        public bool? Publish { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public int? CreditsId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
