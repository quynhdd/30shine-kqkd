﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class ServiceTemp
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public bool IsExfoliate { get; set; }
        public bool IsMasked { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
    }
}
