﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class RatingDetail
    {
        public int Id { get; set; }
        public int? BillId { get; set; }
        public string RatingReasonId { get; set; }
        public int? RatingValue { get; set; }
        public int? RatingBonusValue { get; set; }
        public int? StarNumber { get; set; }
        public bool? Isdelete { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? Iscomeback { get; set; }
        public int? Version { get; set; }
        public int? StepNumber { get; set; }
        public bool? RatingReason1 { get; set; }
        public bool? RatingReason2 { get; set; }
        public bool? RatingReason3 { get; set; }
        public bool? RatingReason4 { get; set; }
        public bool? RatingReason5 { get; set; }
        public bool? RatingReason6 { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public bool? IsAuto { get; set; }
        public long? OrderNumber { get; set; }
        public bool? RattingReason7 { get; set; }
    }
}
