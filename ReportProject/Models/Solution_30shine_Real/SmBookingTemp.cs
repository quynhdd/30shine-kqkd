﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class SmBookingTemp
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public int? HourId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public byte? IsDelete { get; set; }
        public int? SalonId { get; set; }
        public int? StylistId { get; set; }
        public int? UserId { get; set; }
        public string Note { get; set; }
        public DateTime? DatedBook { get; set; }
        public bool? IsMakeBill { get; set; }
        public int? FlowTimeKeepingId { get; set; }
        public bool? IsSms { get; set; }
        public int? Smsstatus { get; set; }
        public DateTime? SendDate { get; set; }
        public int? Os { get; set; }
        public bool? NewCustomer { get; set; }
        public string NoteDelete { get; set; }
        public bool? IsCall { get; set; }
        public DateTime? IsCallTime { get; set; }
        public DateTime? IsCallTimeModified { get; set; }
        public bool? IsBookViaBill { get; set; }
        public bool? IsBookAtSalon { get; set; }
        public bool? IsAutoStylist { get; set; }
        public string TokenClient { get; set; }
        public string Macaddress { get; set; }
        public bool? IsLimited { get; set; }
        public bool? IsSetAutoStylist { get; set; }
        public bool? IsBookOnline { get; set; }
        public bool? IsBookStylist { get; set; }
        public int? Order { get; set; }
        public bool? AuthOtpIsSet { get; set; }
        public bool? AuthOtpIsVerified { get; set; }
        public string AuthOtpToken { get; set; }
        public DateTime? AuthOtpTimeSend { get; set; }
        public DateTime? AuthOtpTimeVerified { get; set; }
        public byte? AuthOtpSmsstatus { get; set; }
        public DateTime? AuthOtpTimeReceived { get; set; }
        public int? TheadId { get; set; }
        public string Version { get; set; }
        public int? SuggestSalonId { get; set; }
        public bool? SmsnotiIsSend { get; set; }
        public DateTime? SmsnotiSendTime { get; set; }
        public int? SmsnotiStatus { get; set; }
        public bool? CancelBookIsSendSms { get; set; }
        public DateTime? CancelBookTimeSend { get; set; }
        public byte? CancelBookSmsstatus { get; set; }
        public byte? CancelBookFrom { get; set; }
        public string TextNote1 { get; set; }
        public string TextNote2 { get; set; }
        public string SalonNote { get; set; }
        public DateTime? TimeSalonNote { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public double CustomerScore { get; set; }
        public int? SubHour { get; set; }
        public int? CustomerId { get; set; }
    }
}
