﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class TblCusInputBooking
    {
        public int Id { get; set; }
        public string CusName { get; set; }
        public string CusPhone { get; set; }
        public bool? Publish { get; set; }
        public bool? IsDelete { get; set; }
        public string Note { get; set; }
        public bool? Called { get; set; }
        public int? SalonId { get; set; }
        public DateTime? DateBook { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? StaffId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
