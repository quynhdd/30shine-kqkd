﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class InventoryImport
    {
        public int Id { get; set; }
        public string ProductIds { get; set; }
        public int? TotalMoney { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? CompleteBillTime { get; set; }
        public bool? IsDelete { get; set; }
        public int? SalonId { get; set; }
        public int? RecipientId { get; set; }
        public int? DoId { get; set; }
        public string Note { get; set; }
        public DateTime? ImportDate { get; set; }
        public int? ImportType { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
