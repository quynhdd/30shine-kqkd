﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class Appointment
    {
        public int Id { get; set; }
        public string AppointmentCode { get; set; }
        public DateTime? AppointmentTime { get; set; }
        public DateTime? CreatedTime { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public bool? CustomerNoInfo { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool? IsPending { get; set; }
        public bool? IsCancel { get; set; }
        public byte? IsDelete { get; set; }
        public int? StylistId { get; set; }
        public int? SalonId { get; set; }
        public int? SellerId { get; set; }
        public string PdfbillCode { get; set; }
        public bool? IsNotify { get; set; }
        public int? TeamId { get; set; }
        public string ServiceIds { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
