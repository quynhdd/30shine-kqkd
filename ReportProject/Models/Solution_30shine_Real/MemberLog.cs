﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class MemberLog
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Phone { get; set; }
        public int ProductId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int Amount { get; set; }
        public int? IsDelete { get; set; }
        public int BillId { get; set; }
        public int? Type { get; set; }
        public bool? IsSuccess { get; set; }
        public string CustomerName { get; set; }
        public bool IsCancel { get; set; }
        public string Note { get; set; }
    }
}
