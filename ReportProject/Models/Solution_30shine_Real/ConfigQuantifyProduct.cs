﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class ConfigQuantifyProduct
    {
        public int Id { get; set; }
        public int? DepartmentId { get; set; }
        public int? ServiceId { get; set; }
        public double? Volume { get; set; }
        public int? ProductId { get; set; }
        public int? TotalNumberService { get; set; }
        public double? Quantify { get; set; }
        public bool? IsPublish { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
    }
}
