﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class Application
    {
        public int Id { get; set; }
        public string Uid { get; set; }
        public DateTime? CreateTime { get; set; }
        public string Description { get; set; }
        public DateTime? DeleteTime { get; set; }
        public string Name { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
    }
}
