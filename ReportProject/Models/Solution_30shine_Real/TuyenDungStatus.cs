﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class TuyenDungStatus
    {
        public int Id { get; set; }
        public string StatusName { get; set; }
        public bool? IsDelete { get; set; }
        public bool? Approve { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
