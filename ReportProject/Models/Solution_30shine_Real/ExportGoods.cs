﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class ExportGoods
    {
        public int Id { get; set; }
        public int ExportId { get; set; }
        public int RecipientId { get; set; }
        public string GoodsIds { get; set; }
        public string Note { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? Level { get; set; }
        public byte? IsDelete { get; set; }
        public int? SalonId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
