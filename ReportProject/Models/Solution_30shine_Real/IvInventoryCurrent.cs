﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class IvInventoryCurrent
    {
        public int Id { get; set; }
        public int? InventoryId { get; set; }
        public int ProductId { get; set; }
        public double Cost { get; set; }
        public DateTime? AccountingDate { get; set; }
        public int? Begin { get; set; }
        public int? Import { get; set; }
        public int? Export { get; set; }
        public int? SellOrUse { get; set; }
        public int? End { get; set; }
        public double? VolumeRemain { get; set; }
        public int? SuggestOrder { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }
    }
}
