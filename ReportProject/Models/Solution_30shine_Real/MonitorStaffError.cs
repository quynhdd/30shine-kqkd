﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class MonitorStaffError
    {
        public int Id { get; set; }
        public int ReporterId { get; set; }
        public int? SalonId { get; set; }
        public int? StaffTypeId { get; set; }
        public int? StaffId { get; set; }
        public int? HandleId { get; set; }
        public string Images { get; set; }
        public int CategoryRootId { get; set; }
        public int CategorySubId { get; set; }
        public string NoteByReporter { get; set; }
        public int? ReviewerId { get; set; }
        public string NoteByReviewer { get; set; }
        public bool IsPending { get; set; }
        public bool IsCommit { get; set; }
        public int StatusError { get; set; }
        public int? ResolverId { get; set; }
        public DateTime? ResolveTime { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ConfirmTime { get; set; }
        public DateTime? CommitTime { get; set; }
        public DateTime? ModifyDate { get; set; }
        public bool IsDelete { get; set; }
        public int? PeriodBeforeConfirm { get; set; }
        public int? PeriodBeforeCommit { get; set; }
        public DateTime? ErrorTime { get; set; }
        public int? ErpStaffTypeId { get; set; }
    }
}
