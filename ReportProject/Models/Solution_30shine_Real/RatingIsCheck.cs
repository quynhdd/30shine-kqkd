﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class RatingIsCheck
    {
        public int Id { get; set; }
        public int? BillId { get; set; }
        public int? CustomerId { get; set; }
        public int? SalonId { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDelete { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
