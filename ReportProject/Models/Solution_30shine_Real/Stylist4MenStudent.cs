﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class Stylist4MenStudent
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public byte? Gender { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int? SnDay { get; set; }
        public int? SnMonth { get; set; }
        public int? SnYear { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public string Address { get; set; }
        public int? ClassId { get; set; }
        public bool? IsGraduate { get; set; }
        public bool? IsHire { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? Publish { get; set; }
        public bool? IsDelete { get; set; }
        public string Note { get; set; }
        public string ImageCmt1 { get; set; }
        public string ImageCmt2 { get; set; }
        public int? NumberOfCreateProfile { get; set; }
        public bool? IsAccept { get; set; }
        public int? TotalAmountCollected { get; set; }
        public int? TotalAmountPaid { get; set; }
        public int? TotalBill { get; set; }
        public int? LevelOfConcern { get; set; }
        public int? StudyPackageId { get; set; }
        public string NumberCmt { get; set; }
        public int? PointLtcat { get; set; }
        public int? Vote { get; set; }
        public string Images { get; set; }
        public int? PointTheoryCut { get; set; }
        public int? SalonId { get; set; }
        public int? StatusTuition { get; set; }
        public int? PointLtkn { get; set; }
        public int? Type { get; set; }
        public int? StaffId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
