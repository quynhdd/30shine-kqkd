﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class StaffAvatar
    {
        public int Id { get; set; }
        public int? ApproveUserId { get; set; }
        public int? StaffId { get; set; }
        public string Images { get; set; }
        public int? StatusId { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
    }
}
