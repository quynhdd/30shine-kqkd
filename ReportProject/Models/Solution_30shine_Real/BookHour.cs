﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class BookHour
    {
        public int Id { get; set; }
        public string Hour { get; set; }
        public DateTime? Day { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? Publish { get; set; }
        public byte? IsDelete { get; set; }
        public int? Slot { get; set; }
        public TimeSpan? HourFrame { get; set; }
        public DateTime? DatedBook { get; set; }
        public int? SalonId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
