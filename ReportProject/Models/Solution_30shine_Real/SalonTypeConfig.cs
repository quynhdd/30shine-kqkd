﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class SalonTypeConfig
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public int? SalonType { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDelete { get; set; }
    }
}
