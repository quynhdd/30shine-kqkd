﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class TblPayon
    {
        public int Id { get; set; }
        public int? TypeStaffId { get; set; }
        public int? KeyId { get; set; }
        public int? ForeignId { get; set; }
        public int? Value { get; set; }
        public string Hint { get; set; }
        public bool? Publish { get; set; }
        public byte? IsDelete { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? BaseSalary { get; set; }
        public byte? PayByTime { get; set; }
        public string Description { get; set; }
        public int? StaffId { get; set; }
        public string Key { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
