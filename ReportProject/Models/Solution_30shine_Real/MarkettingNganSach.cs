﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class MarkettingNganSach
    {
        public int Id { get; set; }
        public decimal? SoTien { get; set; }
        public DateTime? Ngay { get; set; }
        public string GhiChu { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifileDate { get; set; }
        public bool? IsDelete { get; set; }
        public bool? Publish { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
