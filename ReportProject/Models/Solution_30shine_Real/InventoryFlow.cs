﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class InventoryFlow
    {
        public int Id { get; set; }
        public int? InvenId { get; set; }
        public int? ProductId { get; set; }
        public int? Quantity { get; set; }
        public int? Cost { get; set; }
        public int? Price { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
