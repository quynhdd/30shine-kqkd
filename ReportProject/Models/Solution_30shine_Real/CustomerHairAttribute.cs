﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class CustomerHairAttribute
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public string ChatToc { get; set; }
        public string DoCung { get; set; }
        public string KhuonMat { get; set; }
        public string DangCam { get; set; }
        public string AnMac { get; set; }
        public string ThoiQuenVuotSap { get; set; }
        public string ThoiQuenSay { get; set; }
        public string NgheNghiep { get; set; }
        public string GhiChuKhac { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDelete { get; set; }
    }
}
