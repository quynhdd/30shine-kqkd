﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class ErrorCutReason
    {
        public int Id { get; set; }
        public string Reason { get; set; }
        public bool? IsDelete { get; set; }
        public int? OrderBy { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
