﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class SmBillTempFlowProduct
    {
        public int Id { get; set; }
        public int? BillId { get; set; }
        public int? ProductId { get; set; }
        public int? Price { get; set; }
        public int? Quantity { get; set; }
        public DateTime? CreatedDate { get; set; }
        public byte? IsDelete { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? SalonId { get; set; }
        public int? SellerId { get; set; }
        public int? VoucherPercent { get; set; }
        public int? PromotionMoney { get; set; }
        public int? Cost { get; set; }
        public double? ForSalary { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
