﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class ServiceSalonGoldTime
    {
        public int Id { get; set; }
        public int? ServiceSalonConfigId { get; set; }
        public int? HoursId { get; set; }
        public double? CoefficientGolden { get; set; }
        public DateTime? ModifiledTime { get; set; }
        public DateTime? CreateTime { get; set; }
        public bool? IsPublish { get; set; }
        public bool? IsDelete { get; set; }
    }
}
