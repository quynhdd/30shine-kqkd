﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class SpecialCustomer
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public int? CustomerTypeId { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? IsSms { get; set; }
        public int? Smsstatus { get; set; }
        public DateTime? SendDate { get; set; }
        public bool? Publish { get; set; }
        public int? BrowseId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
