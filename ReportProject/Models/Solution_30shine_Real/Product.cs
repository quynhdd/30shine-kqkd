﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int? Type { get; set; }
        public int? Price { get; set; }
        public int? OldPrice { get; set; }
        public byte? IsVoucher { get; set; }
        public int? VoucherValue { get; set; }
        public string Description { get; set; }
        public string TechInfo { get; set; }
        public DateTime? CreatedDate { get; set; }
        public byte? Publish { get; set; }
        public byte? IsDelete { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? VoucherPercent { get; set; }
        public byte? Status { get; set; }
        public int? Cost { get; set; }
        public double? ForSalary { get; set; }
        public int? CategoryId { get; set; }
        public int? OnWebId { get; set; }
        public int? BrandId { get; set; }
        public string Volume { get; set; }
        public string ModelName { get; set; }
        public string MapIdProduct { get; set; }
        public int? PriceCombo { get; set; }
        public bool? CheckCombo { get; set; }
        public int? Order { get; set; }
        public bool? CheckInventoryHc { get; set; }
        public int? GroupProductId { get; set; }
        public bool? IsCheckVatTu { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public string BarCode { get; set; }
        public int? MemberTypeAdd { get; set; }
        public int? MemberAddDay { get; set; }
        public string InventoryType { get; set; }
    }
}
