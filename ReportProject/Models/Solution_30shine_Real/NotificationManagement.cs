﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class NotificationManagement
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Descriptions { get; set; }
        public string Url { get; set; }
        public string Images { get; set; }
        public int? Type { get; set; }
        public bool? IsPublish { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
    }
}
