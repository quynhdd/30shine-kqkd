﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class QuanHuyen
    {
        public int Id { get; set; }
        public int? TinhThanhId { get; set; }
        public string TenQuanHuyen { get; set; }
        public int? ThuTu { get; set; }
        public int? TrangThai { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
