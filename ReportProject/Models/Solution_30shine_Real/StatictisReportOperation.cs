﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class StatictisReportOperation
    {
        public int Id { get; set; }
        public int SalonId { get; set; }
        public string SalonName { get; set; }
        public double TotalIncome { get; set; }
        public double TotalServiceInCome { get; set; }
        public double TotalProductIncome { get; set; }
        public double WorktimeStylist { get; set; }
        public int NumberOfTurns { get; set; }
        public int NumberCustomerOld { get; set; }
        public int NumberBillNotImg { get; set; }
        public int NumberBillKhl { get; set; }
        public int TotalStarNumber { get; set; }
        public int NumberErrorMonitoring { get; set; }
        public int NumberStylistTimekeeping { get; set; }
        public int NumberSkinnerTimekeeping { get; set; }
        public int NumberStylist { get; set; }
        public int NumberCanleBooking { get; set; }
        public int NumberDeleteBooking { get; set; }
        public int OvertimeHoursStylist { get; set; }
        public int NumberSlotBooking { get; set; }
        public int NumberBooking { get; set; }
        public int NumberBookingBefor { get; set; }
        public int NumberBillWaited { get; set; }
        public DateTime? WorkDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime CreateDate { get; set; }
        public int NumberBill1Star { get; set; }
        public int NumberBill2Star { get; set; }
        public int NumberBill3Star { get; set; }
        public int NumberBill4Star { get; set; }
        public int NumberBill5Star { get; set; }
        public int TotalShineCombo { get; set; }
        public int TotalKidCombo { get; set; }
        public int TotalProtein { get; set; }
        public int TotalMask { get; set; }
        public int TotalExFoliation { get; set; }
        public int TotalGroupUonDuoi { get; set; }
        public int TotalGroupColorCombo { get; set; }
    }
}
