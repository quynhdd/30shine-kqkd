﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class Log
    {
        public int Id { get; set; }
        public DateTime? LogTime { get; set; }
        public string Content { get; set; }
        public int? Objid { get; set; }
        public string Type { get; set; }
        public int? Status { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
