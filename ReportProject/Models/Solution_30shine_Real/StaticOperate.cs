﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class StaticOperate
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public double? Stylist { get; set; }
        public double? Skinner { get; set; }
        public double? PerfectBill { get; set; }
        public double? GoodBill { get; set; }
        public double? BadBill { get; set; }
        public double? OldCustomer { get; set; }
        public double? BookingOnline { get; set; }
        public double? BillOver15m { get; set; }
        public double? BillUnder15m { get; set; }
        public double? BillTimeNull { get; set; }
        public double? BillHasImage { get; set; }
        public double? ProfitByHour { get; set; }
        public double? ProfitByDay { get; set; }
        public double? ServiceByDay { get; set; }
        public double? ServiceByHour { get; set; }
        public DateTime? WorkDate { get; set; }
        public DateTime? CreatedTime { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
