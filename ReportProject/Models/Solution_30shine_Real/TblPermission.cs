﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class TblPermission
    {
        public int PId { get; set; }
        public string PName { get; set; }
        public string PTitle { get; set; }
        public int? POder { get; set; }
        public bool? PPublish { get; set; }
        public DateTime? PCreatedDate { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
