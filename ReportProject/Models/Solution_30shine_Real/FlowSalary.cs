﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class FlowSalary
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public DateTime SDate { get; set; }
        public double? FixSalary { get; set; }
        public double? AllowanceSalary { get; set; }
        public double? PartTimeSalary { get; set; }
        public double? ServiceSalary { get; set; }
        public double? ProductSalary { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public byte? IsDelete { get; set; }
        public double? RatingPoint { get; set; }
        public int? BillNormal { get; set; }
        public int? BillNormalGreat { get; set; }
        public int? BillNormalGood { get; set; }
        public int? BillNormalBad { get; set; }
        public int? BillNormalNorating { get; set; }
        public int? BillSpecial { get; set; }
        public int? BillSpecialGreat { get; set; }
        public int? BillSpecialGood { get; set; }
        public int? BillSpecialBad { get; set; }
        public int? BillSpecialNorating { get; set; }
        public int? MistakePoint { get; set; }
        public int? SalonId { get; set; }
        public double? WaitTimeMistake { get; set; }
        public double? SalesSalary { get; set; }
        public double? SalaryCare { get; set; }
        public int? MistakeExtra { get; set; }
        public int? MistakeTemporary { get; set; }
        public int? MistakeOrther { get; set; }
        public int? MistakeInsurrance { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
