﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class StaffProfileMap
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public int StatusProfileId { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool IsDelete { get; set; }
    }
}
