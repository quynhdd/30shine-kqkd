﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine_Real
{
    public partial class MarketingChiPhiPhanBo
    {
        public int Id { get; set; }
        public DateTime? NgayChi { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifileDate { get; set; }
        public int? SalonId { get; set; }
        public int? IdChienDich { get; set; }
        public decimal? TongChi { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
