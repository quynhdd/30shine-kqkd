﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class Vietnam
    {
        public int Id { get; set; }
        public string City { get; set; }
        public int? CityId { get; set; }
        public string District { get; set; }
        public int? DistrictId { get; set; }
        public string Ward { get; set; }
        public int? WardId { get; set; }
        public string WardLevel { get; set; }
    }
}
