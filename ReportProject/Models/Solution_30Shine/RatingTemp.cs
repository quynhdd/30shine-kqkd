﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class RatingTemp
    {
        public int Id { get; set; }
        public int SalonId { get; set; }
        public int RatingValue { get; set; }
        public int? Status { get; set; }
        public int IsDelete { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? AccountId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
