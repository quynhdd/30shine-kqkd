﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class FlowGoods
    {
        public int Id { get; set; }
        public int ExportGoodsId { get; set; }
        public int GoodsId { get; set; }
        public int? Cost { get; set; }
        public int? Price { get; set; }
        public int? Quantity { get; set; }
        public int? VoucherPercent { get; set; }
        public int? PromotionMoney { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public byte? IsDelete { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
