﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class SalonDailyCost
    {
        public int Id { get; set; }
        public int SalonId { get; set; }
        public double ElectricityAndWaterBill { get; set; }
        public double RentWithTax { get; set; }
        public double CapitalSpending { get; set; }
        public double AdvertisementExpend { get; set; }
        public double ShippingExpend { get; set; }
        public double InternetAndPhoneBill { get; set; }
        public double SocialInsuranceAndFixedCost { get; set; }
        public double IncomeTaxes { get; set; }
        public double SalonUnplannedSpending { get; set; }
        public double ThaiHaRentAndSeviceCost { get; set; }
        public double OfficeStaffSalary { get; set; }
        public double SalesSalary { get; set; }
        public double ItSalary { get; set; }
        public double OfficeStaffSocialInsurance { get; set; }
        public double UnplannedSpending { get; set; }
        public DateTime ReportDate { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime ModifyTime { get; set; }
        public double DailyCostInventory { get; set; }
        public double ProductPrice { get; set; }
        public double Compensation { get; set; }
        public double OtherIncome { get; set; }
    }
}
