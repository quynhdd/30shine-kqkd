﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class Stylist4MenGraduationScore
    {
        public int Id { get; set; }
        public int? StaffId { get; set; }
        public int? PointPracticeCut { get; set; }
        public int? PointTheoryCut { get; set; }
        public int? PointTheoryChemistry { get; set; }
        public int? PointPacticeChemistry { get; set; }
        public int? StatusId { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDelete { get; set; }
        public bool? Publish { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
