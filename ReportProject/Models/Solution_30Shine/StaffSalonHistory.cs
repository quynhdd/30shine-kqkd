﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class StaffSalonHistory
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public int OlderSalonId { get; set; }
        public int CurrentSalonId { get; set; }
        public DateTime? TransferDate { get; set; }
        public bool? IsDelete { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
