﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class DcustomerCall
    {
        public int IdCustomerCall { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? DatetimeCall { get; set; }
        public DateTime? StartTimeCall { get; set; }
        public DateTime? EndCallTime { get; set; }
        public int? TypeOfCallId { get; set; }
        public int? ResultOfCallId { get; set; }
        public string NoteOfCall { get; set; }
        public string DeviceImei { get; set; }
        public int? SalonId { get; set; }
        public int? DepartmentId { get; set; }
        public int? StaffId { get; set; }
        public int? CallId { get; set; }
        public int? BookingId { get; set; }
    }
}
