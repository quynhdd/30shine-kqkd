﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class IvCheckOut
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public int? WarehouseId { get; set; }
        public int? ProductId { get; set; }
        public int? Quantity { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }
        public int? Week { get; set; }
        public int? Month { get; set; }
    }
}
