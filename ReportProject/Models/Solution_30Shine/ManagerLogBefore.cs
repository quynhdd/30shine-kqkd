﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class ManagerLogBefore
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public DateTime? Today { get; set; }
        public long? TotalStylist { get; set; }
        public long? TotalSkinner { get; set; }
        public int? TotalWorkHourStylist { get; set; }
        public bool? IsFacilities { get; set; }
        public string NoteFacilities { get; set; }
        public bool? IsChargeHotline { get; set; }
        public bool? IsHotline { get; set; }
        public bool? IsMonitor { get; set; }
        public int? ConfirmDryTowel { get; set; }
        public int? FirstWatch { get; set; }
        public int? SecondWatch { get; set; }
        public int? ThirdWatch { get; set; }
        public int? FourthWatch { get; set; }
        public int? FifthWatch { get; set; }
        public int? SixthWatch { get; set; }
        public string NoteWatch { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDelete { get; set; }
    }
}
