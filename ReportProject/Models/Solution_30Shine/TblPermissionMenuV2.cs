﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class TblPermissionMenuV2
    {
        public int MId { get; set; }
        public int? MParentId { get; set; }
        public string MName { get; set; }
        public string UrlRewrite { get; set; }
        public string UrlMain { get; set; }
        public bool? MPublish { get; set; }
        public DateTime? MCreatedDate { get; set; }
        public string MIcon { get; set; }
        public string MClassTag { get; set; }
        public string MPageId { get; set; }
        public bool? MenuShow { get; set; }
        public string Target { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
