﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class TrackingWebEvent
    {
        public int Id { get; set; }
        public string EventName { get; set; }
        public DateTime? CreatedTime { get; set; }
        public bool? IsDelete { get; set; }
        public string Description { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
