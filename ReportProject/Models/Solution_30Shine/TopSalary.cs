﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class TopSalary
    {
        public int Id { get; set; }
        public int DepartmentId { get; set; }
        public int StaffId { get; set; }
        public string StaffName { get; set; }
        public string SalonName { get; set; }
        public string SkillLevel { get; set; }
        public double Salary { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public DateTime CreateTime { get; set; }
        public bool? IsDelete { get; set; }
    }
}
