﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class ProductReturn
    {
        public int Id { get; set; }
        public int? BillId { get; set; }
        public int? SalonSellId { get; set; }
        public int? SalonReceiveId { get; set; }
        public int? SellerId { get; set; }
        public int? ReceiverId { get; set; }
        public int? ProductId { get; set; }
        public int? Quantity { get; set; }
        public double? PriceProduct { get; set; }
        public int? ForSalary { get; set; }
        public int? VoucherPercent { get; set; }
        public double? ProductSalary { get; set; }
        public DateTime? DateOfBill { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
