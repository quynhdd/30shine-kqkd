﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class StaffRoll
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
