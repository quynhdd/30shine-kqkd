﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class UserStaff
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int? StaffId { get; set; }
        public DateTime? CreatedTime { get; set; }
    }
}
