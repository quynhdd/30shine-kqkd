﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class TuyenDungUngVien
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Cmt { get; set; }
        public string Cmtimg1 { get; set; }
        public string Cmtimg2 { get; set; }
        public string MainImg { get; set; }
        public DateTime? Step1Time { get; set; }
        public DateTime? Step1ModifiedTime { get; set; }
        public DateTime? Step2Time { get; set; }
        public DateTime? StepEndTime { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? Step2ModifiedTime { get; set; }
        public DateTime? StepEndModifiedTime { get; set; }
        public string ImgSkill1 { get; set; }
        public string ImgSkill2 { get; set; }
        public string VideoLink { get; set; }
        public int? DepartmentId { get; set; }
        public string Step2Note { get; set; }
        public bool? Approve { get; set; }
        public string StepEndNote { get; set; }
        public int? UngVienStatusId { get; set; }
        public string KyNangHoaChat { get; set; }
        public string StepEndNoteStaff { get; set; }
        public int? NguoiGioiThieuId { get; set; }
        public DateTime? NgaySinh { get; set; }
        public int? NguonTuyenDungId { get; set; }
        public int? SalonId { get; set; }
        public int? GioiTinhId { get; set; }
        public string ImgSkillDaoTao1 { get; set; }
        public string ImgSkillDaoTao2 { get; set; }
        public string StepEndNoteDaoTao { get; set; }
        public string LinkFaceBook { get; set; }
        public int? TinhThanhId { get; set; }
        public string MainImg2 { get; set; }
        public int? PointFigure { get; set; }
        public string TestVh { get; set; }
        public bool? ParseVh { get; set; }
        public bool? TestAgain { get; set; }
        public int? TesterId { get; set; }
        public int? StaffId { get; set; }
        public string TestCut { get; set; }
        public string TestChemistry { get; set; }
        public bool? PassCut { get; set; }
        public bool? TestCutAgain { get; set; }
        public bool? PassChemistry { get; set; }
        public bool? TestChemistryAgain { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public DateTime? ProvidedDate { get; set; }
        public string ProvidedLocale { get; set; }
        public int? StatusRelationId { get; set; }
        public string Address { get; set; }
        public int? RecruimentId { get; set; }
    }
}
