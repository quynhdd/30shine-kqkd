﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class Users
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string LastPassword { get; set; }
        public DateTime? LastLogin { get; set; }
        public bool? IsActive { get; set; }
        public bool? FirstLogin { get; set; }
        public DateTime? LastChangePassword { get; set; }
        public int? TimeLivePassword { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? StaffId { get; set; }
        public bool? Valid2Fa { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string OtpChangePass { get; set; }
        public bool? RequiredValid2Fa { get; set; }
    }
}
