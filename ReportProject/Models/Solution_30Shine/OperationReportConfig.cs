﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class OperationReportConfig
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public byte? SalonType { get; set; }
        public string DataJson { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? MonthApply { get; set; }
        public bool? IsRegion { get; set; }
    }
}
