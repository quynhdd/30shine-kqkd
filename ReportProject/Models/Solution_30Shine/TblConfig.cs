﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class TblConfig
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public string Hint { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Meta { get; set; }
        public int? IsDelete { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
