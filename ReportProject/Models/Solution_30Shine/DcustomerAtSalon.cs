﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class DcustomerAtSalon
    {
        public int IdCustomerAtSalon { get; set; }
        public int BillId { get; set; }
        public int CustomerId { get; set; }
        public DateTime DateAtSalon { get; set; }
        public DateTime? TimeArrived { get; set; }
        public DateTime TimePrintBill { get; set; }
        public DateTime? TimeHairMassage { get; set; }
        public DateTime? TimeHairDresser { get; set; }
        public DateTime? TimeUploadTakePhoto { get; set; }
        public DateTime TimeCompleteBill { get; set; }
        public int RatingScore { get; set; }
        public int TotalMonney { get; set; }
        public bool IsUsePromotion { get; set; }
        public int? PromotionId { get; set; }
        public bool IsBuyProduct { get; set; }
        public int ScoreScscbill { get; set; }
        public int SalonId { get; set; }
        public int BookingId { get; set; }
    }
}
