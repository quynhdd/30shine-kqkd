﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30shine
{
    public partial class StaticManagerInfo
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public string SalonName { get; set; }
        public DateTime? WorkDate { get; set; }
        public int? TotalPointKcs { get; set; }
        public int? TotalBillKcs { get; set; }
        public double? Kcsavg { get; set; }
        public int? TotalBill { get; set; }
        public int? TotalWaitTime { get; set; }
        public double? WaitTimeAvg { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
