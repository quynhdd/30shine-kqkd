﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class TypeErrorSpecailCus
    {
        public int Id { get; set; }
        public int? SpecailCusId { get; set; }
        public int? TypeErrorSpecialCusId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDelete { get; set; }
        public bool? Publish { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
