﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class StaffErrorSpecailCus
    {
        public int Id { get; set; }
        public int? SpecialCusId { get; set; }
        public int? StaffId { get; set; }
        public DateTime? Createdate { get; set; }
        public bool? IsDelete { get; set; }
        public bool? Publish { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
