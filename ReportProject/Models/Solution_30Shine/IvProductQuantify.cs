﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class IvProductQuantify
    {
        public int Id { get; set; }
        public int? InventoryId { get; set; }
        public int? ServiceId { get; set; }
        public int? ProductId { get; set; }
        public int? StaffType { get; set; }
        public double? Volume { get; set; }
        public int? TotalNumberService { get; set; }
        public double? Quantify { get; set; }
        public bool? IsPublish { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
