﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class PermissionActionMapApi
    {
        public int Id { get; set; }
        public int? PermissionId { get; set; }
        public int? PageId { get; set; }
        public int? ActionId { get; set; }
        public int? ApiId { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
    }
}
