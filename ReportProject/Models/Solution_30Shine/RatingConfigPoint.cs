﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class RatingConfigPoint
    {
        public int Id { get; set; }
        public int? RealPoint { get; set; }
        public double? ConventionPoint { get; set; }
        public string ConventionName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? IsDelete { get; set; }
        public int? Status { get; set; }
        public int? Hint { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
