﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class ScscCheckError
    {
        public int Id { get; set; }
        public int BillServiceId { get; set; }
        public bool? ImageError { get; set; }
        public string NoteError { get; set; }
        public int? ShapeId { get; set; }
        public int? ConnectTiveId { get; set; }
        public int? SharpNessId { get; set; }
        public int? ComPlatetionId { get; set; }
        public bool? Publish { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifiledDate { get; set; }
        public int? TotalPointScsc { get; set; }
        public int? MaxPointShapId { get; set; }
        public int? MaxPointConnectiveId { get; set; }
        public int? MaxPointSharpNessId { get; set; }
        public int? MaxPointComPlatetionId { get; set; }
        public int? PointError { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public bool AutomaticScore { get; set; }
        public int? HairTipId { get; set; }
        public int? HairRootId { get; set; }
        public int? HairWavesId { get; set; }
        public int? TotalPointScsccurling { get; set; }
        public bool? ImageErrorCurling { get; set; }
    }
}
