﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class IvInventoryHistory
    {
        public int Id { get; set; }
        public int? InventoryId { get; set; }
        public int ProductId { get; set; }
        public double Cost { get; set; }
        public DateTime? AccountingDate { get; set; }
        public int? Begin { get; set; }
        public int? Import { get; set; }
        public int? Export { get; set; }
        public int? SellOrUse { get; set; }
        public int? End { get; set; }
        public double? Volume { get; set; }
        public double? Quantify { get; set; }
        public double? VolumeRemain { get; set; }
        public int? SuggestOrder { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }
        public int UsedInService { get; set; }
        public int ExportToStaff { get; set; }
        public int ExportToMainWh { get; set; }
        public int ExportToSalon { get; set; }
        public int ImportFromPartner { get; set; }
        public int ImportFromSalon { get; set; }
        public int ImportFromStaff { get; set; }
        public int ImportFromReturn { get; set; }
        public int BeingTransported { get; set; }
        public int ImportFromMainWh { get; set; }
    }
}
