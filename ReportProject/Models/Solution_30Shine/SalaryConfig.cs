﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class SalaryConfig
    {
        public int Id { get; set; }
        public int? TypeWork { get; set; }
        public int? DepartmentId { get; set; }
        public int? LevelId { get; set; }
        public double? FixedSalary { get; set; }
        public double? AllowanceSalary { get; set; }
        public double? OvertimeSalary { get; set; }
        public double? RattingSalary { get; set; }
        public double? ProductBonus { get; set; }
        public double? BehaveSalary { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDeleted { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
