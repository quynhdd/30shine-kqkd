﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class TblMedia
    {
        public int Id { get; set; }
        public byte? Type { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Meta { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public byte? Publish { get; set; }
        public byte? IsDelete { get; set; }
        public string Folder { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
