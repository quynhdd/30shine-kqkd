﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class RatingDetailV1
    {
        public int Id { get; set; }
        public int? BillId { get; set; }
        public string RatingReasonId { get; set; }
        public int? RatingValue { get; set; }
        public int? RatingBonusValue { get; set; }
        public int? StarNumber { get; set; }
        public bool? Isdelete { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? Iscomeback { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
