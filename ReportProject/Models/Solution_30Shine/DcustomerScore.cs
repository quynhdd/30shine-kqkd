﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class DcustomerScore
    {
        public int IdCustomerScore { get; set; }
        public int NumberUseServiceAtTime { get; set; }
        public DateTime DateOfBill { get; set; }
        public double CustomerScoreAtTime { get; set; }
        public int RatingScore { get; set; }
        public double StylistScore { get; set; }
        public int CustomerId { get; set; }
        public DateTime CreateDate { get; set; }
        public int BillId { get; set; }
    }
}
