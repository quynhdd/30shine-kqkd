﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class SelfieCode
    {
        public int Id { get; set; }
        public int BillId { get; set; }
        public string CustomerPhone { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
