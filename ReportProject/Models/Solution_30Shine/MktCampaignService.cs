﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class MktCampaignService
    {
        public int Id { get; set; }
        public int ServiceId { get; set; }
        public int CampaignId { get; set; }
        public double DiscountPercent { get; set; }
        public double ServicePrice { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool IsDelete { get; set; }
        public int? TimesUsed { get; set; }
        public int? MoneyPrePaid { get; set; }
        public int? MoneyDeductions { get; set; }
    }
}
