﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class Contracts
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string DescriptionContract { get; set; }
        public int? DepartmentId { get; set; }
        public int? TypeContract { get; set; }
        public string PathContract { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool IsDelete { get; set; }
    }
}
