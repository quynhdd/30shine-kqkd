﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class Mobile
    {
        public int Id { get; set; }
        public int? AndroidVersion { get; set; }
        public string Iosversion { get; set; }
        public string AccessKey { get; set; }
        public string PrivateKey { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsDelete { get; set; }
        public string AppKey { get; set; }
        public int? AppReview { get; set; }
    }
}
