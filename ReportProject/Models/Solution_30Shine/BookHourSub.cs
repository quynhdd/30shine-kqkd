﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class BookHourSub
    {
        public int Id { get; set; }
        public int HourId { get; set; }
        public int SubHourId { get; set; }
        public string Hour { get; set; }
        public TimeSpan HourFrame { get; set; }
        public int SalonId { get; set; }
        public bool Publish { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
