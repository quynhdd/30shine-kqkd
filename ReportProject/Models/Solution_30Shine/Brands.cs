﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class Brands
    {
        public int Id { get; set; }
        public string TenThuongHieu { get; set; }
        public string Mota { get; set; }
        public string XuatXu { get; set; }
        public string Images { get; set; }
        public bool? Status { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
