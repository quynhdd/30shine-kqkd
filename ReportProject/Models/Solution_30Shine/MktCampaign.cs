﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class MktCampaign
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int MaxUsage { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsActive { get; set; }
        public bool IsDelete { get; set; }
        public string Note { get; set; }
        public bool IsBookingPublish { get; set; }
        public string Image { get; set; }
        public int Type { get; set; }
        public string CheckoutLabel { get; set; }
        public string CustomerCondition { get; set; }
        public int? ServiceType { get; set; }
        public int? CampaignMaxUsage { get; set; }
        public int? CustomerType { get; set; }
    }
}
