﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class Authorization
    {
        public int Id { get; set; }
        public string UserType { get; set; }
        public string Uid { get; set; }
        public string UserUid { get; set; }
        public string DeviceUid { get; set; }
        public string ApplicationUid { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? DeleteTime { get; set; }
        public string DeviceType { get; set; }
        public string DeviceName { get; set; }
        public string Token { get; set; }
        public string TokenAlg { get; set; }
        public DateTime? TokenExp { get; set; }
        public string TokenType { get; set; }
        public string TokenIss { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsForever { get; set; }
        public bool? IsDelete { get; set; }
        public string AppVersion { get; set; }
    }
}
