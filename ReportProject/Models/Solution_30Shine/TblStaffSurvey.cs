﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class TblStaffSurvey
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public int? StaffTypeId { get; set; }
        public int? StylistId { get; set; }
        public int? SkinnerId { get; set; }
        public string Note { get; set; }
        public int? Point { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public byte? IsDelete { get; set; }
        public int? UserId { get; set; }
        public DateTime? ServeyDate { get; set; }
        public int? Thread { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
