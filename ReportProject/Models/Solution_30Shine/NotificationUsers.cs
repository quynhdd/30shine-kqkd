﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class NotificationUsers
    {
        public int Id { get; set; }
        public int? NotiId { get; set; }
        public int? UserId { get; set; }
        public string SlugKey { get; set; }
        public int? Status { get; set; }
        public bool? IsPublish { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
    }
}
