﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class IvInventory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public int? SalonId { get; set; }
        public int? StaffId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }
        public int ParentId { get; set; }
    }
}
