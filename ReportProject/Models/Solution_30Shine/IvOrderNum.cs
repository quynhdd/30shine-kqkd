﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class IvOrderNum
    {
        public int Id { get; set; }
        public int? InventoryId { get; set; }
        public int? ProductId { get; set; }
        public int? OrderNum { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }
    }
}
