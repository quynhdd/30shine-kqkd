﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class Staff
    {
        public Staff()
        {
            StaffUploadedImages = new HashSet<StaffUploadedImages>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Code { get; set; }
        public DateTime? BirthDay { get; set; }
        public byte? Gender { get; set; }
        public int? Type { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int? Position { get; set; }
        public int? IdShowroom { get; set; }
        public DateTime? CreatedDate { get; set; }
        public byte? Active { get; set; }
        public string Fullname { get; set; }
        public int? SnDay { get; set; }
        public int? SnMonth { get; set; }
        public int? SnYear { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public string Address { get; set; }
        public byte? IsDelete { get; set; }
        public string Password { get; set; }
        public string Permission { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? SalonId { get; set; }
        public int? SkillLevel { get; set; }
        public string StaffId { get; set; }
        public DateTime? DateJoin { get; set; }
        public string OrderCode { get; set; }
        public string FingerTemplate { get; set; }
        public string FingerToken { get; set; }
        public string Images { get; set; }
        public bool? Publish { get; set; }
        public bool? Ceo { get; set; }
        public string About { get; set; }
        public string Avatar { get; set; }
        public string Job { get; set; }
        public byte[] FingerTemplateBn { get; set; }
        public int? SkinnerIdInGroup { get; set; }
        public int? IsAccountLogin { get; set; }
        public int? IsAppLogin { get; set; }
        public int? TeamId { get; set; }
        public bool? SalaryByPerson { get; set; }
        public bool? RequireEnroll { get; set; }
        public DateTime? NgayTinhThamNien { get; set; }
        public string NganHangSoTk { get; set; }
        public string NganHangChiNhanh { get; set; }
        public string NganHangTen { get; set; }
        public int? NguoiGioiThieuId { get; set; }
        public int? GroupLevelId { get; set; }
        public int? S4mclassId { get; set; }
        public bool? IsHoiQuan { get; set; }
        public int? LevelPoint { get; set; }
        public int? CutTimeTb { get; set; }
        public string Note { get; set; }
        public int? UngvienId { get; set; }
        public string NameCmt { get; set; }
        public string Cmtimg1 { get; set; }
        public string Cmtimg2 { get; set; }
        public string Mst { get; set; }
        public string NumberInsurrance { get; set; }
        public string NganHangTenTk { get; set; }
        public string PermissionId { get; set; }
        public DateTime? IdprovidedDate { get; set; }
        public string IdproviderLocale { get; set; }
        public string IdhomeTown { get; set; }
        public int? StudentId { get; set; }
        public int? SourceId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public double Score { get; set; }
        public string AvartarBooking { get; set; }
        public bool? IsActiveBooking { get; set; }

        public ICollection<StaffUploadedImages> StaffUploadedImages { get; set; }
    }
}
