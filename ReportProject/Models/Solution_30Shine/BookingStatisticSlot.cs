﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class BookingStatisticSlot
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public DateTime? WorkDate { get; set; }
        public int? Slot { get; set; }
        public double? PercentSlot { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
