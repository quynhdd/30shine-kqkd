﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class KetQuaKinhDoanhSalon
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public string SalonName { get; set; }
        public string Description { get; set; }
        public bool? IsDelete { get; set; }
        public bool? Publish { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public string Meta { get; set; }
        public int? Order { get; set; }
        public string ShortName { get; set; }
        public bool? IsSalonHoiQuan { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
