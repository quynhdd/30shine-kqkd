﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class FundImport
    {
        public int Id { get; set; }
        public int? AccountTypeId { get; set; }
        public int? SourceId { get; set; }
        public bool? IsReceipt { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public string Note { get; set; }
        public DateTime? ImportDate { get; set; }
        public int? TotalMoney { get; set; }
        public int? OffenItemId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
