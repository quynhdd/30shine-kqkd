﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class ErpItemCheck
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte? Category { get; set; }
        public string Icon { get; set; }
        public int? Publish { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
