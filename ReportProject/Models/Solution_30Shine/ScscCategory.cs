﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class ScscCategory
    {
        public int IdScscCate { get; set; }
        public string ScscCateName { get; set; }
        public string ScscCateDes { get; set; }
        public bool? Publish { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? ModifiledTime { get; set; }
        public int? ScscCateIdcate { get; set; }
        public string ScscCateImage { get; set; }
        public string ScscCateImageActive { get; set; }
        public int? ScscCatePoint { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
