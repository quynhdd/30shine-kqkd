﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class FlowStaff
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public double? Stylist { get; set; }
        public double? Skinner { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
