﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class Service
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public int? Price { get; set; }
        public int? OldPrice { get; set; }
        public byte? IsVouche { get; set; }
        public int? VoucheValue { get; set; }
        public byte? Publish { get; set; }
        public DateTime? CreateDate { get; set; }
        public byte? IsDelete { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? VoucherPercent { get; set; }
        public byte? Status { get; set; }
        public byte? IsFreeService { get; set; }
        public int? CategoryId { get; set; }
        public byte? CoefficientRating { get; set; }
        public string Images { get; set; }
        public string Videos { get; set; }
        public int? Order { get; set; }
        public bool? IsPrimary { get; set; }
        public bool? ShowOnApp { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
