﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class TrackingWebDataV2
    {
        public int Id { get; set; }
        public int? EventId { get; set; }
        public int? Value { get; set; }
        public DateTime? CreatedTime { get; set; }
        public string TokenKey { get; set; }
        public string Meta { get; set; }
        public int? VersionId { get; set; }
        public int? Counting { get; set; }
        public int? StylistId { get; set; }
        public DateTime? TrackDate { get; set; }
        public int? SalonId { get; set; }
        public string Phone { get; set; }
        public int? BookingId { get; set; }
        public int? HourId { get; set; }
        public int? CountFrames { get; set; }
        public string CusNote1 { get; set; }
        public string CusNote2 { get; set; }
        public string Device { get; set; }
        public string SalonNote { get; set; }
        public DateTime? TimeSalonNote { get; set; }
        public int? PopupSalonStatis { get; set; }
        public int? PopupStylistStatis { get; set; }
        public int? IdsalonBackUp { get; set; }
        public int? IdstylistBackUp { get; set; }
        public string VersionWeb { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public int? SalonType { get; set; }
        public int? CityId { get; set; }
        public bool? IsClickVoucher { get; set; }
        public bool? IsClickChooseCity { get; set; }
    }
}
