﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class MarketingChiphingay
    {
        public int Id { get; set; }
        public int? ItemId { get; set; }
        public DateTime? NgayChi { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifileDate { get; set; }
        public bool? IsDelete { get; set; }
        public int? StaffId { get; set; }
        public int? SalonId { get; set; }
        public decimal? GiaTri { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
