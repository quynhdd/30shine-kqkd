﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class FeedbackPointService
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public int? FeedbackQuantity { get; set; }
        public DateTime? WorkDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
