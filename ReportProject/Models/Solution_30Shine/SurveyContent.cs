﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class SurveyContent
    {
        public int Id { get; set; }
        public int? Pid { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public int? CategoryId { get; set; }
        public bool? Correct { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsDelete { get; set; }
        public bool? Publish { get; set; }
        public bool? Featured { get; set; }
        public int? Order { get; set; }
        public bool IsQuestion { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
