﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class StaticServicesProfit
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public int? TongHoaDon { get; set; }
        public int? ShinCombo { get; set; }
        public int? KidCombo { get; set; }
        public int? Protein { get; set; }
        public int? Duoi { get; set; }
        public int? Uon { get; set; }
        public int? Nhuom { get; set; }
        public int? Tay { get; set; }
        public int? TayX2 { get; set; }
        public DateTime? WorkDate { get; set; }
        public DateTime? CreatedTime { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public int? Scscbill { get; set; }
        public int? Scscpoint { get; set; }
        public double? Scscavg { get; set; }
        public int? BillLongTime { get; set; }
        public double? LongTimePercent { get; set; }
    }
}
