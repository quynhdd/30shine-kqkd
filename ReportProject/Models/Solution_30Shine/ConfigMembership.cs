﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class ConfigMembership
    {
        public int Id { get; set; }
        public int? ProductMemberId { get; set; }
        public int? IdServiceOrProduct { get; set; }
        public int? PriceMember { get; set; }
        public int? Type { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsPublish { get; set; }
    }
}
