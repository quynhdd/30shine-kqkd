﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class WorkTime
    {
        public int Id { get; set; }
        public string WName { get; set; }
        public TimeSpan? StrartTime { get; set; }
        public TimeSpan? EnadTime { get; set; }
        public TimeSpan? Lunchhour { get; set; }
        public TimeSpan? Lunchhour2 { get; set; }
        public bool? Publish { get; set; }
        public byte? IsDelete { get; set; }
        public string Color { get; set; }
        public int? TotalHourFrame { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
