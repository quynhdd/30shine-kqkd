﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class CustomerToken
    {
        public int Id { get; set; }
        public string Imei { get; set; }
        public string Token { get; set; }
        public string CustomerPhone { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsDelete { get; set; }
        public int? CustomerId { get; set; }
    }
}
