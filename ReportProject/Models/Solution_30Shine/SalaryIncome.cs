﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class SalaryIncome
    {
        public int Id { get; set; }
        public int? StaffId { get; set; }
        public int? SalonId { get; set; }
        public double? FixedSalary { get; set; }
        public double? AllowanceSalary { get; set; }
        public double? OvertimeSalary { get; set; }
        public double? ServiceSalary { get; set; }
        public double? ProductSalary { get; set; }
        public double? BehaveSalary { get; set; }
        public double? RatingPoint { get; set; }
        public int? BillNormal { get; set; }
        public int? BillNormalGreat { get; set; }
        public int? BillNormalGood { get; set; }
        public int? BillNormalBad { get; set; }
        public int? BillNormalNoRating { get; set; }
        public int? BillSpecial { get; set; }
        public int? BillSpecialGreat { get; set; }
        public int? BillSpecialGood { get; set; }
        public int? BillSpecialBad { get; set; }
        public int? BillSpecialNoRating { get; set; }
        public DateTime? WorkDate { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
        public double? TotalIncome { get; set; }
        public double? GrandTotalIncome { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDeleted { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public int? LevelId { get; set; }
    }
}
