﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class OrderRecruitingStaffLog
    {
        public int Id { get; set; }
        public int? OrderId { get; set; }
        public int? TypeId { get; set; }
        public int StaffEditId { get; set; }
        public string Value { get; set; }
        public DateTime? CreatedTime { get; set; }
    }
}
