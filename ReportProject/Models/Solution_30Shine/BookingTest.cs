﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class BookingTest
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public int? HourId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public byte? IsDelete { get; set; }
        public int? SalonId { get; set; }
        public int? StylistId { get; set; }
        public int? UserId { get; set; }
        public string Note { get; set; }
        public DateTime? DatedBook { get; set; }
        public bool? IsMakeBill { get; set; }
        public int? FlowTimeKeepingId { get; set; }
        public bool? IsSms { get; set; }
        public int? Smsstatus { get; set; }
        public DateTime? SendDate { get; set; }
        public int? Os { get; set; }
        public bool? NewCustomer { get; set; }
        public string NoteDelete { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
