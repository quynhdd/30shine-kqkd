﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class TblConfigWorkDays
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int WorkDays { get; set; }
        public bool? Publish { get; set; }
        public byte? IsDelete { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
