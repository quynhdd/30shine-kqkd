﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class Stylist4MenClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? GraduateTime { get; set; }
        public bool? Publish { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public int? SalonId { get; set; }
        public int? HomeRoomTeacherId { get; set; }
        public int? TeacherPointCutId { get; set; }
        public int? TeacherOfTheClubId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
