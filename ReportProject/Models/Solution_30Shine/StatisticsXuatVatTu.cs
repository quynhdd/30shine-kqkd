﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class StatisticsXuatVatTu
    {
        public int Id { get; set; }
        public int ExportGoodsId { get; set; }
        public int? TotalBillXuatVatTu { get; set; }
        public int? StaffId { get; set; }
        public int? SalonId { get; set; }
        public DateTime? DateXuatVatTu { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool? IsDelete { get; set; }
        public int? ProductId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
