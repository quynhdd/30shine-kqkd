﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class BookingStylistBackup
    {
        public int Id { get; set; }
        public int? StylistId { get; set; }
        public int? StylistBackupId { get; set; }
        public bool IsDelete { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
