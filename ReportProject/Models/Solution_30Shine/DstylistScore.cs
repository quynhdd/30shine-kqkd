﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class DstylistScore
    {
        public int DstylistScoreId { get; set; }
        public int? StylistId { get; set; }
        public int? Appearence { get; set; }
        public int? Communication { get; set; }
        public double? AvgScsc { get; set; }
        public DateTime? LastDateUpdate { get; set; }
        public double? AvgTimeCut { get; set; }
        public double? StylistScore { get; set; }
    }
}
