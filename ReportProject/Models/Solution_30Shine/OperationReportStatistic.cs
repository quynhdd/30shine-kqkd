﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class OperationReportStatistic
    {
        public int Id { get; set; }
        public int SalonId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifedDate { get; set; }
        public bool IsDelete { get; set; }
        public int? TotalBill { get; set; }
        public int? BillOldCustomer { get; set; }
        public int? BillNewCustomer { get; set; }
        public int? BookInAdvance { get; set; }
        public int? CancelBook { get; set; }
        public int? BookAll { get; set; }
        public int? TotalBuyShineMember { get; set; }
        public int? NewCustomerMember { get; set; }
        public int? OldCustomerMember { get; set; }
        public int? ShineMemberComeBack { get; set; }
        public int? TotalIncomePerShineMember { get; set; }
        public decimal? TotalIncomePerStylist { get; set; }
        public decimal? TotalBillPerStylist { get; set; }
        public decimal? TotalIncomePerTime { get; set; }
        public decimal? TotalBillPerTime { get; set; }
        public decimal? TotalStylistWorkTime { get; set; }
        public decimal? TotalSkinnerWorkTime { get; set; }
        public int? AvgRating { get; set; }
        public int? Mark1and2 { get; set; }
        public int? StaffError { get; set; }
        public int? LongWait { get; set; }
        public int? BillWithoutImage { get; set; }
        public string Revenue { get; set; }
        public DateTime Date { get; set; }
        public int? SpecialCustomer { get; set; }
        public int? TotalIncome { get; set; }
    }
}
