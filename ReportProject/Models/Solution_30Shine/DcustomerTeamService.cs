﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class DcustomerTeamService
    {
        public int IdCustomerTeamService { get; set; }
        public int CustomerId { get; set; }
        public int CheckinId { get; set; }
        public int SkinnerId { get; set; }
        public int StylistId { get; set; }
        public int CheckoutId { get; set; }
        public int SalonId { get; set; }
        public int BillId { get; set; }
        public DateTime DateOfBill { get; set; }
    }
}
