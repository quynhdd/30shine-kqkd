﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class TblPermissionAction
    {
        public int AId { get; set; }
        public string AName { get; set; }
        public string ADescription { get; set; }
        public bool? APublish { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
