﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class MktCampaignUsed
    {
        public int CampaignId { get; set; }
        public int TotalUsed { get; set; }
    }
}
