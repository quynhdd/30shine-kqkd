﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class DcustomerSms
    {
        public int IdCustomerSms { get; set; }
        public int CustomerId { get; set; }
        public DateTime? DateTimeSms { get; set; }
        public DateTime? DateTimeSend { get; set; }
        public DateTime? DateTimeReceived { get; set; }
        public int? TypeOfSmsid { get; set; }
        public string DeviceImei { get; set; }
        public int? SalonId { get; set; }
        public int? DepartmentId { get; set; }
        public int? StaffId { get; set; }
        public int Smsid { get; set; }
        public int? BookingId { get; set; }
    }
}
