﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class StaffRanking
    {
        public int StaffId { get; set; }
        public int? GiaotiepDongnghiep { get; set; }
        public int? GiaotiepKhachhang { get; set; }
        public int? GiaotiepQuanly { get; set; }
        public int? TuvantocKhachhang { get; set; }
        public int? TuvantocDongnghiep { get; set; }
        public int? TuvanHoachat { get; set; }
        public int? TuvanMypham { get; set; }
        public int? Ngoaihinh { get; set; }
    }
}
