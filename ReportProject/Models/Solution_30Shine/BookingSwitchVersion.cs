﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class BookingSwitchVersion
    {
        public int Id { get; set; }
        public string Version { get; set; }
        public DateTime? CreatedTime { get; set; }
        public string TargetVersion { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
