﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class OrderBrokenDeviceHandling
    {
        public int Id { get; set; }
        public int? RegionId { get; set; }
        public int? SalonId { get; set; }
        public int? OrderStaffId { get; set; }
        public int? CategoryId { get; set; }
        public int? TeamId { get; set; }
        public DateTime? BrokenDate { get; set; }
        public string ImagesBroken { get; set; }
        public string DescriptionBroken { get; set; }
        public int? LevelPriority { get; set; }
        public DateTime? DesiredDeadline { get; set; }
        public DateTime? CorrectDeadline { get; set; }
        public int? StaffIdHandling { get; set; }
        public int? StatusHandling { get; set; }
        public DateTime? CompleteDate { get; set; }
        public string ImageHandling { get; set; }
        public int? ProcessHandling { get; set; }
        public DateTime? StatusCheckTime { get; set; }
        public int? StatusCheck { get; set; }
        public string Note { get; set; }
        public int? ReasonRevoke { get; set; }
        public string NoteRevoke { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }
        public string HandlingNote { get; set; }
    }
}
