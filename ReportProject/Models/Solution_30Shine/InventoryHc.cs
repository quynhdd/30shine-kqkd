﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class InventoryHc
    {
        public int Id { get; set; }
        public string InvenData { get; set; }
        public DateTime? IDate { get; set; }
        public int? SalonId { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
        public int? StaffOrderId { get; set; }
    }
}
