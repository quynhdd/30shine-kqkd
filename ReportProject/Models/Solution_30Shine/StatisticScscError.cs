﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class StatisticScscError
    {
        public int Id { get; set; }
        public int? StylistId { get; set; }
        public int? SalonId { get; set; }
        public int? TongKhongAnhScsc { get; set; }
        public int? TongAnhLoiScsc { get; set; }
        public int? TongAnhMoScsc { get; set; }
        public int? TongKhongMoScsc { get; set; }
        public int? TongLoiScscHoacUon { get; set; }
        public int? TongDiemScsc { get; set; }
        public int? TongErrorShape { get; set; }
        public int? TongErrorComplatetion { get; set; }
        public int? TongErrorSharpNess { get; set; }
        public int? TongKhongAnhUon { get; set; }
        public int? TongBillUon { get; set; }
        public int? TongAnhUon { get; set; }
        public int? TongMoLechUon { get; set; }
        public int? TongLoiUon { get; set; }
        public int? TongErrorHairTip { get; set; }
        public int? TongErrorHairRoots { get; set; }
        public int? TongErrorHairWaves { get; set; }
        public DateTime? WorkDate { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public int? TotalBill { get; set; }
        public int? TongDiemUon { get; set; }
        public int? TongMoLechUonScsc { get; set; }
        public int? TongErrorConnective { get; set; }
    }
}
