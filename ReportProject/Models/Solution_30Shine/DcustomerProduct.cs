﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class DcustomerProduct
    {
        public int IdCustomerProduct { get; set; }
        public int CustomerId { get; set; }
        public int BillId { get; set; }
        public int ProductId { get; set; }
        public int ProductPrice { get; set; }
        public int ProductQuantity { get; set; }
        public int VoucherPercent { get; set; }
        public DateTime DateOfBill { get; set; }
        public int SalonId { get; set; }
        public int? SellerId { get; set; }
    }
}
