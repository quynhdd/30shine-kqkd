﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class MonitorCategoryError
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModiyDate { get; set; }
        public bool IsDelete { get; set; }
        public int? TypeError { get; set; }
    }
}
