﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class TblTesst
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public bool? Issms { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
