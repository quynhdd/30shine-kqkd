﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class StaffUploadedImages
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public string Url { get; set; }
        public string Type { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }

        public Staff Staff { get; set; }
    }
}
