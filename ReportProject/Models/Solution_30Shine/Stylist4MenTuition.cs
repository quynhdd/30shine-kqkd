﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class Stylist4MenTuition
    {
        public int Id { get; set; }
        public int? StudentId { get; set; }
        public int? AmountCollected { get; set; }
        public int? PayTheMoney { get; set; }
        public DateTime? CreaetedTime { get; set; }
        public DateTime? ModifiledTime { get; set; }
        public bool? Publish { get; set; }
        public bool? IsDelete { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
