﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class StaffContractMap
    {
        public int Id { get; set; }
        public int? StaffId { get; set; }
        public int? ContractId { get; set; }
        public DateTime? ContractDate { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public DateTime? SignDate { get; set; }
        public bool IsSign { get; set; }
        public bool Active { get; set; }
        public bool IsDelete { get; set; }
    }
}
