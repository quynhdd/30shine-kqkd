﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class SalonService
    {
        public int Id { get; set; }
        public int? SalonId { get; set; }
        public int? ServiceId { get; set; }
        public decimal? Price { get; set; }
        public int? HeSoHl { get; set; }
        public bool? IsCheck { get; set; }
        public bool? IsCheckSalon { get; set; }
        public bool? IsDelete { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
