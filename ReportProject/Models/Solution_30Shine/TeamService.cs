﻿using System;
using System.Collections.Generic;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class TeamService
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte? IsDelete { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? Publish { get; set; }
        public byte? Status { get; set; }
        public string Color { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public bool? IsLogin { get; set; }
        public bool? IsOff { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
