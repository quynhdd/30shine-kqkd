﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ReportProject.Models.Solution_30Shine
{
    public partial class Solution_30shineContextV1 : DbContext
    {
        public virtual DbSet<ApiDeviceManager> ApiDeviceManager { get; set; }
        public virtual DbSet<ApiHairMode> ApiHairMode { get; set; }
        public virtual DbSet<ApiHairModeLevel> ApiHairModeLevel { get; set; }
        public virtual DbSet<ApiHairModeStaff> ApiHairModeStaff { get; set; }
        public virtual DbSet<ApiNotiSendManager> ApiNotiSendManager { get; set; }
        public virtual DbSet<ApiSlide> ApiSlide { get; set; }
        public virtual DbSet<ApiStaffVideo> ApiStaffVideo { get; set; }
        public virtual DbSet<ApiVideo> ApiVideo { get; set; }
        public virtual DbSet<ApiVideoTv> ApiVideoTv { get; set; }
        public virtual DbSet<Application> Application { get; set; }
        public virtual DbSet<Appointment> Appointment { get; set; }
        public virtual DbSet<Authorization> Authorization { get; set; }
        public virtual DbSet<BackupTableServiceSalonConfig> BackupTableServiceSalonConfig { get; set; }
        public virtual DbSet<BillConfirm> BillConfirm { get; set; }
        public virtual DbSet<BillService> BillService { get; set; }
        public virtual DbSet<BillWaitAtSalon> BillWaitAtSalon { get; set; }
        public virtual DbSet<BookHour> BookHour { get; set; }
        public virtual DbSet<BookHourSub> BookHourSub { get; set; }
        public virtual DbSet<Booking> Booking { get; set; }
        public virtual DbSet<BookingChangeStylist> BookingChangeStylist { get; set; }
        public virtual DbSet<BookingSalonBackup> BookingSalonBackup { get; set; }
        public virtual DbSet<BookingSpecialOptionTick> BookingSpecialOptionTick { get; set; }
        public virtual DbSet<BookingStatisticSlot> BookingStatisticSlot { get; set; }
        public virtual DbSet<BookingStatus> BookingStatus { get; set; }
        public virtual DbSet<BookingStylistBackup> BookingStylistBackup { get; set; }
        public virtual DbSet<BookingSuggest> BookingSuggest { get; set; }
        public virtual DbSet<BookingSuggestSalonConfig> BookingSuggestSalonConfig { get; set; }
        public virtual DbSet<BookingSwitchVersion> BookingSwitchVersion { get; set; }
        public virtual DbSet<BookingTest> BookingTest { get; set; }
        public virtual DbSet<Brands> Brands { get; set; }
        public virtual DbSet<Call> Call { get; set; }
        public virtual DbSet<CallBackRequest> CallBackRequest { get; set; }
        public virtual DbSet<CheckinCheckout> CheckinCheckout { get; set; }
        public virtual DbSet<ConfigMembership> ConfigMembership { get; set; }
        public virtual DbSet<ConfigPartTime> ConfigPartTime { get; set; }
        public virtual DbSet<ConfigQuantifyProduct> ConfigQuantifyProduct { get; set; }
        public virtual DbSet<Contracts> Contracts { get; set; }
        public virtual DbSet<CrmVoucherWaitTime> CrmVoucherWaitTime { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<CustomerBookingCheck> CustomerBookingCheck { get; set; }
        public virtual DbSet<CustomerHairAttribute> CustomerHairAttribute { get; set; }
        public virtual DbSet<CustomerHairModeBill> CustomerHairModeBill { get; set; }
        public virtual DbSet<CustomerHairSkin> CustomerHairSkin { get; set; }
        public virtual DbSet<CustomerHairSkinHistory> CustomerHairSkinHistory { get; set; }
        public virtual DbSet<CustomerRating> CustomerRating { get; set; }
        public virtual DbSet<CustomerRatingUuDai> CustomerRatingUuDai { get; set; }
        public virtual DbSet<CustomerType> CustomerType { get; set; }
        public virtual DbSet<CustomerUuDai> CustomerUuDai { get; set; }
        public virtual DbSet<DcustomerAtSalon> DcustomerAtSalon { get; set; }
        public virtual DbSet<DcustomerCall> DcustomerCall { get; set; }
        public virtual DbSet<DcustomerInfo> DcustomerInfo { get; set; }
        public virtual DbSet<DcustomerProduct> DcustomerProduct { get; set; }
        public virtual DbSet<DcustomerProductTotal> DcustomerProductTotal { get; set; }
        public virtual DbSet<DcustomerScore> DcustomerScore { get; set; }
        public virtual DbSet<DcustomerService> DcustomerService { get; set; }
        public virtual DbSet<DcustomerServiceTotal> DcustomerServiceTotal { get; set; }
        public virtual DbSet<DcustomerSms> DcustomerSms { get; set; }
        public virtual DbSet<DcustomerTeamService> DcustomerTeamService { get; set; }
        public virtual DbSet<Device> Device { get; set; }
        public virtual DbSet<DstylistScore> DstylistScore { get; set; }
        public virtual DbSet<ErpCheck3S> ErpCheck3S { get; set; }
        public virtual DbSet<ErpCheckCsvc> ErpCheckCsvc { get; set; }
        public virtual DbSet<ErpItemCheck> ErpItemCheck { get; set; }
        public virtual DbSet<ErrorCutHair> ErrorCutHair { get; set; }
        public virtual DbSet<ErrorCutReason> ErrorCutReason { get; set; }
        public virtual DbSet<ExportGoods> ExportGoods { get; set; }
        public virtual DbSet<FeedbackPointService> FeedbackPointService { get; set; }
        public virtual DbSet<FlowGoods> FlowGoods { get; set; }
        public virtual DbSet<FlowProduct> FlowProduct { get; set; }
        public virtual DbSet<FlowPromotion> FlowPromotion { get; set; }
        public virtual DbSet<FlowSalary> FlowSalary { get; set; }
        public virtual DbSet<FlowService> FlowService { get; set; }
        public virtual DbSet<FlowStaff> FlowStaff { get; set; }
        public virtual DbSet<FlowTimeKeeping> FlowTimeKeeping { get; set; }
        public virtual DbSet<FundAccountType> FundAccountType { get; set; }
        public virtual DbSet<FundImport> FundImport { get; set; }
        public virtual DbSet<FundItemFlow> FundItemFlow { get; set; }
        public virtual DbSet<FundOffenItem> FundOffenItem { get; set; }
        public virtual DbSet<FundSource> FundSource { get; set; }
        public virtual DbSet<HairAttribute> HairAttribute { get; set; }
        public virtual DbSet<HairAttributeProduct> HairAttributeProduct { get; set; }
        public virtual DbSet<ImageData> ImageData { get; set; }
        public virtual DbSet<InventoryData> InventoryData { get; set; }
        public virtual DbSet<InventoryFlow> InventoryFlow { get; set; }
        public virtual DbSet<InventoryFlowHc> InventoryFlowHc { get; set; }
        public virtual DbSet<InventoryHc> InventoryHc { get; set; }
        public virtual DbSet<InventoryImport> InventoryImport { get; set; }
        public virtual DbSet<IvInventory> IvInventory { get; set; }
        public virtual DbSet<IvInventoryCurrent> IvInventoryCurrent { get; set; }
        public virtual DbSet<IvInventoryHistory> IvInventoryHistory { get; set; }
        public virtual DbSet<IvInventoryInitial> IvInventoryInitial { get; set; }
        public virtual DbSet<IvOrder> IvOrder { get; set; }
        public virtual DbSet<IvOrderDetail> IvOrderDetail { get; set; }
        public virtual DbSet<IvOrderNum> IvOrderNum { get; set; }
        public virtual DbSet<IvProductQuantify> IvProductQuantify { get; set; }
        public virtual DbSet<KcsCheck3S> KcsCheck3S { get; set; }
        public virtual DbSet<KcsFaceType> KcsFaceType { get; set; }
        public virtual DbSet<KetQuaKinhDoanhFlowImport> KetQuaKinhDoanhFlowImport { get; set; }
        public virtual DbSet<KetQuaKinhDoanhItemImport> KetQuaKinhDoanhItemImport { get; set; }
        public virtual DbSet<KetQuaKinhDoanhSalon> KetQuaKinhDoanhSalon { get; set; }
        public virtual DbSet<Log> Log { get; set; }
        public virtual DbSet<ManagerLog> ManagerLog { get; set; }
        public virtual DbSet<MapDeviceOwner> MapDeviceOwner { get; set; }
        public virtual DbSet<MarketingChienDich> MarketingChienDich { get; set; }
        public virtual DbSet<MarketingChiphiItem> MarketingChiphiItem { get; set; }
        public virtual DbSet<MarketingChiphingay> MarketingChiphingay { get; set; }
        public virtual DbSet<MarketingChiPhiPhanBo> MarketingChiPhiPhanBo { get; set; }
        public virtual DbSet<MarkettingNganSach> MarkettingNganSach { get; set; }
        public virtual DbSet<MemberLog> MemberLog { get; set; }
        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<MktCampaign> MktCampaign { get; set; }
        public virtual DbSet<MktCampaignBill> MktCampaignBill { get; set; }
        public virtual DbSet<MktCampaignCustomer> MktCampaignCustomer { get; set; }
        public virtual DbSet<MktCampaignService> MktCampaignService { get; set; }
        public virtual DbSet<MktCampaignUsed> MktCampaignUsed { get; set; }
        public virtual DbSet<MktVoucher> MktVoucher { get; set; }
        public virtual DbSet<Mobile> Mobile { get; set; }
        public virtual DbSet<MonitorCategoryError> MonitorCategoryError { get; set; }
        public virtual DbSet<MonitorHandle> MonitorHandle { get; set; }
        public virtual DbSet<MonitorStaffError> MonitorStaffError { get; set; }
        public virtual DbSet<NetworkOperator> NetworkOperator { get; set; }
        public virtual DbSet<NotificationManagement> NotificationManagement { get; set; }
        public virtual DbSet<NotificationUsers> NotificationUsers { get; set; }
        public virtual DbSet<OperationReportConfig> OperationReportConfig { get; set; }
        public virtual DbSet<OperationReportStatistic> OperationReportStatistic { get; set; }
        public virtual DbSet<OrderBrokenDeviceHandling> OrderBrokenDeviceHandling { get; set; }
        public virtual DbSet<OrderBrokenDeviceHandlingLog> OrderBrokenDeviceHandlingLog { get; set; }
        public virtual DbSet<OrderRecruitingStaff> OrderRecruitingStaff { get; set; }
        public virtual DbSet<OrderRecruitingStaffLog> OrderRecruitingStaffLog { get; set; }
        public virtual DbSet<PayMethod> PayMethod { get; set; }
        public virtual DbSet<PermissionAction> PermissionAction { get; set; }
        public virtual DbSet<PermissionDefaultPage> PermissionDefaultPage { get; set; }
        public virtual DbSet<PermissionErp> PermissionErp { get; set; }
        public virtual DbSet<PermissionMenu> PermissionMenu { get; set; }
        public virtual DbSet<PermissionMenuAction> PermissionMenuAction { get; set; }
        public virtual DbSet<PermissionSalonArea> PermissionSalonArea { get; set; }
        public virtual DbSet<PermissionStaff> PermissionStaff { get; set; }
        public virtual DbSet<PreviewImagesReport> PreviewImagesReport { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductReturn> ProductReturn { get; set; }
        public virtual DbSet<ProductUsedStatistic> ProductUsedStatistic { get; set; }
        public virtual DbSet<QlkhoSalonOrder> QlkhoSalonOrder { get; set; }
        public virtual DbSet<QlkhoSalonOrderFlow> QlkhoSalonOrderFlow { get; set; }
        public virtual DbSet<QlkhoSalonOrderStatus> QlkhoSalonOrderStatus { get; set; }
        public virtual DbSet<QuanHuyen> QuanHuyen { get; set; }
        public virtual DbSet<RatingBonus> RatingBonus { get; set; }
        public virtual DbSet<RatingConfigPoint> RatingConfigPoint { get; set; }
        public virtual DbSet<RatingDetail> RatingDetail { get; set; }
        public virtual DbSet<RatingDetailV1> RatingDetailV1 { get; set; }
        public virtual DbSet<RatingIsCheck> RatingIsCheck { get; set; }
        public virtual DbSet<RatingReason> RatingReason { get; set; }
        public virtual DbSet<RatingTemp> RatingTemp { get; set; }
        public virtual DbSet<RealtimeFirebaseTokenId> RealtimeFirebaseTokenId { get; set; }
        public virtual DbSet<SalaryConfig> SalaryConfig { get; set; }
        public virtual DbSet<SalaryConfigStaff> SalaryConfigStaff { get; set; }
        public virtual DbSet<SalaryIncome> SalaryIncome { get; set; }
        public virtual DbSet<SalaryIncomeChange> SalaryIncomeChange { get; set; }
        public virtual DbSet<SalonDailyCost> SalonDailyCost { get; set; }
        public virtual DbSet<SalonService> SalonService { get; set; }
        public virtual DbSet<SalonTypeConfig> SalonTypeConfig { get; set; }
        public virtual DbSet<ScriptData> ScriptData { get; set; }
        public virtual DbSet<ScscCategory> ScscCategory { get; set; }
        public virtual DbSet<ScscCheckError> ScscCheckError { get; set; }
        public virtual DbSet<SelfieCode> SelfieCode { get; set; }
        public virtual DbSet<Service> Service { get; set; }
        public virtual DbSet<ServiceRatingRelationship> ServiceRatingRelationship { get; set; }
        public virtual DbSet<ServiceSalonConfig> ServiceSalonConfig { get; set; }
        public virtual DbSet<ServiceSalonGoldTime> ServiceSalonGoldTime { get; set; }
        public virtual DbSet<ServiceSalonTypeConfig> ServiceSalonTypeConfig { get; set; }
        public virtual DbSet<ServiceTemp> ServiceTemp { get; set; }
        public virtual DbSet<SkinAttribute> SkinAttribute { get; set; }
        public virtual DbSet<SkinAttributeProduct> SkinAttributeProduct { get; set; }
        public virtual DbSet<SmBillTemp> SmBillTemp { get; set; }
        public virtual DbSet<SmBillTempFlowProduct> SmBillTempFlowProduct { get; set; }
        public virtual DbSet<SmBillTempFlowService> SmBillTempFlowService { get; set; }
        public virtual DbSet<SmBookingTemp> SmBookingTemp { get; set; }
        public virtual DbSet<SmEnrollTemp> SmEnrollTemp { get; set; }
        public virtual DbSet<SmEnrollTempHour> SmEnrollTempHour { get; set; }
        public virtual DbSet<SocialThread> SocialThread { get; set; }
        public virtual DbSet<SpecialCusDetail> SpecialCusDetail { get; set; }
        public virtual DbSet<SpecialCustomer> SpecialCustomer { get; set; }
        public virtual DbSet<Staff> Staff { get; set; }
        public virtual DbSet<StaffAutoCondition> StaffAutoCondition { get; set; }
        public virtual DbSet<StaffAutoLevelLog> StaffAutoLevelLog { get; set; }
        public virtual DbSet<StaffAutoLevelup> StaffAutoLevelup { get; set; }
        public virtual DbSet<StaffAvatar> StaffAvatar { get; set; }
        public virtual DbSet<StaffBillServiceDetail> StaffBillServiceDetail { get; set; }
        public virtual DbSet<StaffContractMap> StaffContractMap { get; set; }
        public virtual DbSet<StaffErrorSpecailCus> StaffErrorSpecailCus { get; set; }
        public virtual DbSet<StaffFluctuations> StaffFluctuations { get; set; }
        public virtual DbSet<StaffMistake> StaffMistake { get; set; }
        public virtual DbSet<StaffProcedure> StaffProcedure { get; set; }
        public virtual DbSet<StaffProfileMap> StaffProfileMap { get; set; }
        public virtual DbSet<StaffRanking> StaffRanking { get; set; }
        public virtual DbSet<StaffRoll> StaffRoll { get; set; }
        public virtual DbSet<StaffSalonHistory> StaffSalonHistory { get; set; }
        public virtual DbSet<StaffType> StaffType { get; set; }
        public virtual DbSet<StaticExpense> StaticExpense { get; set; }
        public virtual DbSet<StaticOperate> StaticOperate { get; set; }
        public virtual DbSet<StaticRatingWaitTime> StaticRatingWaitTime { get; set; }
        public virtual DbSet<StaticServicesProfit> StaticServicesProfit { get; set; }
        public virtual DbSet<StatictisReportOperation> StatictisReportOperation { get; set; }
        public virtual DbSet<StatisticSalaryProduct> StatisticSalaryProduct { get; set; }
        public virtual DbSet<StatisticSalaryService> StatisticSalaryService { get; set; }
        public virtual DbSet<StatisticScscError> StatisticScscError { get; set; }
        public virtual DbSet<StatisticsXuatVatTu> StatisticsXuatVatTu { get; set; }
        public virtual DbSet<StyleMaster> StyleMaster { get; set; }
        public virtual DbSet<StyleMasterLog> StyleMasterLog { get; set; }
        public virtual DbSet<StyleMasterStatus> StyleMasterStatus { get; set; }
        public virtual DbSet<Stylist4MenBillCutFree> Stylist4MenBillCutFree { get; set; }
        public virtual DbSet<Stylist4MenClass> Stylist4MenClass { get; set; }
        public virtual DbSet<Stylist4MenCredits> Stylist4MenCredits { get; set; }
        public virtual DbSet<Stylist4MenCustomer> Stylist4MenCustomer { get; set; }
        public virtual DbSet<Stylist4MenGraduationScore> Stylist4MenGraduationScore { get; set; }
        public virtual DbSet<Stylist4MenPointClubs> Stylist4MenPointClubs { get; set; }
        public virtual DbSet<Stylist4MenStudent> Stylist4MenStudent { get; set; }
        public virtual DbSet<Stylist4MenStudyPackage> Stylist4MenStudyPackage { get; set; }
        public virtual DbSet<Stylist4MenTuition> Stylist4MenTuition { get; set; }
        public virtual DbSet<SuKienTeam> SuKienTeam { get; set; }
        public virtual DbSet<SuKienTeamStaff> SuKienTeamStaff { get; set; }
        public virtual DbSet<SurveyContent> SurveyContent { get; set; }
        public virtual DbSet<SurveyFeedback> SurveyFeedback { get; set; }
        public virtual DbSet<SurveyMapQa> SurveyMapQa { get; set; }
        public virtual DbSet<TblCategory> TblCategory { get; set; }
        public virtual DbSet<TblConfig> TblConfig { get; set; }
        public virtual DbSet<TblConfigWorkDays> TblConfigWorkDays { get; set; }
        public virtual DbSet<TblCusInputBooking> TblCusInputBooking { get; set; }
        public virtual DbSet<TblEmailReport> TblEmailReport { get; set; }
        public virtual DbSet<TblFormMisstake> TblFormMisstake { get; set; }
        public virtual DbSet<TblGroupProduct> TblGroupProduct { get; set; }
        public virtual DbSet<TblInformationStaffClub> TblInformationStaffClub { get; set; }
        public virtual DbSet<TblListBack30Day> TblListBack30Day { get; set; }
        public virtual DbSet<TblMedia> TblMedia { get; set; }
        public virtual DbSet<TblPayon> TblPayon { get; set; }
        public virtual DbSet<TblPermission> TblPermission { get; set; }
        public virtual DbSet<TblPermissionAction> TblPermissionAction { get; set; }
        public virtual DbSet<TblPermissionDefaultPage> TblPermissionDefaultPage { get; set; }
        public virtual DbSet<TblPermissionMap> TblPermissionMap { get; set; }
        public virtual DbSet<TblPermissionMapV2> TblPermissionMapV2 { get; set; }
        public virtual DbSet<TblPermissionMenu> TblPermissionMenu { get; set; }
        public virtual DbSet<TblPermissionMenuV2> TblPermissionMenuV2 { get; set; }
        public virtual DbSet<TblPermissionSalonArea> TblPermissionSalonArea { get; set; }
        public virtual DbSet<TblSalon> TblSalon { get; set; }
        public virtual DbSet<TblSkillLevel> TblSkillLevel { get; set; }
        public virtual DbSet<TblStaffOfClubInformation> TblStaffOfClubInformation { get; set; }
        public virtual DbSet<TblStaffSurvey> TblStaffSurvey { get; set; }
        public virtual DbSet<TblStatus> TblStatus { get; set; }
        public virtual DbSet<TblTemp> TblTemp { get; set; }
        public virtual DbSet<TblTesst> TblTesst { get; set; }
        public virtual DbSet<TeamService> TeamService { get; set; }
        public virtual DbSet<TimekeepingConfig> TimekeepingConfig { get; set; }
        public virtual DbSet<TinhThanh> TinhThanh { get; set; }
        public virtual DbSet<TopSalary> TopSalary { get; set; }
        public virtual DbSet<TrackingWebData> TrackingWebData { get; set; }
        public virtual DbSet<TrackingWebDataV2> TrackingWebDataV2 { get; set; }
        public virtual DbSet<TrackingWebDataV22017> TrackingWebDataV22017 { get; set; }
        public virtual DbSet<TrackingWebEvent> TrackingWebEvent { get; set; }
        public virtual DbSet<TuyenDungDanhGia> TuyenDungDanhGia { get; set; }
        public virtual DbSet<TuyenDungNguoiTest> TuyenDungNguoiTest { get; set; }
        public virtual DbSet<TuyenDungNguon> TuyenDungNguon { get; set; }
        public virtual DbSet<TuyenDungSkill> TuyenDungSkill { get; set; }
        public virtual DbSet<TuyenDungSkillLevel> TuyenDungSkillLevel { get; set; }
        public virtual DbSet<TuyenDungSkillLevelMap> TuyenDungSkillLevelMap { get; set; }
        public virtual DbSet<TuyenDungStatus> TuyenDungStatus { get; set; }
        public virtual DbSet<TuyenDungUngVien> TuyenDungUngVien { get; set; }
        public virtual DbSet<TypeErrorSpecailCus> TypeErrorSpecailCus { get; set; }
        public virtual DbSet<Vietnam> Vietnam { get; set; }
        public virtual DbSet<WorkflowFile> WorkflowFile { get; set; }
        public virtual DbSet<WorkTime> WorkTime { get; set; }
        public virtual DbSet<BillServiceHis> BillServiceHis { get; set; }

        // Unable to generate entity type for table 'dba.lock_block'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Service_Rating'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Tbl_Permission_Staff_Roles'. Please see the warning messages.
        public Solution_30shineContextV1(DbContextOptions options) : base(options) { }
        public Solution_30shineContextV1() { }
        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //        {
        //            if (!optionsBuilder.IsConfigured)
        //            {
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //                optionsBuilder.UseSqlServer(@"Server=210.211.99.159;Database=20190111;User id=ad_30s;Password=VDrs8XHyz3Rdc7PAiv5Q;Trusted_Connection=False;");
        //            }
        //        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BillServiceHis>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AppointmentTime).HasColumnType("datetime");

                entity.Property(e => e.BillCode).HasMaxLength(20);

                entity.Property(e => e.CompleteBillTime).HasColumnType("datetime");

                entity.Property(e => e.CustomerCode).HasMaxLength(50);

                entity.Property(e => e.CustomerCode1).HasMaxLength(50);

                entity.Property(e => e.CustomerConfirm).HasDefaultValueSql("((0))");

                entity.Property(e => e.ErrorNote).HasMaxLength(600);

                entity.Property(e => e.EstimateTimeCutTime).HasColumnType("datetime");

                entity.Property(e => e.FeeCod).HasColumnName("FeeCOD");

                entity.Property(e => e.FinishTime).HasColumnType("datetime");

                entity.Property(e => e.HairCutTime0).HasColumnType("datetime");

                entity.Property(e => e.HairCutTime1).HasColumnType("datetime");

                entity.Property(e => e.Hcitem)
                    .HasColumnName("HCItem")
                    .HasMaxLength(50);

                entity.Property(e => e.ImageChecked1).HasMaxLength(600);

                entity.Property(e => e.ImageChecked2).HasMaxLength(600);

                entity.Property(e => e.ImageChecked3).HasMaxLength(600);

                entity.Property(e => e.ImageChecked4).HasMaxLength(600);

                entity.Property(e => e.ImageStatusId).HasMaxLength(100);

                entity.Property(e => e.Images).HasMaxLength(600);

                entity.Property(e => e.ImagesRating).HasMaxLength(600);

                entity.Property(e => e.InProcedureTime).HasColumnType("datetime");

                entity.Property(e => e.InProcedureTimeModifed).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasColumnType("ntext");

                entity.Property(e => e.NoteByStylist).HasMaxLength(600);

                entity.Property(e => e.PdfbillCode)
                    .HasColumnName("PDFBillCode")
                    .HasMaxLength(30);

                entity.Property(e => e.QueueCheckoutInfo)
                    .HasMaxLength(600)
                    .IsUnicode(false);

                entity.Property(e => e.ShampooTime0).HasColumnType("datetime");

                entity.Property(e => e.ShampooTime1).HasColumnType("datetime");

                entity.Property(e => e.StaffHairMassageId).HasColumnName("Staff_HairMassage_Id");

                entity.Property(e => e.StaffHairdresserId).HasColumnName("Staff_Hairdresser_Id");

                entity.Property(e => e.StaffMakeBill).HasColumnName("Staff_MakeBill");

                entity.Property(e => e.UploadImageTime).HasColumnType("datetime");

                entity.Property(e => e.Vipcard).HasColumnName("VIPCard");

                entity.Property(e => e.VipcardGive)
                    .HasColumnName("VIPCardGive")
                    .HasMaxLength(10);

                entity.Property(e => e.VipcardUse)
                    .HasColumnName("VIPCardUse")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<ApiDeviceManager>(entity =>
            {
                entity.ToTable("Api_DeviceManager");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DeviceRegistrationToken).HasMaxLength(250);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Os)
                    .HasColumnName("OS")
                    .HasMaxLength(10);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ApiHairMode>(entity =>
            {
                entity.ToTable("Api_HairMode");

                entity.Property(e => e.CareLink).HasMaxLength(600);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Thumb).HasMaxLength(400);

                entity.Property(e => e.Title).HasMaxLength(600);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ApiHairModeLevel>(entity =>
            {
                entity.ToTable("Api_HairMode_Level");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(600);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(300);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ApiHairModeStaff>(entity =>
            {
                entity.ToTable("Api_HairMode_Staff");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ApiNotiSendManager>(entity =>
            {
                entity.ToTable("Api_NotiSendManager");

                entity.Property(e => e.DeviceRegistrationToken).HasMaxLength(250);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.SendTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ApiSlide>(entity =>
            {
                entity.ToTable("Api_Slide");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Des).HasColumnType("ntext");

                entity.Property(e => e.Img).HasMaxLength(250);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(250);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Url).HasMaxLength(250);
            });

            modelBuilder.Entity<ApiStaffVideo>(entity =>
            {
                entity.ToTable("Api_Staff_Video");

                entity.Property(e => e.Chanel).HasMaxLength(250);

                entity.Property(e => e.DateChanged).HasColumnType("datetime");

                entity.Property(e => e.DateCreate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.Link).HasMaxLength(250);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.PublishDate).HasColumnType("datetime");

                entity.Property(e => e.Thumb).HasMaxLength(250);

                entity.Property(e => e.Title).HasMaxLength(250);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.VideoId).HasMaxLength(250);
            });

            modelBuilder.Entity<ApiVideo>(entity =>
            {
                entity.ToTable("Api_Video");

                entity.Property(e => e.Chanel).HasMaxLength(250);

                entity.Property(e => e.DateChanged).HasColumnType("datetime");

                entity.Property(e => e.DateCreate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.Link).HasMaxLength(250);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.PublishDate).HasColumnType("datetime");

                entity.Property(e => e.Thumb).HasMaxLength(250);

                entity.Property(e => e.Title).HasMaxLength(250);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.VideoId).HasMaxLength(250);
            });

            modelBuilder.Entity<ApiVideoTv>(entity =>
            {
                entity.ToTable("Api_VideoTV");

                entity.Property(e => e.Chanel).HasMaxLength(250);

                entity.Property(e => e.DateChanged).HasColumnType("datetime");

                entity.Property(e => e.DateCreate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.Link).HasMaxLength(250);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.PublishDate).HasColumnType("datetime");

                entity.Property(e => e.Thumb).HasMaxLength(250);

                entity.Property(e => e.Title).HasMaxLength(250);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.VideoId).HasMaxLength(250);
            });

            modelBuilder.Entity<Application>(entity =>
            {
                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DeleteTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1970-1-1')");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Uid)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1970-1-1')");
            });

            modelBuilder.Entity<Appointment>(entity =>
            {
                entity.Property(e => e.AppointmentCode).HasMaxLength(50);

                entity.Property(e => e.AppointmentTime).HasColumnType("datetime");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.CustomerCode).HasMaxLength(50);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.PdfbillCode)
                    .HasColumnName("PDFBillCode")
                    .HasMaxLength(50);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.UpdateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<Authorization>(entity =>
            {
                entity.HasIndex(e => new { e.Token, e.IsDelete })
                    .HasName("IDX_20180718_4950_4949_Solution_30shinedboAuthorization");

                entity.HasIndex(e => new { e.UserUid, e.ApplicationUid })
                    .HasName("IDX_20180719_104_103_Solution_30shinedboAuthorization");

                entity.HasIndex(e => new { e.UserUid, e.DeviceUid, e.ApplicationUid })
                    .HasName("IDX_20180719_107_106_Solution_30shinedboAuthorization");

                entity.HasIndex(e => new { e.UserUid, e.DeviceUid, e.ApplicationUid, e.IsDelete, e.Token })
                    .HasName("IDX_20180718_673_672_Solution_30shinedboAuthorization");

                entity.Property(e => e.AppVersion).HasMaxLength(50);

                entity.Property(e => e.ApplicationUid)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DeleteTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1970-1-1')");

                entity.Property(e => e.DeviceName)
                    .HasMaxLength(250)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DeviceType)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DeviceUid)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Token)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TokenAlg)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TokenExp)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1970-1-1')");

                entity.Property(e => e.TokenIss)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TokenType)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Uid)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1970-1-1')");

                entity.Property(e => e.UserType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserUid)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<BackupTableServiceSalonConfig>(entity =>
            {
                entity.Property(e => e.BackupDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<BillConfirm>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerConfirm).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<BillService>(entity =>
            {
                entity.HasIndex(e => e.BookingId)
                    .HasName("IX_BillService_Bookingid");

                entity.HasIndex(e => e.CustomerCode)
                    .HasName("IX_BillService_Phone");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("idx_CustomerId");

                entity.HasIndex(e => new { e.Id, e.Pending, e.CreatedDate, e.SalonId })
                    .HasName("IDX_20180719_10_9_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.StaffHairdresserId, e.IsDelete, e.SalonId, e.Pending })
                    .HasName("IDX_20180719_4992_4991_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.Id, e.CreatedDate, e.SalonId, e.IsDelete, e.Pending })
                    .HasName("IDX_20180715_163367_163366_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.CreatedDate, e.IsDelete, e.Pending })
                    .HasName("IDX_20180715_163257_163256_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.IsDelete, e.Pending, e.CompleteBillTime })
                    .HasName("IDX_20180715_158060_158059_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.Id, e.SellerId, e.IsDelete, e.Pending, e.CompleteBillTime })
                    .HasName("IDX_20180715_158156_158155_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.Id, e.StaffHairMassageId, e.Pending, e.CreatedDate, e.IsDelete })
                    .HasName("IDX_20180718_29_28_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.Id, e.StaffHairdresserId, e.IsDelete, e.Pending, e.CreatedDate })
                    .HasName("IDX_20180715_158800_158799_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.InProcedureTime, e.StaffHairMassageId, e.IsDelete, e.Pending, e.CreatedDate })
                    .HasName("IDX_20180718_1956_1955_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.StaffHairMassageId, e.IsDelete, e.Pending, e.CreatedDate })
                    .HasName("IDX_20180718_3411_3410_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.StaffHairdresserId, e.IsDelete, e.Pending, e.CreatedDate })
                    .HasName("IDX_20180715_158150_158149_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.SalonId, e.BookingId, e.IsDelete, e.Pending, e.CreatedDate, e.InProcedureTime })
                    .HasName("IDX_20180720_5135_5134_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.StaffHairdresserId, e.ServiceIds, e.IsDelete, e.SalonId, e.Pending, e.CreatedDate })
                    .HasName("IDX_20180718_770_769_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.Id, e.StaffHairdresserId, e.StaffHairMassageId, e.IsDelete, e.Pending, e.CreatedDate, e.SalonId })
                    .HasName("IDX_20180719_633_632_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.CreatedDate, e.ServiceIds, e.InProcedureTime, e.BookingId, e.ReceptionId, e.IsDelete, e.Pending, e.CompleteBillTime })
                    .HasName("IDX_20180718_1599_1598_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.Id, e.ServiceIds, e.Images, e.CompleteBillTime, e.UploadImageTime, e.StaffHairdresserId, e.IsDelete, e.Pending })
                    .HasName("IDX_20180719_21_20_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.StaffHairdresserId, e.ServiceIds, e.Images, e.SalonId, e.IsDelete, e.Pending, e.CreatedDate, e.Id })
                    .HasName("_dta_index_BillService_9_680545658__K13_K21_K8_K1_3_14_15_18");

                entity.HasIndex(e => new { e.Id, e.CreatedDate, e.Mark, e.SalonId, e.BookingId, e.StaffHairMassageId, e.IsDelete, e.Pending, e.CompleteBillTime })
                    .HasName("IDX_20180718_3409_3408_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.Id, e.CreatedDate, e.Mark, e.SalonId, e.BookingId, e.StaffHairdresserId, e.IsDelete, e.Pending, e.CompleteBillTime })
                    .HasName("IDX_20180715_158144_158143_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.Id, e.CreatedDate, e.SalonId, e.BookingId, e.StaffHairMassageId, e.Mark, e.IsDelete, e.Pending, e.CompleteBillTime })
                    .HasName("IDX_20180718_3407_3406_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.Id, e.CreatedDate, e.SalonId, e.BookingId, e.StaffHairdresserId, e.Mark, e.IsDelete, e.Pending, e.CompleteBillTime })
                    .HasName("IDX_20180715_158142_158141_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.Id, e.StaffHairdresserId, e.StaffHairMassageId, e.BillCode, e.PdfbillCode, e.CustomerId, e.IsDelete, e.SalonId, e.Pending, e.CreatedDate })
                    .HasName("IDX_20180715_158030_158029_Solution_30shinedboBillService");

                entity.HasIndex(e => new { e.BillCode, e.PdfbillCode, e.InProcedureTime, e.InProcedureTimeModifed, e.EstimateTimeCut, e.SalonId, e.CreatedDate, e.Pending, e.IsDelete, e.BookingId, e.StaffHairdresserId, e.StaffHairMassageId, e.TeamId, e.Id, e.CustomerId })
                    .HasName("_dta_index_BillService_9_680545658__K18_K8_K21_K13_K54_K3_K9_K27_K1_K40_22_23_52_53_63");

                entity.HasIndex(e => new { e.Id, e.StaffHairdresserId, e.ServiceId, e.ProductIds, e.TotalMoney, e.CreatedDate, e.StaffHairMassageId, e.Mark, e.ServiceIds, e.Images, e.SellerId, e.ReceptionId, e.CustomerId, e.IsPayByCard, e.InProcedureTime, e.InProcedureTimeModifed, e.BookingId, e.UploadImageTime, e.CheckoutId, e.CustomerUsedNumber, e.IsDelete, e.SalonId, e.Pending, e.CompleteBillTime })
                    .HasName("IDX_20180715_158827_158826_Solution_30shinedboBillService");

                entity.Property(e => e.AppointmentTime).HasColumnType("datetime");

                entity.Property(e => e.BillCode).HasMaxLength(20);

                entity.Property(e => e.CompleteBillTime).HasColumnType("datetime");

                entity.Property(e => e.CustomerCode).HasMaxLength(50);

                entity.Property(e => e.CustomerCode1).HasMaxLength(50);

                entity.Property(e => e.CustomerConfirm).HasDefaultValueSql("((0))");

                entity.Property(e => e.ErrorNote).HasMaxLength(600);

                entity.Property(e => e.EstimateTimeCutTime).HasColumnType("datetime");

                entity.Property(e => e.FeeCod).HasColumnName("FeeCOD");

                entity.Property(e => e.FinishTime).HasColumnType("datetime");

                entity.Property(e => e.HairCutTime0).HasColumnType("datetime");

                entity.Property(e => e.HairCutTime1).HasColumnType("datetime");

                entity.Property(e => e.Hcitem)
                    .HasColumnName("HCItem")
                    .HasMaxLength(50);

                entity.Property(e => e.ImageChecked1).HasMaxLength(600);

                entity.Property(e => e.ImageChecked2).HasMaxLength(600);

                entity.Property(e => e.ImageChecked3).HasMaxLength(600);

                entity.Property(e => e.ImageChecked4).HasMaxLength(600);

                entity.Property(e => e.ImageStatusId).HasMaxLength(100);

                entity.Property(e => e.Images).HasMaxLength(600);

                entity.Property(e => e.ImagesRating).HasMaxLength(600);

                entity.Property(e => e.InProcedureTime).HasColumnType("datetime");

                entity.Property(e => e.InProcedureTimeModifed).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasColumnType("ntext");

                entity.Property(e => e.NoteByStylist).HasMaxLength(600);

                entity.Property(e => e.PdfbillCode)
                    .HasColumnName("PDFBillCode")
                    .HasMaxLength(30);

                entity.Property(e => e.ProductIds).HasColumnType("nvarchar(max)");

                entity.Property(e => e.QueueCheckoutInfo)
                    .HasMaxLength(600)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceIds).HasColumnType("nvarchar(max)");

                entity.Property(e => e.ShampooTime0).HasColumnType("datetime");

                entity.Property(e => e.ShampooTime1).HasColumnType("datetime");

                entity.Property(e => e.StaffHairMassageId).HasColumnName("Staff_HairMassage_Id");

                entity.Property(e => e.StaffHairdresserId).HasColumnName("Staff_Hairdresser_Id");

                entity.Property(e => e.StaffMakeBill).HasColumnName("Staff_MakeBill");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.UploadImageTime).HasColumnType("datetime");

                entity.Property(e => e.Vipcard).HasColumnName("VIPCard");

                entity.Property(e => e.VipcardGive)
                    .HasColumnName("VIPCardGive")
                    .HasMaxLength(10);

                entity.Property(e => e.VipcardUse)
                    .HasColumnName("VIPCardUse")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<BillWaitAtSalon>(entity =>
            {
                entity.ToTable("Bill_WaitAtSalon");

                entity.HasIndex(e => new { e.CustomerPhone, e.CreatedTime, e.IsDelete })
                    .HasName("IDX_20180718_7_6_Solution_30shinedboBill_WaitAtSalon");

                entity.HasIndex(e => new { e.IsDelete, e.CustomerPhone, e.CreatedTime })
                    .HasName("IDX_20180718_5_4_Solution_30shinedboBill_WaitAtSalon");

                entity.HasIndex(e => new { e.SalonId, e.CustomerPhone, e.CreatedTime, e.IsDelete })
                    .HasName("IDX_20180715_162385_162384_Solution_30shinedboBill_WaitAtSalon");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(100);

                entity.Property(e => e.CustomerPhone).HasMaxLength(20);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<BookHour>(entity =>
            {
                entity.HasIndex(e => new { e.HourFrame, e.Id })
                    .HasName("_dta_index_BookHour_9_1669580986__K1_10");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DatedBook).HasColumnType("datetime");

                entity.Property(e => e.Day).HasColumnType("date");

                entity.Property(e => e.Hour).HasMaxLength(20);

                entity.Property(e => e.HourFrame).HasColumnType("time(0)");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<BookHourSub>(entity =>
            {
                entity.ToTable("BookHour_Sub");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Hour)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.HourFrame).HasColumnType("time(0)");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Booking>(entity =>
            {
                entity.HasIndex(e => e.BookingTempId)
                    .HasName("IDX_20180715_240_239_Solution_30shinedboBooking");

                entity.HasIndex(e => e.CreatedDate)
                    .HasName("IDX_20180715_125923_125922_Solution_30shinedboBooking");

                entity.HasIndex(e => e.CustomerPhone)
                    .HasName("IX_Booking_Phone");

                entity.HasIndex(e => new { e.BookingTempId, e.IsDelete })
                    .HasName("IDX_20180715_78_77_Solution_30shinedboBooking");

                entity.HasIndex(e => new { e.CustomerPhone, e.DatedBook })
                    .HasName("IDX_20180715_163396_163395_Solution_30shinedboBooking");

                entity.HasIndex(e => new { e.CustomerPhone, e.IsDelete, e.DatedBook })
                    .HasName("IDX_20180715_163183_163182_Solution_30shinedboBooking");

                entity.HasIndex(e => new { e.DatedBook, e.TokenClient, e.IsDelete })
                    .HasName("IDX_20180715_164038_164037_Solution_30shinedboBooking");

                entity.HasIndex(e => new { e.CustomerPhone, e.HourId, e.DatedBook, e.IsDelete })
                    .HasName("IDX_20180715_163738_163737_Solution_30shinedboBooking");

                entity.HasIndex(e => new { e.DatedBook, e.IsBookAtSalon, e.Id, e.HourId })
                    .HasName("_dta_index_Booking_9_1637580872__K1_K4_12_21");

                entity.HasIndex(e => new { e.SalonId, e.CustomerPhone, e.IsDelete, e.DatedBook })
                    .HasName("IDX_20180715_83445_83444_Solution_30shinedboBooking");

                entity.HasIndex(e => new { e.Id, e.CustomerPhone, e.IsDelete, e.IsBookAtSalon, e.SalonId, e.DatedBook })
                    .HasName("IDX_20180718_9_8_Solution_30shinedboBooking");

                entity.HasIndex(e => new { e.Id, e.CustomerName, e.CustomerPhone, e.DatedBook, e.HourId, e.IsDelete, e.CreatedDate })
                    .HasName("IDX_20180715_89247_89246_Solution_30shinedboBooking");

                entity.HasIndex(e => new { e.Id, e.CustomerName, e.CustomerPhone, e.HourId, e.DatedBook, e.IsDelete, e.CreatedDate })
                    .HasName("IDX_20180715_89249_89248_Solution_30shinedboBooking");

                entity.HasIndex(e => new { e.Id, e.CustomerName, e.CustomerPhone, e.HourId, e.SalonId, e.DatedBook, e.IsDelete, e.CreatedDate })
                    .HasName("IDX_20180715_90077_90076_Solution_30shinedboBooking");

                entity.HasIndex(e => new { e.Id, e.HourId, e.CreatedDate, e.StylistId, e.BookingTempId, e.IsBookOnline, e.IsBookStylist, e.IsDelete, e.SalonId, e.DatedBook })
                    .HasName("IDX_20180715_163956_163955_Solution_30shinedboBooking");

                entity.HasIndex(e => new { e.Id, e.HourId, e.CreatedDate, e.StylistId, e.BookingTempId, e.IsBookStylist, e.IsDelete, e.IsBookOnline, e.SalonId, e.DatedBook })
                    .HasName("IDX_20180715_163975_163974_Solution_30shinedboBooking");

                entity.HasIndex(e => new { e.Id, e.CustomerName, e.CustomerPhone, e.HourId, e.CreatedDate, e.ModifiedDate, e.SalonId, e.StylistId, e.UserId, e.Note, e.IsMakeBill, e.FlowTimeKeepingId, e.IsSms, e.Smsstatus, e.SendDate, e.Os, e.NewCustomer, e.NoteDelete, e.IsBookAtSalon, e.Macaddress, e.IsLimited, e.TokenClient, e.BookingTempId, e.IsBookOnline, e.IsBookStylist, e.SmsnotiIsSend, e.SmsnotiSendTime, e.SmsnotiStatus, e.AuthOtpIsSet, e.AuthOtpIsVerified, e.AuthOtpToken, e.AuthOtpTimeSend, e.AuthOtpTimeVerified, e.AuthOtpSmsstatus, e.AuthOtpTimeReceived, e.TheadId, e.Version, e.SuggestSalonId, e.CancelBookIsSendSms, e.CancelBookTimeSend, e.CancelBookSmsstatus, e.CancelBookFrom, e.IsAutoStylist, e.IsSetAutoStylist, e.TextNote1, e.TextNote2, e.SalonNote, e.TimeSalonNote, e.BookingStatusId, e.Uid, e.MigrateStatus, e.IsArrived, e.DatedBook, e.IsDelete })
                    .HasName("IDX_20180715_163740_163739_Solution_30shinedboBooking");

                entity.Property(e => e.AuthOtpIsSet).HasColumnName("AuthOTP_IsSet");

                entity.Property(e => e.AuthOtpIsVerified).HasColumnName("AuthOTP_IsVerified");

                entity.Property(e => e.AuthOtpSmsstatus).HasColumnName("AuthOTP_SMSStatus");

                entity.Property(e => e.AuthOtpTimeReceived)
                    .HasColumnName("AuthOTP_TimeReceived")
                    .HasColumnType("datetime");

                entity.Property(e => e.AuthOtpTimeSend)
                    .HasColumnName("AuthOTP_TimeSend")
                    .HasColumnType("datetime");

                entity.Property(e => e.AuthOtpTimeVerified)
                    .HasColumnName("AuthOTP_TimeVerified")
                    .HasColumnType("datetime");

                entity.Property(e => e.AuthOtpToken)
                    .HasColumnName("AuthOTP_Token")
                    .HasMaxLength(8);

                entity.Property(e => e.CancelBookFrom).HasColumnName("CancelBook_From");

                entity.Property(e => e.CancelBookIsSendSms).HasColumnName("CancelBook_IsSendSMS");

                entity.Property(e => e.CancelBookSmsstatus).HasColumnName("CancelBook_SMSStatus");

                entity.Property(e => e.CancelBookTimeSend)
                    .HasColumnName("CancelBook_TimeSend")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(200);

                entity.Property(e => e.CustomerPhone).HasMaxLength(20);

                entity.Property(e => e.CustomerScore).HasDefaultValueSql("((0))");

                entity.Property(e => e.DatedBook).HasColumnType("datetime");

                entity.Property(e => e.IsCallTime).HasColumnType("datetime");

                entity.Property(e => e.IsCallTimeModified).HasColumnType("datetime");

                entity.Property(e => e.IsSms).HasColumnName("IsSMS");

                entity.Property(e => e.Macaddress)
                    .HasColumnName("MACAddress")
                    .HasMaxLength(100);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Note).HasColumnType("nvarchar(max)");

                entity.Property(e => e.NoteDelete).HasMaxLength(300);

                entity.Property(e => e.Os).HasColumnName("OS");

                entity.Property(e => e.SalonNote).HasMaxLength(300);

                entity.Property(e => e.SendDate).HasColumnType("datetime");

                entity.Property(e => e.SmsnotiIsSend).HasColumnName("SMSNoti_IsSend");

                entity.Property(e => e.SmsnotiSendTime)
                    .HasColumnName("SMSNoti_SendTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SmsnotiStatus).HasColumnName("SMSNoti_Status");

                entity.Property(e => e.Smsstatus).HasColumnName("SMSStatus");

                entity.Property(e => e.SuggestSalonId).HasColumnName("SuggestSalonID");

                entity.Property(e => e.TimeSalonNote).HasColumnType("datetime");

                entity.Property(e => e.TokenClient).HasMaxLength(100);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Version).HasMaxLength(50);
            });

            modelBuilder.Entity<BookingChangeStylist>(entity =>
            {
                entity.HasIndex(e => e.BookingTmpId)
                    .HasName("IDX_20180715_163881_163880_Solution_30shinedboBookingChangeStylist");

                entity.HasIndex(e => new { e.BookingTmpId, e.CustomerPhone })
                    .HasName("IDX_20180715_163883_163882_Solution_30shinedboBookingChangeStylist");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.CustomerPhone).HasMaxLength(50);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<BookingSalonBackup>(entity =>
            {
                entity.ToTable("Booking_SalonBackup");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<BookingSpecialOptionTick>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ConfigKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<BookingStatisticSlot>(entity =>
            {
                entity.ToTable("Booking_Statistic_Slot");

                entity.HasIndex(e => new { e.Slot, e.SalonId, e.IsDelete, e.WorkDate })
                    .HasName("IDX_20180718_4370_4369_Solution_30shinedboBooking_Statistic_Slot");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.SalonId).HasColumnName("SalonID");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.WorkDate).HasColumnType("date");
            });

            modelBuilder.Entity<BookingStatus>(entity =>
            {
                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DeleteTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1970-1-1')");

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1970-1-1')");
            });

            modelBuilder.Entity<BookingStylistBackup>(entity =>
            {
                entity.ToTable("Booking_StylistBackup");

                entity.HasIndex(e => e.StylistId)
                    .HasName("IX_StylistBK")
                    .IsUnique();

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<BookingSuggest>(entity =>
            {
                entity.ToTable("Booking_Suggest");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(200);

                entity.Property(e => e.CustomerPhone).HasMaxLength(20);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<BookingSuggestSalonConfig>(entity =>
            {
                entity.ToTable("Booking_Suggest_SalonConfig");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<BookingSwitchVersion>(entity =>
            {
                entity.ToTable("Booking_Switch_Version");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.TargetVersion).HasMaxLength(200);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Version).HasMaxLength(20);
            });

            modelBuilder.Entity<BookingTest>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(200);

                entity.Property(e => e.CustomerPhone).HasMaxLength(20);

                entity.Property(e => e.DatedBook).HasColumnType("datetime");

                entity.Property(e => e.IsSms).HasColumnName("IsSMS");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NoteDelete).HasMaxLength(300);

                entity.Property(e => e.Os).HasColumnName("OS");

                entity.Property(e => e.SendDate).HasColumnType("datetime");

                entity.Property(e => e.Smsstatus).HasColumnName("SMSStatus");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Brands>(entity =>
            {
                entity.Property(e => e.Images).HasMaxLength(250);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Mota).HasColumnType("ntext");

                entity.Property(e => e.TenThuongHieu).HasMaxLength(250);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.XuatXu).HasMaxLength(250);
            });

            modelBuilder.Entity<Call>(entity =>
            {
                entity.HasIndex(e => e.Receiver)
                    .HasName("IDX_20180715_110186_110185_Solution_30shinedboCall");

                entity.HasIndex(e => e.Sender)
                    .HasName("IDX_20180715_110289_110288_Solution_30shinedboCall");

                entity.HasIndex(e => new { e.IsAutoGenerate, e.CreateTime })
                    .HasName("IDX_20180719_5033_5032_Solution_30shinedboCall");

                entity.HasIndex(e => new { e.Sender, e.StartTime })
                    .HasName("IDX_20180715_110339_110338_Solution_30shinedboCall");

                entity.HasIndex(e => new { e.Id, e.Receiver, e.ReceiverId, e.Status, e.IsDelete, e.SalonId, e.IsAutoGenerate, e.StartTime })
                    .HasName("IDX_20180715_18067_18066_Solution_30shinedboCall");

                entity.HasIndex(e => new { e.Id, e.CreateTime, e.DeleteTime, e.EndTime, e.Receiver, e.SenderId, e.ReceiverId, e.Seq, e.Sender, e.StartTime, e.Status, e.UpdateTime, e.Length, e.IsDelete, e.Uid, e.MigrateStatus, e.Salon, e.IsAutoGenerate, e.SalonId })
                    .HasName("IDX_20180715_110678_110677_Solution_30shinedboCall");

                entity.HasIndex(e => new { e.Id, e.CreateTime, e.DeleteTime, e.EndTime, e.Receiver, e.SenderId, e.ReceiverId, e.Seq, e.Sender, e.Status, e.UpdateTime, e.Length, e.IsDelete, e.Uid, e.MigrateStatus, e.Salon, e.IsAutoGenerate, e.SalonId, e.StartTime })
                    .HasName("IDX_20180715_110686_110685_Solution_30shinedboCall");

                entity.HasIndex(e => new { e.Id, e.CreateTime, e.DeleteTime, e.EndTime, e.Receiver, e.SenderId, e.ReceiverId, e.Seq, e.Sender, e.Status, e.UpdateTime, e.Length, e.IsDelete, e.Uid, e.MigrateStatus, e.Salon, e.SalonId, e.IsAutoGenerate, e.StartTime })
                    .HasName("IDX_20180715_560_559_Solution_30shinedboCall");

                entity.HasIndex(e => new { e.Id, e.CreateTime, e.DeleteTime, e.EndTime, e.Receiver, e.SenderId, e.ReceiverId, e.Seq, e.Sender, e.UpdateTime, e.Length, e.IsDelete, e.Uid, e.MigrateStatus, e.Salon, e.IsAutoGenerate, e.SalonId, e.StartTime, e.Status })
                    .HasName("IDX_20180715_98_97_Solution_30shinedboCall");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DeleteTime).HasColumnType("datetime");

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.Length).HasDefaultValueSql("((1))");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.Receiver)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Salon).HasMaxLength(50);

                entity.Property(e => e.Sender)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Seq).HasDefaultValueSql("((0))");

                entity.Property(e => e.StartTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1970-1-1')");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1970-1-1')");
            });

            modelBuilder.Entity<CallBackRequest>(entity =>
            {
                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerName).HasMaxLength(250);

                entity.Property(e => e.CustomerPhone)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(50);
            });

            modelBuilder.Entity<CheckinCheckout>(entity =>
            {
                entity.HasIndex(e => new { e.IsDelete, e.UniqueKey })
                    .HasName("IDX_20180718_154_153_Solution_30shinedboCheckinCheckout");

                entity.HasIndex(e => new { e.SalonId, e.FlowTimeKeepingId, e.IsComeLateConfirm, e.IsLeaveEarlyConfirm, e.IsLeaveEarly, e.IsComeLate, e.StaffId, e.IsDelete, e.WorkDate })
                    .HasName("IDX_20180718_1791_1790_Solution_30shinedboCheckinCheckout");

                entity.HasIndex(e => new { e.SalonId, e.StaffId, e.FlowTimeKeepingId, e.IsComeLateConfirm, e.IsLeaveEarlyConfirm, e.IsLeaveEarly, e.IsComeLate, e.IsDelete, e.WorkDate })
                    .HasName("IDX_20180715_4026_4025_Solution_30shinedboCheckinCheckout");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.StaffId, e.FlowTimeKeepingId, e.IsComeLateConfirm, e.IsLeaveEarlyConfirm, e.IsLeaveEarly, e.IsComeLate, e.WorkDate, e.IsDelete })
                    .HasName("IDX_20180715_1106_1105_Solution_30shinedboCheckinCheckout");

                entity.HasIndex(e => new { e.CheckinTime, e.CheckoutTime, e.SalonId, e.FlowTimeKeepingId, e.UniqueKey, e.Uid, e.MigrateStatus, e.IsComeLateConfirm, e.IsLeaveEarlyConfirm, e.IsLeaveEarly, e.IsComeLate, e.IsDelete, e.StaffId, e.WorkDate, e.Id })
                    .HasName("_dta_index_CheckinCheckout_9_166013276__K8_K5_K7_K1_2_3_4_6_9_10_11_12_13_14_15");

                entity.Property(e => e.CheckinTime).HasColumnType("datetime");

                entity.Property(e => e.CheckoutTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.UniqueKey).HasMaxLength(300);

                entity.Property(e => e.WorkDate).HasColumnType("date");
            });

            modelBuilder.Entity<ConfigMembership>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ConfigPartTime>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ConfigQuantifyProduct>(entity =>
            {
                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DepartmentId).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsPublish).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.ProductId).HasDefaultValueSql("((0))");

                entity.Property(e => e.Quantify).HasDefaultValueSql("((0))");

                entity.Property(e => e.ServiceId).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalNumberService).HasDefaultValueSql("((0))");

                entity.Property(e => e.Volume).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<Contracts>(entity =>
            {
                entity.Property(e => e.Content)
                    .HasColumnType("ntext")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DepartmentId).HasDefaultValueSql("((0))");

                entity.Property(e => e.DescriptionContract)
                    .HasColumnType("ntext")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasMaxLength(150)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.PathContract)
                    .HasMaxLength(500)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TypeContract).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<CrmVoucherWaitTime>(entity =>
            {
                entity.ToTable("CRM_VoucherWaitTime");

                entity.HasIndex(e => new { e.SkinnerId, e.CreatedTime })
                    .HasName("IDX_20180719_5066_5065_Solution_30shinedboCRM_VoucherWaitTime");

                entity.HasIndex(e => new { e.CustomerId, e.IsUsed, e.IsDelete })
                    .HasName("IDX_20180718_17_16_Solution_30shinedboCRM_VoucherWaitTime");

                entity.HasIndex(e => new { e.SkinnerId, e.IsDelete, e.CreatedTime })
                    .HasName("IDX_20180719_5035_5034_Solution_30shinedboCRM_VoucherWaitTime");

                entity.HasIndex(e => new { e.SkinnerId, e.SalonId, e.CreatedTime })
                    .HasName("IDX_20180719_5070_5069_Solution_30shinedboCRM_VoucherWaitTime");

                entity.HasIndex(e => new { e.SalonId, e.CheckinMoney, e.SkinnerMoney, e.StylistMoney, e.IsDelete, e.CreatedTime })
                    .HasName("IDX_20180719_5027_5026_Solution_30shinedboCRM_VoucherWaitTime");

                entity.HasIndex(e => new { e.Id, e.BillId, e.CustomerId, e.CheckinId, e.StylistId, e.SkinnerId, e.SalonId, e.VoucherPercent, e.ModifiedTime, e.CheckinPoint, e.CheckinMoney, e.SkinnerPoint, e.SkinnerMoney, e.StylistPoint, e.StylistMoney, e.Uid, e.MigrateStatus, e.IsUsed, e.IsDelete, e.CreatedTime })
                    .HasName("IDX_20180715_162114_162113_Solution_30shinedboCRM_VoucherWaitTime");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BillId).HasColumnName("BillID");

                entity.Property(e => e.CheckinId).HasColumnName("CheckinID");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.SalonId).HasColumnName("SalonID");

                entity.Property(e => e.SkinnerId).HasColumnName("SkinnerID");

                entity.Property(e => e.StylistId).HasColumnName("StylistID");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasIndex(e => e.Phone)
                    .HasName("idx_Phone2")
                    .IsUnique();

                entity.HasIndex(e => new { e.Fullname, e.Phone, e.Id })
                    .HasName("_dta_index_Customer_9_20075053__K1_2_3");

                entity.HasIndex(e => new { e.Fullname, e.Phone, e.Id, e.IsDelete })
                    .HasName("_dta_index_Customer_9_20075053__K3_K1_K16_2");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.Phone, e.IsDelete })
                    .HasName("IDX_20180717_629_628_Solution_30shinedboCustomer");

                entity.Property(e => e.AccessToken).HasMaxLength(250);

                entity.Property(e => e.Address).HasColumnType("ntext");

                entity.Property(e => e.CheckCode).HasMaxLength(10);

                entity.Property(e => e.CustomerCode)
                    .HasColumnName("Customer_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.CustomerCode1)
                    .HasColumnName("Customer_Code1")
                    .HasMaxLength(50);

                entity.Property(e => e.DateLastBillService).HasColumnType("date");

                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.FacebookId).HasMaxLength(150);

                entity.Property(e => e.FingerToken).HasMaxLength(20);

                entity.Property(e => e.Fullname).HasMaxLength(200);

                entity.Property(e => e.GoogleId).HasMaxLength(150);

                entity.Property(e => e.InfoFlow).HasColumnName("Info_Flow");

                entity.Property(e => e.IsNoInfor).HasColumnName("isNoInfor");

                entity.Property(e => e.MemberEndTime).HasColumnType("datetime");

                entity.Property(e => e.MemberStartTime).HasColumnType("datetime");

                entity.Property(e => e.MemberType).HasDefaultValueSql("((0))");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Occupation).HasMaxLength(30);

                entity.Property(e => e.Otp)
                    .HasColumnName("OTP")
                    .HasMaxLength(10);

                entity.Property(e => e.Password).HasMaxLength(50);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.Property(e => e.Score).HasDefaultValueSql("((0))");

                entity.Property(e => e.SnDay).HasColumnName("SN_day");

                entity.Property(e => e.SnMonth).HasColumnName("SN_month");

                entity.Property(e => e.SnYear).HasColumnName("SN_year");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<CustomerBookingCheck>(entity =>
            {
                entity.ToTable("Customer_Booking_Check");

                entity.HasIndex(e => new { e.BookingId, e.SalonId, e.IsCall, e.CreatedTime })
                    .HasName("IDX_20180718_1784_1783_Solution_30shinedboCustomer_Booking_Check");

                entity.HasIndex(e => new { e.BookingId, e.SalonId, e.IsCall, e.Status })
                    .HasName("IDX_20180718_1776_1775_Solution_30shinedboCustomer_Booking_Check");

                entity.HasIndex(e => new { e.SalonId, e.BookingId, e.IsCall, e.CreatedTime })
                    .HasName("IDX_20180718_1782_1781_Solution_30shinedboCustomer_Booking_Check");

                entity.HasIndex(e => new { e.SalonId, e.BookingId, e.IsCall, e.Status })
                    .HasName("IDX_20180718_1778_1777_Solution_30shinedboCustomer_Booking_Check");

                entity.HasIndex(e => new { e.Id, e.BookingId, e.CustomerId, e.IsCall, e.Status, e.CreatedTime, e.ModifiedTime, e.Note, e.Uid, e.MigrateStatus, e.SalonId, e.WorkDate })
                    .HasName("IDX_20180719_4997_4996_Solution_30shinedboCustomer_Booking_Check");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BookingId).HasColumnName("BookingID");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.SalonId).HasColumnName("SalonID");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.WorkDate).HasColumnType("date");
            });

            modelBuilder.Entity<CustomerHairAttribute>(entity =>
            {
                entity.Property(e => e.AnMac)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ChatToc)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerId).HasDefaultValueSql("((0))");

                entity.Property(e => e.DangCam)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DoCung)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.GhiChuKhac)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.KhuonMat)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.NgheNghiep)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Note).HasMaxLength(1000);

                entity.Property(e => e.ThoiQuenSay)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ThoiQuenVuotSap)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<CustomerHairModeBill>(entity =>
            {
                entity.ToTable("Customer_HairMode_Bill");

                entity.HasIndex(e => new { e.BillId, e.IsDelete })
                    .HasName("IDX_20180715_162802_162801_Solution_30shinedboCustomer_HairMode_Bill");

                entity.HasIndex(e => new { e.CustomerId, e.BillId })
                    .HasName("IDX_20180715_163129_163128_Solution_30shinedboCustomer_HairMode_Bill");

                entity.HasIndex(e => new { e.BillId, e.IsDelete, e.HairStyleId })
                    .HasName("IDX_20180718_132_131_Solution_30shinedboCustomer_HairMode_Bill");

                entity.HasIndex(e => new { e.BillId, e.CreateDate, e.IsDelete, e.HairStyleId })
                    .HasName("IDX_20180718_136_135_Solution_30shinedboCustomer_HairMode_Bill");

                entity.HasIndex(e => new { e.HairStyleId, e.BillId, e.CreateDate, e.IsDelete })
                    .HasName("IDX_20180715_162975_162974_Solution_30shinedboCustomer_HairMode_Bill");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiledDate).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(300);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<CustomerHairSkin>(entity =>
            {
                entity.ToTable("Customer_Hair_Skin");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.HairAttIds)
                    .HasColumnName("Hair_Att_Ids")
                    .HasMaxLength(250);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.SkinAttIds)
                    .HasColumnName("Skin_Att_Ids")
                    .HasMaxLength(250);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<CustomerHairSkinHistory>(entity =>
            {
                entity.ToTable("Customer_Hair_Skin_History");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.HairAttIds)
                    .HasColumnName("Hair_Att_Ids")
                    .HasMaxLength(250);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.SkinAttIds)
                    .HasColumnName("Skin_Att_Ids")
                    .HasMaxLength(250);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<CustomerRating>(entity =>
            {
                entity.ToTable("Customer_Rating");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(150);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<CustomerRatingUuDai>(entity =>
            {
                entity.ToTable("Customer_Rating_UuDai");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<CustomerType>(entity =>
            {
                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.TypeName).HasMaxLength(300);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<CustomerUuDai>(entity =>
            {
                entity.ToTable("Customer_UuDai");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(600);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(150);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<DcustomerAtSalon>(entity =>
            {
                entity.HasKey(e => new { e.IdCustomerAtSalon, e.BillId });

                entity.ToTable("DCustomerAtSalon");

                entity.Property(e => e.IdCustomerAtSalon).ValueGeneratedOnAdd();

                entity.Property(e => e.DateAtSalon).HasColumnType("datetime");

                entity.Property(e => e.RatingScore).HasDefaultValueSql("((0))");

                entity.Property(e => e.ScoreScscbill).HasColumnName("ScoreSCSCBill");

                entity.Property(e => e.TimeArrived).HasColumnType("datetime");

                entity.Property(e => e.TimeCompleteBill).HasColumnType("datetime");

                entity.Property(e => e.TimeHairDresser).HasColumnType("datetime");

                entity.Property(e => e.TimeHairMassage).HasColumnType("datetime");

                entity.Property(e => e.TimePrintBill).HasColumnType("datetime");

                entity.Property(e => e.TimeUploadTakePhoto).HasColumnType("datetime");

                entity.Property(e => e.TotalMonney).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<DcustomerCall>(entity =>
            {
                entity.HasKey(e => e.IdCustomerCall);

                entity.ToTable("DCustomerCall");

                entity.Property(e => e.DatetimeCall).HasColumnType("datetime");

                entity.Property(e => e.DeviceImei)
                    .HasColumnName("DeviceIMEI")
                    .HasMaxLength(200);

                entity.Property(e => e.EndCallTime).HasColumnType("datetime");

                entity.Property(e => e.NoteOfCall).HasMaxLength(250);

                entity.Property(e => e.StartTimeCall).HasColumnType("datetime");
            });

            modelBuilder.Entity<DcustomerInfo>(entity =>
            {
                entity.HasKey(e => new { e.CustomerId, e.CustomerPhone });

                entity.ToTable("DCustomerInfo");

                entity.Property(e => e.CustomerPhone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerBirthday).HasColumnType("date");

                entity.Property(e => e.CustomerGender).HasDefaultValueSql("((1))");

                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.CustomerScore).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<DcustomerProduct>(entity =>
            {
                entity.HasKey(e => e.IdCustomerProduct);

                entity.ToTable("DCustomerProduct");

                entity.Property(e => e.DateOfBill).HasColumnType("datetime");

                entity.Property(e => e.ProductPrice).HasDefaultValueSql("((0))");

                entity.Property(e => e.ProductQuantity).HasDefaultValueSql("((0))");

                entity.Property(e => e.VoucherPercent).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<DcustomerProductTotal>(entity =>
            {
                entity.ToTable("DCustomerProductTotal");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateLastOfBill).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<DcustomerScore>(entity =>
            {
                entity.HasKey(e => new { e.IdCustomerScore, e.BillId });

                entity.ToTable("DCustomerScore");

                entity.Property(e => e.IdCustomerScore).ValueGeneratedOnAdd();

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DateOfBill).HasColumnType("datetime");
            });

            modelBuilder.Entity<DcustomerService>(entity =>
            {
                entity.HasKey(e => e.IdCustomerUseService);

                entity.ToTable("DCustomerService");

                entity.Property(e => e.DateOfBill).HasColumnType("datetime");

                entity.Property(e => e.ServicePrice).HasDefaultValueSql("((0))");

                entity.Property(e => e.VoucherPercent).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<DcustomerServiceTotal>(entity =>
            {
                entity.ToTable("DCustomerServiceTotal");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateLastOfBill).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<DcustomerSms>(entity =>
            {
                entity.HasKey(e => e.IdCustomerSms);

                entity.ToTable("DCustomerSMS");

                entity.Property(e => e.IdCustomerSms).HasColumnName("IdCustomerSMS");

                entity.Property(e => e.DateTimeReceived).HasColumnType("datetime");

                entity.Property(e => e.DateTimeSend).HasColumnType("datetime");

                entity.Property(e => e.DateTimeSms)
                    .HasColumnName("DateTimeSMS")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeviceImei)
                    .HasColumnName("DeviceIMEI")
                    .HasMaxLength(200);

                entity.Property(e => e.Smsid).HasColumnName("SMSId");

                entity.Property(e => e.TypeOfSmsid).HasColumnName("TypeOfSMSId");
            });

            modelBuilder.Entity<DcustomerTeamService>(entity =>
            {
                entity.HasKey(e => e.BillId);

                entity.ToTable("DCustomerTeamService");

                entity.Property(e => e.BillId).ValueGeneratedNever();

                entity.Property(e => e.DateOfBill).HasColumnType("datetime");

                entity.Property(e => e.IdCustomerTeamService).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Device>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DeviceName).HasMaxLength(150);

                entity.Property(e => e.ImeiOrMacIp)
                    .HasColumnName("ImeiOrMacIP")
                    .HasMaxLength(300);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.UniqueKey).HasMaxLength(300);
            });

            modelBuilder.Entity<DstylistScore>(entity =>
            {
                entity.ToTable("DStylistScore");

                entity.Property(e => e.DstylistScoreId).HasColumnName("DStylistScoreId");

                entity.Property(e => e.AvgScsc).HasColumnName("AvgSCSC");

                entity.Property(e => e.LastDateUpdate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ErpCheck3S>(entity =>
            {
                entity.ToTable("ERP.Check3S");

                entity.Property(e => e.Comment).HasMaxLength(250);

                entity.Property(e => e.DateTime).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.Image).HasColumnType("text");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ErpCheckCsvc>(entity =>
            {
                entity.ToTable("ERP.CheckCSVC");

                entity.Property(e => e.Comment).HasMaxLength(250);

                entity.Property(e => e.ConfirmCreateDate).HasColumnType("datetime");

                entity.Property(e => e.DateTime).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.DoingCreateDate).HasColumnType("datetime");

                entity.Property(e => e.FixDate).HasColumnType("datetime");

                entity.Property(e => e.Image).HasColumnType("text");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ErpItemCheck>(entity =>
            {
                entity.ToTable("ERP.ItemCheck");

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.Icon).HasMaxLength(250);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ErrorCutHair>(entity =>
            {
                entity.Property(e => e.ErrorCut).HasMaxLength(200);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ErrorCutReason>(entity =>
            {
                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Reason).HasMaxLength(400);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ExportGoods>(entity =>
            {
                entity.HasIndex(e => new { e.Level, e.SalonId, e.RecipientId, e.IsDelete })
                    .HasName("IDX_20180718_3775_3774_Solution_30shinedboExportGoods");

                entity.HasIndex(e => new { e.Id, e.ExportId, e.RecipientId, e.GoodsIds, e.Note, e.ModifiedDate, e.Uid, e.MigrateStatus, e.Level, e.SalonId, e.CreatedDate, e.IsDelete })
                    .HasName("IDX_20180718_3777_3776_Solution_30shinedboExportGoods");

                entity.Property(e => e.GoodsIds)
                    .IsRequired()
                    .HasColumnType("nvarchar(max)");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasColumnType("nvarchar(max)");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<FeedbackPointService>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FeedbackQuantity).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.SalonId).HasDefaultValueSql("((0))");

                entity.Property(e => e.WorkDate).HasColumnType("date");
            });

            modelBuilder.Entity<FlowGoods>(entity =>
            {
                entity.HasIndex(e => new { e.ExportGoodsId, e.GoodsId, e.Cost, e.Price, e.Quantity, e.IsDelete })
                    .HasName("IDX_20180720_5103_5102_Solution_30shinedboFlowGoods");

                entity.HasIndex(e => new { e.GoodsId, e.Cost, e.Price, e.Quantity, e.IsDelete, e.ExportGoodsId })
                    .HasName("IDX_20180720_5101_5100_Solution_30shinedboFlowGoods");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<FlowProduct>(entity =>
            {
                entity.HasIndex(e => e.BillId)
                    .HasName("idx_BillId");

                entity.HasIndex(e => new { e.BillId, e.IsDelete, e.CreatedDate })
                    .HasName("IDX_20180715_83448_83447_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.BillId, e.ProductId, e.VoucherPercent })
                    .HasName("IDX_20180720_5121_5120_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.BillId, e.VoucherPercent, e.ProductId })
                    .HasName("IDX_20180720_5124_5123_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.ProductId, e.CreatedDate, e.IsDelete })
                    .HasName("IDX_20180715_1299_1298_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.BillId, e.IsDelete, e.CreatedDate, e.VoucherPercent })
                    .HasName("IDX_20180715_83435_83434_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.ProductId, e.IsDelete, e.SalonId, e.SellerId, e.CreatedDate })
                    .HasName("IDX_20180715_47969_47968_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.ProductId, e.SalonId, e.IsDelete, e.SellerId, e.CreatedDate })
                    .HasName("IDX_20180715_47967_47966_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.BillId, e.Price, e.Quantity, e.VoucherPercent, e.IsDelete, e.CreatedDate })
                    .HasName("IDX_20180715_83359_83358_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.Price, e.ProductId, e.IsDelete, e.SalonId, e.SellerId, e.CreatedDate })
                    .HasName("IDX_20180715_47973_47972_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.ProductId, e.Quantity, e.SalonId, e.IsDelete, e.SellerId, e.CreatedDate })
                    .HasName("IDX_20180715_47964_47963_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.VoucherPercent, e.ProductId, e.IsDelete, e.SalonId, e.SellerId, e.CreatedDate })
                    .HasName("IDX_20180715_47975_47974_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.BillId, e.ProductId, e.Quantity, e.SalonId, e.SalonOrder, e.IsDelete, e.CheckCombo })
                    .HasName("IDX_20180715_68714_68713_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.BillId, e.ProductId, e.Price, e.Quantity, e.VoucherPercent, e.IsDelete, e.ComboId, e.CreatedDate })
                    .HasName("IDX_20180715_7108_7107_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.PromotionMoney, e.IsDelete, e.ComboId, e.ProductId, e.BillId, e.Price, e.Quantity, e.VoucherPercent })
                    .HasName("_dta_index_FlowProduct_9_501576825__K7_K15_K3_K2_K4_K5_K11_12");

                entity.HasIndex(e => new { e.Id, e.BillId, e.Price, e.Quantity, e.ModifiedDate, e.SalonId, e.SellerId, e.VoucherPercent, e.PromotionMoney, e.Cost, e.ForSalary, e.ComboId, e.CheckCombo, e.SalonOrder, e.Uid, e.MigrateStatus, e.IsDelete, e.ProductId, e.CreatedDate })
                    .HasName("IDX_20180715_115763_115762_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.Id, e.BillId, e.ProductId, e.Price, e.Quantity, e.ModifiedDate, e.SalonId, e.SellerId, e.VoucherPercent, e.PromotionMoney, e.Cost, e.ForSalary, e.CheckCombo, e.SalonOrder, e.Uid, e.MigrateStatus, e.IsDelete, e.ComboId, e.CreatedDate })
                    .HasName("IDX_20180715_80036_80035_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.Id, e.BillId, e.ProductId, e.Price, e.Quantity, e.ModifiedDate, e.SalonId, e.SellerId, e.VoucherPercent, e.PromotionMoney, e.Cost, e.ForSalary, e.ComboId, e.CheckCombo, e.SalonOrder, e.Uid, e.MigrateStatus, e.IsDelete, e.CreatedDate })
                    .HasName("IDX_20180715_83944_83943_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.Id, e.BillId, e.ProductId, e.Price, e.Quantity, e.ModifiedDate, e.SellerId, e.VoucherPercent, e.PromotionMoney, e.Cost, e.ForSalary, e.CheckCombo, e.SalonOrder, e.Uid, e.MigrateStatus, e.IsDelete, e.ComboId, e.CreatedDate, e.SalonId })
                    .HasName("IDX_20180715_4181_4180_Solution_30shinedboFlowProduct");

                entity.HasIndex(e => new { e.Id, e.BillId, e.ProductId, e.Price, e.Quantity, e.ModifiedDate, e.SellerId, e.VoucherPercent, e.PromotionMoney, e.Cost, e.ForSalary, e.CheckCombo, e.SalonOrder, e.Uid, e.MigrateStatus, e.IsDelete, e.SalonId, e.ComboId, e.CreatedDate })
                    .HasName("IDX_20180715_19066_19065_Solution_30shinedboFlowProduct");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<FlowPromotion>(entity =>
            {
                entity.HasIndex(e => new { e.BillId, e.IsDelete })
                    .HasName("IDX_20180715_571_570_Solution_30shinedboFlowPromotion");

                entity.HasIndex(e => new { e.BillId, e.Group, e.IsDelete })
                    .HasName("IDX_20180715_3077_3076_Solution_30shinedboFlowPromotion");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<FlowSalary>(entity =>
            {
                entity.HasIndex(e => e.SDate);

                entity.HasIndex(e => e.StaffId)
                    .HasName("IX_FlowSalary_Staffid");

                entity.HasIndex(e => new { e.StaffId, e.RatingPoint, e.BillNormal, e.BillNormalGreat, e.BillNormalGood, e.BillNormalBad, e.BillNormalNorating, e.BillSpecial, e.BillSpecialGreat, e.BillSpecialGood, e.BillSpecialBad, e.BillSpecialNorating, e.MistakePoint, e.SDate })
                    .HasName("IDX_20180715_76110_76109_Solution_30shinedboFlowSalary");

                entity.Property(e => e.AllowanceSalary).HasColumnName("allowanceSalary");

                entity.Property(e => e.BillNormal).HasColumnName("bill_normal");

                entity.Property(e => e.BillNormalBad).HasColumnName("bill_normal_bad");

                entity.Property(e => e.BillNormalGood).HasColumnName("bill_normal_good");

                entity.Property(e => e.BillNormalGreat).HasColumnName("bill_normal_great");

                entity.Property(e => e.BillNormalNorating).HasColumnName("bill_normal_norating");

                entity.Property(e => e.BillSpecial).HasColumnName("bill_special");

                entity.Property(e => e.BillSpecialBad).HasColumnName("bill_special_bad");

                entity.Property(e => e.BillSpecialGood).HasColumnName("bill_special_good");

                entity.Property(e => e.BillSpecialGreat).HasColumnName("bill_special_great");

                entity.Property(e => e.BillSpecialNorating).HasColumnName("bill_special_norating");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FixSalary).HasColumnName("fixSalary");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.MistakePoint).HasColumnName("Mistake_Point");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PartTimeSalary).HasColumnName("partTimeSalary");

                entity.Property(e => e.ProductSalary).HasColumnName("productSalary");

                entity.Property(e => e.RatingPoint).HasColumnName("ratingPoint");

                entity.Property(e => e.SDate)
                    .HasColumnName("sDate")
                    .HasColumnType("date");

                entity.Property(e => e.ServiceSalary).HasColumnName("serviceSalary");

                entity.Property(e => e.StaffId).HasColumnName("staffId");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.WaitTimeMistake).HasColumnName("waitTimeMistake");
            });

            modelBuilder.Entity<FlowService>(entity =>
            {
                entity.HasIndex(e => e.BillId)
                    .HasName("idx_BillId");

                entity.HasIndex(e => new { e.BillId, e.IsDelete, e.CreatedDate })
                    .HasName("IDX_20180715_84356_84355_Solution_30shinedboFlowService");

                entity.HasIndex(e => new { e.ServiceId, e.CreatedDate, e.IsDelete })
                    .HasName("IDX_20180715_1295_1294_Solution_30shinedboFlowService");

                entity.HasIndex(e => new { e.BillId, e.Price, e.Quantity, e.IsDelete, e.CreatedDate })
                    .HasName("IDX_20180715_83470_83469_Solution_30shinedboFlowService");

                entity.HasIndex(e => new { e.BillId, e.IsDelete, e.ServiceId, e.Price, e.Quantity, e.VoucherPercent })
                    .HasName("_dta_index_FlowService_9_533576939__K2_K6_K3_K4_K7_K11");

                entity.HasIndex(e => new { e.BillId, e.Price, e.Quantity, e.VoucherPercent, e.IsDelete, e.CreatedDate })
                    .HasName("IDX_20180715_7106_7105_Solution_30shinedboFlowService");

                entity.HasIndex(e => new { e.BillId, e.Price, e.Quantity, e.VoucherPercent, e.IsDelete, e.ServiceId })
                    .HasName("IDX_20180715_3450_3449_Solution_30shinedboFlowService");

                entity.HasIndex(e => new { e.Id, e.BillId, e.Price, e.CreatedDate, e.IsDelete, e.Quantity, e.ModifiedDate, e.SalonId, e.SellerId, e.VoucherPercent, e.CoefficientRating, e.Uid, e.MigrateStatus, e.ServiceId })
                    .HasName("IDX_20180720_5129_5128_Solution_30shinedboFlowService");

                entity.HasIndex(e => new { e.Id, e.BillId, e.ServiceId, e.Price, e.Quantity, e.ModifiedDate, e.SalonId, e.SellerId, e.VoucherPercent, e.CoefficientRating, e.Uid, e.MigrateStatus, e.IsDelete, e.CreatedDate })
                    .HasName("IDX_20180715_80038_80037_Solution_30shinedboFlowService");

                entity.HasIndex(e => new { e.Id, e.BillId, e.ServiceId, e.Price, e.Quantity, e.ModifiedDate, e.SellerId, e.VoucherPercent, e.CoefficientRating, e.Uid, e.MigrateStatus, e.IsDelete, e.CreatedDate, e.SalonId })
                    .HasName("IDX_20180715_4179_4178_Solution_30shinedboFlowService");

                entity.HasIndex(e => new { e.Id, e.BillId, e.ServiceId, e.Price, e.Quantity, e.ModifiedDate, e.SellerId, e.VoucherPercent, e.CoefficientRating, e.Uid, e.MigrateStatus, e.IsDelete, e.SalonId, e.CreatedDate })
                    .HasName("IDX_20180715_19064_19063_Solution_30shinedboFlowService");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<FlowStaff>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<FlowTimeKeeping>(entity =>
            {
                entity.HasIndex(e => e.IsDelete)
                    .HasName("IX_FlowTimeKeeping_isdelete");

                entity.HasIndex(e => e.StaffId);

                entity.HasIndex(e => e.WorkDate)
                    .HasName("IX_FlowTimeKeeping_Workdate");

                entity.HasIndex(e => new { e.SalonId, e.StaffId, e.WorkDate, e.IsEnroll })
                    .HasName("IDX_20180719_171_170_Solution_30shinedboFlowTimeKeeping");

                entity.HasIndex(e => new { e.StaffId, e.IsDelete, e.IsEnroll, e.WorkDate })
                    .HasName("IDX_20180719_174_173_Solution_30shinedboFlowTimeKeeping");

                entity.HasIndex(e => new { e.StaffId, e.WorkHour, e.IsEnroll, e.WorkTimeId, e.HourIds, e.SalonId, e.WorkDate })
                    .HasName("IDX_20180715_78716_78715_Solution_30shinedboFlowTimeKeeping");

                entity.HasIndex(e => new { e.StaffId, e.WorkHour, e.IsEnroll, e.WorkTimeId, e.HourIds, e.SalonId, e.WorkDate, e.IsDelete })
                    .HasName("IDX_20180715_119758_119757_Solution_30shinedboFlowTimeKeeping");

                entity.Property(e => e.CheckinFirstTime).HasColumnType("datetime");

                entity.Property(e => e.CheckoutLastTime).HasColumnType("datetime");

                entity.Property(e => e.HourIds).HasMaxLength(200);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.WorkDate).HasColumnType("date");
            });

            modelBuilder.Entity<FundAccountType>(entity =>
            {
                entity.ToTable("Fund_AccountType");

                entity.Property(e => e.AccountNumber).HasMaxLength(300);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(300);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<FundImport>(entity =>
            {
                entity.ToTable("Fund_Import");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ImportDate).HasColumnType("date");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<FundItemFlow>(entity =>
            {
                entity.ToTable("Fund_Item_Flow");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ItemTitle).HasMaxLength(300);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<FundOffenItem>(entity =>
            {
                entity.ToTable("Fund_OffenItem");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(600);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<FundSource>(entity =>
            {
                entity.ToTable("Fund_Source");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Hint).HasMaxLength(50);

                entity.Property(e => e.Key).HasMaxLength(50);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(300);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<HairAttribute>(entity =>
            {
                entity.ToTable("Hair_Attribute");

                entity.Property(e => e.HairTypeName).HasMaxLength(250);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<HairAttributeProduct>(entity =>
            {
                entity.ToTable("Hair_Attribute_Product");

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.HairAttributeId)
                    .HasColumnName("Hair_AttributeId")
                    .HasMaxLength(250);

                entity.Property(e => e.HairAttributeIdJson)
                    .HasColumnName("Hair_AttributeId_Json")
                    .HasMaxLength(250);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ProductId).HasMaxLength(250);

                entity.Property(e => e.Title).HasMaxLength(250);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ImageData>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ImageAfter).HasMaxLength(600);

                entity.Property(e => e.ImageBefore).HasMaxLength(600);

                entity.Property(e => e.ImageCheckAfter).HasMaxLength(600);

                entity.Property(e => e.ImageCheckBefore).HasMaxLength(600);

                entity.Property(e => e.Images).HasMaxLength(600);

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Objid).HasColumnName("OBJId");

                entity.Property(e => e.Slugkey).HasMaxLength(50);
            });

            modelBuilder.Entity<InventoryData>(entity =>
            {
                entity.ToTable("Inventory_Data");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.IDate)
                    .HasColumnName("iDate")
                    .HasColumnType("date");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<InventoryFlow>(entity =>
            {
                entity.ToTable("Inventory_Flow");

                entity.HasIndex(e => new { e.InvenId, e.ProductId, e.Quantity, e.IsDelete })
                    .HasName("IDX_2056_2055_Solution_30shinedboInventory_Flow");

                entity.HasIndex(e => new { e.InvenId, e.Quantity, e.IsDelete, e.ProductId })
                    .HasName("IDX_2054_2053_Solution_30shinedboInventory_Flow");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<InventoryFlowHc>(entity =>
            {
                entity.ToTable("Inventory_Flow_HC");

                entity.HasIndex(e => new { e.InvenId, e.ProductId, e.IsDelete, e.CreatedTime })
                    .HasName("IDX_20180719_5012_5011_Solution_30shinedboInventory_Flow_HC");

                entity.HasIndex(e => new { e.InvenId, e.ProductId, e.Quantity, e.IsDelete, e.CreatedTime })
                    .HasName("IDX_20180719_5009_5008_Solution_30shinedboInventory_Flow_HC");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.InvenId).HasColumnName("InvenID");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<InventoryHc>(entity =>
            {
                entity.ToTable("Inventory_HC");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.IDate)
                    .HasColumnName("iDate")
                    .HasColumnType("date");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<InventoryImport>(entity =>
            {
                entity.ToTable("Inventory_Import");

                entity.HasIndex(e => new { e.SalonId, e.ImportDate, e.IsDelete })
                    .HasName("IDX_20180719_50_49_Solution_30shinedboInventory_Import");

                entity.HasIndex(e => new { e.Id, e.ProductIds, e.TotalMoney, e.CreatedDate, e.ModifiedDate, e.CompleteBillTime, e.RecipientId, e.DoId, e.Note, e.ImportType, e.Uid, e.MigrateStatus, e.SalonId, e.IsDelete, e.ImportDate })
                    .HasName("IDX_20180719_14_13_Solution_30shinedboInventory_Import");

                entity.Property(e => e.CompleteBillTime).HasColumnType("datetime");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ImportDate).HasColumnType("date");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Note).HasColumnType("nvarchar(max)");

                entity.Property(e => e.ProductIds).HasColumnType("nvarchar(max)");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<IvInventory>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.ParentId).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<IvInventoryCurrent>(entity =>
            {
                entity.Property(e => e.AccountingDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Begin).HasDefaultValueSql("((0))");

                entity.Property(e => e.BeingTransported).HasDefaultValueSql("((0))");

                entity.Property(e => e.Cost).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.End).HasDefaultValueSql("((0))");

                entity.Property(e => e.Export).HasDefaultValueSql("((0))");

                entity.Property(e => e.ExportToMainWh)
                    .HasColumnName("ExportToMainWH")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ExportToSalon).HasDefaultValueSql("((0))");

                entity.Property(e => e.ExportToStaff).HasDefaultValueSql("((0))");

                entity.Property(e => e.Import).HasDefaultValueSql("((0))");

                entity.Property(e => e.ImportFromMainWh)
                    .HasColumnName("ImportFromMainWH")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ImportFromPartner).HasDefaultValueSql("((0))");

                entity.Property(e => e.ImportFromReturn).HasDefaultValueSql("((0))");

                entity.Property(e => e.ImportFromSalon).HasDefaultValueSql("((0))");

                entity.Property(e => e.ImportFromStaff).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.SellOrUse).HasDefaultValueSql("((0))");

                entity.Property(e => e.SuggestOrder).HasDefaultValueSql("((0))");

                entity.Property(e => e.UsedInService).HasDefaultValueSql("((0))");

                entity.Property(e => e.VolumeRemain).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<IvInventoryHistory>(entity =>
            {
                entity.Property(e => e.AccountingDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Begin).HasDefaultValueSql("((0))");

                entity.Property(e => e.BeingTransported).HasDefaultValueSql("((0))");

                entity.Property(e => e.Cost).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.End).HasDefaultValueSql("((0))");

                entity.Property(e => e.Export).HasDefaultValueSql("((0))");

                entity.Property(e => e.ExportToMainWh)
                    .HasColumnName("ExportToMainWH")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ExportToSalon).HasDefaultValueSql("((0))");

                entity.Property(e => e.ExportToStaff).HasDefaultValueSql("((0))");

                entity.Property(e => e.Import).HasDefaultValueSql("((0))");

                entity.Property(e => e.ImportFromMainWh)
                    .HasColumnName("ImportFromMainWH")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ImportFromPartner).HasDefaultValueSql("((0))");

                entity.Property(e => e.ImportFromReturn).HasDefaultValueSql("((0))");

                entity.Property(e => e.ImportFromSalon).HasDefaultValueSql("((0))");

                entity.Property(e => e.ImportFromStaff).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Quantify).HasDefaultValueSql("((0))");

                entity.Property(e => e.SellOrUse).HasDefaultValueSql("((0))");

                entity.Property(e => e.SuggestOrder).HasDefaultValueSql("((0))");

                entity.Property(e => e.UsedInService).HasDefaultValueSql("((0))");

                entity.Property(e => e.Volume).HasDefaultValueSql("((0))");

                entity.Property(e => e.VolumeRemain).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<IvInventoryInitial>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Initial).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<IvOrder>(entity =>
            {
                entity.Property(e => e.Code)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CosmeticType).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(2000);

                entity.Property(e => e.OrderType).HasDefaultValueSql("((1))");

                entity.Property(e => e.ReceivedDate)
                    .HasColumnName("receivedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.ReviewedDate)
                    .HasColumnName("reviewedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Status).HasDefaultValueSql("((0))");

                entity.Property(e => e.UserIdOrder)
                    .HasColumnName("userIdOrder")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UserIdReceive)
                    .HasColumnName("userIdReceive")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UserIdReview)
                    .HasColumnName("userIdReview")
                    .HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<IvOrderDetail>(entity =>
            {
                entity.Property(e => e.Cost).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.QuantityExport).HasDefaultValueSql("((0))");

                entity.Property(e => e.QuantityImport).HasDefaultValueSql("((0))");

                entity.Property(e => e.QuantityOrder).HasDefaultValueSql("((0))");

                entity.Property(e => e.QuantitySuggest).HasDefaultValueSql("((0))");

                entity.Property(e => e.QuantityType1).HasDefaultValueSql("((0))");

                entity.Property(e => e.QuantityType2).HasDefaultValueSql("((0))");

                entity.Property(e => e.QuantityType3).HasDefaultValueSql("((0))");

                entity.Property(e => e.QuantityType4).HasDefaultValueSql("((0))");

                entity.Property(e => e.QuantityType5).HasDefaultValueSql("((0))");

                entity.Property(e => e.SalonReason).HasDefaultValueSql("((1))");

                entity.Property(e => e.Whreason)
                    .HasColumnName("WHReason")
                    .HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<IvOrderNum>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.OrderNum).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<IvProductQuantify>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsPublish).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Quantify).HasDefaultValueSql("((0))");

                entity.Property(e => e.StaffType).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalNumberService).HasDefaultValueSql("((0))");

                entity.Property(e => e.Volume).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<KcsCheck3S>(entity =>
            {
                entity.ToTable("KCS_Check3S");

                entity.HasIndex(e => new { e.Id, e.UserId, e.Images, e.Note, e.ModifiedTime, e.Uid, e.MigrateStatus, e.SalonId, e.CreatedTime, e.IsDelete })
                    .HasName("IDX_20180718_3615_3614_Solution_30shinedboKCS_Check3S");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Images).HasColumnType("nvarchar(max)");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(600);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<KcsFaceType>(entity =>
            {
                entity.ToTable("KCS_FaceType");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Shade).HasMaxLength(200);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<KetQuaKinhDoanhFlowImport>(entity =>
            {
                entity.ToTable("KetQuaKinhDoanh_FlowImport");

                entity.HasIndex(e => new { e.Value, e.KqkdsalonId, e.KqkditemId, e.ImportDate })
                    .HasName("IDX_20180718_1820_1819_Solution_30shinedboKetQuaKinhDoanh_FlowImport");

                entity.HasIndex(e => new { e.Id, e.Value, e.CreatedTime, e.ModifiedTime, e.Note, e.Uid, e.MigrateStatus, e.KqkdsalonId, e.KqkditemId, e.ImportDate, e.IsDelete })
                    .HasName("IDX_20180718_1831_1830_Solution_30shinedboKetQuaKinhDoanh_FlowImport");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ImportDate).HasColumnType("date");

                entity.Property(e => e.KqkditemId).HasColumnName("KQKDItemId");

                entity.Property(e => e.KqkdsalonId).HasColumnName("KQKDSalonId");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(600);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<KetQuaKinhDoanhItemImport>(entity =>
            {
                entity.ToTable("KetQuaKinhDoanh_ItemImport");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(300);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<KetQuaKinhDoanhSalon>(entity =>
            {
                entity.ToTable("KetQuaKinhDoanh_Salon");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Meta).HasMaxLength(50);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.SalonName).HasMaxLength(300);

                entity.Property(e => e.ShortName).HasMaxLength(100);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Log>(entity =>
            {
                entity.HasIndex(e => e.Objid)
                    .HasName("IDX_20180715_7891_7890_Solution_30shinedboLog");

                entity.HasIndex(e => new { e.Objid, e.Type, e.Status })
                    .HasName("IDX_20180715_7896_7895_Solution_30shinedboLog");

                entity.Property(e => e.LogTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Objid).HasColumnName("OBJId");

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ManagerLog>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Today).HasColumnType("date");
            });

            modelBuilder.Entity<MapDeviceOwner>(entity =>
            {
                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<MarketingChienDich>(entity =>
            {
                entity.ToTable("Marketing_ChienDich");

                entity.Property(e => e.ChiPhi).HasColumnType("decimal(20, 0)");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifileDate).HasColumnType("datetime");

                entity.Property(e => e.NgayBatDau).HasColumnType("datetime");

                entity.Property(e => e.NgayKetThuc).HasColumnType("datetime");

                entity.Property(e => e.TieuDe).HasMaxLength(600);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<MarketingChiphiItem>(entity =>
            {
                entity.ToTable("Marketing_ChiphiItem");

                entity.Property(e => e.CreateTime).HasColumnType("datetime");

                entity.Property(e => e.ItemName)
                    .HasColumnName("Item_Name")
                    .HasMaxLength(600);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<MarketingChiphingay>(entity =>
            {
                entity.ToTable("Marketing_Chiphingay");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.GiaTri).HasColumnType("decimal(20, 0)");

                entity.Property(e => e.ItemId).HasColumnName("Item_Id");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifileDate).HasColumnType("datetime");

                entity.Property(e => e.NgayChi).HasColumnType("datetime");

                entity.Property(e => e.StaffId).HasColumnName("Staff_Id");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<MarketingChiPhiPhanBo>(entity =>
            {
                entity.ToTable("Marketing_ChiPhiPhanBo");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifileDate).HasColumnType("datetime");

                entity.Property(e => e.NgayChi).HasColumnType("datetime");

                entity.Property(e => e.TongChi).HasColumnType("decimal(20, 0)");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<MarkettingNganSach>(entity =>
            {
                entity.ToTable("Marketting_NganSach");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.GhiChu).HasMaxLength(500);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifileDate).HasColumnType("datetime");

                entity.Property(e => e.Ngay).HasColumnType("datetime");

                entity.Property(e => e.SoTien).HasColumnType("decimal(20, 0)");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<MemberLog>(entity =>
            {
                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.CustomerName).HasMaxLength(100);

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.IsDelete)
                    .HasColumnName("isDelete")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasMaxLength(250);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.Property(e => e.StartTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<Message>(entity =>
            {
                entity.HasIndex(e => new { e.IsDelete, e.Status, e.SalonId, e.Receiver, e.SendTime })
                    .HasName("IDX_20180719_955_954_Solution_30shinedboMessage");

                entity.HasIndex(e => new { e.IsDelete, e.Status, e.SalonId, e.Sender, e.SendTime })
                    .HasName("IDX_20180719_932_931_Solution_30shinedboMessage");

                entity.HasIndex(e => new { e.Id, e.ReceiverId, e.Text, e.Seq, e.Status, e.Receiver, e.IsAutoGenerate, e.IsDelete, e.SalonId, e.SendTime })
                    .HasName("IDX_20180715_16925_16924_Solution_30shinedboMessage");

                entity.HasIndex(e => new { e.Id, e.CreateTime, e.DeleteTime, e.SenderId, e.ReceiverId, e.IsAutoGenerate, e.Text, e.Seq, e.Sender, e.Receiver, e.UpdateTime, e.SendTime, e.IsDelete, e.Uid, e.MigrateStatus, e.SendSuccessTime, e.Salon, e.Type, e.SalonId, e.Status })
                    .HasName("IDX_20180715_34999_34998_Solution_30shinedboMessage");

                entity.HasIndex(e => new { e.Id, e.CreateTime, e.DeleteTime, e.SenderId, e.ReceiverId, e.IsAutoGenerate, e.Text, e.Seq, e.Sender, e.Status, e.Receiver, e.UpdateTime, e.SendTime, e.IsDelete, e.Uid, e.MigrateStatus, e.SendSuccessTime, e.Salon, e.Type, e.SalonId })
                    .HasName("IDX_20180715_16929_16928_Solution_30shinedboMessage");

                entity.HasIndex(e => new { e.Id, e.CreateTime, e.DeleteTime, e.SenderId, e.ReceiverId, e.Text, e.Seq, e.Sender, e.Status, e.Receiver, e.UpdateTime, e.IsDelete, e.Uid, e.MigrateStatus, e.SendSuccessTime, e.Salon, e.Type, e.IsAutoGenerate, e.SalonId, e.SendTime })
                    .HasName("IDX_20180715_84_83_Solution_30shinedboMessage");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DeleteTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1970-1-1')");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Receiver)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Salon).HasMaxLength(50);

                entity.Property(e => e.SendSuccessTime).HasColumnType("datetime");

                entity.Property(e => e.SendTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1970-1-1')");

                entity.Property(e => e.Sender)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Seq).HasDefaultValueSql("((1))");

                entity.Property(e => e.Text)
                    .HasColumnType("nvarchar(max)")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1970-1-1')");
            });

            modelBuilder.Entity<MktCampaign>(entity =>
            {
                entity.Property(e => e.CampaignMaxUsage).HasDefaultValueSql("((0))");

                entity.Property(e => e.CheckoutLabel).HasMaxLength(300);

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerCondition).HasMaxLength(500);

                entity.Property(e => e.CustomerType).HasDefaultValueSql("((0))");

                entity.Property(e => e.Description).HasDefaultValueSql("('')");

                entity.Property(e => e.EndDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Label)
                    .HasMaxLength(125)
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.MaxUsage).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasMaxLength(125)
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.Note).HasMaxLength(450);

                entity.Property(e => e.ServiceType).HasDefaultValueSql("((1))");

                entity.Property(e => e.StartDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Type).HasDefaultValueSql("((3))");
            });

            modelBuilder.Entity<MktCampaignBill>(entity =>
            {
                entity.Property(e => e.BillId).HasDefaultValueSql("((0))");

                entity.Property(e => e.CampaignId).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DiscountMoney).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.ServiceId).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<MktCampaignCustomer>(entity =>
            {
                entity.HasIndex(e => e.CampaignId)
                    .HasName("IX_MktCampaignCustomer_ID");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("IX_MktCampaignCustomerID");

                entity.HasIndex(e => new { e.Id, e.CustomerId, e.Used, e.CreatedTime, e.ModifiedTime, e.CampaignId, e.IsDelete })
                    .HasName("IDX_20180718_283_282_Solution_30shinedboMktCampaignCustomer");

                entity.HasIndex(e => new { e.Used, e.CreatedTime, e.ModifiedTime, e.VoucherId, e.IsDelete, e.CampaignId, e.Id, e.CustomerId })
                    .HasName("_dta_index_MktCampaignCustomer_9_1221413586__K7_K2_K1_K3_4_5_6_8");

                entity.Property(e => e.CampaignId).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerId).HasDefaultValueSql("((0))");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Used).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<MktCampaignService>(entity =>
            {
                entity.Property(e => e.CampaignId).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DiscountPercent).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.MoneyDeductions).HasDefaultValueSql("((0))");

                entity.Property(e => e.MoneyPrePaid).HasDefaultValueSql("((0))");

                entity.Property(e => e.ServiceId).HasDefaultValueSql("((0))");

                entity.Property(e => e.ServicePrice).HasDefaultValueSql("((0))");

                entity.Property(e => e.TimesUsed).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<MktCampaignUsed>(entity =>
            {
                entity.HasKey(e => e.CampaignId);

                entity.Property(e => e.CampaignId).ValueGeneratedNever();
            });

            modelBuilder.Entity<MktVoucher>(entity =>
            {
                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<Mobile>(entity =>
            {
                entity.Property(e => e.AccessKey).HasMaxLength(50);

                entity.Property(e => e.AppKey)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Iosversion)
                    .HasColumnName("IOSVersion")
                    .HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PrivateKey).HasMaxLength(50);
            });

            modelBuilder.Entity<MonitorCategoryError>(entity =>
            {
                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModiyDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<MonitorHandle>(entity =>
            {
                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<MonitorStaffError>(entity =>
            {
                entity.HasIndex(e => new { e.StaffId, e.CategoryRootId, e.StatusError, e.IsDelete, e.ErrorTime })
                    .HasName("IDX_20180715_162320_162319_Solution_30shinedboMonitorStaffError");

                entity.HasIndex(e => new { e.StaffId, e.SalonId, e.StatusError, e.IsDelete, e.ErrorTime })
                    .HasName("IDX_20180718_2_1_Solution_30shinedboMonitorStaffError");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.StatusError, e.IsCommit, e.IsDelete, e.CreateDate })
                    .HasName("IDX_20180718_595_594_Solution_30shinedboMonitorStaffError");

                entity.HasIndex(e => new { e.Id, e.ReporterId, e.StaffTypeId, e.StaffId, e.HandleId, e.Images, e.CategoryRootId, e.CategorySubId, e.NoteByReporter, e.ReviewerId, e.NoteByReviewer, e.IsPending, e.IsCommit, e.StatusError, e.ResolverId, e.ResolveTime, e.CreateDate, e.ConfirmTime, e.CommitTime, e.ModifyDate, e.PeriodBeforeConfirm, e.PeriodBeforeCommit, e.ErrorTime, e.SalonId, e.IsDelete })
                    .HasName("IDX_20180718_69_68_Solution_30shinedboMonitorStaffError");

                entity.HasIndex(e => new { e.Id, e.ReporterId, e.StaffTypeId, e.StaffId, e.HandleId, e.Images, e.CategoryRootId, e.CategorySubId, e.NoteByReporter, e.ReviewerId, e.NoteByReviewer, e.IsPending, e.StatusError, e.ResolverId, e.ResolveTime, e.CreateDate, e.ConfirmTime, e.CommitTime, e.ModifyDate, e.PeriodBeforeConfirm, e.PeriodBeforeCommit, e.ErrorTime, e.SalonId, e.IsCommit, e.IsDelete })
                    .HasName("IDX_20180718_438_437_Solution_30shinedboMonitorStaffError");

                entity.Property(e => e.CommitTime).HasColumnType("datetime");

                entity.Property(e => e.ConfirmTime).HasColumnType("datetime");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ErrorTime).HasColumnType("datetime");

                entity.Property(e => e.Images)
                    .IsRequired()
                    .HasColumnType("nvarchar(max)");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.NoteByReporter)
                    .IsRequired()
                    .HasColumnType("nvarchar(max)");

                entity.Property(e => e.NoteByReviewer).HasColumnType("nvarchar(max)");

                entity.Property(e => e.ResolveTime).HasColumnType("datetime");

                entity.Property(e => e.StatusError).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<NetworkOperator>(entity =>
            {
                entity.Property(e => e.Address).HasMaxLength(600);

                entity.Property(e => e.Email).HasMaxLength(300);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.NetworkName).HasMaxLength(300);

                entity.Property(e => e.Phone)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.ShortName).HasMaxLength(200);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Url).HasMaxLength(300);
            });

            modelBuilder.Entity<NotificationManagement>(entity =>
            {
                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Descriptions)
                    .HasMaxLength(250)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Images)
                    .HasMaxLength(300)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IsPublish).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasMaxLength(250)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Title)
                    .HasMaxLength(250)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Type).HasDefaultValueSql("((0))");

                entity.Property(e => e.Url)
                    .HasMaxLength(250)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<NotificationUsers>(entity =>
            {
                entity.HasIndex(e => e.UserId)
                    .HasName("IDX_71345_71344_Solution_30shinedboNotificationUsers");

                entity.HasIndex(e => new { e.UserId, e.SlugKey })
                    .HasName("IDX_71191_71190_Solution_30shinedboNotificationUsers");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsPublish).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.NotiId).HasDefaultValueSql("((0))");

                entity.Property(e => e.SlugKey)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.UserId).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<OperationReportConfig>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DataJson).HasMaxLength(1024);

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.MonthApply).HasColumnType("date");
            });

            modelBuilder.Entity<OperationReportStatistic>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.ModifedDate).HasColumnType("datetime");

                entity.Property(e => e.Revenue).HasMaxLength(1024);

                entity.Property(e => e.TotalBillPerStylist).HasColumnType("decimal(19, 2)");

                entity.Property(e => e.TotalBillPerTime).HasColumnType("decimal(19, 2)");

                entity.Property(e => e.TotalIncomePerStylist).HasColumnType("decimal(19, 2)");

                entity.Property(e => e.TotalIncomePerTime).HasColumnType("decimal(19, 2)");

                entity.Property(e => e.TotalSkinnerWorkTime).HasColumnType("decimal(19, 2)");

                entity.Property(e => e.TotalStylistWorkTime).HasColumnType("decimal(19, 2)");
            });

            modelBuilder.Entity<OrderBrokenDeviceHandling>(entity =>
            {
                entity.Property(e => e.BrokenDate).HasColumnType("datetime");

                entity.Property(e => e.CompleteDate).HasColumnType("datetime");

                entity.Property(e => e.CorrectDeadline).HasColumnType("datetime");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DescriptionBroken).HasMaxLength(500);

                entity.Property(e => e.DesiredDeadline).HasColumnType("datetime");

                entity.Property(e => e.HandlingNote).HasMaxLength(500);

                entity.Property(e => e.ImageHandling).HasMaxLength(1000);

                entity.Property(e => e.ImagesBroken).HasMaxLength(1000);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.NoteRevoke).HasMaxLength(500);

                entity.Property(e => e.StatusCheckTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<OrderBrokenDeviceHandlingLog>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Value).HasMaxLength(150);
            });

            modelBuilder.Entity<OrderRecruitingStaff>(entity =>
            {
                entity.Property(e => e.Completed).HasColumnType("datetime");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Needed).HasColumnType("datetime");

                entity.Property(e => e.StaffDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<OrderRecruitingStaffLog>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Value).HasMaxLength(150);
            });

            modelBuilder.Entity<PayMethod>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<PermissionAction>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<PermissionDefaultPage>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<PermissionErp>(entity =>
            {
                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('1970-1-1')");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<PermissionMenu>(entity =>
            {
                entity.Property(e => e.ClassTag)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.IconImage).IsUnicode(false);

                entity.Property(e => e.Link).HasMaxLength(450);

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.PageId).HasMaxLength(200);

                entity.Property(e => e.Path).HasMaxLength(300);

                entity.Property(e => e.Title).HasMaxLength(450);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<PermissionMenuAction>(entity =>
            {
                entity.HasIndex(e => new { e.PermissionId, e.PageId, e.IsDelete })
                    .HasName("IDX_20180715_88729_88728_Solution_30shinedboPermissionMenuAction");

                entity.HasIndex(e => new { e.PageId, e.PermissionId, e.IsDelete, e.IsActive })
                    .HasName("IDX_20180715_48_47_Solution_30shinedboPermissionMenuAction");

                entity.HasIndex(e => new { e.PageId, e.ActionId, e.PermissionId, e.IsDelete, e.IsActive })
                    .HasName("IDX_20180715_55074_55073_Solution_30shinedboPermissionMenuAction");

                entity.HasIndex(e => new { e.PermissionId, e.ActionId, e.PageId, e.IsDelete, e.IsActive })
                    .HasName("IDX_20180715_40_39_Solution_30shinedboPermissionMenuAction");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<PermissionSalonArea>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<PermissionStaff>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<PreviewImagesReport>(entity =>
            {
                entity.ToTable("PreviewImages_Report");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ReportType).HasMaxLength(300);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasIndex(e => new { e.Name, e.Id })
                    .HasName("_dta_index_Product_9_166523116__K1_2");

                entity.Property(e => e.BarCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CheckInventoryHc).HasColumnName("CheckInventoryHC");

                entity.Property(e => e.Code).HasMaxLength(50);

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.InventoryType)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModelName).HasMaxLength(300);

                entity.Property(e => e.Name).HasMaxLength(300);

                entity.Property(e => e.OldPrice).HasColumnName("Old_Price");

                entity.Property(e => e.TechInfo).HasColumnType("ntext");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Volume).HasMaxLength(300);
            });

            modelBuilder.Entity<ProductReturn>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateOfBill).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ProductUsedStatistic>(entity =>
            {
                entity.HasIndex(e => new { e.ServiceId, e.ProductId, e.QuantifyDefault, e.Quantify, e.SlugKey, e.OrderNumber, e.StaffId, e.IsDelete, e.WorkDate })
                    .HasName("IDX_53888_53887_Solution_30shinedboProductUsedStatistic");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.OrderNumber).HasDefaultValueSql("((0))");

                entity.Property(e => e.ProductId).HasDefaultValueSql("((0))");

                entity.Property(e => e.Quantify).HasDefaultValueSql("((0))");

                entity.Property(e => e.QuantifyDefault).HasDefaultValueSql("((0))");

                entity.Property(e => e.ServiceId).HasDefaultValueSql("((0))");

                entity.Property(e => e.SlugKey)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.StaffId).HasDefaultValueSql("((0))");

                entity.Property(e => e.Status).HasDefaultValueSql("((0))");

                entity.Property(e => e.WorkDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<QlkhoSalonOrder>(entity =>
            {
                entity.ToTable("QLKho_SalonOrder");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.StatusId, e.OrderDate })
                    .HasName("IDX_20180715_163877_163876_Solution_30shinedboQLKho_SalonOrder");

                entity.HasIndex(e => new { e.Id, e.IsDelete, e.SalonId, e.CheckCosmetic, e.ExportTime })
                    .HasName("IDX_20180719_45_44_Solution_30shinedboQLKho_SalonOrder");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.IsDelete, e.CheckCosmetic, e.ReceivedTime })
                    .HasName("IDX_20180718_3470_3469_Solution_30shinedboQLKho_SalonOrder");

                entity.Property(e => e.BillCode).HasMaxLength(20);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ExportTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.NoteKho).HasMaxLength(600);

                entity.Property(e => e.NoteSalon).HasMaxLength(600);

                entity.Property(e => e.OrderDate).HasColumnType("date");

                entity.Property(e => e.ReceivedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<QlkhoSalonOrderFlow>(entity =>
            {
                entity.ToTable("QLKho_SalonOrder_Flow");

                entity.HasIndex(e => new { e.OrderId, e.IsDelete })
                    .HasName("IDX_20180715_163873_163872_Solution_30shinedboQLKho_SalonOrder_Flow");

                entity.HasIndex(e => new { e.Id, e.ProductId, e.Cost, e.Price, e.QuantityOrder, e.CreatedTime, e.ModifiedTime, e.IsDelete, e.QuantityReceived, e.QuantityOwe, e.QuantityExport, e.OrderId })
                    .HasName("IDX_20180715_163875_163874_Solution_30shinedboQLKho_SalonOrder_Flow");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<QlkhoSalonOrderStatus>(entity =>
            {
                entity.ToTable("QLKho_SalonOrder_Status");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(300);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(200);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<QuanHuyen>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.TenQuanHuyen).HasMaxLength(255);

                entity.Property(e => e.TinhThanhId).HasColumnName("TinhThanhID");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<RatingBonus>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<RatingConfigPoint>(entity =>
            {
                entity.ToTable("Rating_ConfigPoint");

                entity.Property(e => e.ConventionName).HasMaxLength(200);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<RatingDetail>(entity =>
            {
                entity.HasIndex(e => e.BillId)
                    .HasName("NonClusteredIndex-20180801-152800")
                    .IsUnique();

                entity.HasIndex(e => new { e.StarNumber, e.BillId })
                    .HasName("KODUOCXOA_dta_index_RatingDetail_9_1563550953__K2D_6")
                    .IsUnique();

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.IsAuto)
                    .HasColumnName("isAuto")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.RatingReasonId).HasColumnName("RatingReasonId ");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<RatingDetailV1>(entity =>
            {
                entity.ToTable("RatingDetail_V1");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.RatingReasonId).HasColumnName("RatingReasonId ");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<RatingIsCheck>(entity =>
            {
                entity.HasIndex(e => new { e.Id, e.BillId, e.CustomerId, e.CreatedTime, e.ModifiedTime, e.IsDelete, e.Uid, e.MigrateStatus, e.SalonId })
                    .HasName("IDX_20180715_9508_9507_Solution_30shinedboRatingIsCheck");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<RatingReason>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<RatingTemp>(entity =>
            {
                entity.ToTable("Rating_Temp");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<RealtimeFirebaseTokenId>(entity =>
            {
                entity.ToTable("Realtime_Firebase_TokenID");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.CurrentToken).HasMaxLength(200);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.OldToken).HasMaxLength(200);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SalaryConfig>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SalaryConfigStaff>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SalaryIncome>(entity =>
            {
                entity.HasIndex(e => new { e.StaffId, e.RatingPoint, e.WorkDate })
                    .HasName("IDX_20180715_691_690_Solution_30shinedboSalaryIncome");

                entity.HasIndex(e => new { e.StaffId, e.SalonId, e.IsDeleted, e.WorkDate })
                    .HasName("IDX_20180715_8831_8830_Solution_30shinedboSalaryIncome");

                entity.HasIndex(e => new { e.SalonId, e.FixedSalary, e.AllowanceSalary, e.OvertimeSalary, e.ServiceSalary, e.ProductSalary, e.BehaveSalary, e.TotalIncome, e.GrandTotalIncome, e.IsDeleted, e.WorkDate })
                    .HasName("IDX_20180719_5038_5037_Solution_30shinedboSalaryIncome");

                entity.HasIndex(e => new { e.Id, e.AllowanceSalary, e.OvertimeSalary, e.ServiceSalary, e.ProductSalary, e.BehaveSalary, e.RatingPoint, e.BillNormal, e.BillNormalGreat, e.BillNormalGood, e.BillNormalBad, e.BillNormalNoRating, e.BillSpecial, e.BillSpecialGreat, e.BillSpecialGood, e.BillSpecialBad, e.BillSpecialNoRating, e.Month, e.Year, e.TotalIncome, e.GrandTotalIncome, e.CreatedTime, e.ModifiedTime, e.Uid, e.MigrateStatus, e.SalonId, e.IsDeleted, e.StaffId, e.FixedSalary, e.WorkDate })
                    .HasName("IDX_20180715_86153_86152_Solution_30shinedboSalaryIncome");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.AllowanceSalary, e.OvertimeSalary, e.ServiceSalary, e.ProductSalary, e.BehaveSalary, e.RatingPoint, e.BillNormal, e.BillNormalGreat, e.BillNormalGood, e.BillNormalBad, e.BillNormalNoRating, e.BillSpecial, e.BillSpecialGreat, e.BillSpecialGood, e.BillSpecialBad, e.BillSpecialNoRating, e.Month, e.Year, e.TotalIncome, e.GrandTotalIncome, e.CreatedTime, e.ModifiedTime, e.Uid, e.MigrateStatus, e.StaffId, e.IsDeleted, e.FixedSalary, e.WorkDate })
                    .HasName("IDX_20180715_26002_26001_Solution_30shinedboSalaryIncome");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.AllowanceSalary, e.OvertimeSalary, e.ServiceSalary, e.ProductSalary, e.BehaveSalary, e.RatingPoint, e.BillNormal, e.BillNormalGreat, e.BillNormalGood, e.BillNormalBad, e.BillNormalNoRating, e.BillSpecial, e.BillSpecialGreat, e.BillSpecialGood, e.BillSpecialBad, e.BillSpecialNoRating, e.Month, e.Year, e.TotalIncome, e.GrandTotalIncome, e.CreatedTime, e.ModifiedTime, e.Uid, e.MigrateStatus, e.StaffId, e.WorkDate, e.IsDeleted, e.FixedSalary })
                    .HasName("IDX_20180715_25999_25998_Solution_30shinedboSalaryIncome");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.FixedSalary, e.AllowanceSalary, e.OvertimeSalary, e.ServiceSalary, e.ProductSalary, e.BehaveSalary, e.RatingPoint, e.BillNormal, e.BillNormalGreat, e.BillNormalGood, e.BillNormalBad, e.BillNormalNoRating, e.BillSpecial, e.BillSpecialGreat, e.BillSpecialGood, e.BillSpecialBad, e.BillSpecialNoRating, e.Month, e.Year, e.TotalIncome, e.GrandTotalIncome, e.CreatedTime, e.ModifiedTime, e.Uid, e.MigrateStatus, e.StaffId, e.WorkDate, e.IsDeleted })
                    .HasName("IDX_20180715_744_743_Solution_30shinedboSalaryIncome");

                entity.HasIndex(e => new { e.Id, e.StaffId, e.FixedSalary, e.AllowanceSalary, e.OvertimeSalary, e.ServiceSalary, e.ProductSalary, e.BehaveSalary, e.RatingPoint, e.BillNormal, e.BillNormalGreat, e.BillNormalGood, e.BillNormalBad, e.BillNormalNoRating, e.BillSpecial, e.BillSpecialGreat, e.BillSpecialGood, e.BillSpecialBad, e.BillSpecialNoRating, e.Month, e.Year, e.TotalIncome, e.GrandTotalIncome, e.CreatedTime, e.ModifiedTime, e.Uid, e.MigrateStatus, e.SalonId, e.IsDeleted, e.WorkDate })
                    .HasName("IDX_20180718_2467_2466_Solution_30shinedboSalaryIncome");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.RatingPoint).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.WorkDate).HasColumnType("date");
            });

            modelBuilder.Entity<SalaryIncomeChange>(entity =>
            {
                entity.HasIndex(e => new { e.SalonId, e.ScoreFactor, e.ChangedType, e.IsDeleted, e.WorkDate })
                    .HasName("IDX_20180715_75481_75480_Solution_30shinedboSalaryIncomeChange");

                entity.HasIndex(e => new { e.StaffId, e.SalonId, e.ChangedType, e.ScoreFactor, e.IsDeleted, e.WorkDate })
                    .HasName("IDX_20180715_75498_75497_Solution_30shinedboSalaryIncomeChange");

                entity.HasIndex(e => new { e.Id, e.StaffId, e.ChangedTypeId, e.Description, e.ScoreFactor, e.SalonId, e.ChangedType, e.WorkDate, e.IsDeleted })
                    .HasName("IDX_20180715_33167_33166_Solution_30shinedboSalaryIncomeChange");

                entity.HasIndex(e => new { e.Id, e.ChangedType, e.ChangedTypeId, e.Description, e.Images, e.Point, e.ScoreFactor, e.CreatedTime, e.ModifiedTime, e.Uid, e.MigrateStatus, e.SalonId, e.IsDeleted, e.StaffId, e.WorkDate })
                    .HasName("IDX_20180715_68617_68616_Solution_30shinedboSalaryIncomeChange");

                entity.HasIndex(e => new { e.Id, e.ChangedType, e.ChangedTypeId, e.Description, e.Images, e.Point, e.ScoreFactor, e.CreatedTime, e.ModifiedTime, e.Uid, e.MigrateStatus, e.StaffId, e.SalonId, e.IsDeleted, e.WorkDate })
                    .HasName("IDX_20180715_55668_55667_Solution_30shinedboSalaryIncomeChange");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.ChangedType, e.ChangedTypeId, e.Description, e.Images, e.Point, e.ScoreFactor, e.CreatedTime, e.ModifiedTime, e.Uid, e.MigrateStatus, e.StaffId, e.IsDeleted, e.WorkDate })
                    .HasName("IDX_20180715_6_5_Solution_30shinedboSalaryIncomeChange");

                entity.HasIndex(e => new { e.Id, e.StaffId, e.SalonId, e.ChangedType, e.ChangedTypeId, e.Description, e.Images, e.Point, e.ScoreFactor, e.CreatedTime, e.ModifiedTime, e.IsDeleted, e.Uid, e.MigrateStatus, e.WorkDate })
                    .HasName("IDX_20180718_386_385_Solution_30shinedboSalaryIncomeChange");

                entity.Property(e => e.ChangedType).HasMaxLength(200);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Description).HasColumnType("nvarchar(max)");

                entity.Property(e => e.Images).HasMaxLength(300);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.WorkDate).HasColumnType("date");
            });

            modelBuilder.Entity<SalonDailyCost>(entity =>
            {
                entity.Property(e => e.AdvertisementExpend).HasDefaultValueSql("((0))");

                entity.Property(e => e.CapitalSpending).HasDefaultValueSql("((0))");

                entity.Property(e => e.Compensation).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DailyCostInventory).HasDefaultValueSql("((0))");

                entity.Property(e => e.ElectricityAndWaterBill).HasDefaultValueSql("((0))");

                entity.Property(e => e.IncomeTaxes).HasDefaultValueSql("((0))");

                entity.Property(e => e.InternetAndPhoneBill).HasDefaultValueSql("((0))");

                entity.Property(e => e.ItSalary).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.OfficeStaffSalary).HasDefaultValueSql("((0))");

                entity.Property(e => e.OfficeStaffSocialInsurance).HasDefaultValueSql("((0))");

                entity.Property(e => e.OtherIncome).HasDefaultValueSql("((0))");

                entity.Property(e => e.ProductPrice).HasDefaultValueSql("((0))");

                entity.Property(e => e.RentWithTax).HasDefaultValueSql("((0))");

                entity.Property(e => e.ReportDate).HasColumnType("datetime");

                entity.Property(e => e.SalesSalary).HasDefaultValueSql("((0))");

                entity.Property(e => e.SalonUnplannedSpending).HasDefaultValueSql("((0))");

                entity.Property(e => e.ShippingExpend).HasDefaultValueSql("((0))");

                entity.Property(e => e.SocialInsuranceAndFixedCost).HasDefaultValueSql("((0))");

                entity.Property(e => e.ThaiHaRentAndSeviceCost).HasDefaultValueSql("((0))");

                entity.Property(e => e.UnplannedSpending).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<SalonService>(entity =>
            {
                entity.ToTable("Salon_Service");

                entity.Property(e => e.HeSoHl).HasColumnName("HeSoHL");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Price).HasColumnType("money");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SalonTypeConfig>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<ScriptData>(entity =>
            {
                entity.Property(e => e.Content).HasColumnType("text");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.File).HasMaxLength(150);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(200);
            });

            modelBuilder.Entity<ScscCategory>(entity =>
            {
                entity.HasKey(e => e.IdScscCate);

                entity.ToTable("SCSC_Category");

                entity.Property(e => e.IdScscCate).HasColumnName("ID_SCSC_Cate");

                entity.Property(e => e.CreateTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiledTime).HasColumnType("datetime");

                entity.Property(e => e.ScscCateDes).HasColumnName("SCSC_Cate_Des");

                entity.Property(e => e.ScscCateIdcate).HasColumnName("SCSC_Cate_IDCate");

                entity.Property(e => e.ScscCateImage).HasColumnName("SCSC_Cate_Image");

                entity.Property(e => e.ScscCateImageActive).HasColumnName("SCSC_Cate_Image_Active");

                entity.Property(e => e.ScscCateName)
                    .HasColumnName("SCSC_Cate_Name")
                    .HasMaxLength(200);

                entity.Property(e => e.ScscCatePoint).HasColumnName("SCSC_Cate_Point");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ScscCheckError>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.BillServiceId });

                entity.ToTable("SCSC_CheckError");

                entity.HasIndex(e => new { e.BillServiceId, e.PointError })
                    .HasName("IDX_20180715_159197_159196_Solution_30shinedboSCSC_CheckError");

                entity.HasIndex(e => new { e.PointError, e.BillServiceId })
                    .HasName("IDX_20180715_158943_158942_Solution_30shinedboSCSC_CheckError");

                entity.HasIndex(e => new { e.BillServiceId, e.ComPlatetionId, e.Publish, e.IsDelete })
                    .HasName("IDX_20180719_5021_5020_Solution_30shinedboSCSC_CheckError");

                entity.HasIndex(e => new { e.BillServiceId, e.ConnectTiveId, e.Publish, e.IsDelete })
                    .HasName("IDX_20180719_5017_5016_Solution_30shinedboSCSC_CheckError");

                entity.HasIndex(e => new { e.BillServiceId, e.ImageError, e.Publish, e.IsDelete })
                    .HasName("IDX_20180719_5023_5022_Solution_30shinedboSCSC_CheckError");

                entity.HasIndex(e => new { e.BillServiceId, e.Publish, e.IsDelete, e.PointError })
                    .HasName("IDX_20180715_158904_158903_Solution_30shinedboSCSC_CheckError");

                entity.HasIndex(e => new { e.BillServiceId, e.ShapeId, e.Publish, e.IsDelete })
                    .HasName("IDX_20180719_5015_5014_Solution_30shinedboSCSC_CheckError");

                entity.HasIndex(e => new { e.BillServiceId, e.SharpNessId, e.Publish, e.IsDelete })
                    .HasName("IDX_20180719_5019_5018_Solution_30shinedboSCSC_CheckError");

                entity.HasIndex(e => new { e.ShapeId, e.ConnectTiveId, e.SharpNessId, e.ComPlatetionId, e.PointError, e.BillServiceId })
                    .HasName("_dta_index_SCSC_CheckError_9_1908707510__K18D_K2_5_6_7_8");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BillServiceId).HasColumnName("BillService_ID");

                entity.Property(e => e.ComPlatetionId).HasColumnName("ComPlatetion_ID");

                entity.Property(e => e.ConnectTiveId).HasColumnName("ConnectTive_ID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.HairRootId).HasColumnName("HairRoot_ID");

                entity.Property(e => e.HairTipId).HasColumnName("HairTip_ID");

                entity.Property(e => e.HairWavesId).HasColumnName("HairWaves_ID");

                entity.Property(e => e.MaxPointComPlatetionId).HasColumnName("MaxPoint_ComPlatetion_ID");

                entity.Property(e => e.MaxPointConnectiveId).HasColumnName("MaxPoint_Connective_ID");

                entity.Property(e => e.MaxPointShapId).HasColumnName("MaxPoint_Shap_ID");

                entity.Property(e => e.MaxPointSharpNessId).HasColumnName("MaxPoint_SharpNess_ID");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiledDate).HasColumnType("datetime");

                entity.Property(e => e.ShapeId).HasColumnName("Shape_ID");

                entity.Property(e => e.SharpNessId).HasColumnName("SharpNess_ID");

                entity.Property(e => e.TotalPointScsc).HasColumnName("TotalPointSCSC");

                entity.Property(e => e.TotalPointScsccurling).HasColumnName("TotalPointSCSCCurling");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SelfieCode>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerPhone)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.HasIndex(e => new { e.Name, e.Id })
                    .HasName("_dta_index_Service_9_258099960__K1_2");

                entity.Property(e => e.Code).HasMaxLength(50);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(300);

                entity.Property(e => e.OldPrice).HasColumnName("Old_Price");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ServiceRatingRelationship>(entity =>
            {
                entity.ToTable("Service_Rating_Relationship");

                entity.HasIndex(e => e.BillServiceId)
                    .HasName("_dta_index_Service_Rating_Relationship_9_823123656__K2");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ServiceSalonConfig>(entity =>
            {
                entity.HasIndex(e => new { e.ServiceId, e.DepartmentId, e.ServiceCoefficient, e.ServiceBonus, e.CoefficientOvertimeHour, e.CoefficientOvertimeDay, e.IsPublish, e.IsDelete, e.SalonId })
                    .HasName("IDX_59357_59356_Solution_30shinedboServiceSalonConfig");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.ServiceCoefficient, e.ServiceBonus, e.CoefficientOvertimeHour, e.CoefficientOvertimeDay, e.IsPublish, e.ServiceId, e.DepartmentId, e.IsDelete })
                    .HasName("IDX_33733_33732_Solution_30shinedboServiceSalonConfig");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.ServiceId, e.ServiceCoefficient, e.ServiceBonus, e.CoefficientOvertimeHour, e.CoefficientOvertimeDay, e.DepartmentId, e.IsPublish, e.IsDelete })
                    .HasName("IDX_38155_38154_Solution_30shinedboServiceSalonConfig");

                entity.HasIndex(e => new { e.Id, e.ServiceCoefficient, e.ServiceBonus, e.CoefficientOvertimeHour, e.CoefficientOvertimeDay, e.SalonId, e.ServiceId, e.DepartmentId, e.IsPublish, e.IsDelete })
                    .HasName("IDX_38153_38152_Solution_30shinedboServiceSalonConfig");

                entity.HasIndex(e => new { e.Id, e.ServiceId, e.ServiceCoefficient, e.ServiceBonus, e.CoefficientOvertimeHour, e.CoefficientOvertimeDay, e.IsPublish, e.SalonId, e.DepartmentId, e.IsDelete })
                    .HasName("IDX_36170_36169_Solution_30shinedboServiceSalonConfig");

                entity.HasIndex(e => new { e.Id, e.ServiceId, e.ServiceCoefficient, e.ServiceBonus, e.CoefficientOvertimeHour, e.CoefficientOvertimeDay, e.CreatedTime, e.ModifiedTime, e.IsPublish, e.IsDelete, e.SalonId, e.DepartmentId })
                    .HasName("IDX_35472_35471_Solution_30shinedboServiceSalonConfig");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<ServiceSalonGoldTime>(entity =>
            {
                entity.Property(e => e.CoefficientGolden).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.HoursId).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsPublish).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiledTime).HasColumnType("datetime");

                entity.Property(e => e.ServiceSalonConfigId).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<ServiceSalonTypeConfig>(entity =>
            {
                entity.Property(e => e.CreateTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<ServiceTemp>(entity =>
            {
                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<SkinAttribute>(entity =>
            {
                entity.ToTable("Skin_Attribute");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.SkinTypeName).HasMaxLength(250);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SkinAttributeProduct>(entity =>
            {
                entity.ToTable("Skin_Attribute_Product");

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ProductId).HasMaxLength(250);

                entity.Property(e => e.SkinAttributeId).HasColumnName("Skin_AttributeId");

                entity.Property(e => e.Title).HasMaxLength(250);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SmBillTemp>(entity =>
            {
                entity.ToTable("SM_BillTemp");

                entity.HasIndex(e => new { e.Id, e.BookingId, e.Hcitem, e.BillServiceId, e.IsDelete, e.CreatedDate })
                    .HasName("IDX_20180715_128_127_Solution_30shinedboSM_BillTemp");

                entity.HasIndex(e => new { e.Id, e.IsDelete, e.Hcitem, e.BillServiceId, e.BookingId, e.CreatedDate })
                    .HasName("IDX_20180719_33_32_Solution_30shinedboSM_BillTemp");

                entity.Property(e => e.BillCode).HasMaxLength(20);

                entity.Property(e => e.BillServiceId).HasColumnName("BillServiceID");

                entity.Property(e => e.CompleteBillTime).HasColumnType("datetime");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FeeCod).HasColumnName("FeeCOD");

                entity.Property(e => e.Hcitem)
                    .HasColumnName("HCItem")
                    .HasMaxLength(50);

                entity.Property(e => e.Images).HasMaxLength(600);

                entity.Property(e => e.InProcedureTime).HasColumnType("datetime");

                entity.Property(e => e.InProcedureTimeModifed).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasColumnType("ntext");

                entity.Property(e => e.PdfbillCode)
                    .HasColumnName("PDFBillCode")
                    .HasMaxLength(30);

                entity.Property(e => e.StaffHairMassageId).HasColumnName("Staff_HairMassage_Id");

                entity.Property(e => e.StaffHairdresserId).HasColumnName("Staff_Hairdresser_Id");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Vipcard).HasColumnName("VIPCard");

                entity.Property(e => e.VipcardGive)
                    .HasColumnName("VIPCardGive")
                    .HasMaxLength(10);

                entity.Property(e => e.VipcardUse)
                    .HasColumnName("VIPCardUse")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<SmBillTempFlowProduct>(entity =>
            {
                entity.ToTable("SM_BillTemp_FlowProduct");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SmBillTempFlowService>(entity =>
            {
                entity.ToTable("SM_BillTemp_FlowService");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SmBookingTemp>(entity =>
            {
                entity.ToTable("SM_BookingTemp");

                entity.HasIndex(e => e.CustomerPhone)
                    .HasName("IX_SM_BookingTemp_Phone");

                entity.HasIndex(e => new { e.CustomerPhone, e.DatedBook, e.IsDelete })
                    .HasName("IDX_20180719_4994_4993_Solution_30shinedboSM_BookingTemp");

                entity.HasIndex(e => new { e.Order, e.DatedBook, e.IsDelete, e.SalonId, e.IsCall, e.HourId, e.Id })
                    .HasName("_dta_index_SM_BookingTemp_9_2003030417__K12_K7_K8_K21_K4_K1_34");

                entity.HasIndex(e => new { e.Id, e.CustomerName, e.CustomerPhone, e.HourId, e.CreatedDate, e.ModifiedDate, e.UserId, e.Note, e.DatedBook, e.IsMakeBill, e.FlowTimeKeepingId, e.IsSms, e.Smsstatus, e.SendDate, e.Os, e.NewCustomer, e.NoteDelete, e.IsCall, e.IsCallTime, e.IsCallTimeModified, e.IsBookViaBill, e.IsBookAtSalon, e.IsAutoStylist, e.TokenClient, e.Macaddress, e.IsLimited, e.IsSetAutoStylist, e.IsBookOnline, e.IsBookStylist, e.Order, e.AuthOtpIsSet, e.AuthOtpIsVerified, e.AuthOtpToken, e.AuthOtpTimeSend, e.AuthOtpTimeVerified, e.AuthOtpSmsstatus, e.AuthOtpTimeReceived, e.TheadId, e.Version, e.SuggestSalonId, e.SmsnotiIsSend, e.SmsnotiSendTime, e.SmsnotiStatus, e.CancelBookIsSendSms, e.CancelBookTimeSend, e.CancelBookSmsstatus, e.CancelBookFrom, e.TextNote1, e.TextNote2, e.SalonNote, e.TimeSalonNote, e.Uid, e.MigrateStatus, e.CustomerScore, e.SubHour, e.IsDelete, e.SalonId, e.StylistId })
                    .HasName("IDX_20180719_13754_13753_Solution_30shinedboSM_BookingTemp");

                entity.Property(e => e.AuthOtpIsSet).HasColumnName("AuthOTP_IsSet");

                entity.Property(e => e.AuthOtpIsVerified).HasColumnName("AuthOTP_IsVerified");

                entity.Property(e => e.AuthOtpSmsstatus).HasColumnName("AuthOTP_SMSStatus");

                entity.Property(e => e.AuthOtpTimeReceived)
                    .HasColumnName("AuthOTP_TimeReceived")
                    .HasColumnType("datetime");

                entity.Property(e => e.AuthOtpTimeSend)
                    .HasColumnName("AuthOTP_TimeSend")
                    .HasColumnType("datetime");

                entity.Property(e => e.AuthOtpTimeVerified)
                    .HasColumnName("AuthOTP_TimeVerified")
                    .HasColumnType("datetime");

                entity.Property(e => e.AuthOtpToken)
                    .HasColumnName("AuthOTP_Token")
                    .HasMaxLength(8);

                entity.Property(e => e.CancelBookFrom).HasColumnName("CancelBook_From");

                entity.Property(e => e.CancelBookIsSendSms).HasColumnName("CancelBook_IsSendSMS");

                entity.Property(e => e.CancelBookSmsstatus).HasColumnName("CancelBook_SMSStatus");

                entity.Property(e => e.CancelBookTimeSend)
                    .HasColumnName("CancelBook_TimeSend")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(200);

                entity.Property(e => e.CustomerPhone).HasMaxLength(20);

                entity.Property(e => e.CustomerScore).HasDefaultValueSql("((0))");

                entity.Property(e => e.DatedBook).HasColumnType("datetime");

                entity.Property(e => e.IsCallTime).HasColumnType("datetime");

                entity.Property(e => e.IsCallTimeModified).HasColumnType("datetime");

                entity.Property(e => e.IsSms).HasColumnName("IsSMS");

                entity.Property(e => e.Macaddress)
                    .HasColumnName("MACAddress")
                    .HasMaxLength(100);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Note).HasColumnType("nvarchar(max)");

                entity.Property(e => e.NoteDelete).HasMaxLength(300);

                entity.Property(e => e.Os).HasColumnName("OS");

                entity.Property(e => e.SalonNote).HasMaxLength(300);

                entity.Property(e => e.SendDate).HasColumnType("datetime");

                entity.Property(e => e.SmsnotiIsSend).HasColumnName("SMSNoti_IsSend");

                entity.Property(e => e.SmsnotiSendTime)
                    .HasColumnName("SMSNoti_SendTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SmsnotiStatus).HasColumnName("SMSNoti_Status");

                entity.Property(e => e.Smsstatus).HasColumnName("SMSStatus");

                entity.Property(e => e.SuggestSalonId).HasColumnName("SuggestSalonID");

                entity.Property(e => e.TimeSalonNote).HasColumnType("datetime");

                entity.Property(e => e.TokenClient).HasMaxLength(100);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Version).HasMaxLength(50);
            });

            modelBuilder.Entity<SmEnrollTemp>(entity =>
            {
                entity.ToTable("SM_EnrollTemp");

                entity.HasIndex(e => new { e.IsEnroll, e.IsDelete, e.WorkDate, e.StaffId })
                    .HasName("IDX_20180715_8834_8833_Solution_30shinedboSM_EnrollTemp");

                entity.HasIndex(e => new { e.SalonId, e.IsEnroll, e.IsDelete, e.StaffId })
                    .HasName("IDX_20180715_44897_44896_Solution_30shinedboSM_EnrollTemp");

                entity.HasIndex(e => new { e.StaffId, e.IsEnroll, e.IsDelete, e.WorkDate })
                    .HasName("IDX_20180715_3_1_Solution_30shinedboSM_EnrollTemp");

                entity.HasIndex(e => new { e.Id, e.CreatedTime, e.ModifiedTime, e.IsDelete, e.Uid, e.MigrateStatus, e.WorkDate, e.StaffId, e.IsEnroll, e.SalonId })
                    .HasName("IDX_20180715_533_532_Solution_30shinedboSM_EnrollTemp");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.WorkDate).HasColumnType("date");
            });

            modelBuilder.Entity<SmEnrollTempHour>(entity =>
            {
                entity.ToTable("SM_EnrollTemp_Hour");

                entity.HasIndex(e => new { e.EnrollId, e.HourId, e.IsDelete })
                    .HasName("IDX_20180715_1693_1692_Solution_30shinedboSM_EnrollTemp_Hour");

                entity.HasIndex(e => new { e.Id, e.HourId, e.CreatedTime, e.ModifiedTime, e.Uid, e.MigrateStatus, e.EnrollId, e.IsDelete })
                    .HasName("IDX_20180715_548_547_Solution_30shinedboSM_EnrollTemp_Hour");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SocialThread>(entity =>
            {
                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(300);

                entity.Property(e => e.StType).HasColumnName("stType");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SpecialCusDetail>(entity =>
            {
                entity.HasIndex(e => new { e.CutErrorDate, e.IsDelete, e.QuantityInvited })
                    .HasName("IDX_20180718_54_53_Solution_30shinedboSpecialCusDetail");

                entity.HasIndex(e => new { e.SpecialCusId, e.IsDelete, e.QuantityInvited })
                    .HasName("IDX_20180718_13_12_Solution_30shinedboSpecialCusDetail");

                entity.HasIndex(e => new { e.CutErrorDate, e.IsDelete, e.SpecialCusId, e.QuantityInvited })
                    .HasName("IDX_20180718_50_49_Solution_30shinedboSpecialCusDetail");

                entity.HasIndex(e => new { e.CutErrorDate, e.SpecialCusId, e.IsDelete, e.QuantityInvited })
                    .HasName("IDX_20180718_118_117_Solution_30shinedboSpecialCusDetail");

                entity.HasIndex(e => new { e.SpecialCusId, e.CutErrorDate, e.IsDelete, e.QuantityInvited })
                    .HasName("IDX_20180718_120_119_Solution_30shinedboSpecialCusDetail");

                entity.HasIndex(e => new { e.SpecialCusId, e.CutErrorDate, e.QuantityInvited, e.IsDelete })
                    .HasName("IDX_20180718_52_51_Solution_30shinedboSpecialCusDetail");

                entity.HasIndex(e => new { e.Id, e.CutErrorDate, e.StaffId, e.SalonId, e.QuantityInvited, e.DiscountServices, e.DiscountCosmetic, e.Note, e.ReasonDiff, e.QuantityFree, e.IsMsm, e.Smsstatus, e.SendDate, e.Uid, e.MigrateStatus, e.SpecialCusId, e.IsDelete })
                    .HasName("IDX_20180718_15_14_Solution_30shinedboSpecialCusDetail");

                entity.HasIndex(e => new { e.Id, e.SpecialCusId, e.StaffId, e.SalonId, e.DiscountServices, e.DiscountCosmetic, e.Note, e.ReasonDiff, e.QuantityFree, e.IsMsm, e.Smsstatus, e.SendDate, e.Uid, e.MigrateStatus, e.CutErrorDate, e.IsDelete, e.QuantityInvited })
                    .HasName("IDX_20180718_116_115_Solution_30shinedboSpecialCusDetail");

                entity.Property(e => e.CutErrorDate).HasColumnType("datetime");

                entity.Property(e => e.IsMsm).HasColumnName("IsMSM");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ReasonDiff).HasMaxLength(600);

                entity.Property(e => e.SendDate).HasColumnType("datetime");

                entity.Property(e => e.Smsstatus).HasColumnName("SMSStatus");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SpecialCustomer>(entity =>
            {
                entity.HasIndex(e => new { e.CustomerId, e.Publish, e.Id, e.CustomerTypeId })
                    .HasName("_dta_index_SpecialCustomer_9_1698533205__K2_K9_K1_K3");

                entity.Property(e => e.BrowseId).HasColumnName("Browse_ID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.IsSms).HasColumnName("IsSMS");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.SendDate).HasColumnType("datetime");

                entity.Property(e => e.Smsstatus).HasColumnName("SMSStatus");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Staff>(entity =>
            {
                entity.HasIndex(e => e.Type);

                entity.HasIndex(e => new { e.Fullname, e.Id })
                    .HasName("_dta_index_Staff_9_1185865270__K1_14");

                entity.HasIndex(e => new { e.Id, e.StudentId })
                    .HasName("IDX_20180718_126_125_Solution_30shinedboStaff");

                entity.HasIndex(e => new { e.Active, e.OrderCode, e.IsDelete })
                    .HasName("IDX_20180718_25_24_Solution_30shinedboStaff");

                entity.HasIndex(e => new { e.Email, e.Active, e.Password })
                    .HasName("IDX_162714_162713_Solution_30shinedboStaff");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.StudentId })
                    .HasName("IDX_20180718_122_121_Solution_30shinedboStaff");

                entity.HasIndex(e => new { e.SalonId, e.Id, e.Fullname })
                    .HasName("_dta_index_Staff_9_1185865270__K25_K1_K14");

                entity.HasIndex(e => new { e.Id, e.Code, e.Fullname, e.Type })
                    .HasName("IDX_20180719_4986_4985_Solution_30shinedboStaff");

                entity.HasIndex(e => new { e.Id, e.Fullname, e.IsDelete, e.Type, e.Active })
                    .HasName("IDX_20180718_2486_2485_Solution_30shinedboStaff");

                entity.HasIndex(e => new { e.SalonId, e.TeamId, e.Type, e.Active, e.IsDelete })
                    .HasName("IDX_20180719_13186_13185_Solution_30shinedboStaff");

                entity.HasIndex(e => new { e.Id, e.Fullname, e.IsAccountLogin, e.IsAppLogin, e.Active, e.IsDelete })
                    .HasName("IDX_20180718_682_681_Solution_30shinedboStaff");

                entity.HasIndex(e => new { e.Id, e.Fullname, e.TeamId, e.GroupLevelId, e.Active, e.SalonId, e.IsDelete, e.Type })
                    .HasName("_dta_index_Staff_9_1185865270__K13_K25_K21_K7_1_14_42_50");

                entity.HasIndex(e => new { e.Id, e.Phone, e.Email, e.Active, e.Fullname, e.IsDelete, e.SalonId, e.SkillLevel, e.DateJoin, e.NgayTinhThamNien, e.Type })
                    .HasName("IDX_20180715_163958_163957_Solution_30shinedboStaff");

                entity.HasIndex(e => new { e.Id, e.Type, e.Phone, e.Email, e.Fullname, e.SnDay, e.SnMonth, e.SalonId, e.SkillLevel, e.NgayTinhThamNien, e.Active, e.IsDelete })
                    .HasName("IDX_20180715_163340_163339_Solution_30shinedboStaff");

                entity.Property(e => e.About).HasColumnType("ntext");

                entity.Property(e => e.Address).HasMaxLength(600);

                entity.Property(e => e.AvartarBooking).HasMaxLength(300);

                entity.Property(e => e.Avatar).HasMaxLength(300);

                entity.Property(e => e.BirthDay).HasColumnType("date");

                entity.Property(e => e.Ceo).HasColumnName("CEO");

                entity.Property(e => e.Cmtimg1)
                    .HasColumnName("CMTimg1")
                    .HasMaxLength(300);

                entity.Property(e => e.Cmtimg2)
                    .HasColumnName("CMTimg2")
                    .HasMaxLength(300);

                entity.Property(e => e.Code).HasColumnType("nchar(10)");

                entity.Property(e => e.CutTimeTb).HasColumnName("CutTimeTB");

                entity.Property(e => e.DateJoin).HasColumnType("date");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.FingerToken).HasMaxLength(20);

                entity.Property(e => e.FirstName).HasMaxLength(50);

                entity.Property(e => e.Fullname).HasMaxLength(200);

                entity.Property(e => e.IdhomeTown)
                    .HasColumnName("IDHomeTown")
                    .HasMaxLength(255);

                entity.Property(e => e.IdprovidedDate)
                    .HasColumnName("IDProvidedDate")
                    .HasColumnType("date");

                entity.Property(e => e.IdproviderLocale)
                    .HasColumnName("IDProviderLocale")
                    .HasMaxLength(225);

                entity.Property(e => e.Images).HasColumnType("ntext");

                entity.Property(e => e.IsAccountLogin).HasColumnName("isAccountLogin");

                entity.Property(e => e.IsAppLogin).HasColumnName("isAppLogin");

                entity.Property(e => e.Job).HasMaxLength(300);

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Mst)
                    .HasColumnName("MST")
                    .HasMaxLength(50);

                entity.Property(e => e.NameCmt)
                    .HasColumnName("NameCMT")
                    .HasMaxLength(200);

                entity.Property(e => e.NganHangChiNhanh)
                    .HasColumnName("NganHang_ChiNhanh")
                    .HasMaxLength(600);

                entity.Property(e => e.NganHangSoTk)
                    .HasColumnName("NganHang_SoTK")
                    .HasMaxLength(50);

                entity.Property(e => e.NganHangTen)
                    .HasColumnName("NganHang_Ten")
                    .HasMaxLength(300);

                entity.Property(e => e.NganHangTenTk)
                    .HasColumnName("NganHang_TenTK")
                    .HasMaxLength(200);

                entity.Property(e => e.NgayTinhThamNien).HasColumnType("date");

                entity.Property(e => e.NguoiGioiThieuId).HasColumnName("NguoiGioiThieuID");

                entity.Property(e => e.NumberInsurrance).HasMaxLength(50);

                entity.Property(e => e.OrderCode).HasMaxLength(10);

                entity.Property(e => e.Password).HasMaxLength(50);

                entity.Property(e => e.Permission).HasMaxLength(50);

                entity.Property(e => e.PermissionId)
                    .HasColumnName("PermissionID")
                    .HasMaxLength(300);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.Property(e => e.S4mclassId).HasColumnName("S4MClassId");

                entity.Property(e => e.Score).HasDefaultValueSql("((0))");

                entity.Property(e => e.SnDay).HasColumnName("SN_day");

                entity.Property(e => e.SnMonth).HasColumnName("SN_month");

                entity.Property(e => e.SnYear).HasColumnName("SN_year");

                entity.Property(e => e.StaffId)
                    .HasColumnName("StaffID")
                    .HasMaxLength(50);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.UngvienId).HasColumnName("UngvienID");
            });

            modelBuilder.Entity<StaffAutoCondition>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Kcs).HasColumnName("KCS");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifieldTime).HasColumnType("datetime");

                entity.Property(e => e.SDate).HasColumnName("sDate");

                entity.Property(e => e.SkillLevelId).HasColumnName("SkillLevel_Id");

                entity.Property(e => e.StaffId).HasColumnName("staff_Id");

                entity.Property(e => e.StaffTypeId).HasColumnName("StaffType_Id");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<StaffAutoLevelLog>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Kcs).HasColumnName("KCS");

                entity.Property(e => e.LevelupId).HasColumnName("Levelup_Id");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifieldTime).HasColumnType("datetime");

                entity.Property(e => e.SDate)
                    .HasColumnName("sDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.SalonId).HasColumnName("Salon_Id");

                entity.Property(e => e.SkillLevelId).HasColumnName("SkillLevel_Id");

                entity.Property(e => e.SkillLevelupId).HasColumnName("SkillLevelup_Id");

                entity.Property(e => e.StaffId).HasColumnName("Staff_Id");

                entity.Property(e => e.StaffTypeId).HasColumnName("StaffType_Id");

                entity.Property(e => e.TslDanhGia)
                    .HasColumnName("TSL_DanhGia")
                    .HasMaxLength(300);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<StaffAutoLevelup>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Kcs).HasColumnName("KCS");

                entity.Property(e => e.LevelupId).HasColumnName("Levelup_Id");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifieldTime).HasColumnType("datetime");

                entity.Property(e => e.SDate)
                    .HasColumnName("sDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.SalonId).HasColumnName("Salon_Id");

                entity.Property(e => e.SkillLevelId).HasColumnName("SkillLevel_Id");

                entity.Property(e => e.SkillLevelupId).HasColumnName("SkillLevelup_Id");

                entity.Property(e => e.StaffId).HasColumnName("Staff_Id");

                entity.Property(e => e.StaffTypeId).HasColumnName("StaffType_Id");

                entity.Property(e => e.TslDanhGia)
                    .HasColumnName("TSL_DanhGia")
                    .HasMaxLength(300);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<StaffAvatar>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Images).HasMaxLength(300);

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<StaffBillServiceDetail>(entity =>
            {
                entity.HasIndex(e => new { e.BillId, e.StaffId })
                    .HasName("IDX_20180715_2051_2050_Solution_30shinedboStaffBillServiceDetail");

                entity.HasIndex(e => new { e.BillId, e.StaffId, e.DepartmentId, e.OvertimeStatusValue, e.IsDelete, e.WorkDate })
                    .HasName("IDX_20180715_80041_80040_Solution_30shinedboStaffBillServiceDetail");

                entity.Property(e => e.BillSalonId).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.ServiceBonus).HasDefaultValueSql("((0))");

                entity.Property(e => e.WorkDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<StaffContractMap>(entity =>
            {
                entity.Property(e => e.ContractDate).HasColumnType("date");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.SignDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<StaffErrorSpecailCus>(entity =>
            {
                entity.Property(e => e.Createdate).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.SpecialCusId).HasColumnName("SpecialCus_ID");

                entity.Property(e => e.StaffId).HasColumnName("Staff_ID");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<StaffFluctuations>(entity =>
            {
                entity.ToTable("Staff_Fluctuations");

                entity.HasIndex(e => new { e.Id, e.CreatedDate, e.StaffId, e.StatusId, e.IsDetele })
                    .HasName("IDX_20180715_162851_162850_Solution_30shinedboStaff_Fluctuations");

                entity.HasIndex(e => new { e.Id, e.StaffId, e.CreatedDate, e.StatusId, e.IsDetele })
                    .HasName("IDX_20180715_162855_162854_Solution_30shinedboStaff_Fluctuations");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.StaffName).HasMaxLength(100);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<StaffMistake>(entity =>
            {
                entity.ToTable("Staff_Mistake");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasMaxLength(255);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<StaffProcedure>(entity =>
            {
                entity.ToTable("Staff.Procedure");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Thumb).HasMaxLength(400);

                entity.Property(e => e.Title).HasMaxLength(600);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<StaffProfileMap>(entity =>
            {
                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.StaffId).HasDefaultValueSql("((0))");

                entity.Property(e => e.StatusProfileId).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<StaffRanking>(entity =>
            {
                entity.HasKey(e => e.StaffId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("Staff_Ranking");

                entity.HasIndex(e => e.StaffId)
                    .HasName("IX_Staff_Ranking")
                    .ForSqlServerIsClustered();

                entity.Property(e => e.StaffId)
                    .HasColumnName("StaffID")
                    .ValueGeneratedNever();

                entity.Property(e => e.GiaotiepDongnghiep).HasColumnName("GIAOTIEP_DONGNGHIEP");

                entity.Property(e => e.GiaotiepKhachhang).HasColumnName("GIAOTIEP_KHACHHANG");

                entity.Property(e => e.GiaotiepQuanly).HasColumnName("GIAOTIEP_QUANLY");

                entity.Property(e => e.Ngoaihinh).HasColumnName("NGOAIHINH");

                entity.Property(e => e.TuvanHoachat).HasColumnName("TUVAN_HOACHAT");

                entity.Property(e => e.TuvanMypham).HasColumnName("TUVAN_MYPHAM");

                entity.Property(e => e.TuvantocDongnghiep).HasColumnName("TUVANTOC_DONGNGHIEP");

                entity.Property(e => e.TuvantocKhachhang).HasColumnName("TUVANTOC_KHACHHANG");
            });

            modelBuilder.Entity<StaffRoll>(entity =>
            {
                entity.ToTable("Staff_Roll");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<StaffSalonHistory>(entity =>
            {
                entity.ToTable("Staff_Salon_History");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.TransferDate).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<StaffType>(entity =>
            {
                entity.ToTable("Staff_Type");

                entity.Property(e => e.Description).HasMaxLength(300);

                entity.Property(e => e.Meta).HasMaxLength(300);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.SkillLevelIds).HasMaxLength(300);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<StaticExpense>(entity =>
            {
                entity.HasIndex(e => new { e.SalonId, e.TotalSales, e.TotalServiceProfit, e.TotalCosmeticProfit, e.TotalProductPrice, e.TotalStaffSalary, e.Mktexpense, e.Itexpense, e.TotalIncomeAfterTax, e.NumberOfTurns, e.TotalTransactionPerCus, e.TotalServicePerCus, e.TotalProductPerCus, e.DirectFee, e.SalaryServicePerServiceIncome, e.TotalProductCapital, e.TotalPayOffKcs, e.TotalDailyCostInventory, e.ElectricityAndWaterBill, e.SalonFee, e.RentWithTax, e.CapitalSpending, e.TotalSmsexpenses, e.ShippingExpend, e.InternetAndPhoneBill, e.SocialInsuranceAndFixedCost, e.Tax, e.IncomeTaxes, e.SalonUnplannedSpending, e.ManageFee, e.OfficeRentAndServiceCose, e.OfficeStaffSalary, e.SalesSalary, e.OfficeStaffSocialInsurance, e.UnplannedSpending, e.Compensation, e.WorkDate })
                    .HasName("IDX_20180715_163931_163930_Solution_30shinedboStaticExpense");

                entity.HasIndex(e => new { e.TotalSales, e.TotalServiceProfit, e.TotalCosmeticProfit, e.TotalProductPrice, e.TotalStaffSalary, e.Mktexpense, e.Itexpense, e.TotalIncomeAfterTax, e.NumberOfTurns, e.TotalTransactionPerCus, e.TotalServicePerCus, e.TotalProductPerCus, e.DirectFee, e.SalaryServicePerServiceIncome, e.TotalProductCapital, e.TotalPayOffKcs, e.TotalDailyCostInventory, e.ElectricityAndWaterBill, e.SalonFee, e.RentWithTax, e.CapitalSpending, e.TotalSmsexpenses, e.ShippingExpend, e.InternetAndPhoneBill, e.SocialInsuranceAndFixedCost, e.Tax, e.IncomeTaxes, e.SalonUnplannedSpending, e.ManageFee, e.OfficeRentAndServiceCose, e.OfficeStaffSalary, e.SalesSalary, e.OfficeStaffSocialInsurance, e.UnplannedSpending, e.Compensation, e.SalonId, e.WorkDate })
                    .HasName("IDX_20180715_163929_163928_Solution_30shinedboStaticExpense");

                entity.HasIndex(e => new { e.Id, e.SalonId, e.ShortName, e.TotalSales, e.TotalServiceProfit, e.TotalCosmeticProfit, e.ProductSalary, e.TotalProductPrice, e.TotalStaffSalary, e.TotalProductUsed, e.OtherExpense, e.Mktexpense, e.Itexpense, e.DayExpense, e.Lngall30shine, e.CreatedTime, e.Uid, e.MigrateStatus, e.TotalIncomeAfterTax, e.NumberOfTurns, e.TotalTransactionPerCus, e.TotalServicePerCus, e.TotalProductPerCus, e.DirectFee, e.SalaryServicePerServiceIncome, e.TotalProductCapital, e.TotalPayOffKcs, e.TotalDailyCostInventory, e.ElectricityAndWaterBill, e.SalonFee, e.RentWithTax, e.CapitalSpending, e.TotalSmsexpenses, e.ShippingExpend, e.InternetAndPhoneBill, e.SocialInsuranceAndFixedCost, e.Tax, e.IncomeTaxes, e.SalonUnplannedSpending, e.ManageFee, e.OfficeRentAndServiceCose, e.OfficeStaffSalary, e.SalesSalary, e.OfficeStaffSocialInsurance, e.UnplannedSpending, e.ModifyTime, e.Compensation, e.WorkDate })
                    .HasName("IDX_20180715_163927_163926_Solution_30shinedboStaticExpense");

                entity.Property(e => e.CapitalSpending).HasDefaultValueSql("((0))");

                entity.Property(e => e.Compensation).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.DirectFee).HasDefaultValueSql("((0))");

                entity.Property(e => e.ElectricityAndWaterBill).HasDefaultValueSql("((0))");

                entity.Property(e => e.IncomeTaxes).HasDefaultValueSql("((0))");

                entity.Property(e => e.InternetAndPhoneBill).HasDefaultValueSql("((0))");

                entity.Property(e => e.Itexpense).HasColumnName("ITExpense");

                entity.Property(e => e.Lngall30shine).HasColumnName("LNGAll30shine");

                entity.Property(e => e.ManageFee).HasDefaultValueSql("((0))");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Mktexpense).HasColumnName("MKTExpense");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.NumberOfTurns).HasDefaultValueSql("((0))");

                entity.Property(e => e.OfficeRentAndServiceCose).HasDefaultValueSql("((0))");

                entity.Property(e => e.OfficeStaffSalary).HasDefaultValueSql("((0))");

                entity.Property(e => e.OfficeStaffSocialInsurance).HasDefaultValueSql("((0))");

                entity.Property(e => e.RentWithTax).HasDefaultValueSql("((0))");

                entity.Property(e => e.SalaryServicePerServiceIncome).HasDefaultValueSql("((0))");

                entity.Property(e => e.SalesSalary).HasDefaultValueSql("((0))");

                entity.Property(e => e.SalonFee).HasDefaultValueSql("((0))");

                entity.Property(e => e.SalonUnplannedSpending).HasDefaultValueSql("((0))");

                entity.Property(e => e.ShippingExpend).HasDefaultValueSql("((0))");

                entity.Property(e => e.ShortName).HasColumnType("nchar(20)");

                entity.Property(e => e.SocialInsuranceAndFixedCost).HasDefaultValueSql("((0))");

                entity.Property(e => e.Tax).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalDailyCostInventory).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalIncomeAfterTax).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalPayOffKcs)
                    .HasColumnName("TotalPayOffKCS")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalProductCapital).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalProductPerCus).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalServicePerCus).HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalSmsexpenses)
                    .HasColumnName("TotalSMSExpenses")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalTransactionPerCus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.UnplannedSpending).HasDefaultValueSql("((0))");

                entity.Property(e => e.WorkDate).HasColumnType("date");
            });

            modelBuilder.Entity<StaticOperate>(entity =>
            {
                entity.HasIndex(e => e.WorkDate)
                    .HasName("IX_StaticOperate_Workdate");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.WorkDate).HasColumnType("date");
            });

            modelBuilder.Entity<StaticRatingWaitTime>(entity =>
            {
                entity.HasIndex(e => e.StaffId)
                    .HasName("IX_StaticRatingWaitTime_staffid");

                entity.HasIndex(e => e.WorkDate)
                    .HasName("IX_StaticRatingWaitTime_wd");

                entity.HasIndex(e => new { e.StaffId, e.Star1, e.Star2, e.Star3, e.Star4, e.Star5, e.TotalBillService, e.IsDelete, e.StaffType, e.WorkDate })
                    .HasName("IDX_20180715_3340_3339_Solution_30shinedboStaticRatingWaitTime");

                entity.HasIndex(e => new { e.StaffId, e.Star1, e.Star2, e.Star3, e.Star4, e.Star5, e.SpeedCut, e.BillNotImages, e.BillErrors, e.Before15p, e.Between15pAnd20p, e.After20p, e.NotTime, e.TotalShinecombo, e.TotalBillService, e.IsDelete, e.StaffType, e.WorkDate })
                    .HasName("IDX_20180715_693_692_Solution_30shinedboStaticRatingWaitTime");

                entity.HasIndex(e => new { e.StaffId, e.Star1, e.Star2, e.Star3, e.Star4, e.Star5, e.SpeedCut, e.BillNotImages, e.BillErrors, e.Before15p, e.Between15pAnd20p, e.After20p, e.NotTime, e.TotalShinecombo, e.TotalBillService, e.WorkDate, e.IsDelete, e.StaffType })
                    .HasName("IDX_20180715_210_209_Solution_30shinedboStaticRatingWaitTime");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.WorkDate).HasColumnType("date");
            });

            modelBuilder.Entity<StaticServicesProfit>(entity =>
            {
                entity.ToTable("StaticServices_Profit");

                entity.HasIndex(e => e.WorkDate)
                    .HasName("IX_StaticServices_Profit_Workdate");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Scscavg).HasColumnName("SCSCAVG");

                entity.Property(e => e.Scscbill).HasColumnName("SCSCBill");

                entity.Property(e => e.Scscpoint).HasColumnName("SCSCPoint");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.WorkDate).HasColumnType("date");
            });

            modelBuilder.Entity<StatictisReportOperation>(entity =>
            {
                entity.ToTable("statictisReportOperation");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.IsDelete).HasColumnName("isDelete");

                entity.Property(e => e.NumberBill1Star)
                    .HasColumnName("numberBill1Star")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberBill2Star)
                    .HasColumnName("numberBill2Star")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberBill3Star)
                    .HasColumnName("numberBill3Star")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberBill4Star)
                    .HasColumnName("numberBill4Star")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberBill5Star)
                    .HasColumnName("numberBill5Star")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberBillKhl)
                    .HasColumnName("numberBillKHL")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberBillNotImg)
                    .HasColumnName("numberBillNotImg")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberBillWaited)
                    .HasColumnName("numberBillWaited")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberBooking)
                    .HasColumnName("numberBooking")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberBookingBefor)
                    .HasColumnName("numberBookingBefor")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberCanleBooking)
                    .HasColumnName("numberCanleBooking")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberCustomerOld)
                    .HasColumnName("numberCustomerOld")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberDeleteBooking)
                    .HasColumnName("numberDeleteBooking")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberErrorMonitoring)
                    .HasColumnName("numberErrorMonitoring")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberOfTurns)
                    .HasColumnName("numberOfTurns")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberSkinnerTimekeeping)
                    .HasColumnName("numberSkinnerTimekeeping")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberSlotBooking)
                    .HasColumnName("numberSlotBooking")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberStylist)
                    .HasColumnName("numberStylist")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NumberStylistTimekeeping)
                    .HasColumnName("numberStylistTimekeeping")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.OvertimeHoursStylist)
                    .HasColumnName("overtimeHoursStylist")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SalonId).HasDefaultValueSql("((0))");

                entity.Property(e => e.SalonName)
                    .HasColumnName("salonName")
                    .HasMaxLength(500);

                entity.Property(e => e.TotalExFoliation)
                    .HasColumnName("totalExFoliation")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalGroupColorCombo)
                    .HasColumnName("totalGroupColorCombo")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalGroupUonDuoi)
                    .HasColumnName("totalGroupUonDuoi")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalIncome)
                    .HasColumnName("totalIncome")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalKidCombo)
                    .HasColumnName("totalKidCombo")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalMask)
                    .HasColumnName("totalMask")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalProductIncome)
                    .HasColumnName("totalProductIncome")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalProtein)
                    .HasColumnName("totalProtein")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalServiceInCome)
                    .HasColumnName("totalServiceInCome")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalShineCombo)
                    .HasColumnName("totalShineCombo")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalStarNumber)
                    .HasColumnName("totalStarNumber")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.WorkDate).HasColumnType("datetime");

                entity.Property(e => e.WorktimeStylist)
                    .HasColumnName("worktimeStylist")
                    .HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<StatisticSalaryProduct>(entity =>
            {
                entity.HasIndex(e => new { e.Id, e.SalonId, e.ProductIds, e.Total, e.TotalMoney, e.CreatedTime, e.ModifiedTime, e.Uid, e.MigrateStatus, e.StaffId, e.IsDelete, e.WorkDate })
                    .HasName("IDX_20180715_23_22_Solution_30shinedboStatisticSalaryProduct");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.WorkDate).HasColumnType("date");
            });

            modelBuilder.Entity<StatisticSalaryService>(entity =>
            {
                entity.HasIndex(e => new { e.Id, e.SalonId, e.ServiceIds, e.Total, e.TotalMoney, e.CreatedTime, e.ModifiedTime, e.Uid, e.MigrateStatus, e.StaffId, e.IsDelete, e.WorkDate })
                    .HasName("IDX_20180715_15_14_Solution_30shinedboStatisticSalaryService");

                entity.HasIndex(e => new { e.Id, e.Total, e.TotalMoney, e.CreatedTime, e.ModifiedTime, e.IsDelete, e.Uid, e.MigrateStatus, e.StaffId, e.SalonId, e.ServiceIds, e.WorkDate })
                    .HasName("IDX_20180715_1236_1235_Solution_30shinedboStatisticSalaryService");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.WorkDate).HasColumnType("date");
            });

            modelBuilder.Entity<StatisticScscError>(entity =>
            {
                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.SalonId).HasDefaultValueSql("((0))");

                entity.Property(e => e.StylistId).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongAnhLoiScsc).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongAnhMoScsc).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongAnhUon).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongBillUon).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongDiemScsc).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongErrorComplatetion).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongErrorHairRoots).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongErrorHairTip).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongErrorHairWaves).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongErrorShape).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongErrorSharpNess).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongKhongAnhScsc).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongKhongAnhUon).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongKhongMoScsc).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongLoiScscHoacUon).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongLoiUon).HasDefaultValueSql("((0))");

                entity.Property(e => e.TongMoLechUon).HasDefaultValueSql("((0))");

                entity.Property(e => e.WorkDate).HasColumnType("date");
            });

            modelBuilder.Entity<StatisticsXuatVatTu>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.ExportGoodsId });

                entity.ToTable("Statistics_XuatVatTu");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ExportGoodsId).HasColumnName("ExportGoods_ID");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateXuatVatTu)
                    .HasColumnName("Date_XuatVatTu")
                    .HasColumnType("date");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.ProductId).HasColumnName("Product_ID");

                entity.Property(e => e.SalonId)
                    .HasColumnName("Salon_ID")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.StaffId)
                    .HasColumnName("Staff_ID")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalBillXuatVatTu)
                    .HasColumnName("TotalBill_XuatVatTu")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<StyleMaster>(entity =>
            {
                entity.HasIndex(e => e.BillId)
                    .HasName("IDX_20180720_5149_5148_Solution_30shinedboStyleMaster");

                entity.HasIndex(e => new { e.PostNumber, e.IsDelete })
                    .HasName("IDX_20180718_1191_1190_Solution_30shinedboStyleMaster");

                entity.HasIndex(e => new { e.StylistId, e.StyleMasterStatusId })
                    .HasName("IDX_20180715_162653_162651_Solution_30shinedboStyleMaster");

                entity.HasIndex(e => new { e.Id, e.PostNumber, e.StylistName, e.Image1, e.Image2, e.Image3, e.Image4, e.TotalLike, e.StyleMasterStatusId, e.IsDelete, e.ApproveTime })
                    .HasName("IDX_20180715_162229_162228_Solution_30shinedboStyleMaster");

                entity.HasIndex(e => new { e.Id, e.PostNumber, e.StylistName, e.Image1, e.Image2, e.Image3, e.Image4, e.TotalLike, e.StyleMasterStatusId, e.IsDelete, e.CreatedTime })
                    .HasName("IDX_20180715_162223_162222_Solution_30shinedboStyleMaster");

                entity.Property(e => e.ApproveTime).HasColumnType("datetime");

                entity.Property(e => e.AutoLikeNumber).HasDefaultValueSql("((0))");

                entity.Property(e => e.BillId).HasColumnName("BillID");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Image1).HasMaxLength(300);

                entity.Property(e => e.Image2).HasMaxLength(300);

                entity.Property(e => e.Image3).HasMaxLength(300);

                entity.Property(e => e.Image4).HasMaxLength(300);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.PostNumber).HasMaxLength(50);

                entity.Property(e => e.StyleMasterStatusId).HasDefaultValueSql("((2))");

                entity.Property(e => e.StylistName).HasMaxLength(250);

                entity.Property(e => e.TotalLike).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<StyleMasterLog>(entity =>
            {
                entity.ToTable("StyleMaster_Log");

                entity.HasIndex(e => new { e.Id, e.StyleMasterId, e.PostNumber, e.StaffId })
                    .HasName("IDX_20180715_162227_162226_Solution_30shinedboStyleMaster_Log");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.PostNumber).HasMaxLength(50);

                entity.Property(e => e.PostedDate).HasColumnType("date");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<StyleMasterStatus>(entity =>
            {
                entity.ToTable("StyleMaster_Status");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.IsDelete).HasColumnName("isDelete");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(150);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Stylist4MenBillCutFree>(entity =>
            {
                entity.ToTable("Stylist4Men_BillCutFree");

                entity.HasIndex(e => e.StudentId)
                    .HasName("IDX_20180715_51471_51470_Solution_30shinedboStylist4Men_BillCutFree");

                entity.HasIndex(e => new { e.Id, e.CreatedTime, e.UploadImageTime, e.HairStyleId, e.StudentId })
                    .HasName("IDX_20180715_51476_51475_Solution_30shinedboStylist4Men_BillCutFree");

                entity.HasIndex(e => new { e.Id, e.CustomerId, e.Images, e.CreatedTime, e.ModifiedTime, e.ClassId, e.ImageStatusId, e.ImageChecked1, e.ImageChecked2, e.ImageChecked3, e.ImageChecked4, e.ErrorNote, e.NoteByStylist, e.Uid, e.MigrateStatus, e.UploadImageTime, e.HairStyleId, e.StudentId, e.IsDelete })
                    .HasName("IDX_20180715_1867_1866_Solution_30shinedboStylist4Men_BillCutFree");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ErrorNote).HasMaxLength(600);

                entity.Property(e => e.ImageChecked1).HasMaxLength(600);

                entity.Property(e => e.ImageChecked2).HasMaxLength(600);

                entity.Property(e => e.ImageChecked3).HasMaxLength(600);

                entity.Property(e => e.ImageChecked4).HasMaxLength(600);

                entity.Property(e => e.ImageStatusId).HasMaxLength(100);

                entity.Property(e => e.Images).HasMaxLength(600);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.NoteByStylist).HasMaxLength(600);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.UploadImageTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<Stylist4MenClass>(entity =>
            {
                entity.ToTable("Stylist4Men_Class");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.GraduateTime).HasColumnType("date");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.StartTime).HasColumnType("date");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Stylist4MenCredits>(entity =>
            {
                entity.ToTable("Stylist4Men_Credits");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Stylist4MenCustomer>(entity =>
            {
                entity.ToTable("Stylist4Men_Customer");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(100);

                entity.Property(e => e.CustomerPhone).HasMaxLength(100);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Stylist4MenGraduationScore>(entity =>
            {
                entity.ToTable("Stylist4Men_GraduationScore");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Stylist4MenPointClubs>(entity =>
            {
                entity.ToTable("Stylist4Men_PointClubs");

                entity.HasIndex(e => e.StaffId)
                    .HasName("IDX_20180720_5095_5094_Solution_30shinedboStylist4Men_PointClubs");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Stylist4MenStudent>(entity =>
            {
                entity.ToTable("Stylist4Men_Student");

                entity.HasIndex(e => new { e.Phone, e.IsDelete })
                    .HasName("IDX_20180719_5004_5003_Solution_30shinedboStylist4Men_Student");

                entity.HasIndex(e => new { e.Email, e.Password, e.Publish, e.IsDelete, e.IsAccept })
                    .HasName("IDX_20180718_4769_4768_Solution_30shinedboStylist4Men_Student");

                entity.HasIndex(e => new { e.Id, e.ClassId, e.StudyPackageId, e.SalonId, e.StatusTuition, e.IsDelete, e.IsAccept })
                    .HasName("IDX_20180718_128_127_Solution_30shinedboStylist4Men_Student");

                entity.HasIndex(e => new { e.Id, e.Fullname, e.Email, e.ClassId, e.TotalAmountCollected, e.TotalAmountPaid, e.TotalBill, e.StudyPackageId, e.PointLtcat, e.Vote, e.PointTheoryCut, e.SalonId, e.StatusTuition, e.PointLtkn, e.IsDelete, e.IsAccept })
                    .HasName("IDX_20180718_124_123_Solution_30shinedboStylist4Men_Student");

                entity.Property(e => e.Address).HasMaxLength(600);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Fullname).HasMaxLength(200);

                entity.Property(e => e.ImageCmt1)
                    .HasColumnName("ImageCMT1")
                    .HasMaxLength(200);

                entity.Property(e => e.ImageCmt2)
                    .HasColumnName("ImageCMT2")
                    .HasMaxLength(200);

                entity.Property(e => e.Images).HasMaxLength(300);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.NumberCmt)
                    .HasColumnName("NumberCMT")
                    .HasMaxLength(20);

                entity.Property(e => e.Password).HasMaxLength(50);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.Property(e => e.PointLtcat).HasColumnName("PointLTCat");

                entity.Property(e => e.PointLtkn).HasColumnName("PointLTKN");

                entity.Property(e => e.SnDay).HasColumnName("SN_day");

                entity.Property(e => e.SnMonth).HasColumnName("SN_month");

                entity.Property(e => e.SnYear).HasColumnName("SN_year");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Stylist4MenStudyPackage>(entity =>
            {
                entity.ToTable("Stylist4Men_StudyPackage");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiledTime).HasColumnType("datetime");

                entity.Property(e => e.NamePackage).HasMaxLength(100);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Stylist4MenTuition>(entity =>
            {
                entity.ToTable("Stylist4Men_Tuition");

                entity.Property(e => e.CreaetedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiledTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SuKienTeam>(entity =>
            {
                entity.ToTable("SuKien_Team");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.TeamName).HasMaxLength(50);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SuKienTeamStaff>(entity =>
            {
                entity.ToTable("SuKien_Team_Staff");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SurveyContent>(entity =>
            {
                entity.Property(e => e.Content).IsRequired();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Pid).HasColumnName("PId");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SurveyFeedback>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Feedback).HasColumnName("feedback");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.StaffId).HasColumnName("staffId");

                entity.Property(e => e.SurveyMapId).HasColumnName("surveyMapId");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SurveyMapQa>(entity =>
            {
                entity.ToTable("SurveyMapQA");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblCategory>(entity =>
            {
                entity.ToTable("Tbl_Category");

                entity.Property(e => e.Description).HasMaxLength(600);

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Meta).HasMaxLength(600);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.Thumb).HasMaxLength(400);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblConfig>(entity =>
            {
                entity.ToTable("Tbl_Config");

                entity.Property(e => e.Description).HasMaxLength(600);

                entity.Property(e => e.Hint).HasMaxLength(50);

                entity.Property(e => e.Key).HasMaxLength(50);

                entity.Property(e => e.Label).HasMaxLength(300);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblConfigWorkDays>(entity =>
            {
                entity.ToTable("Tbl_ConfigWorkDays");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Publish).HasDefaultValueSql("((1))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblCusInputBooking>(entity =>
            {
                entity.ToTable("tbl_CusInputBooking");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CusName).HasMaxLength(50);

                entity.Property(e => e.CusPhone).HasMaxLength(20);

                entity.Property(e => e.DateBook).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasMaxLength(50);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblEmailReport>(entity =>
            {
                entity.ToTable("Tbl_EmailReport");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(300);

                entity.Property(e => e.Permission).HasMaxLength(50);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblFormMisstake>(entity =>
            {
                entity.ToTable("tbl_Form_Misstake");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Formality).HasMaxLength(100);

                entity.Property(e => e.IsDelete).HasColumnName("isDelete");

                entity.Property(e => e.IsPuslish)
                    .HasColumnName("isPuslish")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Slug)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblGroupProduct>(entity =>
            {
                entity.ToTable("Tbl_GroupProduct");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblInformationStaffClub>(entity =>
            {
                entity.ToTable("Tbl_InformationStaffClub");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Images1).HasMaxLength(300);

                entity.Property(e => e.Images2).HasMaxLength(300);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblListBack30Day>(entity =>
            {
                entity.ToTable("tbl_ListBack30Day");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CusName).HasMaxLength(50);

                entity.Property(e => e.CusPhone).HasMaxLength(20);

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModyfiDate).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblMedia>(entity =>
            {
                entity.ToTable("Tbl_Media");

                entity.Property(e => e.Folder).HasMaxLength(300);

                entity.Property(e => e.Key).HasMaxLength(100);

                entity.Property(e => e.Meta).HasMaxLength(200);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Value).HasMaxLength(300);
            });

            modelBuilder.Entity<TblPayon>(entity =>
            {
                entity.ToTable("Tbl_Payon");

                entity.Property(e => e.Hint).HasMaxLength(200);

                entity.Property(e => e.Key).HasMaxLength(200);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblPermission>(entity =>
            {
                entity.HasKey(e => e.PId);

                entity.ToTable("Tbl_Permission");

                entity.Property(e => e.PId).HasColumnName("pID");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.PCreatedDate)
                    .HasColumnName("pCreatedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.PName)
                    .HasColumnName("pName")
                    .HasMaxLength(150);

                entity.Property(e => e.POder).HasColumnName("pOder");

                entity.Property(e => e.PPublish).HasColumnName("pPublish");

                entity.Property(e => e.PTitle)
                    .HasColumnName("pTitle")
                    .HasMaxLength(150);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblPermissionAction>(entity =>
            {
                entity.HasKey(e => e.AId);

                entity.ToTable("Tbl_Permission_Action");

                entity.Property(e => e.AId).HasColumnName("aID");

                entity.Property(e => e.ADescription).HasColumnName("aDescription");

                entity.Property(e => e.AName)
                    .HasColumnName("aName")
                    .HasMaxLength(150);

                entity.Property(e => e.APublish).HasColumnName("aPublish");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblPermissionDefaultPage>(entity =>
            {
                entity.ToTable("Tbl_Permission_DefaultPage");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.MenuId).HasColumnName("MenuID");

                entity.Property(e => e.MenuLink).HasMaxLength(150);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.PId).HasColumnName("pID");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblPermissionMap>(entity =>
            {
                entity.HasKey(e => e.MapId);

                entity.ToTable("Tbl_Permission_Map");

                entity.Property(e => e.MapId).HasColumnName("mapID");

                entity.Property(e => e.AId)
                    .HasColumnName("aID")
                    .HasMaxLength(200);

                entity.Property(e => e.MId).HasColumnName("mID");

                entity.Property(e => e.MName)
                    .HasColumnName("mName")
                    .HasMaxLength(150);

                entity.Property(e => e.MapPublish).HasColumnName("mapPublish");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PId).HasColumnName("pID");

                entity.Property(e => e.PName)
                    .HasColumnName("pName")
                    .HasMaxLength(150);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblPermissionMapV2>(entity =>
            {
                entity.HasKey(e => e.MapId);

                entity.ToTable("Tbl_Permission_Map_V2");

                entity.Property(e => e.MapId).HasColumnName("mapID");

                entity.Property(e => e.AId).HasColumnName("aID");

                entity.Property(e => e.AStatus).HasColumnName("aStatus");

                entity.Property(e => e.MId).HasColumnName("mID");

                entity.Property(e => e.MName)
                    .HasColumnName("mName")
                    .HasMaxLength(150);

                entity.Property(e => e.MapPublish).HasColumnName("mapPublish");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PId).HasColumnName("pID");

                entity.Property(e => e.PName)
                    .HasColumnName("pName")
                    .HasMaxLength(150);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblPermissionMenu>(entity =>
            {
                entity.HasKey(e => e.MId);

                entity.ToTable("Tbl_Permission_Menu");

                entity.Property(e => e.MId).HasColumnName("mID");

                entity.Property(e => e.MClassTag)
                    .HasColumnName("mClassTag")
                    .HasMaxLength(150);

                entity.Property(e => e.MCreatedDate)
                    .HasColumnName("mCreatedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.MIcon).HasColumnName("mIcon");

                entity.Property(e => e.MName)
                    .HasColumnName("mName")
                    .HasMaxLength(150);

                entity.Property(e => e.MPageId)
                    .HasColumnName("mPageId")
                    .HasMaxLength(100);

                entity.Property(e => e.MParentId).HasColumnName("mParentID");

                entity.Property(e => e.MPublish).HasColumnName("mPublish");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Target).HasMaxLength(150);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.UrlMain).HasColumnName("Url_Main");

                entity.Property(e => e.UrlRewrite).HasColumnName("Url_Rewrite");
            });

            modelBuilder.Entity<TblPermissionMenuV2>(entity =>
            {
                entity.HasKey(e => e.MId);

                entity.ToTable("Tbl_Permission_Menu_V2");

                entity.Property(e => e.MId).HasColumnName("mID");

                entity.Property(e => e.MClassTag)
                    .HasColumnName("mClassTag")
                    .HasMaxLength(150);

                entity.Property(e => e.MCreatedDate)
                    .HasColumnName("mCreatedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.MIcon).HasColumnName("mIcon");

                entity.Property(e => e.MName)
                    .HasColumnName("mName")
                    .HasMaxLength(150);

                entity.Property(e => e.MPageId)
                    .HasColumnName("mPageId")
                    .HasMaxLength(100);

                entity.Property(e => e.MParentId).HasColumnName("mParentID");

                entity.Property(e => e.MPublish).HasColumnName("mPublish");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Target).HasMaxLength(150);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.UrlMain).HasColumnName("Url_Main");

                entity.Property(e => e.UrlRewrite).HasColumnName("Url_Rewrite");
            });

            modelBuilder.Entity<TblPermissionSalonArea>(entity =>
            {
                entity.ToTable("Tbl_Permission_Salon_Area");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblSalon>(entity =>
            {
                entity.ToTable("Tbl_Salon");

                entity.HasIndex(e => new { e.ShortName, e.IsSalonHoiQuan, e.Id })
                    .HasName("_dta_index_Tbl_Salon_9_1069558123__K25_K1_24");

                entity.Property(e => e.Address).HasMaxLength(600);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.Email).HasMaxLength(300);

                entity.Property(e => e.Fanpage).HasMaxLength(300);

                entity.Property(e => e.FanpageId).HasMaxLength(50);

                entity.Property(e => e.Gmap)
                    .HasColumnName("GMap")
                    .HasMaxLength(600);

                entity.Property(e => e.ImgMap)
                    .HasColumnName("Img_Map")
                    .HasMaxLength(500);

                entity.Property(e => e.ManagerName).HasMaxLength(200);

                entity.Property(e => e.Maps).HasMaxLength(500);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Mktsorder)
                    .HasColumnName("MKTsorder")
                    .HasMaxLength(10);

                entity.Property(e => e.Name).HasMaxLength(300);

                entity.Property(e => e.OpeningDate).HasColumnType("datetime");

                entity.Property(e => e.Phone).HasMaxLength(300);

                entity.Property(e => e.SalonOtp).HasColumnName("SalonOTP");

                entity.Property(e => e.ShortName).HasMaxLength(100);

                entity.Property(e => e.SmsSignature)
                    .HasColumnName("SMS_Signature")
                    .HasMaxLength(300);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Ward).HasMaxLength(100);
            });

            modelBuilder.Entity<TblSkillLevel>(entity =>
            {
                entity.ToTable("Tbl_SkillLevel");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblStaffOfClubInformation>(entity =>
            {
                entity.ToTable("Tbl_StaffOfClubInformation");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Images1).HasMaxLength(300);

                entity.Property(e => e.Images2).HasMaxLength(300);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblStaffSurvey>(entity =>
            {
                entity.ToTable("Tbl_Staff_Survey");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblStatus>(entity =>
            {
                entity.ToTable("Tbl_Status");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TblTemp>(entity =>
            {
                entity.ToTable("Tbl_Temp");

                entity.Property(e => e.Key).HasMaxLength(200);

                entity.Property(e => e.Meta).HasMaxLength(600);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Value).HasMaxLength(300);
            });

            modelBuilder.Entity<TblTesst>(entity =>
            {
                entity.ToTable("tbl_tesst");

                entity.Property(e => e.Issms).HasColumnName("issms");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(50);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TeamService>(entity =>
            {
                entity.Property(e => e.Account).HasMaxLength(50);

                entity.Property(e => e.Color).HasMaxLength(50);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(300);

                entity.Property(e => e.Password).HasMaxLength(50);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TimekeepingConfig>(entity =>
            {
                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TinhThanh>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.TenTinhThanh).HasMaxLength(255);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.VungMienId).HasColumnName("VungMienID");
            });

            modelBuilder.Entity<TopSalary>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateTime)
                    .HasColumnName("createTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.DepartmentId).HasColumnName("departmentId");

                entity.Property(e => e.IsDelete).HasColumnName("isDelete");

                entity.Property(e => e.Month).HasColumnName("month");

                entity.Property(e => e.Salary)
                    .HasColumnName("salary")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SalonName)
                    .IsRequired()
                    .HasColumnName("salonName")
                    .HasMaxLength(500);

                entity.Property(e => e.SkillLevel)
                    .IsRequired()
                    .HasColumnName("skillLevel")
                    .HasMaxLength(100);

                entity.Property(e => e.StaffId).HasColumnName("staffId");

                entity.Property(e => e.StaffName)
                    .IsRequired()
                    .HasColumnName("staffName")
                    .HasMaxLength(100);

                entity.Property(e => e.Year).HasColumnName("year");
            });

            modelBuilder.Entity<TrackingWebData>(entity =>
            {
                entity.ToTable("TrackingWeb_Data");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.EventId).HasColumnName("EventID");

                entity.Property(e => e.Meta).HasMaxLength(200);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.TokenKey).HasMaxLength(50);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Value).HasMaxLength(200);

                entity.Property(e => e.VersionId).HasColumnName("VersionID");
            });

            modelBuilder.Entity<TrackingWebDataV2>(entity =>
            {
                entity.ToTable("TrackingWeb_DataV2");

                entity.HasIndex(e => new { e.Id, e.Value, e.SalonId, e.Phone, e.HourId, e.CusNote1, e.CusNote2, e.SalonNote, e.TimeSalonNote, e.TrackDate })
                    .HasName("IDX_20180715_26331_26330_Solution_30shinedboTrackingWeb_DataV2");

                entity.HasIndex(e => new { e.Id, e.EventId, e.Value, e.CreatedTime, e.TokenKey, e.Meta, e.VersionId, e.Counting, e.StylistId, e.TrackDate, e.SalonId, e.Phone, e.HourId, e.CountFrames, e.CusNote1, e.CusNote2, e.Device, e.SalonNote, e.TimeSalonNote, e.PopupSalonStatis, e.PopupStylistStatis, e.IdsalonBackUp, e.IdstylistBackUp, e.VersionWeb, e.Uid, e.MigrateStatus, e.BookingId })
                    .HasName("IDX_20180718_20_19_Solution_30shinedboTrackingWeb_DataV2");

                entity.HasIndex(e => new { e.Value, e.CreatedTime, e.Meta, e.VersionId, e.Counting, e.StylistId, e.TrackDate, e.SalonId, e.Phone, e.BookingId, e.HourId, e.CountFrames, e.CusNote1, e.CusNote2, e.Device, e.SalonNote, e.TimeSalonNote, e.PopupSalonStatis, e.PopupStylistStatis, e.IdsalonBackUp, e.IdstylistBackUp, e.VersionWeb, e.Uid, e.MigrateStatus, e.SalonType, e.CityId, e.IsClickVoucher, e.IsClickChooseCity, e.EventId, e.TokenKey, e.Id })
                    .HasName("_dta_index_TrackingWeb_DataV2_9_1814659766__K2_K5_K1_3_4_6_7_8_9_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_27_28_29_");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BookingId).HasColumnName("Booking_ID");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.CusNote1).HasMaxLength(255);

                entity.Property(e => e.CusNote2).HasMaxLength(255);

                entity.Property(e => e.Device).HasMaxLength(255);

                entity.Property(e => e.EventId).HasColumnName("EventID");

                entity.Property(e => e.HourId).HasColumnName("Hour_ID");

                entity.Property(e => e.IdsalonBackUp).HasColumnName("IDSalonBackUP");

                entity.Property(e => e.IdstylistBackUp).HasColumnName("IDStylistBackUp");

                entity.Property(e => e.Meta).HasMaxLength(200);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.SalonId).HasColumnName("SalonID");

                entity.Property(e => e.SalonNote).HasMaxLength(500);

                entity.Property(e => e.StylistId).HasColumnName("StylistID");

                entity.Property(e => e.TimeSalonNote).HasColumnType("datetime");

                entity.Property(e => e.TokenKey).HasMaxLength(50);

                entity.Property(e => e.TrackDate).HasColumnType("date");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.VersionId).HasColumnName("VersionID");

                entity.Property(e => e.VersionWeb)
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TrackingWebDataV22017>(entity =>
            {
                entity.ToTable("TrackingWeb_DataV2_2017");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BookingId).HasColumnName("Booking_ID");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.CusNote1).HasMaxLength(255);

                entity.Property(e => e.CusNote2).HasMaxLength(255);

                entity.Property(e => e.Device).HasMaxLength(255);

                entity.Property(e => e.EventId).HasColumnName("EventID");

                entity.Property(e => e.HourId).HasColumnName("Hour_ID");

                entity.Property(e => e.IdsalonBackUp).HasColumnName("IDSalonBackUP");

                entity.Property(e => e.IdstylistBackUp).HasColumnName("IDStylistBackUp");

                entity.Property(e => e.Meta).HasMaxLength(200);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.SalonId).HasColumnName("SalonID");

                entity.Property(e => e.SalonNote).HasMaxLength(500);

                entity.Property(e => e.StylistId).HasColumnName("StylistID");

                entity.Property(e => e.TimeSalonNote).HasColumnType("datetime");

                entity.Property(e => e.TokenKey).HasMaxLength(50);

                entity.Property(e => e.TrackDate).HasColumnType("date");

                entity.Property(e => e.VersionId).HasColumnName("VersionID");

                entity.Property(e => e.VersionWeb)
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TrackingWebEvent>(entity =>
            {
                entity.ToTable("TrackingWeb_Event");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.EventName).HasMaxLength(100);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TuyenDungDanhGia>(entity =>
            {
                entity.ToTable("TuyenDung_DanhGia");

                entity.HasIndex(e => new { e.UngVienId, e.IsDelete, e.IsStatus })
                    .HasName("IDX_20180718_513_512_Solution_30shinedboTuyenDung_DanhGia");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ImgSkill1).HasMaxLength(200);

                entity.Property(e => e.ImgSkill2).HasMaxLength(200);

                entity.Property(e => e.KyNangHoaChat).HasMaxLength(200);

                entity.Property(e => e.MapSkillLevelId)
                    .HasColumnName("MapSkillLevelID")
                    .HasMaxLength(200);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.UngVienId).HasColumnName("UngVienID");

                entity.Property(e => e.UserTestId).HasColumnName("UserTestID");

                entity.Property(e => e.VideoLink).HasMaxLength(200);
            });

            modelBuilder.Entity<TuyenDungNguoiTest>(entity =>
            {
                entity.ToTable("TuyenDung_NguoiTest");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Email).HasColumnType("nchar(50)");

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Position).HasMaxLength(50);

                entity.Property(e => e.Telephone).HasColumnType("nchar(20)");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TuyenDungNguon>(entity =>
            {
                entity.ToTable("TuyenDung_Nguon");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TuyenDungSkill>(entity =>
            {
                entity.ToTable("TuyenDung_Skill");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.SkillName).HasMaxLength(100);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TuyenDungSkillLevel>(entity =>
            {
                entity.ToTable("TuyenDung_SkillLevel");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.LevelName).HasMaxLength(100);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TuyenDungSkillLevelMap>(entity =>
            {
                entity.ToTable("TuyenDung_SkillLevel_Map");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.LevelId).HasColumnName("LevelID");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.SkillId).HasColumnName("SkillID");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TuyenDungStatus>(entity =>
            {
                entity.ToTable("TuyenDung_Status");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.StatusName).HasMaxLength(50);

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<TuyenDungUngVien>(entity =>
            {
                entity.ToTable("TuyenDung_UngVien");

                entity.HasIndex(e => new { e.Id, e.FullName, e.Phone, e.DepartmentId, e.IsDelete, e.UngVienStatusId })
                    .HasName("IDX_20180718_515_514_Solution_30shinedboTuyenDung_UngVien");

                entity.HasIndex(e => new { e.Id, e.FullName, e.Phone, e.DepartmentId, e.UngVienStatusId, e.Step1Time, e.IsDelete })
                    .HasName("IDX_20180719_5031_5030_Solution_30shinedboTuyenDung_UngVien");

                entity.HasIndex(e => new { e.Id, e.FullName, e.Phone, e.DepartmentId, e.UngVienStatusId, e.SalonId, e.StaffId, e.IsDelete, e.Step1Time })
                    .HasName("IDX_20180719_5006_5005_Solution_30shinedboTuyenDung_UngVien");

                entity.Property(e => e.Address).HasMaxLength(300);

                entity.Property(e => e.Cmt)
                    .HasColumnName("CMT")
                    .HasMaxLength(50);

                entity.Property(e => e.Cmtimg1)
                    .HasColumnName("CMTimg1")
                    .HasMaxLength(300);

                entity.Property(e => e.Cmtimg2)
                    .HasColumnName("CMTimg2")
                    .HasMaxLength(300);

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.ImgSkill1).HasMaxLength(300);

                entity.Property(e => e.ImgSkill2).HasMaxLength(300);

                entity.Property(e => e.ImgSkillDaoTao1).HasMaxLength(300);

                entity.Property(e => e.ImgSkillDaoTao2).HasMaxLength(300);

                entity.Property(e => e.MainImg).HasMaxLength(300);

                entity.Property(e => e.MainImg2).HasMaxLength(300);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.NgaySinh).HasColumnType("datetime");

                entity.Property(e => e.ParseVh).HasColumnName("Parse_VH");

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.PointFigure).HasColumnName("Point_Figure");

                entity.Property(e => e.ProvidedDate).HasColumnType("date");

                entity.Property(e => e.ProvidedLocale).HasMaxLength(300);

                entity.Property(e => e.SalonId).HasColumnName("salonId");

                entity.Property(e => e.Step1ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Step1Time).HasColumnType("datetime");

                entity.Property(e => e.Step2ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.Step2Time).HasColumnType("datetime");

                entity.Property(e => e.StepEndModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.StepEndTime).HasColumnType("datetime");

                entity.Property(e => e.TestChemistry).HasMaxLength(150);

                entity.Property(e => e.TestCut).HasMaxLength(150);

                entity.Property(e => e.TestVh)
                    .HasColumnName("Test_VH")
                    .HasMaxLength(150);

                entity.Property(e => e.TesterId).HasColumnName("TesterID");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.VideoLink).HasMaxLength(300);
            });

            modelBuilder.Entity<TypeErrorSpecailCus>(entity =>
            {
                entity.ToTable("TypeError_SpecailCus");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedTime).HasColumnType("datetime");

                entity.Property(e => e.SpecailCusId).HasColumnName("SpecailCus_ID");

                entity.Property(e => e.TypeErrorSpecialCusId).HasColumnName("TypeError_SpecialCus_ID");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<Vietnam>(entity =>
            {
                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.District).HasMaxLength(50);

                entity.Property(e => e.Ward).HasMaxLength(50);

                entity.Property(e => e.WardLevel)
                    .HasColumnName("Ward_Level")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<WorkflowFile>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DepartmentId).HasColumnName("departmentId");

                entity.Property(e => e.FilePath)
                    .HasColumnName("filePath")
                    .HasMaxLength(300);

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<WorkTime>(entity =>
            {
                entity.Property(e => e.Color).HasMaxLength(50);

                entity.Property(e => e.EnadTime).HasColumnType("time(0)");

                entity.Property(e => e.Lunchhour).HasColumnType("time(0)");

                entity.Property(e => e.Lunchhour2).HasColumnType("time(0)");

                entity.Property(e => e.MigrateStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.StrartTime).HasColumnType("time(0)");

                entity.Property(e => e.Uid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.WName)
                    .HasColumnName("w_Name")
                    .HasMaxLength(20);
            });
        }
    }
}
