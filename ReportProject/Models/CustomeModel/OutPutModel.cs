﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APICheckout.Models.CustomeModel
{
    public class OutPutModel
    {
        public class WaitTime
        {
            public int notTime { get; set; }
            public int after20 { get; set; }
            public int between1520 { get; set; }
            public int before15 { get; set; }
        }
        public class ServicesAll
        {
            public int totalBillservice { get; set; }
            public int totalShinecombo { get; set; }
            public int shineCombo { get; set; }
            public int kidCombo { get; set; }
            public int protein { get; set; }
            public int duoi { get; set; }
            public int uon { get; set; }
            public int nhuom { get; set; }
            public int tay { get; set; }
            public int tayX2 { get; set; }
        }
        public class StarNumber
        {
            public int Star1 { get; set; }
            public int Star2 { get; set; }
            public int Star3 { get; set; }
            public int Star4 { get; set; }
            public int Star5 { get; set; }
        }
        public class Out_Message
        {
            public bool success { get; set; }
            public string status { get; set; }
            public string message { get; set; }
            public object data { get; set; }
        }
        public class OutStatisticScsc
        {
            public int? BillId { get; set; }
            public bool? IsImages { get; set; }
            public int? StylistId { get; set; }
            public int? SalonId { get; set; }
            public bool? ImageError { get; set; }
            public int? ShapeId { get; set; }
            public int? ConnectTiveId { get; set; }
            public int? SharpNessId { get; set; }
            public int? ComplatetionId { get; set; }
            public int? PointError { get; set; }
            public int? HairTipId { get; set; }
            public int? HairRootId { get; set; }
            public int? HairWavesId { get; set; }
            public int? TotalPointSCSCCurling { get; set; }
            public bool? IsUon { get; set; }
            public bool? ImageErrorCurling { get; set; }
            public bool? IsImgUon { get; set; }
        }
        public class OutStarBill
        {
            public int BillId { get; set; }
            public int? Star { get; set; }
            public int? SalonId { get; set; }
            public int? ServiceIdIsCurling { get; set; }
            public int? ServiceIDIsShineCombo { get; set; }
            public int? TotalPointSCSCCurling  { get; set; }
            public int? PointError { get; set; }
            public bool? IsImages { get; set; }
            public int? IsImageCurling { get; set; }
            public bool?  ImageError { get; set; }
            //public int? BookingId { get; set; }
            //public bool? IsBookAtSalon { get; set; }
            //public DateTime? CreatedDate { get; set; }
            //public TimeSpan? HourFrame { get; set; }
            //public DateTime? InProcedureTime { get; set; }
            //public DateTime? DatedBook { get; set; }
            public string SalonShortName { get; set; }
            public int? Order { get; set; }
            public bool? ImageErrorCurling { get; set; }
        }
        public class OutSalonStaffError
        {
            public int? SalonId { get; set; }
            public int? TotalError { get; set; }
        }
        public class OutWaitLongTime
        {
            public int? SalonId { get; set; }
            public int? TotalLongTime { get; set; }
        }
        public class OutReportExperiencePoint
        {
            public int? SalonId { get; set; }
            public int? TotalBill { get; set; }
            public float? RatingTB { get; set; }
            public int? TotalMonitorStaff { get; set; }
            public int? TotalErorrScsc { get; set; }
            public int? TotalErrorScscCurling { get; set; }
            public float? ErrorImage { get; set; }
            public string SalonShortName { get; set; }
            public int? Feedback { get; set; }
            public int? WaitTime15 { get; set; }
            public int? OrderSalon { get; set; }

        }
    }
}
