﻿using ReportProject.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APICheckout.Models.CustomeModel
{
    public class InputModel
    {
        #region Query params
        

        #endregion

        #region Request header
       

        #endregion

        #region Request data
        public class ReqStatisticScsc
        {
            public string FromDate { get; set; }
            public string ToDate { get; set; }
            public string ListSalonId { get; set; }
        }
        public class ReqFeedbackPointService
        {
            public string WorkDate { get; set; }
            public int SalonId { get;set;}
            public int FeedbackQuantity { get; set; }
        }

         
        #endregion

        #region Response data
        public class ResDataUser
        {
            public int Id { get; set; }
            public string Account { get; set; }
            public string Password { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
        }


        #endregion

        #region[Authen]
        public class AuthenModel
        {
            public string Phone { get; set; }
            public string DeviceId { get; set; }
            public string Ip { get; set; }
            public string AppId { get; set; }
            public string AppVersion { get; set; }
            public ResDataUser User { get; set; }
            public object Perm { get; set; }
        }
        #endregion

        /// <summary>
        /// Class chung cho các hàm cần có tham số isUpdate để xử lý
        /// </summary>
        //public class ObjWithCheckUpdate
        //{
        //    public bool IsUpdate { get; set; }
        //    public dynamic Data { get; set; }
        //}

        #region [Product, Service Basic]

        //[{"Id":53,"Code":"SP00036","Name":"Shine Combo 100k","Price":100000,"Quantity":0,"VoucherPercent":0}]
        public class ServiceBasic
        {
            public int Id { get; set; }
            public string Code { get; set; }
            public string Name { get; set; }
            public double Price { get; set; }
            public int Quantity { get; set; }
            public int VoucherPercent { get; set; }
            public int Promotion { get; set; }
            public bool IsCheckVatTu { get; set; }
            public bool CheckCombo { get; set; }
            public int? Order { get; set; }
        }

        //[{"Id":521,"Code":"SP00491","MapIdProduct":"","Name":"Đắp mặt nạ +15k","Price":7500,"Quantity":1,"VoucherPercent":0,"Promotion":0,"SellerId": 163}]
        public class ProductBasic
        {
            public int Id { get; set; }
            public string Code { get; set; }
            public string MapIdProduct { get; set; }
            public string Name { get; set; }
            public double Price { get; set; }
            public int Quantity { get; set; }
            public int VoucherPercent { get; set; }
            public int Promotion { get; set; }
            public bool IsCheckVatTu { get; set; }
            public bool CheckCombo { get; set; }
            public int? Order { get; set; }
            public int SellerId { get; set; }
        }

        #endregion
        /// <summary>
        /// Struct item tồn kho
        /// </summary>
       

    }
}
