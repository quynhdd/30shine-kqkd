﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using ReportProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class BookHourRepo : IBookHourRepo
    {
        private Solution_30shineContext context;
        private DbSet<BookHour> objEntity;
        public BookHourRepo()
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<BookHour>();
        }
        public async Task<IEnumerable<BookHour>> GetByListSalon(IEnumerable<int?> salonIds)
        {
            try
            {
                return await objEntity.AsNoTracking().Where(a =>  salonIds.Contains(a.SalonId)).ToListAsync();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
