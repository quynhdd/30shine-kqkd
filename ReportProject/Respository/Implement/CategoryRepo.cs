﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class CategoryRepo : ICategoryRepo
    {
        private Solution_30shineContext context;

        public CategoryRepo()
        {
            context = new Solution_30shineContext();
        }

        public async Task<IEnumerable<RevenueJson>> GetByIdsAsync(IEnumerable<int> id)
        {
            try
            {
                return await context.TblCategory.Where(a => a.Publish == true && a.IsDelete == 0 && id.Contains(a.Id)).Select(a => new RevenueJson
                {
                    SalonId = 0,
                    CategoryId = a.Id,
                    Name = a.Name,
                    Income = 0,
                    Type = 0
                }).ToListAsync();
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}
