﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using ReportProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.FlowTimeKeepingJoin;

namespace ReportProject.Respository.Implement
{
    public class StaffBillServiceDetailInforRepo : IStaffBillServiceDetailInforRepo
    {
        private Solution_30shineContext context;
        private DbSet<StaffBillServiceDetail> staffBillServiceDetail;
        private ISalonRepo SalonRepo { get; set; } //1
        private IBillServiceRepo BillServiceRepo { get; set; }      //2
        private IFlowTimeKeepingRepo FlowTimeKeepingRepo { get; set; }//2
        private IBookingRepo BookingRepo { get; set; }//2
        private IBookHourRepo BookHourRepo { get; set; }//2
        //private IBookingTempRepo BookingTempRepo { get; set; }//2
        private IStaffRepo StaffRepo { get; set; } // 1
        private IFlowServiceRepo FlowServiceRepo { get; set; } // 2
        private IServiceSalonConfigRepo ServiceSalonConfigRepo { get; set; } // 2
        private IRatingConfigPointRepo RatingConfigPointRepo { get; set; } // 1
        private ISalaryConfigRepo SalaryConfigRepo { get; set; } // 1


        public StaffBillServiceDetailInforRepo(ISalonRepo salonRepo, IBillServiceRepo billServiceRepo, IFlowTimeKeepingRepo flowTimeKeepingRepo, IBookingRepo bookingRepo, IBookHourRepo bookHourRepo, IStaffRepo staffRepo, IFlowServiceRepo flowServiceRepo, IServiceSalonConfigRepo serviceSalonConfigRepo, IRatingConfigPointRepo ratingConfigPointRepo, ISalaryConfigRepo salaryConfigRepo, IStaffBillServiceDetailRepo staffBillServiceDetailRepo)
        {
            context = new Solution_30shineContext();
            staffBillServiceDetail = context.Set<StaffBillServiceDetail>();
            SalonRepo = salonRepo;
            BillServiceRepo = billServiceRepo;
            FlowTimeKeepingRepo = flowTimeKeepingRepo;
            BookingRepo = bookingRepo;
            BookHourRepo = bookHourRepo;
            StaffRepo = staffRepo;
            FlowServiceRepo = flowServiceRepo;
            ServiceSalonConfigRepo = serviceSalonConfigRepo;
            RatingConfigPointRepo = ratingConfigPointRepo;
            SalaryConfigRepo = salaryConfigRepo;

        }


        public async Task<List<StaffBillServiceDetail>> GetStaffBillServiceDetail(DateTime date)
        {
            try
            {
                DateTime fromDate = date;
                DateTime toDate = date.AddDays(+1);
                List<FlowTimeKeepingJoinWorkTime> flowTimeKeepingJoinList = null;
                // check so ngay trong thang 
                DateTime dateNow = DateTime.Now;
                int daysInJuly = System.DateTime.DaysInMonth(dateNow.Year, dateNow.Month) - 4;

                IEnumerable<int?> departmentIds = new List<int?> { Constant.STYLIST, Constant.SKINNER };
                var salonList = SalonRepo.Get();
                var StaffRepoList = StaffRepo.GetStaffByDepartment(departmentIds);
                var RatingConfigPointRepoList = RatingConfigPointRepo.GetAllRatingConfigPoint();
                var SalaryConfigRepoList = SalaryConfigRepo.GetByDepartmentIds(departmentIds);
                // Call data part 1
                Task.WaitAll(salonList, StaffRepoList, RatingConfigPointRepoList);
                var salon = salonList.Result;
                var staffDepartmentList = StaffRepoList.Result;
                var ratingConfigPointList = RatingConfigPointRepoList.Result;
                var salaryConfigList = SalaryConfigRepoList.Result;
                IEnumerable<int?> salonIds = salon.Select(a => a.Id).Cast<int?>().ToList();
                // call data part2 
                var BillServiceRepoList = BillServiceRepo.GetBillServiceByDate(salonIds, fromDate, toDate);
                var FlowServiceRepoList = FlowServiceRepo.GetByDateListSalon(salonIds, fromDate, toDate);
                var BookingRepoList = BookingRepo.getByDateAllBook(salonIds, fromDate, toDate);
                var BookHourRepoList = BookHourRepo.GetByListSalon(salonIds);
                var ServiceSalonConfigRepoList = ServiceSalonConfigRepo.GetbyListSalon(salonIds, departmentIds);


                Task.WaitAll(BillServiceRepoList, FlowServiceRepoList, BookingRepoList, BookHourRepoList, ServiceSalonConfigRepoList);

                var billServiceList = BillServiceRepoList.Result;
                var flowServiceList = FlowServiceRepoList.Result;
                var bookingList = BookingRepoList.Result;
                var bookHourList = BookHourRepoList.Result;
                var serviceSalonConfigList = ServiceSalonConfigRepoList.Result;
                if (date.Day > daysInJuly)
                {
                    DateTime datefirstMonth = new DateTime(date.Year, date.Month, 1);
                    var FlowTimeKeepingRepoList = FlowTimeKeepingRepo.GetByStylistSalonAndDate(salonIds, departmentIds, datefirstMonth, toDate);
                    await (FlowTimeKeepingRepoList);
                    flowTimeKeepingJoinList = FlowTimeKeepingRepoList.Result.ToList();
                }
                else
                {
                    var FlowTimeKeepingRepoList = FlowTimeKeepingRepo.GetByStylistSalonAndDate(salonIds, departmentIds, fromDate, toDate);
                    await (FlowTimeKeepingRepoList);
                    flowTimeKeepingJoinList = FlowTimeKeepingRepoList.Result.ToList();
                }

                var totalTimeKeepingStaff = from a in flowTimeKeepingJoinList
                                            group a.StaffId by a.StaffId into gr
                                            select new
                                            {
                                                staffId = gr.Key,
                                                totalTimeKeeeping = gr.Count()
                                            };
                // xu ly toi ưu tránh join nhieu l

                var BillJoin = from bill in billServiceList
                               join rating in ratingConfigPointList on bill.Mark equals rating.RealPoint
                               join flservice in flowServiceList on bill.Id equals flservice.BillId
                               select new
                               {
                                   bill.Id,
                                   bill.SalonId,
                                   bill.BookingId,
                                   bill.StaffHairdresserId,
                                   bill.StaffHairMassageId,
                                   rating.ConventionPoint,
                                   flservice.ServiceId,
                                   flservice.Quantity,
                                   bill.Mark,
                                   bill.CreatedDate
                               };

                var dateFlowTimeKeepingJoinList = flowTimeKeepingJoinList.Where(a => a.WorkDate == date);

                var staffJoin = from staff in staffDepartmentList
                                join salaryConfig in salaryConfigList on staff.Type equals salaryConfig.DepartmentId
                                join totalFtk in totalTimeKeepingStaff on staff.Id equals totalFtk.staffId
                                join dateftk in dateFlowTimeKeepingJoinList on staff.Id equals dateftk.StaffId
                                where
                                 staff.SkillLevel == salaryConfig.LevelId
                                select new
                                {
                                    staff.Id,
                                    staff.SkillLevel,
                                    staff.Type,
                                    salaryConfig.RattingSalary,
                                    totalFtk.totalTimeKeeeping,
                                    dateftk.StrartTime,
                                    dateftk.EnadTime
                                };

                var bookJoinHour = from book in bookingList
                                   join hour in bookHourList on book.HourId equals hour.Id
                                   select new
                                   {
                                       book.Id,
                                       hour.HourFrame
                                   };


                // get list bill stylist
                var listStaffBillServiceDetailStylist = from bill in BillJoin
                                                        join book in bookJoinHour on bill.BookingId equals book.Id
                                                        join staff in staffJoin on bill.StaffHairdresserId equals staff.Id
                                                        join serviceConfig in serviceSalonConfigList on staff.Type equals serviceConfig.DepartmentId

                                                        where
                                                        bill.SalonId == serviceConfig.SalonId
                                                        && bill.ServiceId == serviceConfig.ServiceId
                                                        //=============
                                                        select new StaffBillServiceDetail
                                                        {
                                                            BillId = bill.Id,
                                                            ServiceId = (int)bill.ServiceId,
                                                            DepartmentId = (int)staff.Type,
                                                            StaffId = staff.Id,
                                                            StaffLevelId = (int)staff.SkillLevel,
                                                            OvertimeStatusValue = staff.totalTimeKeeeping > daysInJuly ? 2 : (book.HourFrame < staff.StrartTime ? 1 : (book.HourFrame > staff.EnadTime ? 1 : 0)),
                                                            RatingMark = (int)bill.Mark,
                                                            RatingMoney = (double)staff.RattingSalary,
                                                            //SalaryCoeficient = GetCoefficient(staff.totalTimeKeeeping, daysInJuly, book.HourFrame, staff.StrartTime, staff.EnadTime, staff.Type, bill.SalonId, bill.ServiceId, serviceSalonConfigList).SalaryCoeficient,
                                                            SalaryCoeficient = GetSalaryCoeficient(staff.totalTimeKeeeping, daysInJuly, book.HourFrame, staff.StrartTime, staff.EnadTime, staff.Type, bill.SalonId, bill.ServiceId, serviceSalonConfigList),

                                                            ServiceCoeficient = (double)serviceConfig.ServiceCoefficient,

                                                            ServiceIncomeBonus =
                                                             (double)staff.RattingSalary
                                                             *
                                                            GetSalaryCoeficient(staff.totalTimeKeeeping, daysInJuly, book.HourFrame, staff.StrartTime, staff.EnadTime, staff.Type, bill.SalonId, bill.ServiceId, serviceSalonConfigList)
                                                            *
                                                            (double)serviceConfig.ServiceCoefficient
                                                            *
                                                            (double)bill.Quantity

                                                            *
                                                            (double)bill.ConventionPoint
                                                            +
                                                            (double)serviceConfig.ServiceBonus,

                                                            IsDelete = false,
                                                            CreateTime = DateTime.Now,
                                                            WorkDate = date,
                                                            Quantity = (int)bill.Quantity,
                                                            ConventionPoint = (double)bill.ConventionPoint,
                                                            ServiceBonus = (double)serviceConfig.ServiceBonus,

                                                        };

                // get list bill Skinner
                var listStaffBillServiceDetailSkinner = from bill in BillJoin
                                                        join book in bookJoinHour on bill.BookingId equals book.Id
                                                        join staff in staffJoin on bill.StaffHairMassageId equals staff.Id
                                                        join serviceConfig in serviceSalonConfigList on staff.Type equals serviceConfig.DepartmentId
                                                        where
                                                        bill.SalonId == serviceConfig.SalonId
                                                        && bill.ServiceId == serviceConfig.ServiceId
                                                        select new StaffBillServiceDetail
                                                        {
                                                            BillId = bill.Id,
                                                            ServiceId = (int)bill.ServiceId,
                                                            DepartmentId = (int)staff.Type,
                                                            StaffId = staff.Id,
                                                            StaffLevelId = (int)staff.SkillLevel,
                                                            OvertimeStatusValue = staff.totalTimeKeeeping > daysInJuly ? 2 : (book.HourFrame < staff.StrartTime ? 1 : (book.HourFrame > staff.EnadTime ? 1 : 0)),
                                                            RatingMark = (int)bill.Mark,
                                                            RatingMoney = (double)staff.RattingSalary,
                                                            SalaryCoeficient = GetSalaryCoeficient(staff.totalTimeKeeeping, daysInJuly, book.HourFrame, staff.StrartTime, staff.EnadTime, staff.Type, bill.SalonId, bill.ServiceId, serviceSalonConfigList),

                                                            ServiceCoeficient = (double)serviceConfig.ServiceCoefficient,

                                                            ServiceIncomeBonus =
                                                             (double)staff.RattingSalary
                                                             *
                                                            GetSalaryCoeficient(staff.totalTimeKeeeping, daysInJuly, book.HourFrame, staff.StrartTime, staff.EnadTime, staff.Type, bill.SalonId, bill.ServiceId, serviceSalonConfigList)
                                                            *
                                                           (double)serviceConfig.ServiceCoefficient
                                                            *
                                                            (double)bill.Quantity

                                                            *
                                                            (double)bill.ConventionPoint
                                                            +
                                                            (double)serviceConfig.ServiceBonus
                                                            *
                                                            (double)bill.Quantity
                                                            ,

                                                            IsDelete = false,
                                                            CreateTime = DateTime.Now,
                                                            WorkDate = bill.CreatedDate,
                                                            Quantity = (int)bill.Quantity,
                                                            ConventionPoint = (double)bill.ConventionPoint,
                                                            ServiceBonus = (double)serviceConfig.ServiceBonus,
                                                            BillSalonId = (int)bill.SalonId
                                                        };

                List<StaffBillServiceDetail> listAddStaffBillServiceDetailListAll = new List<StaffBillServiceDetail>();
                listAddStaffBillServiceDetailListAll.AddRange(listStaffBillServiceDetailStylist.Concat(listStaffBillServiceDetailSkinner));

                return listAddStaffBillServiceDetailListAll;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private double GetSalaryCoeficient(int totalTimeKeeeping, int daysInJuly, TimeSpan? HourFrameBook, TimeSpan? StrartTime, TimeSpan? EndTime, int? departmentId, int? SalonId, int? serviceId, IEnumerable<ServiceSalonConfig> serviceSalonConfigList)
        {
            try
            {
                //var objserviceSalonConfig = new FunCoefficient();
                double serviceSalonConfigValue = 0;

                int CoefficientOvertimeStatusValue = totalTimeKeeeping > daysInJuly ? 2 : (HourFrameBook < StrartTime ? 1 : (HourFrameBook > EndTime ? 1 : 0));
                 serviceSalonConfigValue = serviceSalonConfigList.Where(a => a.DepartmentId == departmentId && a.SalonId == SalonId && a.ServiceId == serviceId).Select(a => new FunCoefficient
                {
                    SalaryCoeficient = CoefficientOvertimeStatusValue == 2 ? (double)a.CoefficientOvertimeDay : (CoefficientOvertimeStatusValue == 1 ? (double)a.CoefficientOvertimeHour : 1)

                }).FirstOrDefault().SalaryCoeficient;

                return serviceSalonConfigValue;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void DeleteAndAdd(IEnumerable<StaffBillServiceDetail> DeleteListStaffBillServiceDetails, IEnumerable<StaffBillServiceDetail> AddListStaffBillServiceDetails)
        {
            try
            {
                //if (DeleteListStaffBillServiceDetails.Any()  || AddListStaffBillServiceDetails.Any() )
                //{
                //    if(DeleteListStaffBillServiceDetails.Any())
                //    {
                        staffBillServiceDetail.RemoveRange(DeleteListStaffBillServiceDetails);
                    //}
                    //if(AddListStaffBillServiceDetails.Any())
                    //{
                        staffBillServiceDetail.AddRangeAsync(AddListStaffBillServiceDetails);
                    //}
                    context.SaveChangesAsync();
                //}
            }
            catch (Exception)
            {

                throw;
            }
        }

        private class FunCoefficient
        {
            public double SalaryCoeficient { get; set; }
            public double ServiceCoeficient { get; set; }
        }
    }
}
