﻿using ReportProject.Respository.Interface;
using ReportProject.Models.Solution_30shine_Real;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.RatingDetailJson;

namespace ReportProject.Respository.Implement
{
    public class RatingDetailRepo : IRatingDetailRepo
    {
        private Solution_30shineContext context;
        private DbSet<RatingDetail> objEntity;
        public RatingDetailRepo()
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<RatingDetail>();
        }

        public async Task<IEnumerable<RatingDetailReal>> getByDateListSalon( DateTime fromDate, DateTime toDate)
        {
            try
            {
                return await objEntity.AsNoTracking().Where(a => a.Isdelete == false && a.CreatedTime > fromDate && a.CreatedTime < toDate )
                    .Select(a=> new RatingDetailReal
                    {
                        Id = a.Id,
                        BillId = a.BillId,
                        CreatedTime = a.CreatedTime,
                        ModifiedTime = a.ModifiedTime,
                        RatingValue = a.RatingValue,
                        StarNumber = a.StarNumber
                    }).ToListAsync();
            }
            catch (Exception)
            {
                context.Dispose();
                throw;
            }
        }
    }
}
