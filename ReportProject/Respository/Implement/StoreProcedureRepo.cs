﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class StoreProcedureRepo : IStoreProcedureRepo
    {
        private Solution_30shineContext context;
        public StoreProcedureRepo()
        {
            context = new Solution_30shineContext();
        }

        public async Task<IEnumerable<CustomerJson>> GetCustomerInfo(DateTime dateFrom, DateTime dateTo, int salonId, int regionId)
        {
            try
            {
                return await context.GetCustomerReport.AsNoTracking().FromSql("[dbo].[Store_OperationReportV3_CustomerInfo_v1] @dateFrom={0}, @dateTo={1}, @salonId={2}, @regionId={3}", dateFrom, dateTo, salonId, regionId).ToListAsync();
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public async Task<IEnumerable<LNGJson>> GetLNGInfo(DateTime dateFrom, DateTime dateTo, int salonId, int regionId, List<int> salonIds)
        {
            try
            {
                return await (from a in context.StaticExpense.AsNoTracking()
                              join b in context.TblSalon.AsNoTracking() on a.SalonId equals b.Id
                              where a.WorkDate >= dateFrom && a.WorkDate < dateTo
                              && salonIds.Contains(a.SalonId ??0)
                              select new LNGJson
                              {
                                  SalonId = b.Id,
                                  DirectFee = a.DirectFee,
                                  SalonFee = a.SalonFee,
                                  ManageFee = a.ManageFee
                              }).ToListAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<IEnumerable<ShineMemberJson>> GetMemberShipInfo(DateTime dateFrom, DateTime dateTo, int salonId, int regionId)
        {
            try
            {
                return await context.GetShineMemberReport.AsNoTracking().FromSql("[dbo].[Store_OperationReportV3_Membership_v1] @dateFrom={0}, @dateTo={1}, @salonId={2}, @regionId={3}", dateFrom, dateTo, salonId, regionId).ToListAsync();

            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public async Task<IEnumerable<ProductivityJson>> GetProductivityInfo(DateTime dateFrom, DateTime dateTo, int salonId, int regionId)
        {
            try
            {
                return await context.GetProductivityReport.AsNoTracking().FromSql("[dbo].[Store_OperationReportV3_Productivity_v1] @dateFrom={0}, @dateTo={1}, @salonId={2}, @regionId={3}", dateFrom, dateTo, salonId, regionId).ToListAsync();
            }
            catch (Exception e)
            { 
                throw e;
            }
        }

        public async Task<IEnumerable<QualityJson>> GetQualityInfo(DateTime dateFrom, DateTime dateTo, int salonId, int regionId)
        {
            try
            {
                return await context.GetQualityReport.AsNoTracking().FromSql("[dbo].[Store_OperationReportV3_Quality_v1] @dateFrom={0}, @dateTo={1}, @salonId={2}, @regionId={3}", dateFrom, dateTo, salonId, regionId).ToListAsync();

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<IEnumerable<RevenueJson>> GetRevenueInfo(DateTime dateFrom, DateTime dateTo, int salonId, int regionId)
        {
            try
            {
                return await context.GetRevenueReport.Distinct().AsNoTracking().FromSql("[dbo].[Store_OperationReportV3_Revenue_v1] @dateFrom={0}, @dateTo={1}, @salonId={2}, @regionId={3} ", dateFrom, dateTo, salonId, regionId).ToListAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<IEnumerable<TotalIncomeJson>> GetTotalIncome(DateTime dateFrom, DateTime dateTo, int salonId, int regionId)
        {
            try
            {
                return await context.GetTotalIncome.AsNoTracking().FromSql("[dbo].[Store_OperationReportV3_TotalIncome_v1] @dateFrom={0}, @dateTo={1}, @salonId={2}, @regionId={3} ", dateFrom, dateTo, salonId, regionId).ToListAsync();
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}
