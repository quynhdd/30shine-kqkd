﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using ReportProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class FlowServiceRepo : IFlowServiceRepo
    {
        private Solution_30shineContext context;
        private DbSet<FlowService> objEntity;

        public FlowServiceRepo()
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<FlowService>();
        }

        public async Task<IEnumerable<FlowService>> GetByDate(int salonId, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (salonId > 0)
                {
                    return await objEntity.AsNoTracking().Where(a => a.IsDelete == 0 && a.CreatedDate >= dateFrom && a.CreatedDate < dateTo.AddDays(Constant.ADD_DAY) && a.SalonId == salonId).Select(a => a).ToListAsync();
                }

                return await objEntity.AsNoTracking()
                    .Where(a => a.IsDelete == 0 && a.CreatedDate >= dateFrom && a.CreatedDate < dateTo.AddDays(Constant.ADD_DAY)).Select(a => a).ToListAsync();
            }
            catch (Exception e)
            {
                context.Dispose();
                throw;
            }
        }

        public async Task<IEnumerable<FlowService>> GetByDateListSalon(IEnumerable<int?> salonIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                return await objEntity.AsNoTracking()
                    .Where(a => a.IsDelete == 0 && a.CreatedDate >= dateFrom.AddDays(-2) && a.CreatedDate < dateTo.AddDays(Constant.ADD_DAY) && salonIds.Contains(a.SalonId)).Select(a => a).ToListAsync();
            }
            catch (Exception)
            {
                context.Dispose();
                throw;
            }
        }
    }
}
