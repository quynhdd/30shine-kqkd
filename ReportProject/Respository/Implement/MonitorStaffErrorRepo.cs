﻿using ReportProject.Models.Solution_30shine_Real;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ReportProject.Respository.Interface;
using static ReportProject.Models.JsonModels.MonitorStaffErrorJson;

namespace ReportProject.Respository.Implement
{
    public class MonitorStaffErrorRepo : IMonitorStaffErrorRepo
    {
        

        private Solution_30shineContext context;
        private DbSet<MonitorStaffError> objEntity;
        public MonitorStaffErrorRepo()
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<MonitorStaffError>();
        }
        public async Task<IEnumerable<MonitorStaffErrorReal>> GetByDateListSalon(IEnumerable<int?> salonIds, DateTime fromDate, DateTime toDate)
        {
            try
            {
                return await objEntity.AsNoTracking().Where(a => a.IsDelete == false && a.IsCommit == true && a.IsCommit == true && a.CreateDate > fromDate && a.CreateDate < toDate)
                    .Select(a=> new MonitorStaffErrorReal
                    {
                        Id = a.Id,
                        CreateDate = a.CreateDate,
                        IsCommit = a.IsCommit,
                        SalonId = a.SalonId,
                        StatusError = a.StatusError
                    }).ToListAsync();
                    
            }
            catch (Exception)
            {
                context.Dispose();
                throw;
            }
        }

        
    }
}
