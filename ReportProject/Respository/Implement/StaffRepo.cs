﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using ReportProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.AccountingReportJson;
using static ReportProject.Models.JsonModels.StaffJson;

namespace ReportProject.Respository.Implement
{
    public class StaffRepo : IStaffRepo
    {
        private Solution_30shineContext context;
        private DbSet<Staff> objStaff;
        private IFlowTimeKeepingRepo FlowTimeKeepingRepo { get; set; }
        private IBillServiceRepo BillServiceRepo { get; set; }

        public StaffRepo(IFlowTimeKeepingRepo flowTimeKeepingRepo)
        {
            context = new Solution_30shineContext();
            objStaff = context.Set<Staff>();
            FlowTimeKeepingRepo = flowTimeKeepingRepo;
        }

        public async Task<IEnumerable<Staff>> GetStaffBySalon(int salonId)
        {
            try
            {
                return await objStaff.AsNoTracking().Where(a => a.SalonId == salonId && (a.Type == Constant.STYLIST || a.Type == Constant.SKINNER)).Select(a => a).ToListAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<IEnumerable<Staff>> GetStylistAndSkinner(int staffType)
        {
            try
            {
                if (staffType > 0)
                {
                    return await context.Staff.AsNoTracking().Where(a => a.Type == staffType).Select(a => a).ToListAsync();
                }

                return await context.Staff.AsNoTracking().Where(a => a.Type == Constant.STYLIST || a.Type == Constant.SKINNER).Select(a => a).ToListAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IEnumerable<StaffInfo> GetStaffInfo(IEnumerable<BillServiceHis> billServices, IEnumerable<Staff> staffs, IEnumerable<SalaryIncome> salaryIncomes, IEnumerable<TblSkillLevel> tblSkillLevels, IEnumerable<StaffType> staffTypes)
        {
            try
            {

                var staffInfo = (from a in billServices
                                 from b in staffs
                                 where a.StaffHairdresserId == b.Id || a.StaffHairMassageId == b.Id
                                 where b.Type == Constant.STYLIST || b.Type == Constant.SKINNER
                                 select new
                                 {
                                     StaffId = b.Id,
                                     StaffName = b.Fullname,
                                     StaffType = b.Type
                                 }).Distinct().ToList();

                var staffInfoListAll = (from a in staffInfo
                                        join b in salaryIncomes on a.StaffId equals b.StaffId into abGroup
                                        from ab in abGroup
                                        join e in staffTypes on a.StaffType equals e.Id
                                        select new StaffInfo
                                        {
                                            StaffId = a.StaffId,
                                            StaffName = a.StaffName,
                                            StaffType = e.Name,
                                            FixSalary = ab?.FixedSalary,
                                            //LevelName = d.Name,
                                            WorkDate = ab.WorkDate,
                                            //CreateDate = c.CreateTime
                                            BehaveSalary = ab?.BehaveSalary
                                        }).ToList();


                return staffInfoListAll;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<IEnumerable<InforStaff>> GetAllStaffType(int staffType)
        {
            try
            {
                return await objStaff.AsNoTracking().Where(a => a.Type == staffType).Select(a => new InforStaff
                {
                    Id = a.Id,
                    Type = a.Type,
                    Phone = a.Phone,
                    Email = a.Email,
                    Fullname = a.Fullname,
                    IsDelete = a.IsDelete,
                    SalonId = a.SalonId,
                    SkillLevel = a.SkillLevel,
                    DateJoin = a.DateJoin,
                    Active = a.Active,
                    NgayTinhThamNien = a.NgayTinhThamNien
                }).ToListAsync();
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public async Task<IEnumerable<InforStaff>> GetStaffByDepartment(IEnumerable<int?> staffTypeIds)
        {
            try
            {
                return await objStaff.AsNoTracking().Where(a => staffTypeIds.Contains(a.Type)).Select(a => new InforStaff
                {
                    Id = a.Id,
                    Type = a.Type,
                    Phone = a.Phone,
                    Email = a.Email,
                    Fullname = a.Fullname,
                    IsDelete = a.IsDelete,
                    SalonId = a.SalonId,
                    SkillLevel = a.SkillLevel,
                    DateJoin = a.DateJoin,
                    Active = a.Active,
                    NgayTinhThamNien = a.NgayTinhThamNien
                }).ToListAsync();
            }
            catch (Exception)
            {
                context.Dispose();
                throw;
            }
        }
    }
}
