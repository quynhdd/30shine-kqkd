﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ReportProject.Respository.Interface;
using ReportProject.Models.Solution_30shine_Real;

namespace ReportProject.Respository.Implement
{
    public class SalonDailyCostRepo : ISalonDailyCostRepo
    {
        private Solution_30shineContext context;
        private DbSet<SalonDailyCost> objEntity;

        public SalonDailyCostRepo()
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<SalonDailyCost>();
        }
        public void Add(IEnumerable<SalonDailyCost> salonDailyCost)
        {
            objEntity.AddRange(salonDailyCost);
            context.SaveChanges();
        }

        public void Update(IEnumerable<SalonDailyCost> salonDailyCost)
        {
            objEntity.UpdateRange(salonDailyCost);
            context.SaveChanges();
        }

        public SalonDailyCost GetBySalonIdAndDate(int salonId, DateTime date)
        {
            var salonDailyCost =
                objEntity.FirstOrDefault(a => a.IsDelete == false && a.SalonId == salonId && a.ReportDate == date);
            return salonDailyCost;
        }
    }
}
