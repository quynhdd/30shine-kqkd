﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.SalonTypeConfigJson;

namespace ReportProject.Respository.Implement
{
    public class ServiceSalonTypeConfigRepo : IServiceSalonTypeConfigRepo
    {
        private Solution_30shineContext dbContext;
        private DbSet<ServiceSalonTypeConfig> dbSet;
        // constructor
        public ServiceSalonTypeConfigRepo()
        {
            dbContext = new Solution_30shineContext();
            dbSet = dbContext.Set<ServiceSalonTypeConfig>();
        }

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="obj"></param>
        public void Add(ServiceSalonTypeConfig obj)
        {
            try
            {
                dbSet.Add(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="obj"></param>
        public void Update(ServiceSalonTypeConfig obj)
        {
            try
            {
                dbSet.Update(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<ServiceSalonTypeConfig> Get(Expression<Func<ServiceSalonTypeConfig, bool>> expression)
        {
            try
            {
                return await dbSet.FirstOrDefaultAsync(expression);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        ///  Get list
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<IEnumerable<ServiceSalonTypeConfig>> GetList(Expression<Func<ServiceSalonTypeConfig, bool>> expression)
        {
            try
            {
                return await dbSet.Where(expression).ToListAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// GetListServiceSalonTypeConfig
        /// </summary>
        /// <returns></returns>
        public async Task<List<OutputServiceSalonTypeConfig>> GetListServiceSalonTypeConfig()
        {
            try
            {
                var data = await (from a in dbContext.ServiceSalonTypeConfig.AsNoTracking()
                                  join b in dbContext.SalonTypeConfig on a.SalonType equals b.SalonType
                                  where a.IsDelete == false
                                  && b.IsDelete == false
                                  select new OutputServiceSalonTypeConfig
                                  {
                                      SalonId = b.SalonId ?? 0,
                                      Departmentd = a.DepartmentId ?? 0,
                                      ServiceId = a.ServiceId ?? 0,
                                      ServiceCoefficient = a.ServiceCoefficient ?? 0,
                                      ServiceBonus = a.ServiceBonus ?? 0,
                                      CoefficientOvertimeDay = a.CoefficientOvertimeDay ?? 0,
                                      CoefficientOvertimeHour = a.CoefficientOvertimeHour ?? 0
                                  }
                            ).ToListAsync();

                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Save change
        /// </summary>
        /// <returns></returns>
        public async Task SaveChangeAysnc()
        {
            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
