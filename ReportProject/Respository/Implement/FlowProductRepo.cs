﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using ReportProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class FlowProductRepo : IFlowProductRepo
    {
        private Solution_30shineContext context;
        private DbSet<FlowProduct> objEntity;

        public FlowProductRepo()
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<FlowProduct>();
        }

        public async Task<IEnumerable<FlowProduct>> GetByDate(int salonId, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (salonId > 0)
                {
                    return await objEntity.AsNoTracking()
                    .Where(a => a.IsDelete == 0 && a.CreatedDate >= dateFrom && a.CreatedDate < dateTo.AddDays(Constant.ADD_DAY) && a.SalonId == salonId && a.ComboId == null).Select(a => a).ToListAsync();
                }

                return await objEntity.AsNoTracking()
                    .Where(a => a.IsDelete == 0 && a.CreatedDate >= dateFrom && a.CreatedDate < dateTo.AddDays(Constant.ADD_DAY) && a.ComboId == null).Select(a => a).ToListAsync();
            }
            catch (Exception e)
            {
                context.Dispose();
                throw e;
            }
        }

        public async Task<IEnumerable<FlowProduct>> GetByDateListSalon(IEnumerable<int?> salonIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                return await objEntity.AsNoTracking()
                    .Where(a => a.IsDelete == 0 && a.CreatedDate >= dateFrom.AddDays(-2) && a.CreatedDate < dateTo.AddDays(Constant.ADD_DAY) && a.ComboId == null && salonIds.Contains(a.SalonId)).Select(a => a).ToListAsync();
            }
            catch (Exception)
            {
                context.Dispose();
                throw;
            }
        }
    }
}
