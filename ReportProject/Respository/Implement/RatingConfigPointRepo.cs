﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class RatingConfigPointRepo : IRatingConfigPointRepo
    {
        private Solution_30shineContext context;
        private DbSet<RatingConfigPoint> objEntity;
        public RatingConfigPointRepo()
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<RatingConfigPoint>();
        }
        public async Task<IEnumerable<RatingConfigPoint>> GetAllRatingConfigPoint()
        {
            try
            {
                return await objEntity.AsNoTracking().Where(a => a.IsDelete == 0 && a.Status == 1 && a.Hint == 3).ToListAsync();
            }
            catch (Exception)
            {
                context.Dispose();
                throw;
            }
        }
    }
}
