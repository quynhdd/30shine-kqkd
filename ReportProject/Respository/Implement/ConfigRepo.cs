﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Utils;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class ConfigRepo : IConfigRepo
    {
        private Solution_30shineContext context;
        private DbSet<TblConfig> objEntity;

        public ConfigRepo()
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<TblConfig>();
        }

        public async Task<TblConfig> GetByKey(string key)
        {
            try
            {
                return await objEntity.FirstOrDefaultAsync(a => a.Key == key);
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public async Task<IEnumerable<TblConfig>> GetByKeys(IEnumerable<string> keys)
        {
            try
            {
                return await objEntity.Where(a => a.IsDelete == 0 && a.Status == 1 && keys.Contains(a.Key)).ToListAsync();
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public string GetEmail(string EMAIL_KEY)
        {
            try
            {
                var email = objEntity.AsNoTracking().Where(a => a.IsDelete != 1 && a.Key == EMAIL_KEY).Select(a => a.Value).FirstOrDefault();
                return email;
                
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
