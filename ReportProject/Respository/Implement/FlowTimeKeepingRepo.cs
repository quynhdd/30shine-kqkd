﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using ReportProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.FlowTimeKeepingJoin;

namespace ReportProject.Respository.Implement
{
    public class FlowTimeKeepingRepo : IFlowTimeKeepingRepo
    {
        private Solution_30shineContext context;
        private DbSet<FlowTimeKeeping> objFlowTimeKeeping;
        
        public FlowTimeKeepingRepo()
        {
            context = new Solution_30shineContext();
            objFlowTimeKeeping = context.Set<FlowTimeKeeping>();
            //BookHourRepo = bookHourRepo;
        }

        public async Task<IEnumerable<FlowTimeKeepingJoinWorkTime>> GetByStylistSalonAndDate(IEnumerable<int?> salonIds, IEnumerable<int?> Stafftypes, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                return await (from a in objFlowTimeKeeping
                              join b in context.WorkTime on a.WorkTimeId equals b.Id
                              join s in context.Staff on a.StaffId equals s.Id
                              where
                              a.IsDelete == 0 && a.IsEnroll == true && a.WorkDate >= dateFrom && a.WorkDate < dateTo && salonIds.Contains(a.SalonId)
                              //&& (s.Type == Constant.STYLIST || s.Type == Constant.SKINNER)
                              && Stafftypes.Contains(s.Type)
                              select new FlowTimeKeepingJoinWorkTime
                              {
                                  Id = a.Id,
                                  StaffId = a.StaffId,
                                  SalonId = a.SalonId,
                                  WorkDate = a.WorkDate,
                                  HourIds = a.HourIds,
                                  //WorkHour = NumberOverTime(a.HourIds, b.StrartTime, b.EnadTime, ListBookHours.Where(p => p.SalonId == a.SalonId)),
                                  WorkTimeId = a.WorkTimeId,
                                  HourIdNumber = a.HourIds.Length - a.HourIds.Replace(",", "").Length + 1,
                                  TotalHourFrame = b.TotalHourFrame,
                                  StrartTime = b.StrartTime,
                                  EnadTime = b.EnadTime,
                                  staffType = s.Type
                              }
                    ).ToListAsync();
            }
            catch (Exception e)
            {
                context.Dispose();
                throw;
            }
        }

        public async Task<IEnumerable<FlowTimeKeeping>> GetBySalonAndDate(int salonId, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                return await objFlowTimeKeeping.AsNoTracking().Where(a => a.IsDelete == 0 && a.IsEnroll == true && a.SalonId == salonId && a.WorkDate >= dateFrom && a.WorkDate < dateTo).Select(a => a).ToListAsync();
            }
            catch (Exception e)
            {
                throw;
            }
        }

    }

}
