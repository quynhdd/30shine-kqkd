﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using ReportProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.AccountingReportJson;

namespace ReportProject.Respository.Implement
{
    public class StaffBillServiceDetailRepo : IStaffBillServiceDetailRepo
    {
        private Solution_30shineContext context;
        private DbSet<StaffBillServiceDetail> objEntity;

        public StaffBillServiceDetailRepo(IFlowServiceRepo flowServiceRepo, IFlowProductRepo flowProductRepo, ISalonRepo salonRepo)
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<StaffBillServiceDetail>();

        }

        public async Task<IEnumerable<StaffBillServiceDetailJson>> Get(DateTime dateFrom, DateTime dateTo,int salonId, int departmentId)
        {
            try
            {
                return await objEntity.Where(a => a.IsDelete == false && a.WorkDate >= dateFrom && a.WorkDate < dateTo && a.OvertimeStatusValue == Constant.NOT_OVERTIME && a.DepartmentId == Constant.STYLIST  && ((a.BillSalonId == salonId) || (salonId == 0)) && ((a.DepartmentId == departmentId) || (departmentId == 0))).Select(a => new StaffBillServiceDetailJson
                {
                    billId = a.BillId,
                    staffId = a.StaffId
                }).Distinct().ToListAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<IEnumerable<StaffBillServiceDetail>> GetByDate(int staffType, DateTime dateFrom, DateTime dateTo , int salonId)
        {
            try
            {
                return await objEntity.Where(a => a.IsDelete == false && a.WorkDate >= dateFrom && a.WorkDate < dateTo && ((a.DepartmentId == staffType) || (staffType == 0)) && ((a.BillSalonId == salonId) || (salonId == 0))).Select(a => a).ToListAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
