﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using ReportProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.AccountingReportJson;

namespace ReportProject.Respository.Implement
{
    public class AccountingReportRepo : IAccountingReportRepo
    {
        private Solution_30shineContext context;
        private DbSet<StaffBillServiceDetail> objStaffBillServiceDetail;
        private DbSet<SalaryConfig> objSalaryConfig;
        private IBillServiceRepo BillServiceRepo { get; set; }
        private ISalonRepo SalonRepo { get; set; }
        private IFlowServiceRepo FlowServiceRepo { get; set; }
        private IFlowProductRepo FlowProductRepo { get; set; }
        private ISalonDailyCostRepo SalonDailyCostRepo { get; set; }
        private IStaffBillServiceDetailRepo StaffBillServiceDetailRepo { get; set; }
        private IStaffRepo StaffRepo { get; set; }
        private IFlowTimeKeepingRepo FlowTimeKeepingRepo { get; set; }
        public AccountingReportRepo(IBillServiceRepo billServiceRepo, ISalonRepo salonRepo, IFlowServiceRepo flowServiceRepo, IFlowProductRepo flowProductRepo,
            ISalonDailyCostRepo salonDailyCostRepo, IStaffBillServiceDetailRepo staffBillServiceDetailRepo, IStaffRepo staffRepo, IFlowTimeKeepingRepo flowTimeKeepingRepo)
        {
            context = new Solution_30shineContext();
            objStaffBillServiceDetail = context.Set<StaffBillServiceDetail>();
            objSalaryConfig = context.Set<SalaryConfig>();
            BillServiceRepo = billServiceRepo;
            SalonRepo = salonRepo;
            FlowServiceRepo = flowServiceRepo;
            FlowProductRepo = flowProductRepo;
            SalonDailyCostRepo = salonDailyCostRepo;
            StaffBillServiceDetailRepo = staffBillServiceDetailRepo;
            StaffRepo = staffRepo;
            FlowTimeKeepingRepo = flowTimeKeepingRepo;
        }

        public async Task<IEnumerable<GetAccountingReportReturnJson>> GetAccountingReports(int salonId, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var dataList = BillServiceRepo.GetObjectReport(salonId, dateFrom, dateTo);

                var staffBillServiceDetailList = StaffBillServiceDetailRepo.Get(dateFrom, dateTo, 0, Constant.STYLIST);

                Task.WaitAll(dataList, staffBillServiceDetailList);
                // list bill service
                var billServices = dataList.Result.billServices;
                // list flowService
                var flowServices = dataList.Result.flowServices;
                // list flowProduct
                var flowProducts = dataList.Result.flowProducts;
                // List staffBillServiceDetail
                var staffBillServiceDetail = staffBillServiceDetailList.Result;
                // List billServiceJoinFlowServices => tong Doanh Thu Dich Vu  : A1
                var billServiceJoinFlowServices = BillServiceRepo.BillServiceJoinFlowServiceConvertMark(billServices, flowServices);
                // list dich vu Thẻ quà tặng Khách mới theo salon
                var billServiceFreeEvent =
                    billServiceJoinFlowServices
                    .Where(a => a.serviceId == Constant.GIFT_CARD_TO_CUSTOMER)
                    .GroupBy(x => x.salonId)
                    .Select(w => new
                    {
                        salonId = w.Key,
                        totalGiftCardToCustomer = w.Sum(x => x.quantity)
                    }).ToList();
                // List  billServiceJoinFlowProducts => Tong Doanh Thu My Pham : A2
                var billServiceJoinFlowProducts = BillServiceRepo.BillServiceJoinFlowProduct(billServices, flowProducts);

                // List tinh shineCombo giam tru theo ngay
                var shineComboWithoutVoucher = (from a in billServiceJoinFlowServices
                                                join b in staffBillServiceDetail on a.billId equals b.billId
                                                where a.serviceId == Constant.SHINE_COMBO && a.voucherPercent == 0 && a.mark == Constant.RAT_HAI_LONG
                                                group a by new { a.salonId, a.CompleteBillTime.Value.Date, b.staffId } into ab
                                                select new
                                                {
                                                    salonId = ab.Key.salonId,
                                                    workDate = ab.Key.Date,
                                                    staffId = ab.Key.staffId,
                                                    revokeNumberRHL4 = ab.Select(x => x.billId).Count() / 4,
                                                    RLH3NumberOld = ab.Select(x => x.billId).Count()
                                                }).ToList();

                var report = new List<AccountingReportJson.GetAccountingReportReturnJson>();

                var flowServiceSalon = new List<BillServiceJoin.BillServiceJoinFlowServiceJson>();
                var flowProductSalon = new List<BillServiceJoin.BillServiceJoinFlowProductJson>();
                double totalServiceIncome, totalProductIncome, totalIncome, totalRemainServiceIncome, sendCompanyAccount, swipeCardMoney, sendPersonalAccount;
                int shineCombo, revokeNumberRHL4;

                foreach (var salon in dataList.Result.salons)
                {
                    var id = salon.Id;

                    flowServiceSalon = billServiceJoinFlowServices.Where(a => a.salonId == id).ToList();

                    flowProductSalon = billServiceJoinFlowProducts.Where(a => a.salonId == id).ToList();

                    int billServiceShineFree = billServiceFreeEvent.Where(a => a.salonId == id).Select(a => a.totalGiftCardToCustomer ?? 0).FirstOrDefault();
                    var shineComboWithoutVoucherSalon = shineComboWithoutVoucher.Where(a => a.salonId == id).ToList();
                    totalServiceIncome = flowServiceSalon.Sum(a => a.totalServiceIncome);

                    totalProductIncome = flowProductSalon.Sum(a => a.totalProductIncome);

                    ////Tổng doanh thu dịch vụ và mỹ phẩm
                    totalIncome = totalServiceIncome + totalProductIncome - (double)billServiceShineFree * 100000;

                    if (totalIncome > 0)
                    {
                        //Số lượng shine combo max điểm hài lòng không giảm giá
                        shineCombo = (int)shineComboWithoutVoucherSalon.Sum(a => a.RLH3NumberOld);

                        //Số lượng shine combo giảm trừ
                        revokeNumberRHL4 = (int)shineComboWithoutVoucherSalon.Sum(a => a.revokeNumberRHL4);

                        //Doanh thu của các service không phải là shine combo
                        totalRemainServiceIncome = flowServiceSalon.Where(a => a.serviceId != Constant.SHINE_COMBO && a.voucherPercent == 0).Sum(a => a.totalServiceIncome);
                        //Quẹt thẻ 
                        swipeCardMoney = flowServiceSalon.Where(a => a.isPayByCard == true).Sum(a => a.totalServiceIncome) + flowProductSalon.Where(a => a.isPayByCard == true).Sum(a => a.totalProductIncome);

                        var data = new GetAccountingReportReturnJson
                        {
                            salonId = id,
                            salonName = salon.ShortName,
                            totalIncome = totalIncome,//A
                            totalServiceIncome = totalServiceIncome, //A1
                            totalProductIncome = totalProductIncome,//A2
                            shineComboFreeIncome = (double)billServiceShineFree * 100000, //A3
                            shineCombo = shineCombo,//F
                            revokeNumberRHL4 = revokeNumberRHL4,//F*
                            shineComboFreeNumber = (int)billServiceShineFree,//E
                            sendCompanyAccount = totalServiceIncome - (revokeNumberRHL4 * 100000),//B
                            sendPersonalAccount = totalIncome - (totalServiceIncome - (revokeNumberRHL4 * 100000)) - swipeCardMoney,//C1
                            swipeCardMoney = swipeCardMoney
                        };

                        report.Add(data);
                    }
                }
                return report;

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<AccountingTaxReportJson> GetAccountingTaxReports(int salonId, int staffType, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var billServiceList = BillServiceRepo.GetByDate(salonId, dateFrom, dateTo);
                var staffBillServiceDetailList = StaffBillServiceDetailRepo.GetByDate(staffType, dateFrom, dateTo, 0);
                var salaryIncomeList = context.SalaryIncome.AsNoTracking().Where(a => a.IsDeleted == false && a.WorkDate >= dateFrom && a.WorkDate < dateTo && a.SalonId == salonId).ToListAsync();
                var flowTimeKeepingList = FlowTimeKeepingRepo.GetBySalonAndDate(salonId, dateFrom, dateTo);
                var staffList = StaffRepo.GetStylistAndSkinner(staffType);
                var staffTypeList = context.StaffType.AsNoTracking().Where(a => a.Id == Constant.STYLIST || a.Id == Constant.SKINNER).Select(a => a).ToListAsync();
                var skillLevelList = context.TblSkillLevel.AsNoTracking().Select(a => a).ToListAsync();
                var flowServiceList = FlowServiceRepo.GetByDate(salonId, dateFrom, dateTo);
                var serviceList = context.Service.AsNoTracking().Select(a => a).ToListAsync();
                var salon = context.TblSalon.AsNoTracking().Where(a => a.Id == salonId).Select(a => a).FirstOrDefaultAsync();
                //var ratingConfig = context.RatingConfigPoint.Where(a => a.IsDelete == 0 && a.Status == 1).Select(a => a).ToListAsync();

                Task.WaitAll(billServiceList, staffBillServiceDetailList, salaryIncomeList, flowTimeKeepingList, staffList, staffTypeList, skillLevelList, flowServiceList, serviceList, salon /*ratingConfig*/);

                var billService = billServiceList.Result;
                var staffBillServiceDetail = staffBillServiceDetailList.Result;
                var salaryIncome = salaryIncomeList.Result;
                var flowTimeKeeping = flowTimeKeepingList.Result;
                var staff = staffList.Result;
                var staffTypes = staffTypeList.Result;
                var skillLevel = skillLevelList.Result;
                var flowService = flowServiceList.Result;
                var services = serviceList.Result;
                var salonName = salon.Result.Name;
                //var ratingConfigRHL = ratingConfig.Result;
                var staffInfoListAll = StaffRepo.GetStaffInfo(billService, staff, salaryIncome, skillLevel, staffTypes);

                var staffInfoList = staffInfoListAll.Select(a => new
                {
                    a.StaffId,
                    a.StaffName,
                    a.StaffType
                }).Distinct().ToList();

                var salaryFix = staffInfoListAll.Select(a => new
                {
                    a.StaffId,
                    a.WorkDate,
                    a.FixSalary,
                    a.BehaveSalary
                }).Distinct().ToList();

                var staffLevel = (from a in staffInfoListAll
                                  join b in staffBillServiceDetail on a.StaffId equals b.StaffId
                                  join c in skillLevel on b.StaffLevelId equals c.Id
                                  select new
                                  {
                                      a.StaffId,
                                      c.Name,
                                      b.CreateTime
                                  }).OrderByDescending(a => a.CreateTime).ToList();

                var staffLevelNow = (from a in staff
                                     join b in skillLevel on a.SkillLevel equals b.Id
                                     select new
                                     {
                                         a.Id,
                                         b.Name
                                     }).ToList();

                if (!staffInfoList.Any())
                {
                    return null;
                }

                var billServiceDetails = BillServiceRepo.GetBillServiceDetails(billService, staffBillServiceDetail, flowService);
                // tinh toan giam tru shine combo
                var billServiceDetailsShineComboReduce = (from a in billServiceDetails
                                                          where
                                                          a.RatingMark == Constant.RAT_HAI_LONG
                                                          && a.ServiceId == Constant.SHINE_COMBO
                                                          && a.VoucherPercent == Constant.NOT_DISCOUNT
                                                          && a.OvertimeStatusValue == Constant.NOT_OVERTIME

                                                          group a by new
                                                          {
                                                              a.StaffId,
                                                              a.CompleteBillTime.Value.Date
                                                          } into g
                                                          select new
                                                          {
                                                              staffId = g.Key.StaffId,
                                                              workDate = g.Key.Date,
                                                              revokeNumberRHL4 = g.Select(x => x.BillId).Count() / 4,
                                                              existNumberRHL3 = g.Select(x => x.BillId).Count() % 4,
                                                              RLH3NumberOld = g.Select(x => x.BillId).Count()
                                                          }).ToList();
                int staffId, workDay, totalRevokeNumberRHL4, totalExistNumberRHL3, totalRHL3Old, totalShineCombo, RHL4Number, RHL3Number, disCountRHL3Number, HLNumber, KHLNumber, RHL5OverTimeDayNumber, RHL5OverTimeHourNumber;

                double serviceIncomeBonusNotOverTime, serviceIncomeBonusOverTimeHour, serviceIncomeBonusOverTimeDay, totalSalary, fixSalartByDay, totalServiceRatedVerySatisfied, RHL4, RHL3, disCountRHL3, HL, KHL, RHL5OverTimeDay, RHL5OverTimeHour, totalServiceTurnoverBonusAll;

                string serviceName = "", staffName = "", level = "", departmentName = "";

                var objDataList = new List<ObjData>();

                var coeficientVerySatisfied = (double)billServiceDetails.Where(w => w.RatingMark == Constant.RAT_HAI_LONG).OrderBy(w => w.ConventionPoint).Select(w => w.ConventionPoint).FirstOrDefault();

                foreach (var staffData in staffInfoList)
                {
                    //Lấy thông tin một nhân viên
                    staffId = (int)staffData.StaffId;
                    staffName = staffData.StaffName;
                    level = staffLevel.Where(a => a.StaffId == staffId).Select(a => a.Name).FirstOrDefault() ?? staffLevelNow.Where(a => a.Id == staffId).Select(a => a.Name).FirstOrDefault();
                    departmentName = staffData.StaffType;
                    fixSalartByDay = (double)salaryFix.Where(a => a.StaffId == staffId).Sum(a => a.FixSalary + a.BehaveSalary);
                    // xu lý rieng cho giam tru bill co dich vu shineCombo
                    var billServiceDetailsShineComboReduceStaff = billServiceDetailsShineComboReduce.Where(a => a.staffId == staffId).Select(a => a).ToList();
                    // xu lý chung
                    var billServiceDetail = billServiceDetails.Where(a => a.StaffId == staffId).Select(a => a).ToList();
                    serviceIncomeBonusNotOverTime = billServiceDetail.Where(a => a.OvertimeStatusValue == Constant.NOT_OVERTIME).Sum(a => a.ServiceIncomeBonus);
                    serviceIncomeBonusOverTimeHour = billServiceDetail.Where(a => a.OvertimeStatusValue == Constant.OVERTIME_HOUR).Sum(a => a.ServiceIncomeBonus);
                    serviceIncomeBonusOverTimeDay = billServiceDetail.Where(a => a.OvertimeStatusValue == Constant.OVERTIME_DAY).Sum(a => a.ServiceIncomeBonus);
                    // tinh so luong RHL3 giam gia 
                    disCountRHL3Number = billServiceDetail.Where(a => a.VoucherPercent > Constant.NOT_DISCOUNT && a.RatingMark == Constant.RAT_HAI_LONG && a.ServiceId == Constant.SHINE_COMBO && a.OvertimeStatusValue == Constant.NOT_OVERTIME).Select(a => a.BillId).Count();
                    // tinh diem hai long RHL giam gia
                    disCountRHL3 = billServiceDetail.Where(a => a.VoucherPercent > Constant.NOT_DISCOUNT && a.RatingMark == Constant.RAT_HAI_LONG && a.ServiceId == Constant.SHINE_COMBO && a.OvertimeStatusValue == Constant.NOT_OVERTIME).Sum(a => a.Coeficient);
                    //totalServiceRatedVerySatisfied = billServiceDetail.Count(a => a.RatingMark == Constant.RAT_HAI_LONG);
                    totalServiceTurnoverBonusAll = serviceIncomeBonusNotOverTime + serviceIncomeBonusOverTimeHour + serviceIncomeBonusOverTimeDay;

                    workDay = flowTimeKeeping.Count(a => a.StaffId == staffId);
                    totalSalary = fixSalartByDay + serviceIncomeBonusNotOverTime + serviceIncomeBonusOverTimeDay + serviceIncomeBonusOverTimeHour;

                    var shineCombo = new ObjDataShineCombo();
                    var otherService = new List<ObjDataOtherService>();

                    var staffServices = billServiceDetail.Select(a => new
                    {
                        a.StaffId,
                        a.ServiceId
                    }).Distinct().ToList();

                    foreach (var serviceData in staffServices)
                    {
                        //Lấy thông tin các service của nhân viên
                        var serviceDetail = billServiceDetail.Where(a => a.ServiceId == serviceData.ServiceId).ToList();
                        var verySatisfiedOvertimeDay = serviceDetail.Where(a => a.RatingMark == Constant.RAT_HAI_LONG && a.OvertimeStatusValue == Constant.OVERTIME_DAY).ToList();
                        var verySatisfiedOvertimeHour = serviceDetail.Where(a => a.RatingMark == Constant.RAT_HAI_LONG && a.OvertimeStatusValue == Constant.OVERTIME_HOUR).ToList();
                        var satisfied = serviceDetail.Where(a => a.RatingMark == Constant.HAI_LONG).ToList();
                        var notSatisfied = serviceDetail.Where(a => a.RatingMark == Constant.KHONG_HAI_LONG).ToList();

                        serviceName = services.Where(a => a.Id == serviceData.ServiceId).Select(a => a.Name).FirstOrDefault();
                        //RHL5OverTimeDay = (double) (verySatisfiedOvertimeDay.Sum(a => a.Coeficient) * coeficientVerySatisfied);
                        RHL5OverTimeDay = (double)(verySatisfiedOvertimeDay.Sum(a => a.Coeficient));
                        //RHL5OverTimeHour = (double)(verySatisfiedOvertimeHour.Sum(a => a.Coeficient) * coeficientVerySatisfied);
                        RHL5OverTimeHour = (double)(verySatisfiedOvertimeHour.Sum(a => a.Coeficient));
                        //HL = (double)(satisfied.Sum(a => a.Coeficient) * coeficientSatisfied);
                        HL = (double)(satisfied.Sum(a => a.Coeficient));
                        //KHL = (double)(notSatisfied.Sum(a => a.Coeficient) * coeficientNotSatisfied);
                        KHL = (double)(notSatisfied.Sum(a => a.Coeficient));

                        totalRevokeNumberRHL4 = billServiceDetailsShineComboReduceStaff.Sum(x => x.revokeNumberRHL4);
                        totalExistNumberRHL3 = billServiceDetailsShineComboReduceStaff.Sum(x => x.existNumberRHL3);
                        totalRHL3Old = billServiceDetailsShineComboReduceStaff.Sum(x => x.RLH3NumberOld);
                        RHL5OverTimeDayNumber = verySatisfiedOvertimeDay.Sum(a => a.Quantity);
                        RHL5OverTimeHourNumber = verySatisfiedOvertimeHour.Sum(a => a.Quantity);
                        // SL RHL4
                        RHL4Number = (int)(coeficientVerySatisfied * totalRevokeNumberRHL4);
                        // Diem RHL
                        RHL4 = coeficientVerySatisfied * totalRevokeNumberRHL4 * Constant.COEFICIENT_RATING_4_SHINECOMBO;
                        // SL RHL3 DisCount
                        // Diem RHL3 DisCount
                        HLNumber = satisfied.Sum(a => a.Quantity);
                        KHLNumber = notSatisfied.Sum(a => a.Quantity);
                        // tinh rieng dich vu shinecombo
                        if (serviceData.ServiceId == Constant.SHINE_COMBO)
                        {
                            // so luong RLH
                            RHL3Number = totalExistNumberRHL3;
                            // diem RHL 3
                            RHL3 = (double)(totalExistNumberRHL3 * coeficientVerySatisfied);

                            totalShineCombo = RHL5OverTimeDayNumber + RHL5OverTimeHourNumber + RHL4Number + RHL3Number + disCountRHL3Number + HLNumber + KHLNumber + totalRevokeNumberRHL4;

                            shineCombo = new ObjDataShineCombo
                            {
                                serviceName = serviceName,
                                totalNumberOfShineCombo = totalShineCombo,
                                totalServiceRatedVerySatisfiedOld = totalRHL3Old,
                                totalServiceRatedVerySatisfiedNew = totalRevokeNumberRHL4,
                                verySatisfied5Hour = RHL5OverTimeHour,
                                verySatisfied5Day = RHL5OverTimeDay,
                                verySatisfied4 = RHL4,
                                verySatisfied3 = RHL3,
                                verySatisfied3Discount = disCountRHL3,
                                satisfied = HL,
                                notSatisfied = KHL
                            };
                        }
                        else
                        {
                            RHL3 = (double)(serviceDetail.Where(a => a.RatingMark == Constant.RAT_HAI_LONG && a.OvertimeStatusValue == Constant.NOT_OVERTIME).Sum(a => a.Coeficient));
                            RHL3Number = serviceDetail.Where(a => a.RatingMark == Constant.RAT_HAI_LONG && a.OvertimeStatusValue == Constant.NOT_OVERTIME).Sum(a => a.Quantity);

                            var otherServiceData = new ObjDataOtherService
                            {
                                serviceName = serviceName,
                                totalServiceNumber = RHL5OverTimeDayNumber + RHL5OverTimeHourNumber + RHL3Number + HLNumber + KHLNumber,
                                verySatisfied5Hour = RHL5OverTimeHour,
                                verySatisfied5Day = RHL5OverTimeDay,
                                verySatisfied3 = RHL3,
                                satisfied = HL,
                                notSatisfied = KHL
                            };

                            otherService.Add(otherServiceData);
                        }
                    }
                    // Tong diem hai long
                    totalServiceRatedVerySatisfied = billServiceDetail.Sum(a => a.Coeficient);

                    var objData = new ObjData
                    {
                        salonName = salonName,
                        id = staffId,
                        staffName = staffName,
                        level = level,
                        departmentName = departmentName,
                        totalSalary = totalSalary,
                        fixSalaryByDay = fixSalartByDay,
                        totalServiceTurnoverBonusAll = (int)totalServiceTurnoverBonusAll,
                        totalServiceTurnoverBonus = (int)serviceIncomeBonusNotOverTime,
                        totalServiceTurnoverHoursOvertimeBonus = (int)serviceIncomeBonusOverTimeHour,
                        totalServiceTurnoverDayOvertimeBonus = (int)serviceIncomeBonusOverTimeDay,
                        workDay = workDay,
                        totalServiceRatedVerySatisfied = totalServiceRatedVerySatisfied,
                        dataShineCombo = shineCombo,
                        dataOtherService = otherService
                    };

                    objDataList.Add(objData);
                }

                #region Find Total Service and Add Missing Service

                //Lấy list toàn bộ các service
                var listServiceName = new List<string>();

                foreach (var item in objDataList)
                {
                    var itemServiceName = item.dataOtherService.Select(a => a.serviceName).ToList();
                    foreach (var name in itemServiceName)
                    {
                        listServiceName.Add(name);
                    }
                }

                listServiceName = listServiceName.Distinct().ToList();

                //Add service vào những staff còn thiếu
                foreach (var item in objDataList)
                {
                    var itemServiceName = item.dataOtherService.Select(a => a.serviceName).ToList();

                    if (listServiceName.Count > itemServiceName.Count)
                    {
                        var notItemService = listServiceName.Except(itemServiceName).ToList();

                        foreach (var notItem in notItemService)
                        {
                            var dataNotIn = new ObjDataOtherService
                            {
                                serviceName = notItem,
                                totalServiceNumber = 0,
                                verySatisfied5Hour = 0,
                                verySatisfied5Day = 0,
                                verySatisfied3 = 0,
                                satisfied = 0,
                                notSatisfied = 0
                            };

                            item.dataOtherService.Add(dataNotIn);
                        }
                    }

                    item.dataOtherService = item.dataOtherService.OrderBy(a => a.serviceName).ToList();
                }
                #endregion

                #region Add Header

                var allService = objDataList.First();

                var header = AddHeader(allService);

                #endregion

                #region Calculate Total

                var otherServiceList = new List<ObjDataOtherService>();
                foreach (var item in objDataList.Select(a => a.dataOtherService).ToList())
                {
                    otherServiceList.AddRange(item);
                }

                var totalOtherServiceList = new List<ObjDataOtherService>();
                foreach (var item in allService.dataOtherService)
                {
                    var otherService = otherServiceList.Where(a => a.serviceName == item.serviceName).ToList();
                    var totalOtherService = new ObjDataOtherService
                    {
                        serviceName = item.serviceName,
                        totalServiceNumber = otherService.Sum(a => a.totalServiceNumber),
                        verySatisfied5Hour = otherService.Sum(a => a.verySatisfied5Hour),
                        verySatisfied5Day = otherService.Sum(a => a.verySatisfied5Day),
                        verySatisfied3 = otherService.Sum(a => a.verySatisfied3),
                        satisfied = otherService.Sum(a => a.satisfied),
                        notSatisfied = otherService.Sum(a => a.notSatisfied)
                    };

                    totalOtherServiceList.Add(totalOtherService);
                }

                var total = new ObjData
                {
                    salonName = salonName,
                    id = 0,
                    staffName = string.Empty,
                    level = string.Empty,
                    departmentName = string.Empty,
                    totalSalary = objDataList.Sum(a => a.totalSalary),
                    fixSalaryByDay = objDataList.Sum(a => a.fixSalaryByDay),
                    totalServiceTurnoverBonusAll = objDataList.Sum(a => a.totalServiceTurnoverBonusAll),
                    totalServiceTurnoverBonus = objDataList.Sum(a => a.totalServiceTurnoverBonus),
                    totalServiceTurnoverHoursOvertimeBonus = objDataList.Sum(a => a.totalServiceTurnoverHoursOvertimeBonus),
                    totalServiceTurnoverDayOvertimeBonus = objDataList.Sum(a => a.totalServiceTurnoverDayOvertimeBonus),
                    workDay = objDataList.Sum(a => a.workDay),
                    totalServiceRatedVerySatisfied = objDataList.Sum(a => a.totalServiceRatedVerySatisfied),
                    dataShineCombo = new ObjDataShineCombo
                    {
                        serviceName = objDataList.Select(a => a.dataShineCombo.serviceName).First(),
                        totalNumberOfShineCombo = objDataList.Sum(a => a.dataShineCombo.totalNumberOfShineCombo),
                        totalServiceRatedVerySatisfiedOld = objDataList.Sum(a => a.dataShineCombo.totalServiceRatedVerySatisfiedOld),
                        totalServiceRatedVerySatisfiedNew = objDataList.Sum(a => a.dataShineCombo.totalServiceRatedVerySatisfiedNew),
                        verySatisfied5Hour = objDataList.Sum(a => a.dataShineCombo.verySatisfied5Hour),
                        verySatisfied5Day = objDataList.Sum(a => a.dataShineCombo.verySatisfied5Day),
                        verySatisfied4 = objDataList.Sum(a => a.dataShineCombo.verySatisfied4),
                        verySatisfied3 = objDataList.Sum(a => a.dataShineCombo.verySatisfied3),
                        verySatisfied3Discount = objDataList.Sum(a => a.dataShineCombo.verySatisfied3Discount),
                        satisfied = objDataList.Sum(a => a.dataShineCombo.satisfied),
                        notSatisfied = objDataList.Sum(a => a.dataShineCombo.notSatisfied)
                    },
                    dataOtherService = totalOtherServiceList.OrderBy(a => a.serviceName).ToList()
                };


                #endregion

                var report = new AccountingTaxReportJson
                {
                    header = header,
                    total = total,
                    data = objDataList.OrderBy(a => a.id).ToList()
                };

                return report;

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private Header AddHeader(ObjData allService)
        {
            try
            {
                var headerService = new List<HeaderOtherService>();

                foreach (var item in allService.dataOtherService)
                {
                    var dataHeaderService = new HeaderOtherService
                    {
                        serviceName = item.serviceName,
                        subHeader = new SubHeaderOtherService
                        {
                            totalServiceNumber = "Tổng số lượng dịch vụ",
                            verySatisfied5Hour = "RHL 5**",
                            verySatisfied5Day = "RHL 5*",
                            verySatisfied3 = "RHL 3*",
                            satisfied = "HL",
                            notSatisfied = "KHL"
                        }
                    };

                    headerService.Add(dataHeaderService);
                }

                var header = new Header
                {
                    salonName = "Tên Salon",
                    id = "Id",
                    staffName = "Tên Nhân Viên",
                    level = "Level",
                    departmentName = "Bộ Phận",
                    totalSalary = "Tổng Lương",
                    fixSalaryByDay = "Lương Cơ Bản Theo Ngày Công",
                    totalServiceTurnoverBonusAll = "Tổng Thưởng Doanh Số Dịch Vụ",
                    totalServiceTurnoverBonus = "Tổng Thưởng Doanh Số Dịch Vụ - Không Tăng Ca",
                    totalServiceTurnoverHoursOvertimeBonus = "Tổng Thưởng Doanh Số Dịch Vụ - Tăng Ca Giờ",
                    totalServiceTurnoverDayOvertimeBonus = "Tổng Thưởng Doanh Số Dịch Vụ - Tăng Ca Ngày",
                    workDay = "Ngày Công",
                    totalServiceRatedVerySatisfied = "Tổng Điểm Hài Lòng",
                    shineComBo = new HeaderShineCombo
                    {
                        serviceName = allService.dataShineCombo.serviceName,
                        subHeader = new SubHeaderShineCombo
                        {
                            totalNumberOfShineCombo = "Tổng Số Lượng Shine Combo",
                            totalServiceRatedVerySatisfiedOld = "Tổng Bill Shine Combo Cũ RHL",
                            totalServiceRatedVerySatisfiedNew = "Tổng Bill Shine Combo RHL Bỏ",
                            verySatisfied5Hour = "RHL 5**",
                            verySatisfied5Day = "RHL 5*",
                            verySatisfied4 = "RHL 4",
                            verySatisfied3 = "RHL 3",
                            verySatisfied3Discount = "RHL3 Giảm giá ",
                            satisfied = "HL",
                            notSatisfied = "KHL"
                        }
                    },
                    service = headerService.OrderBy(a => a.serviceName).ToList()
                };

                return header;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
