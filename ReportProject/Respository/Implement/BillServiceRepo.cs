﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.AccountingReportJson;
using static ReportProject.Models.JsonModels.BillServiceJoin;
using static ReportProject.Models.JsonModels.BillserviceJson;
using static ReportProject.Models.JsonModels.RatingDetailJson;

namespace ReportProject.Respository.Implement
{
    public class BillServiceRepo : IBillServiceRepo
    {
        private Solution_30shineContext context;
        private DbSet<BillServiceHis> objEntity;
        private DbSet<FlowService> objFlowService;
        private IFlowServiceRepo FlowServiceRepo { get; set; }
        private IFlowProductRepo FlowProductRepo { get; set; }
        private ISalonRepo SalonRepo { get; set; }

        public BillServiceRepo(IFlowServiceRepo flowServiceRepo, IFlowProductRepo flowProductRepo, ISalonRepo salonRepo)
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<BillServiceHis>();
            objFlowService = context.Set<FlowService>();
            FlowServiceRepo = flowServiceRepo;
            FlowProductRepo = flowProductRepo;
            SalonRepo = salonRepo;
        }

        public async Task<IEnumerable<BillServiceHis>> GetByDate(int salonId, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (salonId > 0)
                {
                    return await objEntity.AsNoTracking().Where(a =>
                    a.IsDelete == 0 && a.Pending == 0 && a.CompleteBillTime >= dateFrom && a.CompleteBillTime < dateTo && a.SalonId == salonId).Select(a => a).ToListAsync();
                }

                return await objEntity.AsNoTracking().Where(a =>
                    a.IsDelete == 0 && a.Pending == 0 && a.CompleteBillTime >= dateFrom && a.CompleteBillTime < dateTo).Select(a => a).ToListAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public IEnumerable<BillServiceJoinFlowServiceJson> BillServiceJoinFlowService(IEnumerable<BillServiceHis> billServices, IEnumerable<FlowService> flowServices)
        {
            try
            {
                return (from a in billServices
                        join b in flowServices on a.Id equals b.BillId
                        where !string.IsNullOrEmpty(a.ServiceIds)
                        select new BillServiceJoinFlowServiceJson
                        {
                            billId = a.Id,
                            salonId = a.SalonId,
                            serviceId = b.ServiceId,
                            quantity = b.Quantity,
                            totalServiceIncome = (double)(b.Quantity * b.Price * ((1 - decimal.Divide((decimal)b.VoucherPercent, 100)))),
                            mark = a.Mark,
                            voucherPercent = b.VoucherPercent,
                            isPayByCard = a.IsPayByCard,
                            CompleteBillTime = a.CompleteBillTime
                        }).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IEnumerable<BillServiceJoinFlowProductJson> BillServiceJoinFlowProduct(IEnumerable<BillServiceHis> billServices, IEnumerable<FlowProduct> flowProducts)
        {
            try
            {
                return (from a in billServices
                        join b in flowProducts on a.Id equals b.BillId
                        select new BillServiceJoinFlowProductJson
                        {
                            salonId = a.SalonId,
                            totalProductIncome = (double)(b.Quantity * b.Price * ((1 - decimal.Divide((decimal)b.VoucherPercent, 100)))),
                            isPayByCard = a.IsPayByCard
                        }).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<ObjectReport> GetObjectReport(int salonId, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var billServicesList = GetByDate(salonId, dateFrom, dateTo);
                var flowServicesList = FlowServiceRepo.GetByDate(salonId, dateFrom, dateTo);
                var flowProductsList = FlowProductRepo.GetByDate(salonId, dateFrom, dateTo);
                var salonList = SalonRepo.Get();

                Task.WaitAll(billServicesList, flowServicesList, flowProductsList, salonList);

                var billServices = billServicesList.Result;
                var flowServices = flowServicesList.Result;
                var flowProducts = flowProductsList.Result;
                var salons = salonList.Result;

                if (salonId > 0)
                {
                    billServices = billServices.Where(a => a.SalonId == salonId);
                    flowServices = flowServices.Where(a => a.SalonId == salonId);
                    flowProducts = flowProducts.Where(a => a.SalonId == salonId);
                    salons = salons.Where(a => a.Id == salonId);
                }

                var data = new BillServiceJoin.ObjectReport
                {
                    billServices = billServices.ToList(),
                    flowServices = flowServices.ToList(),
                    flowProducts = flowProducts.ToList(),
                    salons = salons.ToList()
                };

                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IEnumerable<BillServiceDetails> GetBillServiceDetails(IEnumerable<BillServiceHis> billServices, IEnumerable<StaffBillServiceDetail> staffBillServiceDetails, IEnumerable<FlowService> flowServices)
        {
            try
            {
                var billServiceDetails = (from a in billServices
                                          join b in staffBillServiceDetails on a.Id equals b.BillId
                                          join c in flowServices on a.Id equals c.BillId
                                          where b.ServiceId == c.ServiceId //&& b.ServiceId == 53 && b.OvertimeStatusValue !=0 && b.StaffId == 516
                                          select new BillServiceDetails
                                          {
                                              BillId = a.Id,
                                              StaffId = b.StaffId,
                                              OvertimeStatusValue = b.OvertimeStatusValue,
                                              ServiceIncomeBonus = b.ServiceIncomeBonus,
                                              //RatingMark = b.RatingMark, 
                                              //  b.RatingMark = 0 or 1 or 2 or 3 or 4 or 5. Nếu b.RatingMark  = 1 or 2 => RatingMark = 1(KHL). Nếu b.RatingMark  = 3 => RatingMark = 2 (HL). Nếu b.RatingMark  = 0 or 4 or 5 = RatingMark = 3 (RHL)
                                              RatingMark = b.RatingMark == 1 ? 1 : (b.RatingMark == 2 ? 1 : (b.RatingMark == 3 ? 2 : 3)),
                                              ServiceId = b.ServiceId,
                                              VoucherPercent = c.VoucherPercent,
                                              Coeficient = b.SalaryCoeficient * b.ServiceCoeficient * b.Quantity * b.ConventionPoint + (double)b.ServiceBonus * b.Quantity,
                                              Quantity = b.Quantity,
                                              ConventionPoint = b.ConventionPoint,
                                              CompleteBillTime = a.CompleteBillTime
                                          }).Distinct().ToList();

                return billServiceDetails;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<IEnumerable<BillserviceReal>> GetBillServiceByDate(IEnumerable<int?> salonIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                return await objEntity.AsNoTracking().Where(a =>
                a.IsDelete == 0 && a.Pending == 0 && a.CompleteBillTime >= dateFrom && a.CompleteBillTime < dateTo && salonIds.Contains(a.SalonId))
                .Select(a => new BillserviceReal
                {
                    Id = a.Id,
                    StaffHairdresserId = a.StaffHairdresserId,
                    ServiceId = a.ServiceId,
                    ProductIds = a.ProductIds,
                    TotalMoney = a.TotalMoney,
                    StaffHairMassageId = a.StaffHairMassageId,
                    Mark = a.Mark,
                    ServiceIds = a.ServiceIds,
                    Images = a.Images,
                    SalonId = a.SalonId,
                    SellerId = a.SellerId,
                    Pending = a.Pending,
                    ReceptionId = a.ReceptionId,
                    CompleteBillTime = a.CompleteBillTime,
                    CustomerId = a.CustomerId,
                    IsPayByCard = a.IsPayByCard,
                    InProcedureTime = a.InProcedureTime,
                    InProcedureTimeModifed = a.InProcedureTimeModifed,
                    BookingId = a.BookingId,
                    UploadImageTime = a.UploadImageTime,
                    CheckoutId = a.CheckoutId,
                    CreatedDate = a.CreatedDate,
                    CustomerUsedNumber = a.CustomerUsedNumber

                }).ToListAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<IEnumerable<BillServiceJoinFlowProductJson>> BillserviceRealJoinFlowProduct(IEnumerable<BillserviceReal> billServiceReal, IEnumerable<FlowProduct> flowProducts)
        {
            try
            {
                return (from a in billServiceReal
                        join b in flowProducts on a.Id equals b.BillId
                        where b.IsDelete == 0
                        select new BillServiceJoinFlowProductJson
                        {
                            salonId = a.SalonId,
                            totalProductIncome = (double)(b.Quantity * b.Price * ((1 - decimal.Divide((decimal)b.VoucherPercent, 100)))),
                            isPayByCard = a.IsPayByCard
                        }).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<IEnumerable<BillServiceJoinFlowServiceJson>> BillserviceRealJoibFlowservice(IEnumerable<BillserviceReal> billServiceReal, IEnumerable<FlowService> flowServices)
        {
            try
            {
                return (from a in billServiceReal
                        join b in flowServices on a.Id equals b.BillId
                        where b.IsDelete == 0
                        select new BillServiceJoinFlowServiceJson
                        {
                            billId = a.Id,
                            salonId = a.SalonId,
                            serviceId = b.ServiceId,
                            quantity = b.Quantity,
                            totalServiceIncome = (double)(b.Quantity * b.Price * ((1 - decimal.Divide((decimal)b.VoucherPercent, 100)))),
                            mark = a.Mark,
                            voucherPercent = b.VoucherPercent,
                            isPayByCard = a.IsPayByCard,
                            CompleteBillTime = a.CompleteBillTime
                        }).ToList();

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<IEnumerable<BillserviceJoinRatingDetail>> BillserviceJoinRatingDetails(IEnumerable<BillserviceReal> billserviceReal, IEnumerable<RatingDetailReal> ratingDetailReal)
        {
            try
            {
                return (from a in billserviceReal
                        join b in ratingDetailReal on a.Id equals b.BillId
                        where !string.IsNullOrEmpty(a.ServiceIds)
                        select new BillserviceJoinRatingDetail
                        {
                            billId = a.Id,
                            salonId = a.SalonId,
                            StarNumber = b.StarNumber ?? 0
                        }).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<IEnumerable<BillServiceJoinFlowServiceJson>> BillserviceRealJoibFlowservicesFollow(IEnumerable<int?> salonIds, IEnumerable<int?> serviceIds, DateTime fromDate, DateTime toDate)
        {
            try
            {
                return await (from a in objEntity
                              join b in objFlowService on a.Id equals b.BillId
                              where
                              a.Pending == 0 && a.IsDelete == 0 && a.CompleteBillTime > fromDate && a.CompleteBillTime < toDate && salonIds.Contains(a.SalonId)
                              && b.IsDelete == 0 && serviceIds.Contains(b.ServiceId)
                              select new BillServiceJoinFlowServiceJson
                              {
                                  billId = a.Id,
                                  salonId = a.SalonId,
                                  serviceId = b.ServiceId,
                                  quantity = b.Quantity,
                                  totalServiceIncome = (double)(b.Quantity * b.Price * ((1 - decimal.Divide((decimal)b.VoucherPercent, 100)))),
                                  mark = a.Mark,
                                  voucherPercent = b.VoucherPercent,
                                  isPayByCard = a.IsPayByCard,
                                  CompleteBillTime = a.CompleteBillTime
                              }).ToListAsync();

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IEnumerable<BillServiceJoinFlowServiceJson> BillServiceJoinFlowServiceConvertMark(IEnumerable<BillServiceHis> billServices, IEnumerable<FlowService> flowServices)
        {
            try
            {
                return (from a in billServices
                        join b in flowServices on a.Id equals b.BillId
                        where !string.IsNullOrEmpty(a.ServiceIds)
                        select new BillServiceJoinFlowServiceJson
                        {
                            billId = a.Id,
                            salonId = a.SalonId,
                            serviceId = b.ServiceId,
                            quantity = b.Quantity,
                            totalServiceIncome = (double)(b.Quantity * b.Price * ((1 - decimal.Divide((decimal)b.VoucherPercent, 100)))),
                            //  b.RatingMark = 0 or 1 or 2 or 3 or 4 or 5. Nếu a.Mark  = 1 or 2 => mark = 1(KHL). Nếu a.Mark  = 3 => mark = 2 (HL). Nếu a.Mark  = 0 or 4 or 5 = mark = 3 (RHL)
                            mark = a.Mark == 1 ? 1 : (a.Mark == 2 ? 1 : (a.Mark == 3 ? 2 : 3)),
                            //mark = a.Mark,
                            voucherPercent = b.VoucherPercent,
                            isPayByCard = a.IsPayByCard,
                            CompleteBillTime = a.CompleteBillTime
                        }).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
