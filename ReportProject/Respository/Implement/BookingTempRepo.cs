﻿
using Microsoft.EntityFrameworkCore;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.BookingJson;

namespace ReportProject.Respository.Implement
{
    public class BookingTempRepo : IBookingTempRepo
    {
        private Solution_30shineContext context;
        private DbSet<SmBookingTemp> objEntity;
        public BookingTempRepo()
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<SmBookingTemp>();
        }

        public async Task<IEnumerable<BookingTempReal>> getByDate(IEnumerable<int?> salonIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                return await objEntity.AsNoTracking().Where(a => a.IsDelete == 0 && a.DatedBook >= dateFrom && a.DatedBook < dateTo && salonIds.Contains(a.SalonId)).Select(a => new BookingTempReal
                {
                    Id = a.Id,
                    IsCall = a.IsCall,
                    IsCallTime = a.IsCallTime,
                    IsCallTimeModified = a.IsCallTimeModified
                }).ToListAsync();

            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}
