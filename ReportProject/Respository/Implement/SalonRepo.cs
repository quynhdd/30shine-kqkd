﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ReportProject.Respository.Interface;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Utils;
using System.Linq.Expressions;

namespace ReportProject.Respository.Implement
{
    public class SalonRepo : ISalonRepo
    {
        private Solution_30shineContext context;
        private DbSet<TblSalon> objSalon;

        public SalonRepo()
        {
            context = new Solution_30shineContext();
            objSalon = context.Set<TblSalon>();
        }

        public async Task<IEnumerable<TblSalon>> Get()
        {
            try
            {
                return await objSalon.AsNoTracking().Where(a => a.IsSalonHoiQuan == false && a.Id != Constant.SALONTEST).Select(a => a).ToListAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<IEnumerable<TblSalon>> GetByRegionId(int regionId)
        {
            try
            {
                return await objSalon.Where(a => a.IsSalonHoiQuan == false && a.Id != Constant.SALONTEST && a.IsDelete == 0 && a.Publish == true && a.RegionId == regionId).ToListAsync();
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public async Task<IEnumerable<TblSalon>> GetBySalonId(int salonId)
        {
            try
            {
                return await objSalon.Where(a => a.IsSalonHoiQuan == false && a.Id != Constant.SALONTEST && a.IsDelete == 0 && a.Publish == true && (a.Id == salonId || salonId == 0)).ToListAsync();
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public async Task<List<TblSalon>> GetList(Expression<Func<TblSalon,bool>> expression)
        {
            try
            {
                var list = await context.TblSalon.Where(expression).ToListAsync();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public IEnumerable<TblSalon> GetList(IEnumerable<int?> salonIds)
        {
            try
            {
                return  objSalon.AsNoTracking().Where(a => a.IsSalonHoiQuan == false && a.Id != Constant.SALONTEST && salonIds.Contains(a.Id)).Select(a => a).ToList();
            }
            catch (Exception e)
            {

                throw ;
            }
        }

        public IEnumerable<TblSalon> GetListParam(int type, int Id)
        {
            try
            {
                if(type == 0) // lay tat ca salon
                {
                    return objSalon.AsNoTracking().Where(a => a.IsSalonHoiQuan == false && a.Id != Constant.SALONTEST).ToList();
                }
                else if(type == 1) // lay salon theo vungId
                {
                    return null;
                }
                else if (type == 2) // lay salon theo salonId
                {
                    return objSalon.AsNoTracking().Where(a => a.IsDelete == 0 && a.Publish == true && a.Id == Id && a.IsSalonHoiQuan == false).ToList();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public IEnumerable<TblSalon> ValidateSalon(IEnumerable<int> salonIds)
        {
            try
            {
                var salons = new List<TblSalon>();
                foreach (var id in salonIds) 
                {
                    var salon = objSalon.FirstOrDefault(a => a.IsDelete != 1 && a.Publish == true && a.Id == id);

                    if (salon != null)
                    {
                        salons.Add(salon);
                    }                 
                }
                return salons;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public IEnumerable<TblSalon> ValidateSalonShortName(IEnumerable<string> salonShortName)
        {
            try
            {
                var salons = new List<TblSalon>();
                foreach (var shortName in salonShortName)
                {
                    var salon = objSalon.FirstOrDefault( a=> a.ShortName == shortName);

                    if (salon == null)
                    {
                        return null;
                    }

                    salons.Add(salon);
                }
                return salons;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
