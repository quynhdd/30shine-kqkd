﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class OperationReportStatisticRepo : IOperationReportStatisticRepo
    {
        private Solution_30shineContext context;

        public OperationReportStatisticRepo()
        {
            context = new Solution_30shineContext();
        }

        public async Task AddAsync(IEnumerable<OperationReportStatistic> data)
        {
            try
            {
                await context.OperationReportStatistic.AddRangeAsync(data);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Delete(IEnumerable<OperationReportStatistic> data)
        {
            try
            {
                context.OperationReportStatistic.RemoveRange(data);
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public async Task<IEnumerable<OperationReportStatistic>> Get(DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                return await context.OperationReportStatistic.AsNoTracking().Where(a => a.Date >= dateFrom && a.Date < dateTo && a.IsDelete == false).ToListAsync();
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public async Task<IEnumerable<OperationReportStatistic>> GetByDay(int salonId, int regionId, DateTime? dateFrom, DateTime? dateTo)
        {
            try
            {
                var data = new List<OperationReportStatistic>();

                if (salonId >= 0 && regionId <= 0)
                {
                    data =  await context.OperationReportStatistic.AsNoTracking().Where(a => a.Date >= dateFrom && a.Date < dateTo && (a.SalonId == salonId || salonId == 0)).ToListAsync();

                    if (!data.Any())
                    {
                        return null;
                    }
                }

                if (regionId > 0)
                {
                    //var salonIds = context.TblSalon.AsNoTracking().Where(a => a.IsDelete == 0 && a.Publish == true && a.IsSalonHoiQuan != true && a.Id != 24 && a.RegionId == regionId).Select(a => a.Id).ToList();
                    var salonIds = context.PermissionSalonArea.Where(r=>r.IsDelete == false && r.IsActive == true && r.StaffId == regionId).Select(r=>r.SalonId ?? 0).ToList();
                    data = await context.OperationReportStatistic.AsNoTracking().Where(a => a.Date >= dateFrom && a.Date < dateTo && salonIds.Contains(a.SalonId)).ToListAsync();

                    if (!data.Any())
                    {
                        return null;
                    }
                }

                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task SaveChangesAsync()
        {
            try
            {
                await context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Update(OperationReportStatistic data)
        {
            try
            {
                context.OperationReportStatistic.Update(data);
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}
