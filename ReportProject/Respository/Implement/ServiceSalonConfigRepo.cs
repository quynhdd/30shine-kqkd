﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class ServiceSalonConfigRepo : IServiceSalonConfigRepo
    {
        private Solution_30shineContext context;
        private DbSet<ServiceSalonConfig> objEntity;
        public ServiceSalonConfigRepo()
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<ServiceSalonConfig>();
        }

        public async Task<IEnumerable<ServiceSalonConfig>> GetbyListSalon(IEnumerable<int?> salonIds, IEnumerable<int?> departmentIds)
        {
            try
            {
                return await objEntity.AsNoTracking().Where(a => a.IsDelete == false && a.IsPublish == true && salonIds.Contains(a.SalonId) && departmentIds.Contains(a.DepartmentId)).ToListAsync();
            }
            catch (Exception)
            {
                context.Dispose();
                throw;
            }

        }

        /// <summary>
        /// Get list expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<IEnumerable<ServiceSalonConfig>> GetList(Expression<Func<ServiceSalonConfig, bool>> expression)
        {
            try
            {
                return await context.ServiceSalonConfig.AsNoTracking().Where(expression).ToListAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        ///  Add range BackupTableServiceSalonConfig
        /// </summary>
        /// <param name="obj"></param>
        public void AddRange(IEnumerable<BackupTableServiceSalonConfig> obj)
        {
            try
            {
                context.BackupTableServiceSalonConfig.AddRange(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        ///  Add
        /// </summary>
        /// <param name="obj"></param>
        public void Add(ServiceSalonConfig obj)
        {
            try
            {
                objEntity.Add(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="obj"></param>
        public void Update(ServiceSalonConfig obj)
        {
            try
            {
                objEntity.Update(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        ///  save change async
        /// </summary>
        /// <returns></returns>
        public async Task SaveChangeAsync()
        {
            try
            {
                await context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
