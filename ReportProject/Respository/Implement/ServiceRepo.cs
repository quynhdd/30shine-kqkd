﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class ServiceRepo : IServiceRepo
    {
        private Solution_30shineContext context;
        private DbSet<Service> objEntity;

        public ServiceRepo(IFlowServiceRepo flowServiceRepo, IFlowProductRepo flowProductRepo, ISalonRepo salonRepo)
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<Service>();

        }
        public async Task<IEnumerable<Service>> GetServices()
        {
            try
            {
                return await objEntity.AsNoTracking().ToListAsync();
            }
            catch (Exception)
            {
                context.Dispose();
                throw;
            }
        }

        public async Task<IEnumerable<Service>> GetServicesFollow(IEnumerable<int?> IdsService)
        {
            try
            {
                return await objEntity.AsNoTracking().Where(a=>IdsService.Contains(a.Id)).ToListAsync();
            }
            catch (Exception)
            {
                context.Dispose();
                throw;
            }
        }
    }
}
