﻿using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class TblConfigRepo : ITblConfigRepo
    {
        private Solution_30shineContext context;
        private DbSet<TblConfig> objEntity;
        public TblConfigRepo()
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<TblConfig>();
        }
        public async Task<TblConfig> GetByKeyConfig(string Key)
        {
            try
            {
                return await objEntity.AsNoTracking().FirstOrDefaultAsync(a=>a.IsDelete == 0 && a.Key == Key);
            }
            catch (Exception)
            {
                context.Dispose();
                throw;
            }
        }
    }
}
