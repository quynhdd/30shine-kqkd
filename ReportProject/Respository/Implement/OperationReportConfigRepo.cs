﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class OperationReportConfigRepo : IOperationReportConfigRepo
    {
        private Solution_30shineContext context;

        public OperationReportConfigRepo()
        {
            context = new Solution_30shineContext();
        }

        public async Task<OperationReportConfig> GetSaleTargets(int salonId, int regionId, DateTime month)
        {
            try
            {
                var data = new OperationReportConfig();

                if (salonId >= 0 && regionId <= 0)
                {

                    data = await context.OperationReportConfig.AsNoTracking().FirstOrDefaultAsync(a => a.SalonId == salonId && a.IsRegion == false && a.IsDelete == false && a.MonthApply.Value.Month == month.Month && a.MonthApply.Value.Year == month.Year);
                }

                if (regionId > 0)
                {
                    data = await context.OperationReportConfig.AsNoTracking().FirstOrDefaultAsync(a => a.SalonId == regionId && a.IsRegion == true && a.IsDelete == false && a.MonthApply.Value.Month == month.Month && a.MonthApply.Value.Year == month.Year);

                }

                return data;

            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}
