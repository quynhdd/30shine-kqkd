﻿using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class StatictisReportOperationRepo : IStatictisReportOperationRepo
    {
        private Solution_30shineContext context;
        private DbSet<StatictisReportOperation> objEntity;
        public StatictisReportOperationRepo()
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<StatictisReportOperation>();
        }
        public async Task<IEnumerable<StatictisReportOperation>> GetByDayListSalon(IEnumerable<int?> idSalons, DateTime fromDate, DateTime toDate)
        {
            try
            {
                return await objEntity.AsNoTracking().Where(a => a.IsDelete == false && a.WorkDate >= fromDate && a.WorkDate < toDate && idSalons.Contains(a.SalonId)).ToListAsync();
            }
            catch (Exception)
            {
                context.Dispose();
                throw;
            }
        }

        //public IEnumerable<StatictisReportOperation> GetListReportByDate(DateTime dateFrom, DateTime dateTo)
        //{
        //    try
        //    {
        //        var StatictisReportOperationAll = objEntity.Where(a => a.WorkDate >= dateFrom && a.WorkDate < dateTo).Select(a => a).ToList();
        //        return StatictisReportOperationAll;
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}
        public void Add(IEnumerable<StatictisReportOperation> staticReportOperation)
        {
            try
            {
                objEntity.AddRange(staticReportOperation);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Delete(IEnumerable<StatictisReportOperation> staticReportOperation)
        {
            try
            {
                objEntity.RemoveRange(staticReportOperation);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                context.Dispose();
                throw e;
            }
        }
    }
}
