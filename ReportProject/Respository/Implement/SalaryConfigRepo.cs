﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class SalaryConfigRepo : ISalaryConfigRepo
    {
        private Solution_30shineContext context;
        private  DbSet<SalaryConfig> objEntity;

        public SalaryConfigRepo()
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<SalaryConfig>();
        }

        public async Task<IEnumerable<SalaryConfig>> GetByDepartmentIds(IEnumerable<int?> departmentIds)
        {
            try
            {
                return await objEntity.AsNoTracking().Where(a => a.IsDeleted == false && departmentIds.Contains(a.DepartmentId)).ToListAsync();
            }
            catch (Exception)
            {
                context.Dispose();
                throw;
            }
        }
    }
}
