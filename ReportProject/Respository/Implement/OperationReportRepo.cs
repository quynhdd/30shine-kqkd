﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using ReportProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.BillserviceJson;

namespace ReportProject.Respository.Implement
{
    public class OperationReportRepo : IOperationReportRepo
    {
        private Solution_30shineContext context;
        //private DbSet<StaticServicesProfit> objStaticServicesProfit;
        //private DbSet<FlowTimeKeeping> objFlowTimeKeeping;
        //private DbSet<TblSalon> objSalon;
        private ISalonRepo SalonRepo { get; set; } //1
        private IBillServiceRepo BillServiceRepo { get; set; }      //2
        private IFlowTimeKeepingRepo FlowTimeKeepingRepo { get; set; }//2
        private IBookingRepo BookingRepo { get; set; }//2
        private IBookHourRepo BookHourRepo { get; set; }//2
        //private IBookingTempRepo BookingTempRepo { get; set; }//2
        private IStaffRepo StaffRepo { get; set; } // 2
        private IFlowServiceRepo FlowServiceRepo { get; set; } // 2
        private IFlowProductRepo FlowProductRepo { get; set; } // 2        
        private IMonitorStaffErrorRepo MonitorStaffErrorRepo { get; set; } // 2
        //private IRatingDetailRepo RatingDetailRepo { get; set; } // 2
        private ITblConfigRepo TblConfigRepo { get; set; } // 2
        private IServiceRepo ServiceRepo { get; set; }//2
        private IStatictisReportOperationRepo StatictisReportOperationRepo { get; set; }

        private IBookHourSubRepo BookHourSubRepo { get; set; }//2

        public OperationReportRepo(ISalonRepo salonRepo, IBillServiceRepo billServiceRepo, IFlowTimeKeepingRepo flowTimeKeepingRepo, IBookingRepo bookingRepo, IBookHourRepo bookHourRepo, /*IBookingTempRepo bookingTempRepo,*/ IStaffRepo staffRepo, IFlowServiceRepo flowServiceRepo, IFlowProductRepo flowProductRepo, IServiceRepo serviceRepo, IMonitorStaffErrorRepo monitorStaffErrorRepo, /*IRatingDetailRepo ratingDetailRepo,*/ ITblConfigRepo tblConfigRepo, IStatictisReportOperationRepo statictisReportOperationRepo, IBookHourSubRepo bookHourSubRepo)
        {
            context = new Solution_30shineContext();
            SalonRepo = salonRepo;
            BillServiceRepo = billServiceRepo;
            FlowTimeKeepingRepo = flowTimeKeepingRepo;
            BookingRepo = bookingRepo;
            BookHourRepo = bookHourRepo;
            //BookingTempRepo = bookingTempRepo;
            StaffRepo = staffRepo;
            FlowServiceRepo = flowServiceRepo;
            FlowProductRepo = flowProductRepo;
            ServiceRepo = serviceRepo;
            MonitorStaffErrorRepo = monitorStaffErrorRepo;
            //RatingDetailRepo = ratingDetailRepo;
            TblConfigRepo = tblConfigRepo;
            StatictisReportOperationRepo = statictisReportOperationRepo;
            //objStaticServicesProfit = context.Set<StaticServicesProfit>();

            //objFlowTimeKeeping = context.Set<FlowTimeKeeping>();
            //objSalon = context.Set<TblSalon>();
            BookHourSubRepo = bookHourSubRepo;
        }
        public async Task<OperationReportJsonOutPut> GetOperationReportToday(int type, int Id, DateTime fromDate)
        {
            try
            {
                DateTime toDate = fromDate.AddDays(+1);
                var salon = SalonRepo.GetListParam(type, Id);
                IEnumerable<int?> salonIds = salon.Select(a => a.Id).Cast<int?>().ToList();
                IEnumerable<int?> listStaffType = new List<int?> { Constant.STYLIST, Constant.SKINNER };
                IEnumerable<int?> ListGroupUon = new List<int?> { 16, 89, 61, 63, 65, 75 };
                IEnumerable<int?> ListGroupNhuom = new List<int?> { 14, 82, 60, 64, 66, 88 };

                var billServiceList = BillServiceRepo.GetBillServiceByDate(salonIds, fromDate, toDate);
                var flowServiceList = FlowServiceRepo.GetByDateListSalon(salonIds, fromDate, toDate);
                var flowProductList = FlowProductRepo.GetByDateListSalon(salonIds, fromDate, toDate);
                var flowTimeKeepingList = FlowTimeKeepingRepo.GetByStylistSalonAndDate(salonIds, listStaffType, fromDate, toDate);
                var bookingList = BookingRepo.getByDate(salonIds, fromDate, toDate);
                var bookingListDelete = BookingRepo.getByDateBookDelete(salonIds, fromDate, toDate);
                var bookHourList = BookHourRepo.GetByListSalon(salonIds);
                var monitorStaffErrorList = MonitorStaffErrorRepo.GetByDateListSalon(salonIds, fromDate, toDate);
                var bookHourSubList = BookHourSubRepo.GetBySalonId(salonIds);
                //var ratingDetailRepoList = RatingDetailRepo.getByDateListSalon(fromDate, toDate);
                //var bookingTempList = BookingTempRepo.getByDate(salonIds, fromDate, toDate);
                var staffList = StaffRepo.GetAllStaffType(Constant.STYLIST);
                var objTblConfig = TblConfigRepo.GetByKeyConfig(Constant.KEY_SERVICE_FOLLOW);
                Task.WaitAll(billServiceList, flowServiceList, flowProductList, flowTimeKeepingList, bookingList, bookingListDelete, bookHourList, monitorStaffErrorList, /*ratingDetailRepoList,*/ staffList, objTblConfig, bookHourSubList);
                //  list IdsService config follow
                List<int?> ServiceFollow = objTblConfig.Result.Value.Split(',').Select(Int32.Parse).Cast<int?>().ToList();

                var serviceFolowList = ServiceRepo.GetServicesFollow(ServiceFollow);
                await (serviceFolowList);
                // End test list service
                var billServices = billServiceList.Result;
                var billOnlyServices = billServices.Where(a => !string.IsNullOrEmpty(a.ServiceIds));
                var flowProducs = flowProductList.Result;
                var flowServices = flowServiceList.Result;
                var flowTimeKeepings = flowTimeKeepingList.Result;
                var bookHourSubs = bookHourSubList.Result;

                var bookings = bookingList.Result;
                var bookingsDelete = bookingListDelete.Result;
                var bookHours = bookHourList.Result;

                // test
                var flowTimeKeepingsStylist = flowTimeKeepings.Where(a => a.staffType == Constant.STYLIST).Select(a => new FlowTimeKeepingJoin.FlowTimeKeepingJoinWorkTime
                {
                    Id = a.Id,
                    StaffId = a.StaffId,
                    SalonId = a.SalonId,
                    WorkDate = a.WorkDate,
                    WorkHour = NumberOverTime(a.HourIds, a.StrartTime, a.EnadTime, bookHours.Where(p => p.SalonId == a.SalonId)),
                    WorkTimeId = a.WorkTimeId,
                    HourIdNumber = a.HourIdNumber,
                    TotalHourFrame = a.TotalHourFrame,
                    StrartTime = a.StrartTime,
                    EnadTime = a.EnadTime,
                    staffType = a.staffType
                });

                //End test
                //var bookingTemps = bookingTempList.Result;
                var staffs = staffList.Result;
                // lay list dich vu theo doi
                var servicesFollows = serviceFolowList.Result;
                var monitorStaffErrors = monitorStaffErrorList.Result;
                //var ratingDetails = ratingDetailRepoList.Result;
                // join billservice FlowProduct
                var billServiceJoinFlowProducts = BillServiceRepo.BillserviceRealJoinFlowProduct(billServices, flowProducs).Result;
                // join billservice FlowService
                var billServiceJoinFlowServices = BillServiceRepo.BillserviceRealJoibFlowservice(billServices, flowServices).Result;
                // join billservice Ratingdetail
                //var billserviceJoinRatingDetail = BillServiceRepo.BillserviceJoinRatingDetails(billServices, ratingDetails).Result;
                // xu ly book join boohour join bill
                var bookJoinBillHour = (from a in bookings
                                        join h in bookHourSubs on a.HourSubId equals h.SubHourId
                                        join b in billServices on a.Id equals b.BookingId into abGroup
                                        from ab in abGroup.DefaultIfEmpty()
                                        select new
                                        {
                                            bookingId = a.Id,
                                            salonId = a.SalonId,
                                            DatedBook = a.DatedBook,
                                            IsBookOnline = a.IsBookOnline,
                                            HourBook = h.HourFrame.Hours,
                                            MinuteBook = h.HourFrame.Minutes,
                                            BillId = ab?.Id,
                                            CreatedDateBill = ab?.CreatedDate,
                                            InProcedureTime = ab?.InProcedureTime,
                                            InProcedureTimeModifed = ab?.InProcedureTimeModifed,
                                            ServiceIds = ab?.ServiceIds
                                        }
                                        );
                // xu ly data = > bill cho lau
                var billServiceBooking = bookJoinBillHour.Where(a => a.BillId != null && !string.IsNullOrEmpty(a.ServiceIds))

                    .Select(a => new
                    {
                        billId = a.BillId,
                        salonId = a.salonId,
                        // XY LY TIME CHO LAU 
                        timeWaited =
                   // trường hợp checkin sau gio book
                   (a.CreatedDateBill - a.DatedBook.Value.AddHours(a.HourBook).AddMinutes(a.MinuteBook)).Value.TotalSeconds > 0 ? (((a.InProcedureTime == null ? a.CreatedDateBill.Value.AddDays(1) : a.InProcedureTime) - a.CreatedDateBill).Value.TotalMinutes >= Constant.TIME_WAITED ? 1 : 0)
                   // checkin trước giờ booking
                   :
                   (((a.InProcedureTime == null ? a.CreatedDateBill.Value.AddDays(1) : a.InProcedureTime) - a.DatedBook.Value.AddHours(a.HourBook).AddMinutes(a.MinuteBook)).Value.TotalMinutes >= Constant.TIME_WAITED ? 1 : 0)
                    }).ToList();

                var test = billServiceBooking.Count();

                // xu ly data Service Follow
                var listServiceFollow = billServiceJoinFlowServices.Where(a => ServiceFollow.Contains(a.serviceId)).GroupBy(a => new
                {
                    a.salonId,
                    a.serviceId
                })
                .Select(a => new ListServiceFollow
                {
                    salonId = a.Key.salonId,
                    serviceId = a.Key.serviceId,
                    quantity = a.Sum(s => s.quantity)
                });

                int numberOfTurns = 0 // so luong khach
                    , numberCustomerOld = 0 // so luong khach cu
                    , numberBillNotImg = 0 // so luong bill khong anh
                    , numberBill1Star = 0 // so luong bill 1 sao
                    , numberBill2Star = 0 // so luong bill 2 sao
                    , numberBill3Star = 0 // so luong bill 3 sao
                    , numberBill4Star = 0 // so luong bill 4 sao
                    , numberBill5Star = 0 // so luong bill 5 sao
                    , totalStarNumber = 0 // tong so sao danhg gia
                    , numberErrorMonitoring = 0 // so luong loi giam sat
                    , numberStylistTimekeeping = 0 // so luong stylist cham cong
                    , numberSkinnerTimekeeping = 0 // so luong skinner cham cong
                    , numberStylist = 0 // so luong stylist chinh thuc
                    , numberCanleBooking = 0 // so luong huy book online                  
                    , numberDeleteBooking = 0 // so luong xoa book online
                    , overtimeHoursStylist = 0 // Số giờ tăng ca của stylist
                    , numberSlotBooking = 0 // Tổng số slot được booking
                    , numberBooking = 0 // Tổng số booking 
                    , numberBookingBefor = 0 // Tổng số bill đặt trước hoàn thành  bill service
                    , numberBillWaited = 0 // bill chờ lâu
                    , totalShineCombo = 0 // tong so luong billShinecombo
                    , totalKidCombo = 0 // tong so luong KidCombo
                    , totalProtein = 0 // tong so luong PROTEIN
                    , totalMask = 0 // tong so luong Tay da chet
                    , totalExFoliation = 0 // tong so luong dap mat na
                    , totalGroupUonDuoi = 0 // tong so luong group uon + duoi
                    , totalGroupColorCombo = 0; // tong so luong group Color ComBo ( Nhuộm )

                int numberTotalUonDuoi = 0// tong so luong uon duoi
                    , IdServiceUonDuoi = 0 // khai bao Id fix
                 , totalNumberService = 0; // tong so luong cua service
                double
                      totalIncome = 0
                    , totalServiceInCome = 0 // tong doanh thu dich vu
                    , totalProductIncome = 0 // tong doanh thu my pham
                    , worktimeStylist = 0;// số Giờ làm việc

                string ServiceName = "", ServiceNameUonDuoi = ""; // Ten dich vu 
                var dataReport = new List<OperationReportJsonReal>();
                foreach (var item in salon)
                {
                    var objDataServiceList = new List<ObjServiceFollow>();
                    var idSalon = item.Id;
                    var SalonName = item.ShortName;
                    var objSalon = salon.Where(a => a.Id == idSalon).FirstOrDefault();
                    var billServicesSalon = billOnlyServices.Where(a => a.SalonId == idSalon);
                    var flowServicesSalon = billServiceJoinFlowServices.Where(a => a.salonId == idSalon);
                    var flowProductSalon = billServiceJoinFlowProducts.Where(a => a.salonId == idSalon);
                    var flowTimeKeepingsStylistSalon = flowTimeKeepingsStylist.Where(a => a.SalonId == idSalon);
                    var flowTimeKeepingsSkinnerSalon = flowTimeKeepings.Where(a => a.SalonId == idSalon && a.staffType == Constant.SKINNER);
                    var staffsSalon = staffs.Where(a => a.SalonId == idSalon && a.IsDelete == 0 && a.Active == 1);
                    var ServiceFollowSalon = listServiceFollow.Where(a => a.salonId == idSalon).ToList();
                    //var RatetingDetailSalon = billserviceJoinRatingDetail.Where(a => a.salonId == idSalon).ToList();
                    var monitorStaffErrorsSalon = monitorStaffErrors.Where(a => a.SalonId == idSalon).ToList();
                    var bookingsDeleteSalon = bookingsDelete.Where(a => a.SalonId == idSalon).ToList();
                    var bookingsSalon = bookings.Where(a => a.SalonId == idSalon).ToList();
                    var bookJoinBillHourSalon = bookJoinBillHour.Where(a => a.salonId == idSalon).ToList();
                    var billServiceBookingSalon = billServiceBooking.Where(a => a.salonId == idSalon).ToList();
                    // lay so lieu

                    totalServiceInCome = flowServicesSalon.Sum(a => a.totalServiceIncome);    // tong doanh thu dich vu
                    totalProductIncome = flowProductSalon.Sum(a => a.totalProductIncome); // tong doanh thu my pham
                    totalIncome = totalServiceInCome + totalProductIncome; // tong doanh thu
                    numberOfTurns = billServicesSalon.Count(); // so luong khach
                    numberCustomerOld = billServicesSalon.Where(a => (a.CustomerUsedNumber ?? 0) > 1).Count(); // so luong khach cu
                    numberBillNotImg = billServicesSalon.Where(a => string.IsNullOrEmpty(a.Images)).Count(); // so luong bill khong anh
                    totalStarNumber = billServicesSalon.Sum(a => a.Mark ?? 0); // so luong bill khong hai long
                    numberBill1Star = billServicesSalon.Where(a => (a.Mark ?? 0) == 1).Count(); // so luong bill 1 sao
                    numberBill2Star = billServicesSalon.Where(a => (a.Mark ?? 0) == 2).Count(); // so luong bill 2 sao
                    numberBill3Star = billServicesSalon.Where(a => (a.Mark ?? 0) == 3).Count(); // so luong bill 3 sao
                    numberBill4Star = billServicesSalon.Where(a => (a.Mark ?? 0) == 4).Count(); // so luong bill 4 sao
                    numberBill5Star = billServicesSalon.Where(a => (a.Mark ?? 0) == 5).Count(); // so luong bill 5 sao
                    numberErrorMonitoring = monitorStaffErrorsSalon.Count(); // so loi giam sat                    
                    numberStylist = staffsSalon.Count(); // so luong stylist chinh thuc
                    numberStylistTimekeeping = flowTimeKeepingsStylistSalon.Count(); // so luong stylist cham cong
                    numberSkinnerTimekeeping = flowTimeKeepingsSkinnerSalon.Count(); // so luong skiner cham cong
                    overtimeHoursStylist = flowTimeKeepingsStylistSalon.Sum(a => a.WorkHour ?? 0); // // Số giờ tăng ca của stylist
                    worktimeStylist = flowTimeKeepingsStylistSalon.Sum(a => a.TotalHourFrame ?? 0); // số Giờ làm việc                   

                    numberSlotBooking = flowTimeKeepingsStylistSalon.Sum(a => a.HourIdNumber); // Tổng số slot được booking
                    numberDeleteBooking = bookingsDeleteSalon.Count(); // so luong xoa book online
                    numberBooking = bookingsSalon.Where(a => a.IsBookOnline == true).Count(); // Tổng số booking  online
                    numberBookingBefor = bookJoinBillHourSalon.Where(a => a.IsBookOnline == true && a.BillId != null).Count(); // Tổng số bill đặt trước hoàn thành bill service
                    numberCanleBooking = bookJoinBillHourSalon.Where(a => a.IsBookOnline == true && a.BillId == null).Count(); // Tổng số bill đặt trước không đến
                    numberBillWaited = billServiceBookingSalon.Where(a => a.timeWaited == 1).Count(); // bill chờ lâu
                    totalShineCombo = flowServicesSalon.Where(a => a.serviceId == Constant.SHINE_COMBO).Sum(a => a.quantity ?? 0); //  tong so luong billShinecombo
                    totalKidCombo = flowServicesSalon.Where(a => a.serviceId == Constant.KID_COMBO).Sum(a => a.quantity ?? 0); //  tong so luong KidCombo
                    totalKidCombo = flowServicesSalon.Where(a => a.serviceId == Constant.KID_COMBO).Sum(a => a.quantity ?? 0); //  tong so luong KidCombo
                    totalProtein = flowServicesSalon.Where(a => a.serviceId == Constant.PROTEIN).Sum(a => a.quantity ?? 0); //  tong so luong PROTEIN
                    totalExFoliation = flowServicesSalon.Where(a => a.serviceId == Constant.EXFOLIATION).Count(); //  tong so luong Tay da chet
                    totalMask = flowServicesSalon.Where(a => a.serviceId == Constant.MASK).Sum(a => a.quantity ?? 0); //  tong so luong dap mat na
                    totalGroupUonDuoi = flowServicesSalon.Where(a => ListGroupUon.Contains(a.serviceId)).Sum(a => a.quantity ?? 0); //  tong so luong group uon + duoi
                    totalGroupColorCombo = flowServicesSalon.Where(a => ListGroupNhuom.Contains(a.serviceId)).Sum(a => a.quantity ?? 0); //  tong so luong group Color ComBo ( Nhuộm )

                    #region List service follow of salon
                    // List service follow of salon

                    //foreach (var itemService in ServiceFollowSalon)
                    //{

                    //    if (itemService.serviceId == Constant.CURLING || itemService.serviceId == Constant.STRAIGHTEN)
                    //    {
                    //        numberTotalUonDuoi = numberTotalUonDuoi + ServiceFollowSalon.Where(a => a.serviceId == itemService.serviceId).Sum(a => a.quantity ?? 0);
                    //        IdServiceUonDuoi = Constant.CURLING;
                    //    }
                    //    else
                    //    {
                    //        totalNumberService = ServiceFollowSalon.Where(a => a.serviceId == itemService.serviceId).Sum(a => a.quantity ?? 0);
                    //        var objDataService = new ObjServiceFollow
                    //        {
                    //            id = itemService.serviceId,
                    //            totalServiceNumber = totalNumberService
                    //        };
                    //        objDataServiceList.Add(objDataService);
                    //    }

                    //}
                    //objDataServiceList.Insert(0, new ObjServiceFollow { id = IdServiceUonDuoi, totalServiceNumber = numberTotalUonDuoi });
                    //objDataServiceList = (from a in servicesFollows
                    //                      join b in objDataServiceList on a.Id equals b.id into abGroup
                    //                      from ab in abGroup.DefaultIfEmpty()
                    //                      where a.Id != Constant.STRAIGHTEN
                    //                      select new ObjServiceFollow
                    //                      {
                    //                          id = a.Id,
                    //                          serviceName = a.Name,
                    //                          totalServiceNumber = ab?.totalServiceNumber == null ? 0 : ab.totalServiceNumber,
                    //                      }).OrderBy(a => a.id).ToList();
                    // End List service follow of salon
                    #endregion
                    var objOperationReportJsonReal = new OperationReportJsonReal
                    {
                        salonId = idSalon,
                        salonName = SalonName,
                        totalIncome = totalIncome,
                        totalServiceInCome = totalServiceInCome,
                        totalProductIncome = totalProductIncome,
                        worktimeStylist = worktimeStylist,
                        numberOfTurns = numberOfTurns,
                        numberCustomerOld = numberCustomerOld,
                        numberBillWaited = numberBillWaited,
                        numberBillNotImg = numberBillNotImg,
                        numberBill1Star = numberBill1Star,
                        numberBill2Star = numberBill2Star,
                        numberBill3Star = numberBill3Star,
                        numberBill4Star = numberBill4Star,
                        numberBill5Star = numberBill5Star,
                        totalStarNumber = totalStarNumber,
                        numberErrorMonitoring = numberErrorMonitoring,
                        numberStylistTimekeeping = numberStylistTimekeeping,
                        numberSkinnerTimekeeping = numberSkinnerTimekeeping,
                        numberStylist = numberStylist,
                        numberCanleBooking = numberCanleBooking,
                        numberDeleteBooking = numberDeleteBooking,
                        overtimeHoursStylist = overtimeHoursStylist,
                        numberSlotBooking = numberSlotBooking,
                        numberBooking = numberBooking,
                        numberBookingBefor = numberBookingBefor,
                        //DataServiceFollow = objDataServiceList
                        totalShineCombo = totalShineCombo,
                        totalKidCombo = totalKidCombo,
                        totalProtein = totalProtein,
                        totalMask = totalMask,
                        totalExFoliation = totalExFoliation,
                        totalGroupUonDuoi = totalGroupUonDuoi,
                        totalGroupColorCombo = totalGroupColorCombo,
                        salonOrder = item.Order
                    };
                    dataReport.Add(objOperationReportJsonReal);

                }
                var OperationReportJsonOutPutReal = new OperationReportJsonOutPut
                {
                    //headerServiceFollow = HeaderServiceFollow(servicesFollows).Result.OrderBy(a => a.id).ToList(),
                    totalServiceFollow = ListTotalServiceFollow(listServiceFollow, servicesFollows).Result.OrderBy(a => a.id).ToList(),
                    dataBodyReport = dataReport.Where(a => a.totalIncome > 0).OrderBy(a => a.salonOrder).ToList()
                };
                return OperationReportJsonOutPutReal;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<OperationReportJsonOutPut> GetByDayOperationReport(DateTime fromDate, DateTime toDate, int type, int id)
        {
            try
            {
                var dataReport = new List<OperationReportJsonReal>();
                var salon = SalonRepo.GetListParam(type, id);
                IEnumerable<int?> salonIds = salon.Select(a => a.Id).Cast<int?>().ToList();
                var objTblConfig = TblConfigRepo.GetByKeyConfig(Constant.KEY_SERVICE_FOLLOW);
                var statictisReportOperationRepoList = StatictisReportOperationRepo.GetByDayListSalon(salonIds, fromDate, toDate);
                await (objTblConfig);
                //  list IdsService config follow
                List<int?> ServiceFollow = objTblConfig.Result.Value.Split(',').Select(Int32.Parse).Cast<int?>().ToList();

                var billserviceRealJoibFlowservicesFollowRepoList = BillServiceRepo.BillserviceRealJoibFlowservicesFollow(salonIds, ServiceFollow, fromDate, toDate);
                var serviceFolowList = ServiceRepo.GetServicesFollow(ServiceFollow);
                await (serviceFolowList);
                // lay list dich vu theo doi
                var servicesFollows = serviceFolowList.Result;

                Task.WaitAll(statictisReportOperationRepoList, billserviceRealJoibFlowservicesFollowRepoList);
                var statictisReportOperation = statictisReportOperationRepoList.Result;
                var billserviceRealJoibFlowservicesFollow = billserviceRealJoibFlowservicesFollowRepoList.Result;
                var listServiceFollow = billserviceRealJoibFlowservicesFollow.Where(a => ServiceFollow.Contains(a.serviceId)).GroupBy(a => new
                {
                    a.salonId,
                    a.serviceId
                })
                .Select(a => new ListServiceFollow
                {
                    salonId = a.Key.salonId,
                    serviceId = a.Key.serviceId,
                    quantity = a.Sum(s => s.quantity)
                });

                int numberTotalUonDuoi = 0// tong so luong uon duoi
                    , IdServiceUonDuoi = 0 // khai bao Id fix
                 , totalNumberService = 0; // tong so luong cua service
                foreach (var item in salon)
                {
                    var objDataServiceList = new List<ObjServiceFollow>();
                    var idSalon = item.Id;
                    var SalonName = item.ShortName;
                    var statictisReportOperationSalon = statictisReportOperation.Where(a => a.SalonId == idSalon).ToList();
                    var ServiceFollowSalon = listServiceFollow.Where(a => a.salonId == idSalon).ToList();
                    #region List service follow of salon
                    // List service follow of salon
                    //foreach (var itemService in ServiceFollowSalon)
                    //{

                    //    if (itemService.serviceId == Constant.CURLING || itemService.serviceId == Constant.STRAIGHTEN)
                    //    {
                    //        numberTotalUonDuoi = numberTotalUonDuoi + ServiceFollowSalon.Where(a => a.serviceId == itemService.serviceId).Sum(a => a.quantity ?? 0);
                    //        IdServiceUonDuoi = Constant.CURLING;
                    //    }
                    //    else
                    //    {
                    //        totalNumberService = ServiceFollowSalon.Where(a => a.serviceId == itemService.serviceId).Sum(a => a.quantity ?? 0);
                    //        var objDataService = new ObjServiceFollow
                    //        {
                    //            id = itemService.serviceId,
                    //            totalServiceNumber = totalNumberService
                    //        };
                    //        objDataServiceList.Add(objDataService);
                    //    }

                    //}
                    //objDataServiceList.Insert(0, new ObjServiceFollow { id = IdServiceUonDuoi, totalServiceNumber = numberTotalUonDuoi });
                    //objDataServiceList = (from a in servicesFollows
                    //                      join b in objDataServiceList on a.Id equals b.id into abGroup
                    //                      from ab in abGroup.DefaultIfEmpty()
                    //                      where a.Id != Constant.STRAIGHTEN
                    //                      select new ObjServiceFollow
                    //                      {
                    //                          id = a.Id,
                    //                          serviceName = a.Name,
                    //                          totalServiceNumber = ab?.totalServiceNumber == null ? 0 : ab.totalServiceNumber,
                    //                      }).OrderBy(a => a.id).ToList();

                    // End List service follow of salon
                    #endregion
                    var objOperationReportJsonReal = new OperationReportJsonReal
                    {
                        salonId = idSalon,
                        salonName = SalonName,
                        totalIncome = statictisReportOperationSalon.Sum(a => a.TotalIncome),
                        totalServiceInCome = statictisReportOperationSalon.Sum(a => a.TotalServiceInCome),
                        totalProductIncome = statictisReportOperationSalon.Sum(a => a.TotalProductIncome),
                        worktimeStylist = statictisReportOperationSalon.Sum(a => a.WorktimeStylist),
                        numberOfTurns = statictisReportOperationSalon.Sum(a => a.NumberOfTurns),
                        numberCustomerOld = statictisReportOperationSalon.Sum(a => a.NumberCustomerOld),
                        numberBillWaited = statictisReportOperationSalon.Sum(a => a.NumberBillWaited),
                        numberBillNotImg = statictisReportOperationSalon.Sum(a => a.NumberBillNotImg),
                        //numberBillKHL = statictisReportOperationSalon.Sum(a => a.NumberBillKhl),
                        numberBill1Star = statictisReportOperationSalon.Sum(a => a.NumberBill1Star),
                        numberBill2Star = statictisReportOperationSalon.Sum(a => a.NumberBill2Star),
                        numberBill3Star = statictisReportOperationSalon.Sum(a => a.NumberBill3Star),
                        numberBill4Star = statictisReportOperationSalon.Sum(a => a.NumberBill5Star),
                        numberBill5Star = statictisReportOperationSalon.Sum(a => a.NumberBill5Star),
                        totalStarNumber = statictisReportOperationSalon.Sum(a => a.TotalStarNumber),
                        numberErrorMonitoring = statictisReportOperationSalon.Sum(a => a.NumberErrorMonitoring),
                        numberStylistTimekeeping = statictisReportOperationSalon.Sum(a => a.NumberStylistTimekeeping),
                        numberSkinnerTimekeeping = statictisReportOperationSalon.Sum(a => a.NumberSkinnerTimekeeping),
                        numberStylist = statictisReportOperationSalon.Sum(a => a.NumberStylist),
                        numberCanleBooking = statictisReportOperationSalon.Sum(a => a.NumberCanleBooking),
                        numberDeleteBooking = statictisReportOperationSalon.Sum(a => a.NumberDeleteBooking),
                        overtimeHoursStylist = statictisReportOperationSalon.Sum(a => a.OvertimeHoursStylist),
                        numberSlotBooking = statictisReportOperationSalon.Sum(a => a.NumberSlotBooking),
                        numberBooking = statictisReportOperationSalon.Sum(a => a.NumberBooking),
                        numberBookingBefor = statictisReportOperationSalon.Sum(a => a.NumberBookingBefor),
                        //DataServiceFollow = objDataServiceList
                        totalShineCombo = statictisReportOperationSalon.Sum(a => a.TotalShineCombo),
                        totalKidCombo = statictisReportOperationSalon.Sum(a => a.TotalKidCombo),
                        totalProtein = statictisReportOperationSalon.Sum(a => a.TotalProtein),
                        totalMask = statictisReportOperationSalon.Sum(a => a.TotalMask),
                        totalExFoliation = statictisReportOperationSalon.Sum(a => a.TotalExFoliation),
                        totalGroupUonDuoi = statictisReportOperationSalon.Sum(a => a.TotalGroupUonDuoi),
                        totalGroupColorCombo = statictisReportOperationSalon.Sum(a => a.TotalGroupColorCombo),
                        salonOrder = item.Order
                    };
                    dataReport.Add(objOperationReportJsonReal);

                }
                var OperationReportJsonOutPutReal = new OperationReportJsonOutPut
                {
                    //headerServiceFollow = HeaderServiceFollow(servicesFollows).Result.OrderBy(a => a.id).ToList(),
                    totalServiceFollow = ListTotalServiceFollow(listServiceFollow, servicesFollows).Result.OrderBy(a => a.id).ToList(),
                    dataBodyReport = dataReport.Where(a => a.totalIncome > 0).OrderBy(a => a.salonOrder).ToList()
                };

                return OperationReportJsonOutPutReal;
            }
            catch (Exception)
            {

                throw;
            }

        }

        //public async Task<IEnumerable<ObjHeaderServiceFollow>> HeaderServiceFollow(IEnumerable<Service> servicesFollows)
        //{
        //    try
        //    {
        //        int IdServiceUonDuoiHeader = 0;// khai bao Id fix header
        //        string ServiceNameUonDuoiHeader = "";
        //        #region xu ly header dich vu theo doi
        //        var HeaderServiceList = new List<ObjHeaderServiceFollow>();

        //        foreach (var item in servicesFollows)
        //        {
        //            int idService = item.Id;
        //            string NameService = item.Name;
        //            if (idService == Constant.CURLING || idService == Constant.STRAIGHTEN)
        //            {
        //                if (idService == Constant.CURLING)
        //                {
        //                    IdServiceUonDuoiHeader = idService;
        //                    ServiceNameUonDuoiHeader = NameService;
        //                }
        //            }
        //            else
        //            {
        //                var ObjHeaderServiceFollow = new ObjHeaderServiceFollow
        //                {
        //                    id = idService,
        //                    serviceName = NameService
        //                };
        //                HeaderServiceList.Add(ObjHeaderServiceFollow);
        //            }
        //        }
        //        var ObjHeaderServiceFollowFix = new ObjHeaderServiceFollow
        //        {
        //            id = IdServiceUonDuoiHeader,
        //            serviceName = ServiceNameUonDuoiHeader
        //        };
        //        HeaderServiceList.Insert(0, new ObjHeaderServiceFollow { id = IdServiceUonDuoiHeader, serviceName = ServiceNameUonDuoiHeader });
        //        #endregion
        //        return HeaderServiceList;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }

        //}

        public async Task<IEnumerable<ObjServiceFollow>> ListTotalServiceFollow(IEnumerable<ListServiceFollow> listServiceFollow, IEnumerable<Service> servicesFollows)
        {
            try
            {
                int IdServiceUonDuoiTotal = 0;
                int totalServiceUonDuoi = 0;
                var ListTotalService = new List<ObjServiceFollow>();

                var servicesFollowsTotal = listServiceFollow.GroupBy(a => a.serviceId).Select(a => new
                {
                    serviceId = a.Key,
                    totalQuantity = a.Sum(s => s.quantity)
                }).ToList();
                foreach (var item in servicesFollowsTotal)
                {
                    int Idservice = (int)item.serviceId;
                    int totalQuantity = item.totalQuantity ?? 0;
                    if (Idservice == Constant.CURLING || Idservice == Constant.STRAIGHTEN)
                    {
                        IdServiceUonDuoiTotal = Constant.CURLING;
                        totalServiceUonDuoi = totalServiceUonDuoi + totalQuantity;
                    }
                    else
                    {
                        var ObjServiceFollowReal = new ObjServiceFollow
                        {
                            id = Idservice,
                            totalServiceNumber = totalQuantity
                        };
                        ListTotalService.Add(ObjServiceFollowReal);
                    }
                }
                ListTotalService.Insert(0, new ObjServiceFollow { id = IdServiceUonDuoiTotal, totalServiceNumber = totalServiceUonDuoi });
                ListTotalService = (from a in servicesFollows
                                    join b in ListTotalService on a.Id equals b.id into abGroup
                                    from ab in abGroup.DefaultIfEmpty()
                                    where a.Id != Constant.STRAIGHTEN
                                    select new ObjServiceFollow
                                    {
                                        id = a.Id,
                                        serviceName = a.Name,
                                        totalServiceNumber = ab?.totalServiceNumber == null ? 0 : ab.totalServiceNumber,
                                    }).OrderBy(a => a.id).ToList();
                return ListTotalService;
            }
            catch (Exception)
            {

                throw;
            }
        }
        private int? NumberOverTime(string HourIds, TimeSpan? StrartTime, TimeSpan? EnadTime, IEnumerable<BookHour> BookHourSalon)
        {
            try
            {

                var ListHourId = HourIds.Split(',').Select(a => new
                {
                    HourId = Convert.ToInt32(a)
                }).ToList();
                var HourIdsJoinBookHourSalon = (from a in ListHourId
                                                join b in BookHourSalon on a.HourId equals b.Id
                                                select new
                                                {
                                                    a.HourId,
                                                    OverTime = b.HourFrame < StrartTime ? 1 : (b.HourFrame > EnadTime ? 1 : 0)
                                                }).ToList();
                var NumberOverTimeHour = HourIdsJoinBookHourSalon.Where(a => a.OverTime == 1).Count();

                return NumberOverTimeHour;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public class ListServiceFollow
        {
            public int? salonId { get; set; }
            public int? serviceId { get; set; }
            public int? quantity { get; set; }
        }
    }
}

