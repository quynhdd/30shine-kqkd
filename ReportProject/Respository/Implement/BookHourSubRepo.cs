﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class BookHourSubRepo : IBookHourSubRepo
    {
        private Solution_30shineContext context;
        private DbSet<BookHourSub> objEntity;
        public BookHourSubRepo()
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<BookHourSub>();
        }

        public async Task<IEnumerable<BookHourSub>> GetBySalonId(IEnumerable<int?> salonIds)
        {
            try
            {
                return await objEntity.Where(a => salonIds.Contains(a.SalonId)).ToListAsync();
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}
