﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.Implement
{
    public class SalonTypeConfigRepo : ISalonTypeConfigRepo
    {
        private Solution_30shineContext dbContext;
        private DbSet<SalonTypeConfig> dbSet;
        //constructor
        public SalonTypeConfigRepo()
        {
            dbContext = new Solution_30shineContext();
            dbSet = dbContext.Set<SalonTypeConfig>();
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<SalonTypeConfig> Get(Expression<Func<SalonTypeConfig, bool>> expression)
        {
            try
            {
                return await dbSet.FirstOrDefaultAsync(expression);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// add range
        /// </summary>
        /// <param name="obj"></param>
        public void Add(SalonTypeConfig obj)
        {
            try
            {
                dbSet.Add(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="obj"></param>
        public void Update(SalonTypeConfig obj)
        {
            try
            {
                dbSet.Update(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// save change async
        /// </summary>
        /// <returns></returns>
        public async Task SaveChangeAsync()
        {
            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
