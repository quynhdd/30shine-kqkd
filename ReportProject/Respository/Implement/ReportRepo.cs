﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ReportProject.Respository.Interface;
using ReportProject.Utils;
using ReportProject.Models.JsonModels;
using Newtonsoft.Json;
using ReportProject.Utils;
using System.Net.Http;
using ReportProject.Models.Solution_30shine_Real;

namespace ReportProject.Respository.Implement
{
    public class ReportRepo : IReportRepo
    {
        private Solution_30shineContext context;
        private DbSet<BillService> objBillServiceRead;
        private DbSet<TblSalon> objSalonRead;
        private DbSet<FlowService> objFlowServiceRead;
        private DbSet<FlowProduct> objFlowProductRead;
        private DbSet<SalaryIncome> objSalaryIncomeRead;
        private DbSet<SalonDailyCost> objSalonDailyCostRead;
        private DbSet<TinhThanh> objCityRead;
        private DbSet<StaticExpense> objStaticExpenseRead;
        private DbSet<Product> objProductRead;
        private DbSet<SalaryConfigStaff> objSalaryConfigStaffRead;
        private DbSet<SalaryIncomeChange> objSalaryIncomeChangeRead;
        private DbSet<CrmVoucherWaitTime> objCrmVoucherWaitTimeRead;
        private DbSet<InventoryFlowHc> objInventoryFlowHcRead;
        private DbSet<InventoryHc> objInventoryHcRead;
        private DbSet<QlkhoSalonOrder> objQlkhoSalonOrderRead;

        CultureInfo culture = new CultureInfo("vi-VN");
        static HttpClient client = new HttpClient();

        public ReportRepo()
        {
            context = new Solution_30shineContext();
            objBillServiceRead = context.Set<BillService>();
            objSalonRead = context.Set<TblSalon>();
            objFlowServiceRead = context.Set<FlowService>();
            objFlowProductRead = context.Set<FlowProduct>();
            objSalaryIncomeRead = context.Set<SalaryIncome>();
            objSalonDailyCostRead = context.Set<SalonDailyCost>();
            objCityRead = context.Set<TinhThanh>();
            objStaticExpenseRead = context.Set<StaticExpense>();
            objProductRead = context.Set<Product>();
            objSalaryConfigStaffRead = context.Set<SalaryConfigStaff>();
            objSalaryIncomeChangeRead = context.Set<SalaryIncomeChange>();
            objCrmVoucherWaitTimeRead = context.Set<CrmVoucherWaitTime>();
            objInventoryFlowHcRead = context.Set<InventoryFlowHc>();
            objInventoryHcRead = context.Set<InventoryHc>();
            objQlkhoSalonOrderRead = context.Set<QlkhoSalonOrder>();
        }

        public async Task<IEnumerable<ReportJson>> GetReport(DateTime dateFrom)
        {
            try
            {
                //var dateFrom = Convert.ToDateTime("22-04-2018", culture);
                var dateTo = dateFrom.AddDays(1);
                #region Xử lý lấy dữ liệu
                var billService = objBillServiceRead.AsNoTracking().Where(a =>
                    a.IsDelete == 0 && a.Pending == 0 && a.CompleteBillTime >= dateFrom && a.CompleteBillTime < dateTo
                    //&& !string.IsNullOrEmpty(a.ServiceIds)
                    ).Select(a => new
                    {
                        a.Id,
                        a.SalonId,
                        a.Images,
                        a.ServiceIds
                    }).ToArrayAsync();

                var salons = objSalonRead.AsNoTracking().Where(a => a.IsSalonHoiQuan == false && a.Id != Constant.SALONTEST).Select(a => new
                {
                    Id = a.Id,
                    Name = a.ShortName,
                    Order = a.Order,
                    CityId = a.CityId
                }).OrderBy(a => a.Order).ToArrayAsync();

                var flowService = objFlowServiceRead.AsNoTracking()
                    .Where(a => a.IsDelete == 0 && a.CreatedDate >= dateFrom && a.CreatedDate < dateTo.AddDays(Constant.ADD_DAY)).Select(a => new
                    {
                        a.BillId,
                        a.Quantity,
                        a.Price,
                        a.VoucherPercent
                    }).ToArrayAsync();

                var flowProduct = objFlowProductRead.AsNoTracking()
                    .Where(a => a.IsDelete == 0 && a.CreatedDate >= dateFrom && a.CreatedDate < dateTo.AddDays(Constant.ADD_DAY) && a.ComboId == null).Select(a => new
                    {
                        a.BillId,
                        a.Quantity,
                        a.Price,
                        a.VoucherPercent,
                        a.ProductId
                    }).ToArrayAsync();


                var salonDailyCost = objSalonDailyCostRead.AsNoTracking()
                    .Where(a => a.IsDelete == false && a.ReportDate >= dateFrom && a.ReportDate < dateTo).Select(a => a)
                    .ToArrayAsync();
                // update 20190226 select them MemberTypeAdd
                var product = objProductRead.AsNoTracking().Where(w => w.IsDelete == 0).Select(a => new
                {
                    a.Id,
                    a.Cost,
                    a.MemberTypeAdd
                }).ToArrayAsync();

                #region Sửa ngày 21/04

                var salaryIncome = objSalaryIncomeRead.AsNoTracking().Where(a => a.IsDeleted == false && a.WorkDate >= dateFrom && a.WorkDate < dateTo).Select(a => new
                {
                    a.SalonId,
                    a.FixedSalary,
                    a.AllowanceSalary,
                    a.OvertimeSalary,
                    a.ProductSalary,
                    a.ServiceSalary,
                    a.TotalIncome,
                    a.GrandTotalIncome,
                    a.BehaveSalary
                }).ToArrayAsync();

                var salaryIncomeChange = objSalaryIncomeChangeRead.AsNoTracking().Where(a => a.IsDeleted == false && a.WorkDate >= dateFrom && a.WorkDate < dateTo).Select(a => new
                {
                    a.SalonId,
                    a.ChangedType,
                    a.Point,
                }).ToArrayAsync();

                var salaryConfigStaff = objSalaryConfigStaffRead.AsNoTracking().Where(a => a.IsDeleted == false && a.CreatedTime >= dateFrom && a.CreatedTime < dateTo).Select(a => new
                {
                    a.SalonId,
                    a.FixSalaryOscillation
                }).ToArrayAsync();

                var crmVoucherWaitTime = objCrmVoucherWaitTimeRead.AsNoTracking().Where(a => a.IsDelete == false && a.CreatedTime >= dateFrom && a.CreatedTime < dateTo).Select(a => new
                {
                    a.SalonId,
                    a.StylistMoney,
                    a.SkinnerMoney,
                    a.CheckinMoney
                }).ToArrayAsync();

                #endregion
                Task.WaitAll(billService, salons, flowService, flowProduct, salonDailyCost, product, salaryIncome, salaryIncomeChange, salaryConfigStaff, crmVoucherWaitTime);

                var listBillService = billService.Result;
                var listSalons = salons.Result;
                var listFlowService = flowService.Result;
                var listFlowProduct = flowProduct.Result;
                var listSalonDailyCost = salonDailyCost.Result;
                var listProduct = product.Result;
                var listSalaryIncome = salaryIncome.Result;
                var listSalaryIncomeChange = salaryIncomeChange.Result;
                var listSalaryConfigStaff = salaryConfigStaff.Result;
                var listCrmVoucherWaitTime = crmVoucherWaitTime.Result;

                // get list productId Shinemember - 20190226
                var ProductIdShineMember = listProduct.Where(a => a.MemberTypeAdd == 2).Select(a => a.Id).ToList();

                var serviceIncome = (from a in listBillService
                                     join b in listFlowService on a.Id equals b.BillId
                                     where !string.IsNullOrEmpty(a.ServiceIds)
                                     select new
                                     {
                                         a.SalonId,
                                         totalServiceIncome = b.Quantity * b.Price * ((1 - decimal.Divide((decimal)b.VoucherPercent, 100)))
                                     }).ToArray();
                // 20190225 (update)
                // lay tong doanh thu MP
                var getListDTProduct = (from a in listBillService
                                        join b in listFlowProduct on a.Id equals b.BillId
                                        join c in listProduct on b.ProductId equals c.Id
                                        select new
                                        {
                                            a.SalonId,
                                            totalProductIncome = b.Quantity * b.Price * ((1 - decimal.Divide((decimal)b.VoucherPercent, 100))),
                                            totalProdutRealPrice = b.Quantity * c.Cost,
                                            b.ProductId
                                        }).ToArray();

                // tong doanh thu san pham khong tinh ShineMember
                var productIncome = getListDTProduct.Where(a => !ProductIdShineMember.Contains(a.ProductId.Value)).ToList();

                // Doanh thu ShineMember
                var productIncomeShinemember = getListDTProduct.Where(a => ProductIdShineMember.Contains(a.ProductId.Value)).ToList();

                var billWithImage = listBillService.Where(a => !string.IsNullOrEmpty(a.Images));
                #endregion
                var report = new List<ReportJson>();
                foreach (var salon in listSalons)
                {
                    var salonId = salon.Id;


                    //Chi phí bù lương
                    var buLuong = listSalaryIncomeChange.Where(a => a.SalonId == salonId && a.ChangedType == Constant.BU_LUONG).Sum(c => c.Point);
                    //Bảo hiểm của nhân viên
                    var baoHiem = listSalaryIncomeChange.Where(a => a.SalonId == salonId && a.ChangedType == Constant.BAO_HIEM).Sum(c => c.Point);
                    //Các chi phí phát sinh khác, xử phạt
                    var khac = listSalaryIncomeChange.Where(a => a.SalonId == salonId && (a.ChangedType == Constant.KHAC || a.ChangedType == Constant.XU_PHAT)).Sum(c => c.Point);
                    //Lương ứng của nhân viên

                    var ungLuong = listSalaryIncomeChange.Where(a => a.SalonId == salonId && a.ChangedType == Constant.UNG_LUONG).Sum(c => c.Point);
                    //Lương thưởng 5c
                    var luong5c = listSalaryIncomeChange.Where(a => a.SalonId == salonId && a.ChangedType == Constant.LUONG_5C).Sum(c => c.Point);
                    //Thuế thu nhập cá nhân
                    var thueThuNhapCaNhan = listSalaryIncomeChange.Where(a => a.SalonId == salonId && a.ChangedType == Constant.THUE_THU_NHAP_CA_NHAN).Sum(c => c.Point);
                    //Voucher wait time
                    var voucherWaitTime = listCrmVoucherWaitTime.Where(a => a.SalonId == salonId).Sum(a => a.CheckinMoney + a.SkinnerMoney + a.StylistMoney);

                    //lương cứng
                    var fixedSalary = listSalaryIncome.Where(a => a.SalonId == salonId).Sum(a => a.FixedSalary);
                    var behaveSalary = listSalaryIncome.Where(a => a.SalonId == salonId).Sum(a => a.BehaveSalary);
                    var fixSalaryOscillation = listSalaryConfigStaff.Where(a => a.SalonId == salonId).Sum(a => a.FixSalaryOscillation);
                    var overtimeSalary = listSalaryIncome.Where(a => a.SalonId == salonId).Sum(a => a.OvertimeSalary);
                    var serviceSalary = listSalaryIncome.Where(a => a.SalonId == salonId).Sum(a => a.ServiceSalary);

                    //K. Giá vốn dịch vụ tiền lương dịch vụ = STYLIST + SKINNER + CHECKIN + CHECKOUT + BAOVE
                    var totalSalaryService = (double)Math.Round((decimal)(fixedSalary + behaveSalary + fixSalaryOscillation + overtimeSalary + serviceSalary + buLuong - voucherWaitTime - baoHiem - khac - ungLuong + (luong5c) - thueThuNhapCaNhan));


                    #region Tính toán                   
                    //var totalDailyCostInventory = dailyCostInventoryResult.Where(a => a.salonId == salonId).Sum(a => a.totalMoney);

                    //C. Tổng khách = TỔNG BILL
                    int? numberOfTurns = listBillService.Where(a => a.SalonId == salonId && !string.IsNullOrEmpty(a.ServiceIds)).Select(a => a.Id).Count();

                    //E. Tổng doanh thu dịch vụ
                    var totalServiceProfit = serviceIncome.Where(a => a.SalonId == salonId).Sum(a => a.totalServiceIncome);

                    //F. Tổng doanh thu sản phẩm (update 20190226 tach Doanh thu MP ko tinh Shinemember)
                    var totalProductIncome = productIncome.Where(a => a.SalonId == salonId).Sum(a => a.totalProductIncome);

                    // add 20190226 Doanh thu product chi co ShineMember
                    var totalProductInComeShinemember = productIncomeShinemember.Where(a => a.SalonId == salonId).Sum(a => a.totalProductIncome);

                    //D. Tổng doanh thu = E + F
                    var totalIncome = (double)(totalServiceProfit + totalProductIncome + totalProductInComeShinemember);

                    //G. Tổng doanh thu trên khách = D/C
                    var totalTransactionPerCus = numberOfTurns == 0 ? 0 : (int)Math.Round((decimal)(totalIncome / numberOfTurns));

                    //H. Tổng doanh thu dịch vụ trên khách = E/C
                    var totalServicePerCus = numberOfTurns == 0 ? 0 : (int)Math.Round((decimal)(totalServiceProfit / numberOfTurns));

                    //I. Tổng doanh thu sản phẩm trên khách = F/C
                    var totalProductPerCus = numberOfTurns == 0 ? 0 : (int)Math.Round((decimal)(totalProductIncome / numberOfTurns));

                    //L. Tỉ lệ lương giá vốn trên doanh thu dịch vụ = K/E
                    var salaryServicePerServiceIncome = totalServiceProfit == 0
                        ? 0
                        : Convert.ToDouble(
                            Math.Round(decimal.Divide((int)totalSalaryService, (int)totalServiceProfit), 2));

                    //O. Lương thưởng doanh số KCS = 5% * TỔNG BILL CÓ ẢNH
                    var dataBillWithImage = billWithImage.Count(a => a.SalonId == salonId);
                    int? totalPayOffKCS = (int)(500 * ((float)90 / 100) * dataBillWithImage);

                    //M. Giá vốn dịch vụ tiền lương mỹ phẩm = 10% * F
                    var totalProductCapital = listSalaryIncome.Where(a => a.SalonId == salonId).Sum(a => a.ProductSalary);

                    // Get data table SalonDailyCost
                    var dailyCostBySalon = listSalonDailyCost.FirstOrDefault(a => a.SalonId == salonId);

                    // add20190402 Luong BV + Checkout
                    var totalSecurityCheckOutSalary = dailyCostBySalon?.SecurityCheckoutSalary ?? 0;

                    // add update 20190305

                    //V. Chi phí SMS
                    var totalSMSExpenes = dailyCostBySalon?.Smsexpenses ?? 0;

                    //Q.Tiền điện nước
                    var electricityAndWaterBill = dailyCostBySalon?.ElectricityAndWaterBill ?? 0;

                    //S. Tiền thuê cửa hàng đã bao gồm chi phí thuế 
                    var rentWithTax = dailyCostBySalon?.RentWithTax ?? 0;

                    //T. Phân bổ đầu tư khấu hao chi phí trả trước
                    var capitalSpending = dailyCostBySalon?.CapitalSpending ?? 0;

                    //U. CP quảng cáo
                    var advertisementExpend = dailyCostBySalon?.AdvertisementExpend ?? 0;

                    //W. Chi phí vận chuyển TQ
                    var shippingExpend = dailyCostBySalon?.ShippingExpend ?? 0;

                    //X. Cước điện thoại và Internet
                    var internetAndPhoneBill = dailyCostBySalon?.InternetAndPhoneBill ?? 0;

                    //Y. Tiền bảo hiểm xã hội, KPCĐ
                    var socialInsuranceAndFixedCost = dailyCostBySalon?.SocialInsuranceAndFixedCost ?? 0;

                    //AA. Thuế thu nhập doanh nghiệp
                    var incomeTaxes = dailyCostBySalon?.IncomeTaxes ?? 0;

                    //AB. Chi phí vận hành phát sinh hàng ngày
                    var salonUnplannedSpending = dailyCostBySalon?.SalonUnplannedSpending ?? 0;

                    //AD. Chi phí thuê văn phòng thái hà
                    var officeRentAndSeviceCost = dailyCostBySalon?.ThaiHaRentAndSeviceCost ?? 0;

                    //AE. Lương cơ bản
                    var officeStaffSalary = dailyCostBySalon?.OfficeStaffSalary ?? 0;

                    //AF. Lương thuong khac update 20190402
                    var salesSalary = dailyCostBySalon?.SalesSalary ?? 0;

                    //AG. Chi phí lương IT
                    var itSalary = dailyCostBySalon?.ItSalary ?? 0;

                    //AH. Tiền bảo hiểm xã hội bộ phận chung
                    var officeStaffSocialInsurance = dailyCostBySalon?.OfficeStaffSocialInsurance ?? 0;

                    //AI. Chi phí phát sinh hằng ngày (vận hành chung)
                    var unplannedSpending = dailyCostBySalon?.UnplannedSpending ?? 0;

                    //daily Cost inventory
                    var totalDailyCostInventory = dailyCostBySalon?.DailyCostInventory ?? 0;

                    //thêm bù trừ
                    var compensation = dailyCostBySalon?.Compensation ?? 0;

                    //T
                    var totalProdutRealPrice =
                        productIncome.Where(a => a.SalonId == salonId).Sum(a => a.totalProdutRealPrice);

                    //20190225
                    // phan bo shinemember trong ky ke toan hang ngay statistic sau khi import len
                    var totalSMDistributionToday = dailyCostBySalon?.Smdistribution ?? 0;

                    // tong doanh thu Shinemember phan bo trong ky hom nay he thong tu dong tinh
                    var totalShineMemberDistribution = totalProductInComeShinemember / 12;

                    // tinh doanh thu thuan 
                    var totalNetRevenue = (double)totalServiceProfit + (double)totalProductIncome + totalSMDistributionToday + (double)totalShineMemberDistribution;

                    //Z. Thuế
                    var tax = Convert.ToDouble(Math.Round(decimal.Divide(6, 100), 3)) *
                                 Convert.ToDouble(totalNetRevenue);

                    //J. Giá vốn trực tiếp => :( ĐM không tra được tiếng anh nó là gì luôn :((((
                    // Cong thuc moi update 20190402
                    var directFee = (double)Math.Round((decimal)(totalSalaryService + totalProductCapital + totalSecurityCheckOutSalary +
                                    totalProdutRealPrice + compensation + totalPayOffKCS + totalDailyCostInventory));
                    //R. Chi phí salon /update 20190402
                    var salonFee = rentWithTax + capitalSpending + electricityAndWaterBill + advertisementExpend + totalSMSExpenes + shippingExpend + tax + incomeTaxes + salonUnplannedSpending;

                    //AC. Chi phí quản lý/ update 20190402
                    var manageFee = internetAndPhoneBill + socialInsuranceAndFixedCost + officeRentAndSeviceCost + officeStaffSalary + itSalary + unplannedSpending + salesSalary;

                    //DQ. Thu nhap khac
                    var totalOtherIncome = dailyCostBySalon?.OtherIncome ?? 0;

                    //Update 20190304
                    // cong Thuc tinh moi
                    var totalIncomeAfterTax = (double)totalServiceProfit + (double)totalProductIncome + totalSMDistributionToday + (double)totalShineMemberDistribution +
                                             totalOtherIncome - directFee - salonFee - manageFee -
                                            ((totalSMDistributionToday + (double)totalShineMemberDistribution) * (10 / 100)) + ((double)totalProductInComeShinemember * (10 / 100));
                    #endregion

                    var data = new ReportJson
                    {
                        salonId = salonId,
                        salonName = salon.Name,
                        salonOrder = salon.Order,
                        salonCity = salon.CityId,
                        totalIncomeAfterTax = totalIncomeAfterTax,
                        numberOfTurns = numberOfTurns,
                        // update 20190304
                        totalSales = totalIncome,
                        totalServiceProfit = (double)totalServiceProfit,
                        totalProductProfit = (double)totalProductIncome,
                        totalTransactionPerCus = totalTransactionPerCus,
                        totalServicePerCus = totalServicePerCus,
                        totalProductPerCus = totalProductPerCus,
                        otherIncome = totalOtherIncome,
                        directFee = directFee,
                        totalStaffSalary = totalSalaryService,
                        salaryServicePerServiceIncome = salaryServicePerServiceIncome,
                        totalProductCapital = totalProductCapital,
                        totalProdutPrice = totalProdutRealPrice,
                        totalPayOffKCS = totalPayOffKCS,
                        totalDailyCostInventory = totalDailyCostInventory,
                        electricityAndWaterBill = electricityAndWaterBill,
                        salonFee = salonFee,
                        rentWithTax = rentWithTax,
                        capitalSpending = capitalSpending,
                        mktExpense = advertisementExpend,
                        totalSMSExpenes = totalSMSExpenes,
                        shippingExpend = shippingExpend,
                        internetAndPhoneBill = internetAndPhoneBill,
                        socialInsuranceAndFixedCost = socialInsuranceAndFixedCost,
                        tax = tax,
                        incomeTaxes = incomeTaxes,
                        salonUnplannedSpending = salonUnplannedSpending,
                        manageFee = manageFee,
                        officeRentAndSeviceCost = officeRentAndSeviceCost,
                        officeStaffSalary = officeStaffSalary,
                        salesSalary = salesSalary,
                        itSalary = itSalary,
                        officeStaffSocialInsurance = officeStaffSocialInsurance,
                        unplannedSpending = unplannedSpending,
                        compensation = compensation,
                        // add 20190225
                        // doanh thu phan bo shinemember trong ky ke toan
                        smDistributionToday = totalSMDistributionToday,
                        // doanh thu phan bo shinemember trong ky hom nay
                        totalShineMemberDistribution = (double)totalShineMemberDistribution,
                        // doanh thu shinemember ban trong ngay
                        totalProductShineMember = (double)totalProductInComeShinemember,
                        // doanh thu thuan
                        totalNetRevenue = totalNetRevenue,
                        // luong BV + Checkout 20190402
                        totalSecurityCheckOutSalary = totalSecurityCheckOutSalary
                    };

                    if (!(totalIncome == 0 && directFee == 0 && salonFee == 0 && manageFee == 0 && salaryServicePerServiceIncome == 0))
                    {
                        report.Add(data);
                    }
                }

                return report.OrderByDescending(a => a.totalIncomeAfterTax);

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IEnumerable<ReportJson> GetReportByDate(DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var salons = objSalonRead.AsNoTracking().Where(a =>
                        a.IsDelete == 0 && a.Publish == true && a.IsSalonHoiQuan == false && a.Id != Constant.SALONTEST).Select(a => new
                        {
                            Id = a.Id,
                            Order = a.Order,
                            SalonShortName = a.ShortName,
                            SalonCity = a.CityId
                        }).OrderBy(a => a.Order).ToArray();

                var salonList = (from a in context.StaticExpense.AsNoTracking()
                                 join b in context.TblSalon.AsNoTracking() on a.SalonId equals b.Id
                                 where a.WorkDate >= dateFrom && a.WorkDate < dateTo.AddDays(1)
                                 select new ReportJson
                                 {
                                     salonId = (int)a.SalonId,
                                     salonName = b.ShortName,
                                     salonOrder = b.Order,
                                     salonCity = b.CityId,
                                     totalIncomeAfterTax = a.TotalIncomeAfterTax,
                                     numberOfTurns = a.NumberOfTurns,
                                     totalSales = a.TotalSales,
                                     totalServiceProfit = a.TotalServiceProfit,
                                     totalProductProfit = a.TotalCosmeticProfit,
                                     totalTransactionPerCus = a.TotalTransactionPerCus,
                                     totalServicePerCus = a.TotalServicePerCus,
                                     totalProductPerCus = a.TotalProductPerCus,
                                     directFee = a.DirectFee,//
                                     totalStaffSalary = a.TotalStaffSalary,
                                     salaryServicePerServiceIncome = a.SalaryServicePerServiceIncome,
                                     totalProductCapital = a.TotalProductCapital,
                                     totalProdutPrice = a.TotalProductPrice,
                                     totalPayOffKCS = a.TotalPayOffKcs,
                                     totalDailyCostInventory = a.TotalDailyCostInventory,
                                     electricityAndWaterBill = a.ElectricityAndWaterBill,
                                     salonFee = a.SalonFee,//
                                     rentWithTax = a.RentWithTax,
                                     capitalSpending = a.CapitalSpending,
                                     mktExpense = a.Mktexpense,
                                     totalSMSExpenes = a.TotalSmsexpenses,
                                     shippingExpend = a.ShippingExpend,
                                     internetAndPhoneBill = a.InternetAndPhoneBill,
                                     socialInsuranceAndFixedCost = a.SocialInsuranceAndFixedCost,
                                     tax = a.Tax,
                                     incomeTaxes = a.IncomeTaxes,
                                     salonUnplannedSpending = a.SalonUnplannedSpending,
                                     manageFee = a.ManageFee,//
                                     officeRentAndSeviceCost = a.OfficeRentAndServiceCose,
                                     officeStaffSalary = a.OfficeStaffSalary,
                                     salesSalary = a.SalesSalary,
                                     itSalary = a.Itexpense,
                                     officeStaffSocialInsurance = a.OfficeStaffSocialInsurance,
                                     unplannedSpending = a.UnplannedSpending,
                                     compensation = a.Compensation,
                                     otherIncome = a.TotalOtherIncome,
                                     // doanh thu shinemember phan bo ke toan
                                     smDistributionToday = a.TotalSmdistributionToday,
                                     // doanh thu shinemember phan bo hom nay
                                     totalShineMemberDistribution = a.TotalSmdistributon,
                                     // doanh thu shinemeber
                                     totalProductShineMember = a.TotalProductSm,
                                     // doanh thu thuan
                                     totalNetRevenue = a.TotalNetRevenue,
                                     // Luong BV + Checkout
                                     totalSecurityCheckOutSalary = a.TotalSecurityCheckoutSalary
                                 }).ToArray();

                var salon = new List<ReportJson>();

                foreach (var s in salons)
                {

                    var salonTotal = salonList.Where(a => a.salonId == s.Id).ToList();

                    var numberOfSalon = salonTotal.Count();

                    if (numberOfSalon > 0)
                    {
                        var data = new ReportJson
                        {
                            salonId = s.Id,
                            salonName = s.SalonShortName,
                            salonOrder = s.Order,
                            salonCity = s.SalonCity,
                            totalIncomeAfterTax = salonTotal.Sum(b => b.totalIncomeAfterTax),
                            numberOfTurns = salonTotal.Sum(b => b.numberOfTurns),
                            totalSales = salonTotal.Sum(b => b.totalSales),
                            totalServiceProfit = salonTotal.Sum(b => b.totalServiceProfit),
                            totalProductProfit = salonTotal.Sum(b => b.totalProductProfit),
                            totalTransactionPerCus = salonTotal.Sum(a => a.numberOfTurns) == 0 ? 0 : (int)Math.Round((decimal)(salonTotal.Sum(a => a.totalSales) / salonTotal.Sum(a => a.numberOfTurns))),
                            totalServicePerCus = salonTotal.Sum(a => a.numberOfTurns) == 0 ? 0 : (int)Math.Round((decimal)(salonTotal.Sum(a => a.totalServiceProfit) / salonTotal.Sum(a => a.numberOfTurns))),
                            totalProductPerCus = salonTotal.Sum(a => a.numberOfTurns) == 0 ? 0 : (int)Math.Round((decimal)(salonTotal.Sum(a => a.totalProductProfit) / salonTotal.Sum(a => a.numberOfTurns))),
                            directFee = salonTotal.Sum(b => b.directFee),
                            totalStaffSalary = salonTotal.Sum(b => b.totalStaffSalary),
                            salaryServicePerServiceIncome = (double)Math.Round(decimal.Divide((decimal)salonTotal.Sum(b => b.salaryServicePerServiceIncome), numberOfSalon), 2),
                            totalProductCapital = salonTotal.Sum(b => b.totalProductCapital),
                            totalProdutPrice = salonTotal.Sum(b => b.totalProdutPrice),
                            totalPayOffKCS = salonTotal.Sum(b => b.totalPayOffKCS),
                            totalDailyCostInventory = salonTotal.Sum(b => b.totalDailyCostInventory),
                            electricityAndWaterBill = salonTotal.Sum(b => b.electricityAndWaterBill),
                            salonFee = salonTotal.Sum(b => b.salonFee),
                            rentWithTax = salonTotal.Sum(b => b.rentWithTax),
                            capitalSpending = salonTotal.Sum(b => b.capitalSpending),
                            mktExpense = salonTotal.Sum(b => b.mktExpense),
                            totalSMSExpenes = salonTotal.Sum(b => b.totalSMSExpenes),
                            shippingExpend = salonTotal.Sum(b => b.shippingExpend),
                            internetAndPhoneBill = salonTotal.Sum(b => b.internetAndPhoneBill),
                            socialInsuranceAndFixedCost = salonTotal.Sum(b => b.socialInsuranceAndFixedCost),
                            tax = salonTotal.Sum(b => b.tax),
                            incomeTaxes = salonTotal.Sum(b => b.incomeTaxes),
                            salonUnplannedSpending = salonTotal.Sum(b => b.salonUnplannedSpending),
                            manageFee = salonTotal.Sum(b => b.manageFee),
                            officeRentAndSeviceCost = salonTotal.Sum(b => b.officeRentAndSeviceCost),
                            officeStaffSalary = salonTotal.Sum(b => b.officeStaffSalary),
                            salesSalary = salonTotal.Sum(b => b.salesSalary),
                            itSalary = salonTotal.Sum(b => b.itSalary),
                            officeStaffSocialInsurance = salonTotal.Sum(b => b.officeStaffSocialInsurance),
                            unplannedSpending = salonTotal.Sum(b => b.unplannedSpending),
                            compensation = salonTotal.Sum(b => b.compensation),
                            otherIncome = salonTotal.Sum(b => b.otherIncome),
                            smDistributionToday = salonTotal.Sum(b => b.smDistributionToday),
                            totalShineMemberDistribution = salonTotal.Sum(b => b.totalShineMemberDistribution),
                            totalProductShineMember = salonTotal.Sum(b => b.totalProductShineMember),
                            totalNetRevenue = salonTotal.Sum(b => b.totalNetRevenue),
                            // add 20190402
                            totalSecurityCheckOutSalary = salonTotal.Sum(b => b.totalSecurityCheckOutSalary)
                        };

                        salon.Add(data);
                    }
                }

                return salon.OrderByDescending(a => a.totalIncomeAfterTax);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public StaticExpense GetBySalonIdAndDate(int salonId, DateTime date)
        {
            try
            {
                var data = objStaticExpenseRead.FirstOrDefault(a => a.SalonId == salonId && a.WorkDate >= date && a.WorkDate < date.AddDays(1));
                return data;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void Add(IEnumerable<StaticExpense> staticExpense)
        {
            try
            {
                objStaticExpenseRead.AddRange(staticExpense);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public void Update(IEnumerable<StaticExpense> staticExpense)
        {
            try
            {
                objStaticExpenseRead.UpdateRange(staticExpense);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Delete(IEnumerable<StaticExpense> staticExpense)
        {
            try
            {
                objStaticExpenseRead.RemoveRange(staticExpense);
                context.SaveChanges();
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public IEnumerable<StaticExpense> GetListReportByDate(DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var staticExpense = objStaticExpenseRead.Where(a => a.WorkDate >= dateFrom && a.WorkDate < dateTo).Select(a => a).ToList();
                return staticExpense;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}