﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.BillserviceJson;
using static ReportProject.Models.JsonModels.BookingJson;

namespace ReportProject.Respository.Implement
{
    public class BookingRepo : IBookingRepo
    {
        private Solution_30shineContext context;
        private DbSet<Booking> objEntity;
        public BookingRepo()
        {
            context = new Solution_30shineContext();
            objEntity = context.Set<Booking>();
        }

        public IEnumerable<BookingJoin.BookingJoinBillService> getBookingJoinBillserviceJoinBookingTemp(IEnumerable<BillserviceReal> billservices, IEnumerable<BookingReal> bookings, IEnumerable<BookingTempReal> bookingtemps)
        {
            try
            {
                return null;
                //return  from a in BookingReal 
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<IEnumerable<Booking>> GetBookings(DateTime dateBook)
        {
            try
            {
                return await context.Booking.AsNoTracking().Where(a => a.IsDelete == 0 && a.Os != null && a.DatedBook == dateBook).ToListAsync();
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public async Task<IEnumerable<BookingReal>> getByDate(IEnumerable<int?> salonIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                return await objEntity.AsNoTracking().Where(a => a.IsDelete == 0 && a.DatedBook >= dateFrom && a.DatedBook < dateTo && salonIds.Contains(a.SalonId)).Select(a => new BookingReal
                {
                    Id = a.Id,
                    HourId = a.HourId,
                    SalonId = a.SalonId,
                    StylistId = a.StylistId,
                    DatedBook = a.DatedBook,
                    BookingTempId = a.BookingTempId,
                    IsBookOnline = a.IsBookOnline,
                    IsBookStylist = a.IsBookStylist,
                    CreatedDate = a.CreatedDate,
                    HourSubId = a.SubHour
                }).ToListAsync();

            }
            catch (Exception e)
            {

                throw;
            }
        }

        public async Task<IEnumerable<BookingReal>> getByDateAllBook(IEnumerable<int?> salonIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                return await objEntity.AsNoTracking().Where(a => a.DatedBook >= dateFrom && a.DatedBook < dateTo && salonIds.Contains(a.SalonId)).Select(a => new BookingReal
                {
                    Id = a.Id,
                    HourId = a.HourId,
                    SalonId = a.SalonId,
                    StylistId = a.StylistId,
                    DatedBook = a.DatedBook,
                    BookingTempId = a.BookingTempId,
                    IsBookOnline = a.IsBookOnline,
                    IsBookStylist = a.IsBookStylist,
                    CreatedDate = a.CreatedDate,
                    IsDelete = a.IsDelete,
                    Os = a.Os
                }).ToListAsync();

            }
            catch (Exception e)
            {

                throw;
            }
        }

        public async Task<IEnumerable<BookingReal>> getByDateBookDelete(IEnumerable<int?> salonIds, DateTime dateFrom, DateTime dateTo)
        {
            return await objEntity.AsNoTracking().Where(a => a.IsDelete == 1 && a.DatedBook >= dateFrom && a.DatedBook < dateTo && salonIds.Contains(a.SalonId) && a.IsBookOnline == true).Select(a => new BookingReal
            {
                Id = a.Id,
                HourId = a.HourId,
                SalonId = a.SalonId,
                StylistId = a.StylistId,
                DatedBook = a.DatedBook,
                BookingTempId = a.BookingTempId,
                IsBookOnline = a.IsBookOnline,
                IsBookStylist = a.IsBookStylist,
                CreatedDate = a.CreatedDate
            }).ToListAsync();
        }
    }
}
