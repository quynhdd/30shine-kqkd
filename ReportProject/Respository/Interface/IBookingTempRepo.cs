﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.BookingJson;

namespace ReportProject.Respository.Interface
{
    public interface IBookingTempRepo
    {
        Task<IEnumerable<BookingTempReal>> getByDate(IEnumerable<int?> salonIds, DateTime dateFrom, DateTime dateTo);
    }
}
