﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.SalonTypeConfigJson;

namespace ReportProject.Respository.Interface
{
    public interface IServiceSalonTypeConfigRepo
    {
        Task<ServiceSalonTypeConfig> Get(Expression<Func<ServiceSalonTypeConfig, bool>> expression);
        Task<IEnumerable<ServiceSalonTypeConfig>> GetList(Expression<Func<ServiceSalonTypeConfig, bool>> expression);
        Task<List<OutputServiceSalonTypeConfig>> GetListServiceSalonTypeConfig();
        void Add(ServiceSalonTypeConfig obj);
        void Update(ServiceSalonTypeConfig obj);
        Task SaveChangeAysnc();
    }
}
