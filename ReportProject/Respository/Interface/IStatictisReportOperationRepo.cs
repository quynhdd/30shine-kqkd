﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Interface
{
    public interface IStatictisReportOperationRepo
    {
        Task<IEnumerable<StatictisReportOperation>> GetByDayListSalon(IEnumerable<int?> idSalons, DateTime fromDate, DateTime toDate);
        //IEnumerable<StatictisReportOperation> GetListReportByDate(DateTime dateFrom, DateTime dateTo);
        void Add(IEnumerable<StatictisReportOperation> staticReportOperation);
        void Delete(IEnumerable<StatictisReportOperation> staticReportOperation);
    }
}
