﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ReportProject.Models.Solution_30shine_Real;

namespace ReportProject.Respository.Interface
{
    public interface ISalonRepo
    {
        IEnumerable<TblSalon> ValidateSalon(IEnumerable<int> salonIds);
        IEnumerable<TblSalon> ValidateSalonShortName(IEnumerable<string> salonShortName);
        Task<IEnumerable<TblSalon>> Get();
        IEnumerable<TblSalon> GetList(IEnumerable<int?> salonIds);
        IEnumerable<TblSalon> GetListParam(int type, int Id);
        Task<IEnumerable<TblSalon>> GetByRegionId(int regionId);
        Task<IEnumerable<TblSalon>> GetBySalonId(int salonId);
        Task<List<TblSalon>> GetList(Expression<Func<TblSalon,bool>> expression);
    }
}
