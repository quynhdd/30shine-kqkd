﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.BillserviceJson;
using static ReportProject.Models.JsonModels.BookingJoin;
using static ReportProject.Models.JsonModels.BookingJson;

namespace ReportProject.Respository.Interface
{
    public interface IBookingRepo
    {
        Task<IEnumerable<BookingReal>> getByDate(IEnumerable<int?> salonIds, DateTime dateFrom, DateTime dateTo);
        Task<IEnumerable<BookingReal>> getByDateAllBook(IEnumerable<int?> salonIds, DateTime dateFrom, DateTime dateTo);
        IEnumerable<BookingJoinBillService> getBookingJoinBillserviceJoinBookingTemp(IEnumerable<BillserviceReal> billservices, IEnumerable<BookingReal> bookings, IEnumerable<BookingTempReal> bookingtemps);
        Task<IEnumerable<BookingReal>> getByDateBookDelete(IEnumerable<int?> salonIds, DateTime dateFrom, DateTime dateTo);
        Task<IEnumerable<Booking>> GetBookings(DateTime dateBook);
    }
}
