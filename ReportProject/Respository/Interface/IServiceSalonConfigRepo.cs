﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.Interface
{
    public interface IServiceSalonConfigRepo
    {
        Task<IEnumerable<ServiceSalonConfig>> GetbyListSalon(IEnumerable<int?> salonIds, IEnumerable<int?> departmentIds);
        Task<IEnumerable<ServiceSalonConfig>> GetList(Expression<Func<ServiceSalonConfig, bool>> expression);
        void Add(ServiceSalonConfig obj);
        void Update(ServiceSalonConfig obj);
        void AddRange(IEnumerable<BackupTableServiceSalonConfig> obj);
        Task SaveChangeAsync();
    }
}
