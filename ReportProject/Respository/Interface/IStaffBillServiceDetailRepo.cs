﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.AccountingReportJson;

namespace ReportProject.Respository.Interface
{
    public interface IStaffBillServiceDetailRepo
    {
        Task<IEnumerable<StaffBillServiceDetailJson>> Get(DateTime dateFrom, DateTime dateTo,int salonId, int departmentId);
        Task<IEnumerable<StaffBillServiceDetail>> GetByDate(int staffType, DateTime dateFrom, DateTime dateTo, int salonId);
    }
}
