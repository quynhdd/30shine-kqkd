﻿using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Implement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Interface
{
    public interface IOperationReportStatisticRepo
    {
        Task<IEnumerable<OperationReportStatistic>> GetByDay(int salonId, int regionId, DateTime? dateFrom, DateTime? dateTo);
        void Delete(IEnumerable<OperationReportStatistic> data);
        Task SaveChangesAsync();
        Task AddAsync(IEnumerable<OperationReportStatistic> data);
        void Update(OperationReportStatistic data);
        Task<IEnumerable<OperationReportStatistic>> Get(DateTime dateFrom, DateTime dateTo);
    }
}
