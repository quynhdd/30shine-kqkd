﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Interface
{
    public interface IConfigRepo
    {
        string GetEmail(string EMAIL_KEY);
        Task<TblConfig> GetByKey(string key);
        Task<IEnumerable<TblConfig>> GetByKeys(IEnumerable<string> keys);

    }
}
