﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Semantics;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;

namespace ReportProject.Respository.Interface
{
    public interface IReportRepo

    {
        Task<IEnumerable<ReportJson>> GetReport(DateTime dateFrom);
        //Task<IEnumerable<StaticExpenseJson.Obj>> GetReportVer2();
        IEnumerable<ReportJson> GetReportByDate(DateTime dateFrom, DateTime dateTo);

        StaticExpense GetBySalonIdAndDate(int salonId, DateTime date);

        void Add(IEnumerable<StaticExpense> staticExpense);
        void Update(IEnumerable<StaticExpense> staticExpense);
        void Delete(IEnumerable<StaticExpense> staticExpense);

        IEnumerable<StaticExpense> GetListReportByDate(DateTime dateFrom, DateTime dateTo);
    } 
}