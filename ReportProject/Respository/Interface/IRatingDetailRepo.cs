﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.RatingDetailJson;

namespace ReportProject.Respository.Interface
{
   public interface IRatingDetailRepo
    {
        Task<IEnumerable<RatingDetailReal>> getByDateListSalon( DateTime fromDate, DateTime toDate);

    }
}
