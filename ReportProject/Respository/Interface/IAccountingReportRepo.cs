﻿using ReportProject.Models.JsonModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.AccountingReportJson;

namespace ReportProject.Respository.Interface
{
    public interface IAccountingReportRepo
    {
        Task<IEnumerable<GetAccountingReportReturnJson>> GetAccountingReports(int salonId, DateTime dateFrom, DateTime dateTo);
        Task<AccountingTaxReportJson> GetAccountingTaxReports(int salonId, int staffType, DateTime dateFrom, DateTime dateTo);
        //Header AddHeader(ObjData allService);
    }
}
