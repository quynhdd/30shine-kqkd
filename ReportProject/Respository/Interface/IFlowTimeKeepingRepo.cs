﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.FlowTimeKeepingJoin;

namespace ReportProject.Respository.Interface
{
    public interface IFlowTimeKeepingRepo
    {
        Task<IEnumerable<FlowTimeKeeping>> GetBySalonAndDate(int salonId, DateTime dateFrom, DateTime dateTo);
        Task<IEnumerable<FlowTimeKeepingJoinWorkTime>> GetByStylistSalonAndDate(IEnumerable<int?> salonIds, IEnumerable<int?> Stafftypes, DateTime dateFrom, DateTime dateTo);
    }
}
