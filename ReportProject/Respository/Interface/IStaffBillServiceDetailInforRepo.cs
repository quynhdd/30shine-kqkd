﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Interface
{
    public interface IStaffBillServiceDetailInforRepo
    {
        Task<List<StaffBillServiceDetail>> GetStaffBillServiceDetail(DateTime fromDate);
        void DeleteAndAdd(IEnumerable<StaffBillServiceDetail> DeleteListStaffBillServiceDetails,IEnumerable<StaffBillServiceDetail> AddListStaffBillServiceDetails);        
    }
}
