﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ReportProject.Models.Solution_30shine_Real;

namespace ReportProject.Respository.Interface
{
    public interface ISalonDailyCostRepo
    {
        void Add(IEnumerable<SalonDailyCost> salonDailyCost);
        void Update(IEnumerable<SalonDailyCost> salonDailyCost);
        SalonDailyCost GetBySalonIdAndDate(int salonId, DateTime date);
    }
}
