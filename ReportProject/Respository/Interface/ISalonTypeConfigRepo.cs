﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.Interface
{
    public interface ISalonTypeConfigRepo
    {
        Task<SalonTypeConfig> Get(Expression<Func<SalonTypeConfig, bool>> expression);
        void Add(SalonTypeConfig obj);
        void Update(SalonTypeConfig obj);
        Task SaveChangeAsync();
    }
}
