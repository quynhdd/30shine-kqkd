﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Interface
{
    public interface IOperationReportConfigRepo
    {
        Task<OperationReportConfig> GetSaleTargets(int salonId, int regionId, DateTime month);
    }
}
