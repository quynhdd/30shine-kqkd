﻿using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Interface
{
    public interface IOperationReportRepo
    {
        Task<OperationReportJsonOutPut> GetOperationReportToday(int type, int id, DateTime fromDate);

        Task<OperationReportJsonOutPut> GetByDayOperationReport(DateTime fromDate, DateTime toDate, int type, int id);
    }
}
