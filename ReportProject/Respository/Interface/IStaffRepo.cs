﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.AccountingReportJson;
using static ReportProject.Models.JsonModels.StaffJson;

namespace ReportProject.Respository.Interface
{
    public interface IStaffRepo
    {
        Task<IEnumerable<Staff>> GetStaffBySalon(int salonId);
        Task<IEnumerable<Staff>> GetStylistAndSkinner(int staffType);
        IEnumerable<StaffInfo> GetStaffInfo(IEnumerable<BillServiceHis> billServices, IEnumerable<Staff> staffs, IEnumerable<SalaryIncome> salaryIncomes, IEnumerable<TblSkillLevel> tblSkillLevels, IEnumerable<StaffType> staffTypes);
        Task<IEnumerable<InforStaff>> GetAllStaffType(int staffType);
        Task<IEnumerable<InforStaff>> GetStaffByDepartment(IEnumerable<int?> staffTypeIds);
    }
}
