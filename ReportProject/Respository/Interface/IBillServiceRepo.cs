﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.AccountingReportJson;
using static ReportProject.Models.JsonModels.BillServiceJoin;
using static ReportProject.Models.JsonModels.BillserviceJson;
using static ReportProject.Models.JsonModels.RatingDetailJson;

namespace ReportProject.Respository.Interface
{
    public interface IBillServiceRepo
    {
        Task<IEnumerable<BillServiceHis>> GetByDate(int salonId, DateTime dateFrom, DateTime dateTo);
        IEnumerable<BillServiceJoinFlowServiceJson> BillServiceJoinFlowService(IEnumerable<BillServiceHis> billServices, IEnumerable<FlowService> flowServices);
        IEnumerable<BillServiceJoinFlowProductJson> BillServiceJoinFlowProduct(IEnumerable<BillServiceHis> billServices, IEnumerable<FlowProduct> flowProducts);
        Task<ObjectReport> GetObjectReport(int salonId, DateTime dateFrom, DateTime dateTo);
        IEnumerable<BillServiceDetails> GetBillServiceDetails(IEnumerable<BillServiceHis> billServices, IEnumerable<StaffBillServiceDetail> staffBillServiceDetails, IEnumerable<FlowService> flowServices);
        Task<IEnumerable<BillserviceReal>> GetBillServiceByDate(IEnumerable<int?> salonIds, DateTime dateFrom, DateTime dateTo);
        Task<IEnumerable<BillServiceJoinFlowProductJson>> BillserviceRealJoinFlowProduct(IEnumerable<BillserviceReal> billServiceReal, IEnumerable<FlowProduct> flowProducts);
        Task<IEnumerable<BillServiceJoinFlowServiceJson>> BillserviceRealJoibFlowservice(IEnumerable<BillserviceReal> billServiceReal, IEnumerable<FlowService> flowServices);
        //BillserviceJoinRatingDetail
        Task<IEnumerable<BillserviceJoinRatingDetail>> BillserviceJoinRatingDetails(IEnumerable<BillserviceReal> billserviceReal, IEnumerable<RatingDetailReal> ratingDetailReal);

        Task<IEnumerable<BillServiceJoinFlowServiceJson>> BillserviceRealJoibFlowservicesFollow( IEnumerable<int?> salonIds, IEnumerable<int?> serviceIds , DateTime fromDate, DateTime toDate);

        IEnumerable<BillServiceJoinFlowServiceJson> BillServiceJoinFlowServiceConvertMark(IEnumerable<BillServiceHis> billServices, IEnumerable<FlowService> flowServices);
    }
}
