﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Interface
{
    public interface ITblConfigRepo
    {
        Task<TblConfig> GetByKeyConfig(string Key);
    }
}
