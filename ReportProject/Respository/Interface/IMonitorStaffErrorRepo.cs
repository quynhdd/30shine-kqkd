﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ReportProject.Models.JsonModels.MonitorStaffErrorJson;

namespace ReportProject.Respository.Interface
{
    public interface IMonitorStaffErrorRepo
    {
        Task<IEnumerable<MonitorStaffErrorReal>> GetByDateListSalon(IEnumerable<int?> salonIds, DateTime fromDate, DateTime toDate);
    }
}
