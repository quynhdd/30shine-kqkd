﻿using ReportProject.Models.JsonModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Interface
{
    public interface IStoreProcedureRepo
    {
        Task<IEnumerable<CustomerJson>> GetCustomerInfo(DateTime dateFrom, DateTime dateTo, int salonId, int regionId);
        Task<IEnumerable<ShineMemberJson>> GetMemberShipInfo(DateTime dateFrom, DateTime dateTo, int salonId, int regionId);
        Task<IEnumerable<ProductivityJson>> GetProductivityInfo(DateTime dateFrom, DateTime dateTo, int salonId, int regionId);
        Task<IEnumerable<QualityJson>> GetQualityInfo(DateTime dateFrom, DateTime dateTo, int salonId, int regionId);
        Task<IEnumerable<RevenueJson>> GetRevenueInfo(DateTime dateFrom, DateTime dateTo, int salonId, int regionId);
        Task<IEnumerable<LNGJson>> GetLNGInfo(DateTime dateFrom, DateTime dateTo, int salonId, int regionId, List<int> salonIds);
        Task<IEnumerable<TotalIncomeJson>> GetTotalIncome(DateTime dateFrom, DateTime dateTo, int salonId, int regionId);

    }
}
