﻿using ReportProject.Models.Solution_30shine_Real;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Respository.Interface
{
    public interface IFlowServiceRepo
    {
        Task<IEnumerable<FlowService>> GetByDate(int salonId, DateTime dateFrom, DateTime dateTo);
        Task<IEnumerable<FlowService>> GetByDateListSalon(IEnumerable<int?> salonIds, DateTime dateFrom, DateTime dateTo);
    }
}
