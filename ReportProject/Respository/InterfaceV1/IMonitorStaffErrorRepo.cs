﻿using ReportProject.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APICheckout.Models.CustomeModel.OutPutModel;

namespace ReportProject.Respository.InterfaceV1
{
    public interface IMonitorStaffErrorRepo
    {
        void Add(MonitorStaffError obj);
        void AddRange(List<MonitorStaffError> list);
        void Update(MonitorStaffError obj);
        void UpdateRange(List<MonitorStaffError> list);
        Task<MonitorStaffError> Get(Expression<Func<MonitorStaffError, bool>> expression);
        Task<List<MonitorStaffError>> GetList(Expression<Func<MonitorStaffError, bool>> expression);
        Task<List<OutSalonStaffError>> GetListSalonError(DateTime fromDate, DateTime toDate, List<int> listSalonId);
    }
}
