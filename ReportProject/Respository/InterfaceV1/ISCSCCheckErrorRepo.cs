﻿using ReportProject.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APICheckout.Models.CustomeModel.OutPutModel;

namespace ReportProject.Respository.InterfaceV1
{
    public interface ISCSCCheckErrorRepo
    {
        void Add(ScscCheckError obj);
        void AddRange(List<ScscCheckError> list);
        void Update(ScscCheckError obj);
        void UpdateRange(List<ScscCheckError> list);
        Task<ScscCheckError> Get(Expression<Func<ScscCheckError, bool>> expression);
        Task<List<ScscCheckError>> GetList(Expression<Func<ScscCheckError, bool>> expression);
        Task<List<OutStatisticScsc>> GetListBillScsc(DateTime fromDate, DateTime toDate);
    }
}
