﻿using ReportProject.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.InterfaceV1
{
    public interface IScscCategoryRepo
    {
        void Add(ScscCategory obj);
        void AddRange(List<ScscCategory> list);
        void Update(ScscCategory obj);
        void UpdateRange(List<ScscCategory> list);
        Task<ScscCategory> Get(Expression<Func<ScscCategory, bool>> expression);
        Task<List<ScscCategory>> GetList(Expression<Func<ScscCategory, bool>> expression);
    }
}
