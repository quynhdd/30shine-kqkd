﻿using ReportProject.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.InterfaceV1
{
    public interface IStaffRepo
    {
        void Add(Staff obj);
        void AddRange(List<Staff> list);
        void Update(Staff obj);
        void UpdateRange(List<Staff> list);
        Task<Staff> Get(Expression<Func<Staff, bool>> expression);
        Task<List<Staff>> GetList(Expression<Func<Staff, bool>> expression);
    }
}
