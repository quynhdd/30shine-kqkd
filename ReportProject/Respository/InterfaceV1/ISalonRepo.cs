﻿using ReportProject.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.InterfaceV1
{
    public interface ISalonRepo
    {
        void Add(TblSalon obj);
        void AddRange(List<TblSalon> list);
        void Update(TblSalon obj);
        void UpdateRange(List<TblSalon> list);
        Task<TblSalon> Get(Expression<Func<TblSalon, bool>> expression);
        Task<List<TblSalon>> GetList(Expression<Func<TblSalon, bool>> expression);
        Task<List<int>> GetList(int regionId);
    }
}
