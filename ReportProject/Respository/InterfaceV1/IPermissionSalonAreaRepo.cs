﻿using ReportProject.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.InterfaceV1
{
    public interface IPermissionSalonAreaRepo
    {
        void Add(PermissionSalonArea obj);
        void AddRange(List<PermissionSalonArea> list);
        void Update(PermissionSalonArea obj);
        void UpdateRange(List<PermissionSalonArea> list);
        Task SaveChangeAsync();
        void Remove(PermissionSalonArea obj);
        Task<PermissionSalonArea> Get(Expression<Func<PermissionSalonArea, bool>> expression);
        Task<List<PermissionSalonArea>> GetList(Expression<Func<PermissionSalonArea, bool>> expression);
    }
}
