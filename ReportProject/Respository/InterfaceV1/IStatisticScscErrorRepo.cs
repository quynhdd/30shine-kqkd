﻿using ReportProject.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.InterfaceV1
{
    public interface IStatisticScscErrorRepo
    {
        void Add(StatisticScscError obj);
        void AddRange(List<StatisticScscError> list);
        void Update(StatisticScscError obj);
        void UpdateRange(List<StatisticScscError> list);
        Task SaveChangeAsync();
        void Remove(StatisticScscError obj);
        Task<StatisticScscError> Get(Expression<Func<StatisticScscError, bool>> expression);
        Task<List<StatisticScscError>> GetList(Expression<Func<StatisticScscError, bool>> expression);
    }
}
