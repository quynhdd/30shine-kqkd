﻿using ReportProject.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.InterfaceV1
{
    public interface IOperationReportStatisticRepo
    {
        void Add(OperationReportStatistic obj);
        void AddRange(List<OperationReportStatistic> list);
        void Update(OperationReportStatistic obj);
        void UpdateRange(List<OperationReportStatistic> list);
        Task<OperationReportStatistic> Get(Expression<Func<OperationReportStatistic, bool>> expression);
        Task<List<OperationReportStatistic>> GetList(Expression<Func<OperationReportStatistic, bool>> expression);
    }
}
