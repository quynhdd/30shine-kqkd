﻿using ReportProject.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.InterfaceV1
{
    public interface IFeedbackPointServiceRepo
    {
        void Add(FeedbackPointService obj);
        void AddRange(List<FeedbackPointService> list);
        void Update(FeedbackPointService obj);
        void UpdateRange(List<FeedbackPointService> list);
        Task<FeedbackPointService> Get(Expression<Func<FeedbackPointService, bool>> expression);
        Task<List<FeedbackPointService>> GetList(Expression<Func<FeedbackPointService, bool>> expression);
        Task SaveChangeAsync();
    }
}
