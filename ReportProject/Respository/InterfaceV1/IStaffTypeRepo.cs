﻿using ReportProject.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.InterfaceV1
{
    public interface IStaffTypeRepo
    {
        void Add(StaffType obj);
        void AddRange(List<StaffType> list);
        void Update(StaffType obj);
        void UpdateRange(List<StaffType> list);
        Task SaveChangeAsync();
        void Remove(StaffType obj);
        Task<StaffType> Get(Expression<Func<StaffType, bool>> expression);
        Task<List<StaffType>> GetList(Expression<Func<StaffType, bool>> expression);
    }
}
