﻿using ReportProject.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APICheckout.Models.CustomeModel.OutPutModel;

namespace ReportProject.Respository.InterfaceV1
{
    public interface IBillserviceRepo
    {
        void Add(BillServiceHis obj);
        void AddRange(List<BillServiceHis> list);
        void Update(BillServiceHis obj);
        void UpdateRange(List<BillServiceHis> list);
        Task<BillServiceHis> Get(Expression<Func<BillServiceHis, bool>> expression);
        Task<List<BillServiceHis>> GetList(Expression<Func<BillServiceHis, bool>> expression);
        Task<List<OutStarBill>> GetListStaffBill(DateTime fromDate, DateTime toDate, List<int> listUonId, List<int> listSalonId);
        Task<List<OutWaitLongTime>> GetListLongTime(DateTime fromDate, DateTime toDate, List<int> listSalonId);

    }
}
