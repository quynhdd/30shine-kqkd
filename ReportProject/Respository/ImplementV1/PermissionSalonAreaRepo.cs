﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30Shine;
using ReportProject.Respository.InterfaceV1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.ImplementV1
{
    public class PermissionSalonAreaRepo:IPermissionSalonAreaRepo
    {
        private readonly Models.Solution_30Shine.Solution_30shineContextV1 dbContext;
        private readonly DbSet<PermissionSalonArea> dbSet;

        public PermissionSalonAreaRepo(Solution_30shineContextV1 dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<PermissionSalonArea>();
        }
        public void Add(PermissionSalonArea obj)
        {
            try
            {
                dbSet.Add(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void AddRange(List<PermissionSalonArea> list)
        {
            try
            {
                dbSet.AddRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void Update(PermissionSalonArea obj)
        {
            try
            {
                dbSet.Update(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void UpdateRange(List<PermissionSalonArea> list)
        {
            try
            {
                dbSet.UpdateRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task SaveChangeAsync()
        {
            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public void Remove(PermissionSalonArea obj)
        {
            try
            {
                dbSet.Remove(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<PermissionSalonArea> Get(Expression<Func<PermissionSalonArea, bool>> expression)
        {
            try
            {
                var data = await dbSet.FirstOrDefaultAsync(expression);
                return data;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public async Task<List<PermissionSalonArea>> GetList(Expression<Func<PermissionSalonArea, bool>> expression)
        {
            try
            {
                var data = await dbSet.Where(expression).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
