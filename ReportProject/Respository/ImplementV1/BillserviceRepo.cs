﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30Shine;
using ReportProject.Respository.InterfaceV1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APICheckout.Models.CustomeModel.OutPutModel;

namespace ReportProject.Respository.ImplementV1
{
    public class BillserviceRepo : IBillserviceRepo
    {
        private readonly Solution_30shineContextV1 dbContext;
        private readonly DbSet<BillServiceHis> dbSet;

        public BillserviceRepo(Solution_30shineContextV1 dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<BillServiceHis>();
        }

        public void Add(BillServiceHis obj)
        {
            try
            {
                dbSet.Add(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void AddRange(List<BillServiceHis> list)
        {
            try
            {
                dbSet.AddRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Update(BillServiceHis obj)
        {
            try
            {
                dbSet.Update(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void UpdateRange(List<BillServiceHis> list)
        {
            try
            {
                dbSet.UpdateRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<BillServiceHis> Get(Expression<Func<BillServiceHis, bool>> expression)
        {
            try
            {
                var data = await dbSet.FirstOrDefaultAsync(expression);
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<BillServiceHis>> GetList(Expression<Func<BillServiceHis, bool>> expression)
        {
            try
            {
                var data = await dbSet.Where(expression).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<OutStarBill>> GetListStaffBill(DateTime fromDate, DateTime toDate, List<int> listUonId, List<int> listSalonId)
        {
            try
            {
                var sql = $@"DECLARE 
		                    @timeFrom DATE,
		                    @timeTo DATE,
		                    @strSalonId VARCHAR(1000),
		                    @strUonId VARCHAR(20)
		                    SET @timeFrom = '{string.Format("{0:yyyy-MM-dd}", fromDate)}'
		                    SET @timeTo = '{string.Format("{0:yyyy-MM-dd}", toDate)}'
		                    SET @strSalonId = '{string.Join(",", listSalonId)}'
		                    SET @strUonId = '{string.Join(",", listUonId)}'
		                    BEGIN
			                    WITH tempBill AS (
				                    SELECT a.Id AS BillId,a.SalonId,a.Mark AS Star,a.IsImages,b.ShortName AS SalonShortName,b.[Order],
				                    CASE WHEN c.ServiceId > 0 THEN 1 ELSE NULL END AS ServiceIdIsCurling,
				                    CASE WHEN d.ServiceId > 0 THEN 1 ELSE NULL END AS ServiceIDIsShineCombo
				                    FROM dbo.BillServiceHis AS a
				                    INNER JOIN dbo.Tbl_Salon AS b ON a.SalonId = b.Id
				                    LEFT JOIN dbo.FlowService AS c ON a.Id = c.BillId AND c.IsDelete = 0 AND  EXISTS (SELECT Item FROM dbo.SplitString(@strUonId,',') WHERE c.ServiceId = CAST(Item AS INT))
				                    LEFT JOIN dbo.FlowService AS d ON a.Id = d.BillId AND d.IsDelete = 0 AND d.ServiceId = 53
				                    WHERE a.IsDelete = 0 AND b.IsDelete = 0 AND a.Pending = 0 AND LEN(a.ServiceIds) > 0 AND  
				                    b.IsSalonHoiQuan = 0 AND a.CompleteBillTime BETWEEN @timeFrom AND @timeTo AND 
				                    EXISTS (SELECT * FROM dbo.SplitString(@strSalonId,',') WHERE a.SalonId = CAST(Item AS INT)) 
				
				                    GROUP BY a.Id,a.SalonId,a.IsImages,a.Mark,b.ShortName,b.[Order],
				                    CASE WHEN c.ServiceId > 0 THEN 1 ELSE NULL END,
				                    CASE WHEN d.ServiceId > 0 THEN 1 ELSE NULL END
			                    ) 
			
			                    SELECT a.*,b.PointError,b.TotalPointSCSCCurling,b.ImageError,b.ImageErrorCurling,
                                CASE WHEN a.ServiceIdIsCurling = 1 AND LEN(e.ImageBefore) > 0 AND LEN(e.ImageAfter) > 0 THEN 1 ELSE NULL END AS IsImageCurling
			                    FROM tempBill AS a
			                    LEFT JOIN dbo.SCSC_CheckError AS b ON a.BillId = b.BillService_ID AND b.IsDelete = 0
			                    LEFT JOIN dbo.ImageData AS e ON a.BillId = e.OBJId AND e.Slugkey = 'image_bill_curling' AND e.IsDelete = 0
		                    END";
                var data = new List<OutStarBill>();
                var command = dbContext.Database.GetDbConnection().CreateCommand();
                command.CommandText = sql;
                dbContext.Database.OpenConnection();
                var reader = command.ExecuteReader();
                int integer = 0;
                bool bol = false;
                TimeSpan time = new TimeSpan();
                DateTime date = new DateTime();
                while (reader.Read())
                {
                    var record = new OutStarBill();
                    record.BillId = int.TryParse(reader["BillId"].ToString(), out integer) ? integer : 0;
                    record.SalonId = int.TryParse(reader["SalonId"].ToString(), out integer) ? integer : 0;
                    record.Star = int.TryParse(reader["Star"].ToString(), out integer) ? integer : 0;
                    record.ServiceIdIsCurling = int.TryParse(reader["ServiceIdIsCurling"].ToString(), out integer) ? integer : 0;
                    record.ServiceIDIsShineCombo = int.TryParse(reader["ServiceIDIsShineCombo"].ToString(), out integer) ? integer : 0;
                    if (string.IsNullOrEmpty(reader["PointError"].ToString()))
                    {
                        record.PointError = null;
                    }
                    else
                    {
                        record.PointError = int.TryParse(reader["PointError"].ToString(), out integer) ? integer : 0;

                    }
                    if (string.IsNullOrEmpty(reader["TotalPointSCSCCurling"].ToString()))
                    {
                        record.TotalPointSCSCCurling = null;
                    }
                    else
                    {
                        record.TotalPointSCSCCurling = int.TryParse(reader["TotalPointSCSCCurling"].ToString(), out integer) ? integer : 0;

                    }
                    record.IsImages = bool.TryParse(reader["IsImages"].ToString(), out bol) ? bol : false;
                    record.ImageErrorCurling = bool.TryParse(reader["ImageErrorCurling"].ToString(), out bol) ? bol : false;
                    record.ImageError = bool.TryParse(reader["ImageError"].ToString(), out bol) ? bol : false;
                    record.IsImageCurling = int.TryParse(reader["IsImageCurling"].ToString(), out integer) ? integer : 0;
                    record.SalonShortName = reader["SalonShortName"].ToString();
                    record.Order = int.TryParse(reader["Order"].ToString(), out integer) ? integer : 0;
                    data.Add(record);
                }

                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<OutWaitLongTime>> GetListLongTime(DateTime fromDate, DateTime toDate, List<int> listSalonId)
        {
            try
            {
                var list = dbContext.OperationReportStatistic.Where(r => r.Date >= fromDate && r.Date <= toDate && listSalonId.ToArray().Contains(r.SalonId))
                    .GroupBy(r => r.SalonId).Select(r => new OutWaitLongTime
                    {
                        SalonId = r.Key,
                        TotalLongTime = r.Select(g=>g.LongWait).Sum()
                    }).ToList();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
