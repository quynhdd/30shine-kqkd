﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30Shine;
using ReportProject.Respository.InterfaceV1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APICheckout.Models.CustomeModel.OutPutModel;

namespace ReportProject.Respository.ImplementV1
{
    public class MonitorStaffErrorRepo : IMonitorStaffErrorRepo
    {
        private readonly Models.Solution_30Shine.Solution_30shineContextV1 dbContext;
        private readonly DbSet<MonitorStaffError> dbSet;

        public MonitorStaffErrorRepo(Solution_30shineContextV1 dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<MonitorStaffError>();
        }
        public void Add(MonitorStaffError obj)
        {
            try
            {
                dbSet.Add(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void AddRange(List<MonitorStaffError> list)
        {
            try
            {
                dbSet.AddRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void Update(MonitorStaffError obj)
        {
            try
            {
                dbSet.Update(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void UpdateRange(List<MonitorStaffError> list)
        {
            try
            {
                dbSet.UpdateRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<MonitorStaffError> Get(Expression<Func<MonitorStaffError, bool>> expression)
        {
            try
            {
                var data = await dbSet.FirstOrDefaultAsync(expression);
                return data;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public async Task<List<MonitorStaffError>> GetList(Expression<Func<MonitorStaffError, bool>> expression)
        {
            try
            {
                var data = await dbSet.Where(expression).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<List<OutSalonStaffError>> GetListSalonError(DateTime fromDate, DateTime toDate, List<int> listSalonId)
        {
            try
            {
                var listErrorAndImportant = new List<OutSalonStaffError>();
                var listTotal = dbSet.Where(r => r.IsDelete == false && r.ErrorTime >= fromDate && r.ErrorTime <= toDate && listSalonId.ToArray().Contains(r.SalonId ?? 0)).ToList();
                if (listTotal.Any())
                {
                    // Trường hợp có lỗi 
                    listErrorAndImportant = listTotal.Where(r => r.StatusError == 4 || r.StatusError == 10)
                             .GroupBy(r => r.SalonId).Select(r => new OutSalonStaffError
                             {
                                 SalonId = r.Key ?? 0,
                                 TotalError = r.Count()
                             }).ToList();
                    //// trường hợp là nhắc nhở
                    //var listReminder = listTotal.Where(r => r.StatusError == 1).GroupBy(r => new { r.CategoryRootId, r.SalonId }).Select(r => new
                    //{
                    //    SalonId = r.Key.SalonId,
                    //    CategoryRootId = r.Key.CategoryRootId,
                    //    TotalReminder = (int?)Math.Round((double)r.Count() / 3)
                    //}).ToList();
                    //// tinh loi: 1 CategoryRootId/3 làm tròn = 1 thì là 1 lỗi
                    //if (listReminder.Any())
                    //{
                    //    var listErrorReminder = listReminder.GroupBy(r => r.SalonId).Select(r => new OutSalonStaffError
                    //    {
                    //        SalonId = r.Key,
                    //        TotalError = r.Select(g=>g.TotalReminder).Sum()
                    //    }).ToList();
                    //    list = listErrorAndImportant.Union(listErrorReminder).GroupBy(r => r.SalonId).Select(r => new OutSalonStaffError
                    //    {
                    //        SalonId = r.Key,
                    //        TotalError = r.Select(g => g.TotalError).Sum()
                    //    }).ToList();
                    //}
                    //else
                    //{
                    //    list = listErrorAndImportant;
                    //}
                }
                return listErrorAndImportant;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
