﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30Shine;
using ReportProject.Respository.InterfaceV1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.ImplementV1
{
    public class StaffRepo:IStaffRepo
    {
        private readonly Solution_30shineContextV1 dbContext;
        private readonly DbSet<Staff> dbSet;

        public StaffRepo(Solution_30shineContextV1 dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<Staff>();
        }
        public void Add(Staff obj)
        {
            try
            {
                dbSet.Add(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void AddRange(List<Staff> list)
        {
            try
            {
                dbSet.AddRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void Update(Staff obj)
        {
            try
            {
                dbSet.Update(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void UpdateRange(List<Staff> list)
        {
            try
            {
                dbSet.UpdateRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<Staff> Get(Expression<Func<Staff,bool>> expression)
        {
            try
            {
                var data = await dbSet.FirstOrDefaultAsync(expression);
                return data;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public async Task<List<Staff>> GetList(Expression<Func<Staff,bool>> expression)
        {
            try
            {
                var data = await dbSet.Where(expression).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
