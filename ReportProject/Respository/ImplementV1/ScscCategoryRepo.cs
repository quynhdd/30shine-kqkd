﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30Shine;
using ReportProject.Respository.InterfaceV1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.ImplementV1
{
    public class ScscCategoryRepo:IScscCategoryRepo
    {
        private readonly Solution_30shineContextV1 dbContext;
        private readonly DbSet<ScscCategory> dbSet;

        public ScscCategoryRepo(Solution_30shineContextV1 dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<ScscCategory>();
        }
        public void Add(ScscCategory obj)
        {
            try
            {
                dbSet.Add(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void AddRange(List<ScscCategory> list)
        {
            try
            {
                dbSet.AddRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void Update(ScscCategory obj)
        {
            try
            {
                dbSet.Update(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void UpdateRange(List<ScscCategory> list)
        {
            try
            {
                dbSet.UpdateRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<ScscCategory> Get(Expression<Func<ScscCategory,bool>> expression)
        {
            try
            {
                var data = await dbSet.FirstOrDefaultAsync(expression);
                return data;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public async Task<List<ScscCategory>> GetList(Expression<Func<ScscCategory,bool>> expression)
        {
            try
            {
                var data = await dbSet.Where(expression).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
