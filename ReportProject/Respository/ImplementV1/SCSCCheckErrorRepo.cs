﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30Shine;
using ReportProject.Respository.InterfaceV1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APICheckout.Models.CustomeModel.OutPutModel;

namespace ReportProject.Respository.ImplementV1
{
    public class SCSCCheckErrorRepo : ISCSCCheckErrorRepo
    {
        private readonly Solution_30shineContextV1 dbContext;
        private readonly DbSet<ScscCheckError> dbSet;

        public SCSCCheckErrorRepo(Solution_30shineContextV1 dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<ScscCheckError>();
        }
        public void Add(ScscCheckError obj)
        {
            try
            {
                dbSet.Add(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void AddRange(List<ScscCheckError> list)
        {
            try
            {
                dbSet.AddRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void Update(ScscCheckError obj)
        {
            try
            {
                dbSet.Update(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void UpdateRange(List<ScscCheckError> list)
        {
            try
            {
                dbSet.UpdateRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<ScscCheckError> Get(Expression<Func<ScscCheckError, bool>> expression)
        {
            try
            {
                var data = await dbSet.FirstOrDefaultAsync(expression);
                return data;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public async Task<List<ScscCheckError>> GetList(Expression<Func<ScscCheckError, bool>> expression)
        {
            try
            {
                var data = await dbSet.Where(expression).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<List<OutStatisticScsc>> GetListBillScsc(DateTime fromDate, DateTime toDate)
        {
            try
            {
                var sql = $@"DECLARE 
	                        @TimeFrom DATETIME ,
	                        @TimeTo DATETIME 
	                        SET @TimeFrom = '{string.Format("{0:yyyy-MM-dd}", fromDate)}'
	                        SET @TimeTo = '{string.Format("{0:yyyy-MM-dd}", toDate)}'
	                         BEGIN
	                           WITH 
		                        BillTem AS (
			                       SELECT bill.Id AS BillId, bill.IsImages , 
				                        bill.Staff_Hairdresser_Id  AS StylistId, bill.SalonId,  c.ImageError, 
										c.Shape_ID AS ShapeId, c.ConnectTive_ID AS ConnectTiveId, 
				                        c.SharpNess_ID AS SharpNessId, c.ComPlatetion_ID AS ComPlatetionId, 
										c.PointError, c.HairTip_ID AS HairTipId, c.HairRoot_ID AS HairRootId, 
										c.HairWaves_ID AS HairWavesId, c.TotalPointSCSCCurling,
				                        (CASE WHEN fl.ServiceId = 16 THEN CAST( 1 AS BIT) ELSE CAST(0 AS BIT ) END ) AS IsUon ,c.ImageErrorCurling
				                        ,(CASE WHEN LEN(imgd.ImageBefore) > 0 AND LEN(imgd.ImageAfter) > 0 THEN CAST( 1 AS BIT)  ELSE CAST(0 AS BIT ) END)  AS isImgUon
			                        FROM dbo.BillService AS bill
			                        LEFT JOIN dbo.ImageData AS imgd ON bill.Id = imgd.OBJId AND imgd.Slugkey = 'image_bill_curling'
			                        LEFT JOIN dbo.FlowService AS fl ON bill.Id = fl.BillId AND fl.ServiceId = 16 AND fl.IsDelete = 0
			                        LEFT JOIN dbo.SCSC_CheckError AS c ON c.BillService_ID = bill.Id  AND c.IsDelete = 0 
			                        WHERE  bill.IsDelete = 0 AND bill.Pending = 0 AND
									bill.CreatedDate BETWEEN @TimeFrom AND @TimeTo AND LEN(bill.ServiceIds ) > 0
		                        )

								SELECT * FROM BillTem
							END";
                var command = dbContext.Database.GetDbConnection().CreateCommand();
                command.CommandText = sql;
                dbContext.Database.OpenConnection();
                var reader = command.ExecuteReader();
                var list = new List<OutStatisticScsc>();
                while (reader.Read())
                {
                    var record = new OutStatisticScsc();
                    bool bol = false;
                    record.BillId = int.TryParse(reader["BillId"].ToString(), out var integer) ? integer : 0;
                    record.ComplatetionId = int.TryParse(reader["ComPlatetionId"].ToString(), out integer) ? integer : 0;
                    record.ConnectTiveId = int.TryParse(reader["ConnectTiveId"].ToString(), out integer) ? integer : 0;
                    record.HairRootId = int.TryParse(reader["HairRootId"].ToString(), out integer) ? integer : 0;
                    record.HairTipId = int.TryParse(reader["HairTipId"].ToString(), out integer) ? integer : 0;
                    record.HairWavesId = int.TryParse(reader["HairWavesId"].ToString(), out integer) ? integer : 0;
                    if (reader["ImageError"].ToString() == "" || reader["ImageError"] == null)
                    {
                        record.ImageError = null;
                    }
                    else
                    {
                        record.ImageError = bool.TryParse(reader["ImageError"].ToString(), out bol) ? bol : false;
                    }

                    if (reader["ImageErrorCurling"].ToString() == "" || reader["ImageErrorCurling"] == null)
                    {
                        record.ImageErrorCurling = null;
                    }
                    else
                    {
                        record.ImageErrorCurling = bool.TryParse(reader["ImageErrorCurling"].ToString(), out bol) ? bol : false;
                    }
                    if (reader["IsImages"] == null || reader["IsImages"].ToString() == "")
                    {
                        record.IsImages = null;
                    }
                    else
                    {
                        record.IsImages = bool.TryParse(reader["IsImages"].ToString(), out bol) ? bol : false;
                    }

                    if(reader["isImgUon"] == null || reader["isImgUon"].ToString() == "")
                    {
                        record.IsImgUon = null;
                    }
                    else
                    {
                        record.IsImgUon = bool.TryParse(reader["isImgUon"].ToString(), out bol) ? bol : false;
                    }

                    if(reader["IsUon"] == null || reader["IsUon"].ToString() == "")
                    {
                         record.IsUon = null;
                    }
                    else
                    {
                        record.IsUon = bool.TryParse(reader["IsUon"].ToString(), out bol) ? bol : false;
                    }

                    if(reader["PointError"] == null || reader["PointError"].ToString() == "")
                    {
                         record.PointError = null;
                    }
                    else
                    {
                        record.PointError = int.TryParse(reader["PointError"].ToString(), out integer) ? integer : 0;
                    }


                    record.SalonId = int.TryParse(reader["SalonId"].ToString(), out integer) ? integer : 0;
                    record.ShapeId = int.TryParse(reader["ShapeId"].ToString(), out integer) ? integer : 0;
                    record.SharpNessId = int.TryParse(reader["SharpNessId"].ToString(), out integer) ? integer : 0;
                    record.StylistId = int.TryParse(reader["StylistId"].ToString(), out integer) ? integer : 0;

                    if(reader["TotalPointSCSCCurling"] == null || reader["TotalPointSCSCCurling"].ToString() == "")
                    {
                         record.TotalPointSCSCCurling = null;
                    }
                    else
                    {
                        record.TotalPointSCSCCurling = int.TryParse(reader["TotalPointSCSCCurling"].ToString(), out integer) ? integer : 0;
                    }
                    list.Add(record);
                }
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
