﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30Shine;
using ReportProject.Respository.InterfaceV1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.ImplementV1
{
    public class OperationReportStatisticRepo:IOperationReportStatisticRepo
    {
        private readonly Solution_30shineContextV1 dbContext;
        private readonly DbSet<OperationReportStatistic> dbSet;

        public OperationReportStatisticRepo(Solution_30shineContextV1 dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<OperationReportStatistic>();
        }
        public void Add(OperationReportStatistic obj)
        {
            try
            {
                dbSet.Add(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void AddRange(List<OperationReportStatistic> list)
        {
            try
            {
                dbSet.AddRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void Update(OperationReportStatistic obj)
        {
            try
            {
                dbSet.Update(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void UpdateRange(List<OperationReportStatistic> list)
        {
            try
            {
                dbSet.UpdateRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<OperationReportStatistic> Get(Expression<Func<OperationReportStatistic,bool>> expression)
        {
            try
            {
                var data = await dbSet.FirstOrDefaultAsync(expression);
                return data;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public async Task<List<OperationReportStatistic>> GetList(Expression<Func<OperationReportStatistic,bool>> expression)
        {
            try
            {
                var data = await dbSet.Where(expression).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task SaveChangeAsync()
        {
            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
