﻿using Microsoft.EntityFrameworkCore;
using ReportProject.Models.Solution_30Shine;
using ReportProject.Respository.InterfaceV1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReportProject.Respository.ImplementV1
{
    public class SalonRepo:ISalonRepo
    {
        private readonly Solution_30shineContextV1 dbContext;
        private readonly DbSet<TblSalon> dbSet;

        public SalonRepo(Solution_30shineContextV1 dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<TblSalon>();
        }
        public void Add(TblSalon obj)
        {
            try
            {
                dbSet.Add(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void AddRange(List<TblSalon> list)
        {
            try
            {
                dbSet.AddRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void Update(TblSalon obj)
        {
            try
            {
                dbSet.Update(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void UpdateRange(List<TblSalon> list)
        {
            try
            {
                dbSet.UpdateRange(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<TblSalon> Get(Expression<Func<TblSalon,bool>> expression)
        {
            try
            {
                var data = await dbSet.FirstOrDefaultAsync(expression);
                return data;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public async Task<List<TblSalon>> GetList(Expression<Func<TblSalon,bool>> expression)
        {
            try
            {
                var data = await dbSet.Where(expression).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<List<int>> GetList(int regionId)
        {
            try
            {
                var data = await dbContext.PermissionSalonArea.Where(r=>r.StaffId == regionId && r.IsDelete == false).Select(r=>r.SalonId ??0).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
