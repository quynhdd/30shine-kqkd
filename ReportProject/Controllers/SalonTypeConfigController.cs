﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using ReportProject.Utils;
using static ReportProject.Models.JsonModels.SalonTypeConfigJson;

namespace ReportProject.Controllers
{
    [Produces("application/json")]
    [Route("api/salon-type-config")]
    public class SalonTypeConfigController : Controller
    {
        private ILogger<SalonTypeConfigController> Logger { get; }
        private ISalonTypeConfigRepo SalonTypeConfigRepo { get; set; }
        // constructor
        public SalonTypeConfigController(ILogger<SalonTypeConfigController> logger,
                                          ISalonTypeConfigRepo salonTypeConfigRepo)
        {
            Logger = logger;
            SalonTypeConfigRepo = salonTypeConfigRepo;
        }

        /// <summary>
        /// Insert Or update
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        ///
        ///       {
        ///         "userId": 1,
        ///         "data": [
        ///           {
        ///             "salonId": 2,
        ///             "salonType": 1
        ///           },
        ///           {
        ///             "salonId": 3,
        ///             "salonType": 2
        ///           }
        ///         ]
        ///      }
        ///
        /// </remarks>
        [HttpPost]
        public async Task<IActionResult> InsertOrUpdate([FromBody] InputSalonTypeConfigJson input)
        {
            try
            {
                if (input == null)
                {
                    return BadRequest(new { message = "Kiểm tra lại file excel!" });
                }
                if (input.UserId < 0)
                {
                    return BadRequest(new { message = "Check user login!" });
                }
                if (input.data.Count <= 0)
                {
                    return BadRequest(new { message = "Data does not exist!" });
                }
                if (input.data.Select(s => s.SalonId).Distinct().Count() < input.data.Select(s => s.SalonId).Count())
                {
                    return BadRequest(new { message = "Kiểm tra lại file excel bị trùng salon!" });
                }
                if (input.data.Where(w => w.SalonId <= 0).Count() > 0)
                {
                    return BadRequest(new { message = "Kiểm tra lại file excel SalonId <= 0" });
                }
                if (input.data.Where(w => w.SalonType <= 0).Count() > 0)
                {
                    return BadRequest(new { message = "Kiểm tra lại file excel SalonType <= 0" });
                }
                //add
                foreach (var item in input.data)
                {
                    var checkExist = await SalonTypeConfigRepo.Get(a => a.SalonId == item.SalonId);
                    if (checkExist == null)
                    {
                        var obj = new SalonTypeConfig();
                        obj.SalonId = item.SalonId;
                        obj.SalonType = item.SalonType;
                        obj.UserId = input.UserId;
                        obj.CreatedTime = DateTime.Now;
                        obj.IsDelete = false;
                        SalonTypeConfigRepo.Add(obj);
                    }
                    else
                    {
                        checkExist.UserId = input.UserId;
                        checkExist.SalonType = item.SalonType;
                        checkExist.ModifiedTime = DateTime.Now;
                        SalonTypeConfigRepo.Update(checkExist);
                    }
                }
                // save change async
                await SalonTypeConfigRepo.SaveChangeAsync();
                //
                return Ok(new { message = "Success!" });
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}