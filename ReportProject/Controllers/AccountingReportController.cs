﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ReportProject.Respository.Interface;

namespace ReportProject.Controllers
{
    [Produces("application/json")]
    [Route("api/accounting-report")]
    public class AccountingReportController : Controller
    {
        private IAccountingReportRepo AccountingReportRepo;
        private ISalonRepo SalonRepo;
        private ISalonDailyCostRepo SalonDailyCostRepo;

        CultureInfo culture = new CultureInfo("vi-VN");

        public AccountingReportController(IAccountingReportRepo accountingReportRepo, ISalonRepo salonRepo, ISalonDailyCostRepo salonDailyCostRepo)
        {
            AccountingReportRepo = accountingReportRepo;
            SalonRepo = salonRepo;
            SalonDailyCostRepo = salonDailyCostRepo;
        }

        //[ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public async Task<IActionResult> GetReport([FromQuery] int salonId, string dateFrom, string dateTo)
        {
            try
            {
                var from = DateTime.ParseExact(dateFrom, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                var to = DateTime.ParseExact(dateTo, @"dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                var report = await AccountingReportRepo.GetAccountingReports(salonId, from, to);
                if (!report.Any())
                {
                    return NoContent();
                }
                return Ok(report);
            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }

        [HttpGet("accounting-tax")]
        public async Task<IActionResult> GetAccountingTaxReport([FromQuery] int salonId, int staffType ,string dateFrom, string dateTo)
        {
            try
            {
                if (staffType == 0)
                {
                    return BadRequest();
                }

                var from = DateTime.ParseExact(dateFrom, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                var to = DateTime.ParseExact(dateTo, @"dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);

                if (DateTime.Today < from || DateTime.Today < to)
                {
                    return BadRequest();
                }

                var report = await AccountingReportRepo.GetAccountingTaxReports(salonId, staffType,from, to);
                if (report == null)
                {
                    return NoContent();
                }
                return Ok(report);
            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}