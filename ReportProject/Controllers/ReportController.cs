﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReportProject.Utils;
using Newtonsoft.Json;
using ReportProject.Models.JsonModels;
using ReportProject.Respository.Interface;
using ReportProject.Models.Solution_30shine_Real;
using static ReportProject.Models.JsonModels.RankReportJson;

namespace ReportProject.Controllers
{
    [Produces("application/json")]
    [Route("api/report")]
    public class ReportController : Controller
    {
        private IReportRepo ReportRepo { get; set; }
        private ISalonRepo SalonRepo { get; set; }
        private ISalonDailyCostRepo SalonDailyCostRepo { get; set; }
        private IConfigRepo ConfigRepo { get; set; }
        private ILogger<ReportController> Logger { get; }
        CultureInfo culture = new CultureInfo("vi-VN");
        static HttpClient client = new HttpClient();

        public ReportController(IReportRepo reportRepo, ISalonRepo salonRepo, ISalonDailyCostRepo salonDailyCostRepo, IConfigRepo configRepo, ILogger<ReportController> logger)
        {
            ReportRepo = reportRepo;
            SalonRepo = salonRepo;
            SalonDailyCostRepo = salonDailyCostRepo;
            ConfigRepo = configRepo;
            Logger = logger;
        }

        /// <summary>
        /// Send mail chinh thuc
        /// </summary>
        /// <returns></returns>
        [HttpPost("send-email")]
        public async Task<IActionResult> SendMail()
        {
            try
            {
                var objRank = new RankReportBCKQKDJson();
                int priceCompare = Constant.PRICE_COMPARE;
                string str = "";
                // dateFrom = Convert.ToDateTime("03-02-2019", culture);
                var dateFrom = DateTime.Today;
                var report = ReportRepo.GetReportByDate(dateFrom, dateFrom);

                if (!report.Any())
                {
                    return NoContent();
                }
                var listSalon = report;
                int countListSalon = listSalon.Count();
                #region  html send mail
                // str table thead
                str = "<div>" +
                        "<h2 style =\"width:100%; text-align:center;\" > Báo cáo kết quả kinh doanh 30shine ngày " + DateTime.Today.ToString("dd/MM/yyyy") + " </h2>" +
                        "<table style =\" border: 1px solid black; margin: 0 auto; border-collapse: collapse; padding: 5px;\">" +
                        "<thead>" +
                        "<tr>" +
                         "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" rowspan=\"2\" > STT </th>" +
                        "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 90px;\" rowspan=\"2\"> Salon</th>" +
                        "<th style =\"font-size:14px; width: 120px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" rowspan=\"2\"> Tổng Lợi Nhuận Sau Thuế </th>" +
                        "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" rowspan=\"2\"> Lượt khách</th>" +
                        "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px; background-color :#ffe400;\" rowspan=\"2\" > Doanh thu thuần </th>" +
                        "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" colspan =\"4\" >Doanh Thu</th>" +
                        "<th style =\"font-size:14px; background-color :#ffe400; border: 1px solid black; border-collapse: collapse; padding: 5px;\" rowspan=\"2\"> Giá Vốn Trực Tiếp  </th>" +
                        "<th style =\"font-size:14px; background-color :#ffe400; border: 1px solid black; border-collapse: collapse; padding: 5px;\" rowspan=\"2\"> Chi Phí Salon" +
                        "</th>" +
                        "<th style =\"font-size:14px; background-color :#ffe400; border: 1px solid black; border-collapse: collapse; padding: 5px;\" rowspan=\"2\"> Chi Phí Quản Lý  </th>" +
                        "</tr>" +
                        "<tr>" +

                        "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > Tổng Doanh Thu </th>" +
                        "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > Dịch Vụ </th>" +
                        "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > Mỹ Phẩm </th>" +
                         "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > DT SHINE MB </th>" +
                        "</tr>" +
                        "</thead>" +
                        "<tbody>";
                // string html tinh trung binh
                str = str + "<tr>" +
                        "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" colspan = \"2\" >  TB/ Salon </ td>" +
                        "<td style =\" font-size:14px; font-weight: bold; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.totalIncomeAfterTax) / countListSalon))) + "  </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.numberOfTurns) / countListSalon))) + " </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.totalNetRevenue) / countListSalon))) + " </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.totalSales) / countListSalon))) + " </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.totalServiceProfit) / countListSalon))) + "  </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.totalProductProfit) / countListSalon))) + " </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.totalProductShineMember) / countListSalon))) + " </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.directFee) / countListSalon))) + " </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" >" + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.salonFee) / countListSalon))) + "  </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.manageFee) / countListSalon))) + " </td>" +
                        "</tr> ";
                // string html tinh tong
                str = str + "<tr>" +
                    "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" colspan = \"2\" > Toàn hệ thống </td>" +
                    "<td style =\" font-size:14px; font-weight: bold; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalIncomeAfterTax)) + "  </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.numberOfTurns)) + " </td>" +
                    // add 20190228
                    // Doanh thu thuan
                    "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalNetRevenue)) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalSales)) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalServiceProfit)) + "  </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalProductProfit)) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalProductShineMember)) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.directFee)) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.salonFee)) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.manageFee)) + " </td>" +
                    "</tr> ";

                int STT = 0;

                foreach (var obj in listSalon)
                {
                    objRank = returnRankReportBCKQKD((int)obj.totalIncomeAfterTax, priceCompare);
                    str = str + "<tr>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + ++STT + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 90px;\" > " + obj.salonName + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;  background-color : " + objRank.colorRank + "\"\" > " + FormatExtension.FormatNumber(obj.totalIncomeAfterTax) + "  </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.numberOfTurns) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.totalNetRevenue) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.totalSales) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.totalServiceProfit) + "  </td>" +
                    "<td style =\" font-size:14px; font-weight: normal; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.totalProductProfit) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.totalProductShineMember) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.directFee) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.salonFee) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.manageFee) + " </td>" +
                    "</tr> ";
                }

                str = str + "</tbody>" +
                    "</table>" +
                    "</div>";
                #endregion
                if (str == "")
                {
                    return NotFound(new { message = "Không có data!" });
                }
                var getEmail = ConfigRepo.GetEmail(Constant.EMAIL_KEY);
                if (getEmail == null)
                {
                    return NotFound(new { message = "Không có email" });
                }
                var listEmails = getEmail.Split(",").ToList();
                var objSendMail = new PostEmailJson
                {
                    to = listEmails,
                    subject = "[KQKD " + DateTime.Today.ToString("dd/MM") + "]"
                                        + " KH: " + FormatExtension.FormatNumber(listSalon.Sum(a => a.numberOfTurns)) + ""
                                        + " - DT: " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalNetRevenue)) + ""
                                        + " - LNST: " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalIncomeAfterTax)) + "",
                    message = str
                };

                var responseEmail = await client.PostAsync(Constant.APIPUSHNOTIC + Constant.uri_PUSH_NOTIC_EMAIL, new StringContent(JsonConvert.SerializeObject(objSendMail), Encoding.UTF8, "application/json"));

                if (!responseEmail.IsSuccessStatusCode)
                {
                    return BadRequest(new { message = "API Gửi Mail Có Lỗi!" });
                }

                return Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// send mail lan 1 cho ke toan
        /// </summary>
        /// <returns></returns>
        [HttpPost("pre-send-email")]
        public async Task<IActionResult> PreSendMail()
        {
            try
            {
                var objRank = new RankReportBCKQKDJson();
                int priceCompare = Constant.PRICE_COMPARE;
                string str = "";
                //var dateFrom = Convert.ToDateTime("19-03-2019", culture);
                var dateFrom = DateTime.Today;
                var report = ReportRepo.GetReportByDate(dateFrom, dateFrom);

                if (!report.Any())
                {
                    return NoContent();
                }
                var listSalon = report;
                int countListSalon = listSalon.Count();
                #region  html send mail
                // str table thead
                str = "<div>" +
                        "<h2 style =\"width:100%; text-align:center;\" > Báo cáo kết quả kinh doanh 30shine ngày " + DateTime.Today.ToString("dd/MM/yyyy") + " </h2>" +
                        "<table style =\" border: 1px solid black; margin: 0 auto; border-collapse: collapse; padding: 5px;\">" +
                        "<thead>" +
                        "<tr>" +
                         "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" rowspan=\"2\" > STT </th>" +
                        "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 90px;\" rowspan=\"2\"> Salon</th>" +
                        "<th style =\"font-size:14px; width: 120px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" rowspan=\"2\"> Tổng Lợi Nhuận Sau Thuế </th>" +
                        "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" rowspan=\"2\"> Lượt khách</th>" +
                        "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px; background-color :#ffe400;\" rowspan=\"2\" > Doanh thu thuần </th>" +
                        "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" colspan =\"4\" >Doanh Thu</th>" +
                        "<th style =\"font-size:14px; background-color :#ffe400; border: 1px solid black; border-collapse: collapse; padding: 5px;\" rowspan=\"2\"> Giá Vốn Trực Tiếp  </th>" +
                        "<th style =\"font-size:14px; background-color :#ffe400; border: 1px solid black; border-collapse: collapse; padding: 5px;\" rowspan=\"2\"> Chi Phí Salon" +
                        "</th>" +
                        "<th style =\"font-size:14px; background-color :#ffe400; border: 1px solid black; border-collapse: collapse; padding: 5px;\" rowspan=\"2\"> Chi Phí Quản Lý  </th>" +
                        "</tr>" +
                        "<tr>" +

                        "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > Tổng Doanh Thu </th>" +
                        "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > Dịch Vụ </th>" +
                        "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > Mỹ Phẩm </th>" +
                         "<th style =\"font-size:14px; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > DT SHINE MB </th>" +
                        "</tr>" +
                        "</thead>" +
                        "<tbody>";
                // string html tinh trung binh
                str = str + "<tr>" +
                        "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" colspan = \"2\" >  TB/ Salon </ td>" +
                        "<td style =\" font-size:14px; font-weight: bold; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.totalIncomeAfterTax) / countListSalon))) + "  </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.numberOfTurns) / countListSalon))) + " </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.totalNetRevenue) / countListSalon))) + " </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.totalSales) / countListSalon))) + " </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.totalServiceProfit) / countListSalon))) + "  </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.totalProductProfit) / countListSalon))) + " </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.totalProductShineMember) / countListSalon))) + " </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.directFee) / countListSalon))) + " </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" >" + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.salonFee) / countListSalon))) + "  </td>" +
                        "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber((int)Math.Round((decimal)(listSalon.Sum(a => a.manageFee) / countListSalon))) + " </td>" +
                        "</tr> ";
                // string html tinh tong
                str = str + "<tr>" +
                    "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" colspan = \"2\" > Toàn hệ thống </td>" +
                    "<td style =\" font-size:14px; font-weight: bold; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalIncomeAfterTax)) + "  </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.numberOfTurns)) + " </td>" +
                    // add 20190228
                    // Doanh thu thuan
                    "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalNetRevenue)) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalSales)) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalServiceProfit)) + "  </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalProductProfit)) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalProductShineMember)) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.directFee)) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.salonFee)) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: bold;   border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(listSalon.Sum(a => a.manageFee)) + " </td>" +
                    "</tr> ";

                int STT = 0;

                foreach (var obj in listSalon)
                {
                    objRank = returnRankReportBCKQKD((int)obj.totalIncomeAfterTax, priceCompare);
                    str = str + "<tr>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + ++STT + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 90px;\" > " + obj.salonName + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;  background-color : " + objRank.colorRank + "\"\" > " + FormatExtension.FormatNumber(obj.totalIncomeAfterTax) + "  </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.numberOfTurns) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.totalNetRevenue) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.totalSales) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.totalServiceProfit) + "  </td>" +
                    "<td style =\" font-size:14px; font-weight: normal; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.totalProductProfit) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal; border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.totalProductShineMember) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.directFee) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.salonFee) + " </td>" +
                    "<td style =\" font-size:14px; font-weight: normal;  border: 1px solid black; border-collapse: collapse; padding: 5px;\" > " + FormatExtension.FormatNumber(obj.manageFee) + " </td>" +
                    "</tr> ";
                }

                str = str + "</tbody>" +
                    "</table>" +
                    "</div>";
                #endregion
                if (str == "")
                {
                    return NotFound(new { message = "Không có data!" });
                }
                var getEmail = ConfigRepo.GetEmail(Constant.PRE_EMAIL_KEY);
                if (getEmail == null)
                {
                    return NotFound(new { message = "Không có email" });
                }
                var listEmails = getEmail.Split(",").ToList();
                //[KQKD 21/02 ] KH : 7,485 - DT: 1,295 tỷ - LNST: - 276 tr
                var objSendMail = new PostEmailJson
                {
                    to = listEmails,
                    subject = "[KQKD " + DateTime.Today.ToString("dd/MM") + "]"
                                        + " KH: " + FormatExtension.FormatNumber(listSalon.Sum(a => a.numberOfTurns)) + ""
                                        + " - DT: " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalNetRevenue)) + ""
                                        + " - LNST: " + FormatExtension.FormatNumber(listSalon.Sum(a => a.totalIncomeAfterTax)) + "",
                    message = str
                };

                var responseEmail = await client.PostAsync(Constant.APIPUSHNOTIC + Constant.uri_PUSH_NOTIC_EMAIL, new StringContent(JsonConvert.SerializeObject(objSendMail), Encoding.UTF8, "application/json"));

                if (!responseEmail.IsSuccessStatusCode)
                {
                    return BadRequest(new { message = "API Gửi Mail Có Lỗi!" });
                }

                return Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        ///  get bao cao KQKD ngay hom nay
        /// </summary>
        /// <returns></returns>
        [HttpGet("today")]
        public async Task<IActionResult> GetReport()
        {

            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var dateFrom = DateTime.Today;
                //var dateFrom = Convert.ToDateTime("03-05-2018", culture);
                var report = await ReportRepo.GetReport(dateFrom);
                var elapsedMs = watch.ElapsedMilliseconds;
                return Ok(report);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Get bao cao KQKD ngay khac
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        [HttpGet("other-day")]
        public async Task<IActionResult> GetReportOtherDay([FromQuery] string date)
        {

            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var dateFrom = DateTime.ParseExact(date, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                var report = await ReportRepo.GetReport(dateFrom);
                var elapsedMs = watch.ElapsedMilliseconds;
                return Ok(report);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Get data bao cao KQKD 
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetReportByDate([FromQuery] string from, string to)
        {
            try
            {
                Logger.LogInformation(from + "/" + to);

                if (DatetimeExtension.StringToDateTime(Constant.DATETIME, from) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME2, from) == null
                    || DatetimeExtension.StringToDateTime(Constant.DATETIME, to) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME2, to) == null)
                {
                    return BadRequest();
                }

                var dateFrom = DateTime.ParseExact(from, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                var dateTo = DateTime.ParseExact(to, @"dd/MM/yyyy", CultureInfo.InvariantCulture);

                var report = ReportRepo.GetReportByDate(dateFrom, dateTo);

                if (report == null)
                {
                    return NoContent();
                }

                return Ok(report);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }

        }

        /// <summary>
        ///  statistic bao cao kq kinh doanh hang dem
        /// </summary>
        /// <returns></returns>
        [HttpPost("static-expense")]
        public async Task<IActionResult> PostReport()
        {
            try
            {
                var dateFrom = DateTime.Today;
                //var dateFrom = Convert.ToDateTime("10-10-2018", culture);
                var report = await ReportRepo.GetReport(dateFrom);

                var salonIds = report.Select(a => a.salonId);

                var salons = SalonRepo.ValidateSalon(salonIds);

                if (salons == null)
                {
                    return NoContent();
                }

                var listData = new List<StaticExpense>();
                foreach (var data in report)
                {
                    var checkExsit = ReportRepo.GetBySalonIdAndDate(data.salonId, DateTime.Today);
                    if (checkExsit != null)
                    {
                        return BadRequest(new { message = "Đã có bản ghi" });
                    }

                    var dailyReport = new StaticExpense();
                    dailyReport.WorkDate = DateTime.Today;
                    dailyReport.SalonId = data.salonId;
                    dailyReport.TotalIncomeAfterTax = data.totalIncomeAfterTax;
                    dailyReport.NumberOfTurns = data.numberOfTurns;
                    dailyReport.TotalSales = data.totalSales;
                    dailyReport.TotalServiceProfit = data.totalServiceProfit;
                    dailyReport.TotalCosmeticProfit = data.totalProductProfit;
                    dailyReport.TotalTransactionPerCus = data.totalTransactionPerCus;
                    dailyReport.TotalServicePerCus = data.totalServicePerCus;
                    dailyReport.TotalProductPerCus = data.totalProductPerCus;
                    dailyReport.TotalOtherIncome = data.otherIncome;
                    dailyReport.DirectFee = data.directFee;
                    dailyReport.TotalStaffSalary = data.totalStaffSalary;
                    dailyReport.SalaryServicePerServiceIncome = data.salaryServicePerServiceIncome;
                    dailyReport.TotalProductCapital = data.totalProductCapital;
                    dailyReport.TotalProductPrice = data.totalProdutPrice;
                    dailyReport.TotalPayOffKcs = data.totalPayOffKCS;
                    dailyReport.TotalDailyCostInventory = data.totalDailyCostInventory;
                    dailyReport.ElectricityAndWaterBill = data.electricityAndWaterBill;
                    dailyReport.SalonFee = data.salonFee;
                    dailyReport.RentWithTax = data.rentWithTax;
                    dailyReport.CapitalSpending = data.capitalSpending;
                    dailyReport.Mktexpense = data.mktExpense;
                    dailyReport.TotalSmsexpenses = data.totalSMSExpenes;
                    dailyReport.ShippingExpend = data.shippingExpend;
                    dailyReport.InternetAndPhoneBill = data.internetAndPhoneBill;
                    dailyReport.SocialInsuranceAndFixedCost = data.socialInsuranceAndFixedCost;
                    dailyReport.Tax = data.tax;
                    dailyReport.IncomeTaxes = data.incomeTaxes;
                    dailyReport.SalonUnplannedSpending = data.salonUnplannedSpending;
                    dailyReport.ManageFee = data.manageFee;
                    dailyReport.OfficeRentAndServiceCose = data.officeRentAndSeviceCost;
                    dailyReport.OfficeStaffSalary = data.officeStaffSalary;
                    dailyReport.SalesSalary = data.salesSalary;
                    dailyReport.Itexpense = data.itSalary;
                    dailyReport.OfficeStaffSocialInsurance = data.officeStaffSocialInsurance;
                    dailyReport.UnplannedSpending = data.unplannedSpending;
                    dailyReport.CreatedTime = DateTime.Now;
                    dailyReport.Compensation = (double)Math.Round((decimal)data.compensation);
                    //add 20190225
                    // doanh thu phan bo shinemember trong ky ke toan
                    dailyReport.TotalSmdistributionToday = data.smDistributionToday;
                    // doanh thu phan bo shinemember trong ky hom nay
                    dailyReport.TotalSmdistributon = data.totalShineMemberDistribution;
                    // tong doanh thu Shinemember
                    dailyReport.TotalProductSm = data.totalProductShineMember;
                    // doanh thu thuan
                    dailyReport.TotalNetRevenue = data.totalNetRevenue;
                    // Luong bv + checkout
                    dailyReport.TotalSecurityCheckoutSalary = data.totalSecurityCheckOutSalary;

                    listData.Add(dailyReport);
                }

                ReportRepo.Add(listData);

                return Ok(new { message = Constant.SUCCESS });
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// import chi phi hang ngay
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost("salon-daily-cost")]
        public async Task<IActionResult> PostSalonDailyReport([FromBody] List<PostDailyReportJson> data)
        {
            try
            {
                Logger.LogInformation(JsonConvert.SerializeObject(data));
                var salonIds = data.Select(a => a.salonId);
                var shortNames = data.Select(a => a.salonShortName);
                foreach (var id in salonIds)
                {
                    if (data.Where(a => a.salonId == id).Select(a => DateTime.ParseExact(a.reportDate, @"d/M/yyyy", CultureInfo.InvariantCulture)).Distinct().Count()
                               != data.Where(a => a.salonId == id).Select(a => DateTime.ParseExact(a.reportDate, @"d/M/yyyy", CultureInfo.InvariantCulture)).Count())
                    {
                        return BadRequest(new { message = "Duplicate date ở salon có Id là" + id + "!" });
                    }
                }

                if (data.Any(a => DateTime.ParseExact(a.reportDate, @"d/M/yyyy", CultureInfo.InvariantCulture) < DateTime.Today))
                {
                    return BadRequest(new { message = "Có bản ghi là ngày trong quá khứ" });
                }

                var firstReportDate = data.First().reportDate;
                bool allEqual = data.All(a => a.reportDate == firstReportDate);

                if (allEqual == false)
                {
                    return BadRequest(new { message = "Chỉ được nhập vào 1 ngày!" });
                }

                var salons = SalonRepo.ValidateSalon(salonIds);
                var salonShortName = SalonRepo.ValidateSalonShortName(shortNames);

                if (salons == null)
                {
                    return NotFound(new { message = "Có bản ghi salon không tồn tại" });
                }

                if (salonShortName == null)
                {
                    return NotFound(new { message = "Tên salon viết tắt không tồn tại" });
                }

                var validateConvert = SalonDailyCostConvert(data);

                if (validateConvert == null)
                {
                    return BadRequest(new { message = "Bản ghi nhập vào phải là số!" });
                }

                var listUpdate = new List<SalonDailyCost>();
                var listPost = new List<SalonDailyCost>();

                foreach (var report in validateConvert)
                {

                    var date = DateTime.ParseExact(report.reportDate, @"d/M/yyyy", CultureInfo.InvariantCulture);

                    var dailyCost = new SalonDailyCost();

                    dailyCost.SalonId = report.salonId;
                    dailyCost.ElectricityAndWaterBill = Convert.ToDouble(report.electricityAndWaterBill);
                    dailyCost.RentWithTax = Convert.ToDouble(report.rentWithTax);
                    dailyCost.CapitalSpending = Convert.ToDouble(report.capitalSpending);
                    dailyCost.AdvertisementExpend = Convert.ToDouble(report.advertisementExpend);
                    dailyCost.ShippingExpend = Convert.ToDouble(report.shippingExpend);
                    dailyCost.InternetAndPhoneBill = Convert.ToDouble(report.internetAndPhoneBill);
                    dailyCost.SocialInsuranceAndFixedCost = Convert.ToDouble(report.socialInsuranceAndFixedCost);
                    dailyCost.IncomeTaxes = Convert.ToDouble(report.incomeTaxes);
                    dailyCost.SalonUnplannedSpending = Convert.ToDouble(report.salonUnplannedSpending);
                    dailyCost.ThaiHaRentAndSeviceCost = Convert.ToDouble(report.thaiHaRentAndSeviceCost);
                    dailyCost.OfficeStaffSalary = Convert.ToDouble(report.officeStaffSalary);
                    dailyCost.SalesSalary = Convert.ToDouble(report.salesSalary);
                    dailyCost.ItSalary = Convert.ToDouble(report.itSalary);
                    dailyCost.OfficeStaffSocialInsurance = Convert.ToDouble(report.officeStaffSocialInsurance);
                    dailyCost.UnplannedSpending = Convert.ToDouble(report.unplannedSpending);
                    dailyCost.ReportDate = date;
                    dailyCost.IsDelete = false;
                    dailyCost.ModifyTime = DateTime.Now;
                    dailyCost.DailyCostInventory = Convert.ToDouble(report.dailyCostInventory);
                    dailyCost.Compensation = Convert.ToDouble(report.compensation);
                    dailyCost.OtherIncome = Convert.ToDouble(report.otherIncome);
                    // add 20190222
                    // phan bo shinemember ke toan import
                    dailyCost.Smdistribution = Convert.ToDouble(report.smDistribution);
                    // Thuong doanh so KCS
                    dailyCost.PayOffKcs = Convert.ToDouble(report.payOffKcs);
                    // Chi phi SMS
                    dailyCost.Smsexpenses = Convert.ToDouble(report.smsExpenses);
                    // Thue GTGT
                    dailyCost.Tax = Convert.ToDouble(report.tax);
                    // luong bao ve + checkout
                    dailyCost.SecurityCheckoutSalary = Convert.ToDouble(report.securityCheckoutSalary);

                    var checkExist = SalonDailyCostRepo.GetBySalonIdAndDate(report.salonId, date);

                    if (checkExist != null)
                    {
                        checkExist.SalonId = report.salonId;
                        checkExist.ElectricityAndWaterBill = Convert.ToDouble(report.electricityAndWaterBill);
                        checkExist.RentWithTax = Convert.ToDouble(report.rentWithTax);
                        checkExist.CapitalSpending = Convert.ToDouble(report.capitalSpending);
                        checkExist.AdvertisementExpend = Convert.ToDouble(report.advertisementExpend);
                        checkExist.ShippingExpend = Convert.ToDouble(report.shippingExpend);
                        checkExist.InternetAndPhoneBill = Convert.ToDouble(report.internetAndPhoneBill);
                        checkExist.SocialInsuranceAndFixedCost = Convert.ToDouble(report.socialInsuranceAndFixedCost);
                        checkExist.IncomeTaxes = Convert.ToDouble(report.incomeTaxes);
                        checkExist.SalonUnplannedSpending = Convert.ToDouble(report.salonUnplannedSpending);
                        checkExist.ThaiHaRentAndSeviceCost = Convert.ToDouble(report.thaiHaRentAndSeviceCost);
                        checkExist.OfficeStaffSalary = Convert.ToDouble(report.officeStaffSalary);
                        checkExist.SalesSalary = Convert.ToDouble(report.salesSalary);
                        checkExist.ItSalary = Convert.ToDouble(report.itSalary);
                        checkExist.OfficeStaffSocialInsurance = Convert.ToDouble(report.officeStaffSocialInsurance);
                        checkExist.UnplannedSpending = Convert.ToDouble(report.unplannedSpending);
                        checkExist.ReportDate = date;
                        checkExist.IsDelete = false;
                        checkExist.ModifyTime = DateTime.Now;
                        checkExist.DailyCostInventory = Convert.ToDouble(report.dailyCostInventory);
                        checkExist.Id = checkExist.Id;
                        checkExist.CreateTime = checkExist.CreateTime;
                        checkExist.Compensation = Convert.ToDouble(report.compensation);
                        checkExist.OtherIncome = Convert.ToDouble(report.otherIncome);
                        // add 20190222
                        // phan bo shinemember ke toan import
                        checkExist.Smdistribution = Convert.ToDouble(report.smDistribution);
                        // Thuong doanh so KCS
                        checkExist.PayOffKcs = Convert.ToDouble(report.payOffKcs);
                        // Chi phi SMS
                        checkExist.Smsexpenses = Convert.ToDouble(report.smsExpenses);
                        // Thue GTGT
                        checkExist.Tax = Convert.ToDouble(report.tax);
                        // luong bao ve + checkout
                        checkExist.SecurityCheckoutSalary = Convert.ToDouble(report.securityCheckoutSalary);
                        listUpdate.Add(checkExist);
                    }
                    else
                    {
                        listPost.Add(dailyCost);
                    }
                }

                SalonDailyCostRepo.Add(listPost);

                if (listUpdate.Any())
                {
                    SalonDailyCostRepo.Update(listUpdate);
                }

                if (data.Any(a => DateTime.ParseExact(a.reportDate, @"d/M/yyyy", CultureInfo.InvariantCulture) == DateTime.Today && DateTime.Now.Hour >= 22 && DateTime.Now.Minute > 50))
                {
                    await ScheduleRerun(DateTime.Today, DateTime.Today);
                }

                return Ok(new { message = Constant.SUCCESS, listPost });
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Import chi phi ngay cu
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost("salon-daily-cost-day-past")]
        public async Task<IActionResult> PostSalonDailyReportDayPast([FromBody] List<PostDailyReportJson> data)
        {
            try
            {
                Logger.LogInformation(JsonConvert.SerializeObject(data));
                var salonIds = data.Select(a => a.salonId);
                var shortNames = data.Select(a => a.salonShortName);
                var reportDate = DateTime.ParseExact(data.First().reportDate, @"d/M/yyyy", CultureInfo.InvariantCulture);

                foreach (var id in salonIds)
                {
                    if (data.Where(a => a.salonId == id).Select(a => DateTime.ParseExact(a.reportDate, @"d/M/yyyy", CultureInfo.InvariantCulture)).Distinct().Count() != data.Where(a => a.salonId == id).Select(a => DateTime.ParseExact(a.reportDate, @"d/M/yyyy", CultureInfo.InvariantCulture)).Count())
                    {
                        return BadRequest(new { message = "Duplicate date ở salon có Id là" + id + "!" });
                    }
                }

                if (data.Any(a => DateTime.ParseExact(a.reportDate, @"d/M/yyyy", CultureInfo.InvariantCulture) >= DateTime.Today))
                {
                    return BadRequest(new { message = "Bản ghi phải là ngày trong qúa khứ!" });
                }

                var firstReportDate = data.First().reportDate;
                bool allEqual = data.All(a => a.reportDate == firstReportDate);

                if (allEqual == false)
                {
                    return BadRequest(new { message = "Chỉ được nhập vào 1 ngày!" });
                }

                var salons = SalonRepo.ValidateSalon(salonIds);
                var salonShortName = SalonRepo.ValidateSalonShortName(shortNames);

                if (salons == null)
                {
                    return NotFound(new { message = "Có bản ghi salon không tồn tại" });
                }

                if (salonShortName == null)
                {
                    return NotFound(new { message = "Tên salon viết tắt không tồn tại" });
                }

                var validateConvert = SalonDailyCostConvert(data);

                if (validateConvert == null)
                {
                    return BadRequest(new { message = "Bản ghi nhập vào phải là số!" });
                }

                var listUpdate = new List<SalonDailyCost>();
                var listPost = new List<SalonDailyCost>();

                foreach (var report in validateConvert)
                {

                    var date = DateTime.ParseExact(report.reportDate, @"d/M/yyyy", CultureInfo.InvariantCulture);

                    var dailyCost = new SalonDailyCost();

                    dailyCost.SalonId = report.salonId;
                    dailyCost.ElectricityAndWaterBill = Convert.ToDouble(report.electricityAndWaterBill);
                    dailyCost.RentWithTax = Convert.ToDouble(report.rentWithTax);
                    dailyCost.CapitalSpending = Convert.ToDouble(report.capitalSpending);
                    dailyCost.AdvertisementExpend = Convert.ToDouble(report.advertisementExpend);
                    dailyCost.ShippingExpend = Convert.ToDouble(report.shippingExpend);
                    dailyCost.InternetAndPhoneBill = Convert.ToDouble(report.internetAndPhoneBill);
                    dailyCost.SocialInsuranceAndFixedCost = Convert.ToDouble(report.socialInsuranceAndFixedCost);
                    dailyCost.IncomeTaxes = Convert.ToDouble(report.incomeTaxes);
                    dailyCost.SalonUnplannedSpending = Convert.ToDouble(report.salonUnplannedSpending);
                    dailyCost.ThaiHaRentAndSeviceCost = Convert.ToDouble(report.thaiHaRentAndSeviceCost);
                    dailyCost.OfficeStaffSalary = Convert.ToDouble(report.officeStaffSalary);
                    dailyCost.SalesSalary = Convert.ToDouble(report.salesSalary);
                    dailyCost.ItSalary = Convert.ToDouble(report.itSalary);
                    dailyCost.OfficeStaffSocialInsurance = Convert.ToDouble(report.officeStaffSocialInsurance);
                    dailyCost.UnplannedSpending = Convert.ToDouble(report.unplannedSpending);
                    dailyCost.ReportDate = date;
                    dailyCost.IsDelete = false;
                    dailyCost.ModifyTime = DateTime.Now;
                    dailyCost.DailyCostInventory = Convert.ToDouble(report.dailyCostInventory);
                    dailyCost.Compensation = Convert.ToDouble(report.compensation);
                    dailyCost.OtherIncome = Convert.ToDouble(report.otherIncome);
                    // add 20190222
                    // phan bo shinemember ke toan import
                    dailyCost.Smdistribution = Convert.ToDouble(report.smDistribution);
                    // Thuong doanh so KCS
                    dailyCost.PayOffKcs = Convert.ToDouble(report.payOffKcs);
                    // Chi phi SMS
                    dailyCost.Smsexpenses = Convert.ToDouble(report.smsExpenses);
                    // Thue GTGT
                    dailyCost.Tax = Convert.ToDouble(report.tax);
                    // luong bao ve + checkout
                    dailyCost.SecurityCheckoutSalary = Convert.ToDouble(report.securityCheckoutSalary);

                    var checkExist = SalonDailyCostRepo.GetBySalonIdAndDate(report.salonId, date);

                    if (checkExist != null)
                    {
                        checkExist.SalonId = report.salonId;
                        checkExist.ElectricityAndWaterBill = Convert.ToDouble(report.electricityAndWaterBill);
                        checkExist.RentWithTax = Convert.ToDouble(report.rentWithTax);
                        checkExist.CapitalSpending = Convert.ToDouble(report.capitalSpending);
                        checkExist.AdvertisementExpend = Convert.ToDouble(report.advertisementExpend);
                        checkExist.ShippingExpend = Convert.ToDouble(report.shippingExpend);
                        checkExist.InternetAndPhoneBill = Convert.ToDouble(report.internetAndPhoneBill);
                        checkExist.SocialInsuranceAndFixedCost = Convert.ToDouble(report.socialInsuranceAndFixedCost);
                        checkExist.IncomeTaxes = Convert.ToDouble(report.incomeTaxes);
                        checkExist.SalonUnplannedSpending = Convert.ToDouble(report.salonUnplannedSpending);
                        checkExist.ThaiHaRentAndSeviceCost = Convert.ToDouble(report.thaiHaRentAndSeviceCost);
                        checkExist.OfficeStaffSalary = Convert.ToDouble(report.officeStaffSalary);
                        checkExist.SalesSalary = Convert.ToDouble(report.salesSalary);
                        checkExist.ItSalary = Convert.ToDouble(report.itSalary);
                        checkExist.OfficeStaffSocialInsurance = Convert.ToDouble(report.officeStaffSocialInsurance);
                        checkExist.UnplannedSpending = Convert.ToDouble(report.unplannedSpending);
                        checkExist.ReportDate = date;
                        checkExist.IsDelete = false;
                        checkExist.ModifyTime = DateTime.Now;
                        checkExist.DailyCostInventory = Convert.ToDouble(report.dailyCostInventory);
                        checkExist.Id = checkExist.Id;
                        checkExist.CreateTime = checkExist.CreateTime;
                        checkExist.Compensation = Convert.ToDouble(report.compensation);
                        checkExist.OtherIncome = Convert.ToDouble(report.otherIncome);
                        // add 20190222
                        // phan bo shinemember ke toan import
                        checkExist.Smdistribution = Convert.ToDouble(report.smDistribution);
                        // Thuong doanh so KCS
                        checkExist.PayOffKcs = Convert.ToDouble(report.payOffKcs);
                        // Chi phi SMS
                        checkExist.Smsexpenses = Convert.ToDouble(report.smsExpenses);
                        // Thue GTGT
                        checkExist.Tax = Convert.ToDouble(report.tax);
                        // luong Bv + checkout
                        checkExist.SecurityCheckoutSalary = Convert.ToDouble(report.securityCheckoutSalary);

                        listUpdate.Add(checkExist);
                    }
                    else
                    {
                        listPost.Add(dailyCost);
                    }

                }

                SalonDailyCostRepo.Add(listPost);

                if (listUpdate.Any())
                {
                    SalonDailyCostRepo.Update(listUpdate);
                }

                await ScheduleRerun(reportDate, reportDate);
                return Ok(new { message = Constant.SUCCESS, listPost });
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Chay lai bao cao KQKD thu 2 hang tuan va ngay dau tien cua thang
        /// </summary>
        /// <returns></returns>
        [HttpPost("static-expense-rerun")]
        public async Task<IActionResult> PutStaticExpense()
        {
            try
            {

                string messageReturnWeek = "Today is not Monday";
                string messageReturnMonth = "Today is not the first day or second day of month";
                var today = DateTime.Today;
                var firstDayOfThisMonth = today.AddDays(-(today.Day - 1));
                var secondDayOfThisMonth = firstDayOfThisMonth.AddDays(1);

                if (today.DayOfWeek == DayOfWeek.Monday)
                {
                    var dateFrom = today.AddMonths(-1);
                    var dateTo = today.AddDays(-1);
                    await ScheduleRerun(dateFrom, dateTo);
                    messageReturnWeek = "Successfully!";
                }

                if (today == firstDayOfThisMonth || today == secondDayOfThisMonth)
                {
                    var lastDayOfLastMonth = firstDayOfThisMonth.AddDays(-1);
                    var firstDayOfLastMonth = firstDayOfThisMonth.AddMonths(-1);
                    await ScheduleRerun(lastDayOfLastMonth, firstDayOfLastMonth);
                    messageReturnMonth = "Successfully!";
                }

                return Ok(new { message = "Weekly rerun:" + " " + messageReturnWeek + " " + "||" + " " + "Monthly rerun:" + " " + messageReturnMonth });

            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }

        }

        /// <summary>
        /// chay lai bao cao KQKD
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        [HttpPost("rerun")]
        public async Task<IActionResult> RerunMonth([FromQuery] string dateFrom, string dateTo)
        {
            try
            {
                var from = DateTime.ParseExact(dateFrom, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                var to = DateTime.ParseExact(dateTo, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                var today = DateTime.Today;

                if (from > to)
                {
                    return BadRequest(new { message = "DateFrom > DateTo" });
                }

                if (from > today || to > today)
                {
                    return BadRequest(new { message = "Có ngày trong tương lai" });
                }

                if (from > DateTime.Today || to > DateTime.Today)
                {
                    return BadRequest(new { message = "Ngày phải trong quá khứ!" });
                }

                if (((today == from || today == to) && DateTime.Now.Hour > 22) || today > to)
                {
                    await ScheduleRerun(from, to);
                    return Ok();
                }
                else
                {
                    return NotFound(new { message = "Chỉ chạy được sau 22:00" });
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPut("salon-daily-cost")]
        public IActionResult PutSalonDailyCost([FromBody] List<PostDailyReportJson> data)
        {
            try
            {
                Logger.LogInformation(JsonConvert.SerializeObject(data));

                var salonIds = data.Select(a => a.salonId);
                var shortNames = data.Select(a => a.salonShortName);

                foreach (var id in salonIds)
                {
                    if (data.Where(a => a.salonId == id).Select(a => DateTime.ParseExact(a.reportDate, @"d/M/yyyy", CultureInfo.InvariantCulture)).Distinct().Count() != data.Where(a => a.salonId == id).Select(a => DateTime.ParseExact(a.reportDate, @"d/M/yyyy", CultureInfo.InvariantCulture)).Count())
                    {
                        return BadRequest(new { message = "Duplicate date at salon có Id là" + id + "!" });
                    }
                }

                if (data.Any(a => DateTime.ParseExact(a.reportDate, @"d/M/yyyy", CultureInfo.InvariantCulture) < DateTime.Today))
                {
                    return BadRequest(new { message = "Có bản ghi ngày trong quá khứ!" });
                }

                if (data.Any(a => DateTime.ParseExact(a.reportDate, @"d/M/yyyy", CultureInfo.InvariantCulture) == DateTime.Today) && DateTime.Now.Hour > 21 && DateTime.Now.Minute > 50)
                {
                    return BadRequest(new { message = "Có bản ghi trong ngày không thể ghi nhận vì đã quá 22 giờ 50!" });
                }

                var salons = SalonRepo.ValidateSalon(salonIds);
                var salonShortName = SalonRepo.ValidateSalonShortName(shortNames);

                if (salons == null)
                {
                    return NotFound(new { message = "Có bản ghi salon không tồn tại" });
                }

                if (salonShortName == null)
                {
                    return NotFound(new { message = "Có bản ghi tên salon viết tắt không tồn tại" });
                }

                var validateConvert = SalonDailyCostConvert(data);

                if (validateConvert == null)
                {
                    return BadRequest(new { message = "Bản ghi nhập vào phải là số!" });
                }

                var listUpdate = new List<SalonDailyCost>();

                foreach (var report in validateConvert)
                {

                    var date = DateTime.ParseExact(report.reportDate, @"d/M/yyyy", CultureInfo.InvariantCulture);

                    var checkExist = SalonDailyCostRepo.GetBySalonIdAndDate(report.salonId, date);

                    if (checkExist == null)
                    {
                        return BadRequest("Không thể update vì có bản ghi không tồn tại");
                    }
                    var dailyCost = new SalonDailyCost();
                    dailyCost.SalonId = report.salonId;
                    dailyCost.ElectricityAndWaterBill = Convert.ToDouble(report.electricityAndWaterBill);
                    dailyCost.RentWithTax = Convert.ToDouble(report.rentWithTax);
                    dailyCost.CapitalSpending = Convert.ToDouble(report.capitalSpending);
                    dailyCost.AdvertisementExpend = Convert.ToDouble(report.advertisementExpend);
                    dailyCost.ShippingExpend = Convert.ToDouble(report.shippingExpend);
                    dailyCost.InternetAndPhoneBill = Convert.ToDouble(report.internetAndPhoneBill);
                    dailyCost.SocialInsuranceAndFixedCost = Convert.ToDouble(report.socialInsuranceAndFixedCost);
                    dailyCost.IncomeTaxes = Convert.ToDouble(report.incomeTaxes);
                    dailyCost.SalonUnplannedSpending = Convert.ToDouble(report.salonUnplannedSpending);
                    dailyCost.ThaiHaRentAndSeviceCost = Convert.ToDouble(report.thaiHaRentAndSeviceCost);
                    dailyCost.OfficeStaffSalary = Convert.ToDouble(report.officeStaffSalary);
                    dailyCost.SalesSalary = Convert.ToDouble(report.salesSalary);
                    dailyCost.ItSalary = Convert.ToDouble(report.itSalary);
                    dailyCost.OfficeStaffSocialInsurance = Convert.ToDouble(report.officeStaffSocialInsurance);
                    dailyCost.UnplannedSpending = Convert.ToDouble(report.unplannedSpending);
                    dailyCost.ReportDate = date;
                    dailyCost.IsDelete = false;
                    dailyCost.ModifyTime = DateTime.Now;

                    listUpdate.Add(dailyCost);
                }

                SalonDailyCostRepo.Update(listUpdate);

                return Ok(new { message = Constant.SUCCESS, listUpdate });
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }
        }

        #region Private method

        private async Task<object> ScheduleRerun(DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var from = Convert.ToDateTime(dateFrom, culture);
                var to = Convert.ToDateTime(dateTo, culture);
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = from; date <= to; date = date.AddDays(1))
                    allDates.Add(date);

                var staticOld = ReportRepo.GetListReportByDate(from, to.AddDays(1));
                ReportRepo.Delete(staticOld);

                var listUpdate = new List<StaticExpense>();
                var listAdd = new List<StaticExpense>();
                foreach (var workDate in allDates)
                {
                    var report = await ReportRepo.GetReport(workDate);

                    if (report.Count() > 0)
                    {
                        foreach (var salonReport in report)
                        {
                            var checkExist = ReportRepo.GetBySalonIdAndDate(salonReport.salonId, workDate);
                            if (checkExist == null)
                            {
                                var newRecord = new StaticExpense();

                                newRecord.WorkDate = workDate;
                                newRecord.SalonId = salonReport.salonId;
                                newRecord.TotalIncomeAfterTax = salonReport.totalIncomeAfterTax;
                                newRecord.NumberOfTurns = salonReport.numberOfTurns;
                                newRecord.TotalSales = salonReport.totalSales;
                                newRecord.TotalServiceProfit = salonReport.totalServiceProfit;
                                newRecord.TotalCosmeticProfit = salonReport.totalProductProfit;
                                newRecord.TotalTransactionPerCus = salonReport.totalTransactionPerCus;
                                newRecord.TotalServicePerCus = salonReport.totalServicePerCus;
                                newRecord.TotalProductPerCus = salonReport.totalProductPerCus;
                                newRecord.DirectFee = salonReport.directFee;
                                newRecord.TotalStaffSalary = salonReport.totalStaffSalary;
                                newRecord.SalaryServicePerServiceIncome = salonReport.salaryServicePerServiceIncome;
                                newRecord.TotalProductCapital = salonReport.totalProductCapital;
                                newRecord.TotalProductPrice = salonReport.totalProdutPrice;
                                newRecord.TotalPayOffKcs = salonReport.totalPayOffKCS;
                                newRecord.TotalDailyCostInventory = salonReport.totalDailyCostInventory;
                                newRecord.ElectricityAndWaterBill = salonReport.electricityAndWaterBill;
                                newRecord.SalonFee = salonReport.salonFee;
                                newRecord.RentWithTax = salonReport.rentWithTax;
                                newRecord.CapitalSpending = salonReport.capitalSpending;
                                newRecord.Mktexpense = salonReport.mktExpense;
                                newRecord.TotalSmsexpenses = salonReport.totalSMSExpenes;
                                newRecord.ShippingExpend = salonReport.shippingExpend;
                                newRecord.InternetAndPhoneBill = salonReport.internetAndPhoneBill;
                                newRecord.SocialInsuranceAndFixedCost = salonReport.socialInsuranceAndFixedCost;
                                newRecord.Tax = salonReport.tax;
                                newRecord.IncomeTaxes = salonReport.incomeTaxes;
                                newRecord.SalonUnplannedSpending = salonReport.salonUnplannedSpending;
                                newRecord.ManageFee = salonReport.manageFee;
                                newRecord.OfficeRentAndServiceCose = salonReport.officeRentAndSeviceCost;
                                newRecord.OfficeStaffSalary = salonReport.officeStaffSalary;
                                newRecord.SalesSalary = salonReport.salesSalary;
                                newRecord.Itexpense = salonReport.itSalary;
                                newRecord.OfficeStaffSocialInsurance = salonReport.officeStaffSocialInsurance;
                                newRecord.UnplannedSpending = salonReport.unplannedSpending;
                                newRecord.CreatedTime = DateTime.Now;
                                newRecord.Compensation = (double)Math.Round((decimal)salonReport.compensation);
                                newRecord.TotalOtherIncome = salonReport.otherIncome;
                                // add 20190225
                                // phan bo shinemember trong ky ke toan hang ngay
                                newRecord.TotalSmdistributionToday = salonReport.smDistributionToday;
                                // phan bo shinemember trong ky hang ngay auto tinh toan
                                newRecord.TotalSmdistributon = salonReport.totalShineMemberDistribution;
                                // doanh thu shinemember ban trong ngay
                                newRecord.TotalProductSm = salonReport.totalProductShineMember;
                                // doanh thu thuan
                                newRecord.TotalNetRevenue = salonReport.totalNetRevenue;
                                // Luong bao ve  + checkout
                                newRecord.TotalSecurityCheckoutSalary = salonReport.totalSecurityCheckOutSalary;

                                listAdd.Add(newRecord);
                            }
                        }
                    }
                }

                if (listAdd.Any())
                {
                    ReportRepo.Add(listAdd);
                }

                return new { listUpdate, listAdd };
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private List<PostDailyReportJson> SalonDailyCostConvert(List<PostDailyReportJson> data)
        {
            var list = new List<PostDailyReportJson>();
            foreach (var item in data)
            {
                var returnDataDailyReportJson = new PostDailyReportJson
                {
                    reportDate = item.reportDate,
                    salonId = item.salonId,
                    salonShortName = item.salonShortName,
                    electricityAndWaterBill = item.electricityAndWaterBill.Trim().Replace(",", ""),
                    rentWithTax = item.rentWithTax.Trim().Replace(",", ""),
                    capitalSpending = item.capitalSpending.Trim().Replace(",", ""),
                    advertisementExpend = item.advertisementExpend.Trim().Replace(",", ""),
                    shippingExpend = item.shippingExpend.Trim().Replace(",", ""),
                    internetAndPhoneBill = item.internetAndPhoneBill.Trim().Replace(",", ""),
                    socialInsuranceAndFixedCost = item.socialInsuranceAndFixedCost.Trim().Replace(",", ""),
                    incomeTaxes = item.incomeTaxes.Trim().Replace(",", ""),
                    salonUnplannedSpending = item.salonUnplannedSpending.Trim().Replace(",", ""),
                    thaiHaRentAndSeviceCost = item.thaiHaRentAndSeviceCost.Trim().Replace(",", ""),
                    officeStaffSalary = item.officeStaffSalary.Trim().Replace(",", ""),
                    salesSalary = item.salesSalary.Trim().Replace(",", ""),
                    itSalary = item.itSalary.Trim().Replace(",", ""),
                    officeStaffSocialInsurance = item.officeStaffSocialInsurance.Trim().Replace(",", ""),
                    unplannedSpending = item.unplannedSpending.Trim().Replace(",", ""),
                    dailyCostInventory = item.dailyCostInventory.Trim().Replace(",", ""),
                    compensation = item.compensation.Trim().Replace(",", ""),
                    otherIncome = item.otherIncome.Trim().Replace(",", ""),
                    smDistribution = item.smDistribution.Trim().Replace(",", ""),
                    payOffKcs = item.payOffKcs.Trim().Replace(",", ""),
                    smsExpenses = item.smsExpenses.Trim().Replace(",", ""),
                    tax = item.tax.Trim().Replace(",", ""),
                    securityCheckoutSalary = item.securityCheckoutSalary.Trim().Replace(",", "")
                };

                //var electricityAndWaterBill = Regex.IsMatch(returnDataDailyReportJson.electricityAndWaterBill, @"^\d+$");
                //var rentWithTax = Regex.IsMatch(returnDataDailyReportJson.rentWithTax, @"^\d+$");
                //var capitalSpending = Regex.IsMatch(returnDataDailyReportJson.capitalSpending, @"^\d+$");
                //var advertisementExpend = Regex.IsMatch(returnDataDailyReportJson.advertisementExpend, @"^\d+$");
                //var shippingExpend = Regex.IsMatch(returnDataDailyReportJson.shippingExpend, @"^\d+$");
                //var internetAndPhoneBill = Regex.IsMatch(returnDataDailyReportJson.internetAndPhoneBill, @"^\d+$");
                //var socialInsuranceAndFixedCost = Regex.IsMatch(returnDataDailyReportJson.socialInsuranceAndFixedCost, @"^\d+$");
                //var incomeTaxes = Regex.IsMatch(returnDataDailyReportJson.incomeTaxes, @"^\d+$");
                //var salonUnplannedSpending = Regex.IsMatch(returnDataDailyReportJson.salonUnplannedSpending, @"^\d+$");
                //var thaiHaRentAndSeviceCost = Regex.IsMatch(returnDataDailyReportJson.thaiHaRentAndSeviceCost, @"^\d+$");
                //var officeStaffSalary = Regex.IsMatch(returnDataDailyReportJson.officeStaffSalary, @"^\d+$");
                //var salesSalary = Regex.IsMatch(returnDataDailyReportJson.salesSalary, @"^\d+$");
                //var itSalary = Regex.IsMatch(returnDataDailyReportJson.itSalary, @"^\d+$");
                //var officeStaffSocialInsurance = Regex.IsMatch(returnDataDailyReportJson.officeStaffSocialInsurance, @"^\d+$");
                //var unplannedSpending = Regex.IsMatch(returnDataDailyReportJson.unplannedSpending, @"^\d+$");
                //var dailyCostInventory = Regex.IsMatch(returnDataDailyReportJson.dailyCostInventory, @"^\d+$");
                //var compensation = Regex.IsMatch(returnDataDailyReportJson.compensation, @"^\d+$");

                //if (electricityAndWaterBill == false || rentWithTax == false || capitalSpending == false || advertisementExpend == false
                //    || shippingExpend == false || internetAndPhoneBill == false || socialInsuranceAndFixedCost == false || incomeTaxes == false
                //    || salonUnplannedSpending == false || thaiHaRentAndSeviceCost == false || officeStaffSalary == false || salesSalary == false ||
                //    itSalary == false || officeStaffSocialInsurance == false || unplannedSpending == false || dailyCostInventory == false)
                //{
                //    return null;
                //}

                list.Add(returnDataDailyReportJson);

            }

            return list;


        }

        private RankReportBCKQKDJson returnRankReportBCKQKD(int money, int priceCompare)
        {
            try
            {
                var objRank = new RankReportBCKQKDJson();
                if (money > priceCompare)
                {
                    objRank.rank = "A";
                    objRank.colorRank = "#00ff00";
                }
                if (money < priceCompare && money > 0)
                {
                    objRank.rank = "B";
                    objRank.colorRank = "#ffff99";
                }
                if (money < 0)
                {
                    objRank.rank = "C";
                    objRank.colorRank = "#ff0000";
                }
                return objRank;
            }
            catch (Exception e)
            {

                throw e;
            }

        }

        #endregion
    }
}