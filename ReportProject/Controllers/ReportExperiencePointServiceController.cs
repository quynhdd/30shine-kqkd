﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using APICheckout.Extentions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReportProject.Models.Solution_30Shine;
using ReportProject.Respository.InterfaceV1;
using static APICheckout.Models.CustomeModel.InputModel;
using static APICheckout.Models.CustomeModel.OutPutModel;

namespace ReportProject.Controllers
{
    [Produces("application/json")]
    [Route("api/report-point-service")]
    public class ReportExperiencePointServiceController : Controller
    {
        private IStatisticScscErrorRepo StatisticScscErrorRepo;
        private ILogger<ReportExperiencePointServiceController> Logger { get; }
        private IPushNotice PushNotice;
        private IFeedbackPointServiceRepo FeedbackPointServiceRepo;
        private IBillserviceRepo BillserviceRepo;
        private IMonitorStaffErrorRepo MonitorStaffErrorRepo;
        private ISalonRepo SalonRepo;

        public ReportExperiencePointServiceController(IStatisticScscErrorRepo statisticScscErrorRepo,
            ILogger<ReportExperiencePointServiceController> logger,
            IPushNotice pushNotice,
            IFeedbackPointServiceRepo feedbackPointServiceRepo,
            IBillserviceRepo billserviceRepo,
            IMonitorStaffErrorRepo monitorStaffErrorRepo,
            ISalonRepo salonRepo
            )
        {
            StatisticScscErrorRepo = statisticScscErrorRepo;
            Logger = logger;
            PushNotice = pushNotice;
            BillserviceRepo = billserviceRepo;
            FeedbackPointServiceRepo = feedbackPointServiceRepo;
            MonitorStaffErrorRepo = monitorStaffErrorRepo;
            SalonRepo = salonRepo;
        }
        [HttpPost]
        public async Task<IActionResult> PostImportPointService([FromBody] List<ReqFeedbackPointService> param)
        {
            try
            {
                if (param == null || param.Count <= 0) return BadRequest("Data Not Exits!");
                // Check trùng 1 salon 1 ngày chỉ có 1 bản ghi
                var listCheck = param.GroupBy(r => new { r.SalonId, r.WorkDate }).Select(r => new
                {
                    SalonId = r.Key.SalonId,
                    WorkDate = r.Key.WorkDate,
                    Count = r.Count()
                }).Where(r => r.Count > 1).ToList();
                if (listCheck.Count > 0)
                {
                    return BadRequest("File bị trùng lặp, vui lòng sửa lại");
                }
                var listSalonIdParam = param.Select(g => g.SalonId).ToList();
                var listExitsFeedback = await FeedbackPointServiceRepo.GetList(r => listSalonIdParam.Contains(r.SalonId ?? 0) && r.IsDelete == false);
                foreach (var item in param)
                {
                    var workDate = DateTime.ParseExact(item.WorkDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                    var recordOld = listExitsFeedback.Where(r => r.SalonId == item.SalonId && r.WorkDate == workDate).FirstOrDefault();
                    if (recordOld != null)
                    {
                        recordOld.FeedbackQuantity = item.FeedbackQuantity;
                        recordOld.ModifiedDate = DateTime.Now;
                        FeedbackPointServiceRepo.Update(recordOld);
                    }
                    else
                    {
                        var record = new FeedbackPointService();
                        record.FeedbackQuantity = item.FeedbackQuantity;
                        record.SalonId = item.SalonId;
                        record.IsDelete = false;
                        record.WorkDate = workDate;
                        FeedbackPointServiceRepo.Add(record);
                    }
                }

                await FeedbackPointServiceRepo.SaveChangeAsync();
                return Ok(new { success = "success" });
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [HttpGet]
        public async Task<IActionResult> GetReportPointService([FromQuery] string fromDate, string toDate, int regionId, int salonId)
        {
            try
            {
                if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate)) return BadRequest("Ngày không hợp lệ");
                var fromdate = DateTime.ParseExact(fromDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                var todate = DateTime.ParseExact(toDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                todate = todate.AddDays(1);
                if (todate <= fromdate)
                {
                    return BadRequest("Ngày không hợp lệ");
                }
                // List Id uốn
                var listUonId = new List<int>() { 16};
                var listSalonId = new List<int>();
                if (regionId > 0 && salonId == 0)
                {
                    
                    var lstSalon = await SalonRepo.GetList(regionId);
                    if (lstSalon.Any())
                    {
                        listSalonId = lstSalon;
                    }
                }
                else
                {
                    var salon = await SalonRepo.GetList(r => r.IsDelete == 0 && (r.Id == salonId || salonId == 0) && r.Id != 24);
                    if (salon.Any())
                    {
                        listSalonId = salon.Select(r => r.Id).ToList();
                    }
                }
                // Danh sach Bill
                var listBillService = await BillserviceRepo.GetListStaffBill(fromdate, todate, listUonId, listSalonId);
                // Tong bill
                var totalBill = listBillService.GroupBy(r => new { r.SalonId, r.SalonShortName, r.Order }).Select(r => new
                {
                    SalonId = (int?)r.Key.SalonId,
                    SalonShortName = r.Key.SalonShortName,
                    OrderSalon = (int?)r.Key.Order,
                    TotalBill = (int?)r.Count()
                }).ToList();

                // 1 sao
                var list1Sao = listBillService.Where(r => r.Star == 1).GroupBy(r => r.SalonId).Select(r => new
                {
                    SalonId = (int?)r.Key,
                    TotalStar1 = (int?)r.Count()
                }).ToList();
                // 2 sao
                var list2Sao = listBillService.Where(r => r.Star == 2).GroupBy(r => r.SalonId).Select(r => new
                {
                    SalonId = (int?)r.Key,
                    TotalStar2 = (int?)r.Count()
                }).ToList();
                // 3 sao
                var list3Sao = listBillService.Where(r => r.Star == 3).GroupBy(r => r.SalonId).Select(r => new
                {
                    SalonId = (int?)r.Key,
                    TotalStar3 = (int?)r.Count()
                }).ToList();
                // 4 sao
                var list4Sao = listBillService.Where(r => r.Star == 4).GroupBy(r => r.SalonId).Select(r => new
                {
                    SalonId = (int?)r.Key,
                    TotalStar4 = (int?)r.Count()
                }).ToList();
                //  5 sao
                var list5Sao = listBillService.Where(r => r.Star == 5).GroupBy(r => r.SalonId).Select(r => new
                {
                    SalonId = (int?)r.Key,
                    TotalStar5 = (int?)r.Count()
                }).ToList();
                // Loi giam sat
                var listError = await MonitorStaffErrorRepo.GetListSalonError(fromdate, todate, listSalonId);
                // loi scsc
                var listTotalScsc0 = listBillService.Where(r => r.PointError == 0 && r.ImageError == false).GroupBy(r => r.SalonId).Select(r => new
                {
                    SalonId = (int?)r.Key,
                    TotalPointScsc0 = (int?)r.Count()
                }).ToList();
                // Loi uon
                var listTotalCurling0 = listBillService.Where(r => r.TotalPointSCSCCurling == 0 && r.ServiceIdIsCurling > 0 && r.IsImageCurling > 0 && r.ImageErrorCurling != true)
                    .GroupBy(r => r.SalonId).Select(r => new 
                    {
                        SalonId = (int?)r.Key,
                        TotalPointCurling0 = (int?)r.Count()
                    }).ToList();
                // Tong bill anh uon
                var totalBillNotImageCurling = listBillService.Where(r => r.IsImageCurling != 1 && r.ServiceIdIsCurling > 0)
                    .GroupBy(r => r.SalonId).Select(r => new
                    {
                        SalonId = (int?)r.Key,
                        TotalImageCurling = (int?)r.Count()
                    }).ToList();
                // Tong mo lech uon
                var totalBillMoLechCurling = listBillService.Where(r=>r.IsImageCurling == 1 && r.ImageErrorCurling == true)
                    .GroupBy(r => r.SalonId).Select(r => new
                    {
                        SalonId = (int?)r.Key,
                        TotalMoLechUon = (int?)r.Count()
                    }).ToList();
                // Phan hoi khach hang
                var listFeedbackPointRepo = await FeedbackPointServiceRepo.GetList(r => r.IsDelete == false && r.WorkDate >= fromdate.Date && r.WorkDate <= todate.AddDays(-1) && listSalonId.Contains(r.SalonId ?? 0));
                var listFeedbackPoint = listFeedbackPointRepo.GroupBy(r => r.SalonId).Select(r => new
                {
                    SalonId = (int?)r.Key,
                    TotalFeedback = (int?)r.Sum(g => g.FeedbackQuantity)
                }).ToList();
                // bill khong anh
                var listTotalNotImageScsc = listBillService.Where(r => r.IsImages != true).GroupBy(r => r.SalonId).Select(r => new
                {
                    SalonId = (int?)r.Key,
                    TotalNotImageScsc = (int?)r.Count()
                }).ToList();
                // Tong mo lech scsc
                var totalMoLechScsc = listBillService.Where(r=>r.IsImages == true && r.ImageError == true)
                    .GroupBy(r => r.SalonId).Select(r => new
                    {
                        SalonId = (int?)r.Key,
                        TotalMoLechScsc = (int?)r.Count()
                    }).ToList();
                var listLongTime = await BillserviceRepo.GetListLongTime(fromdate.Date, todate.Date.AddDays(-1),listSalonId);

                var list = (
                            from a in totalBill
                            join b in list1Sao on a.SalonId equals b.SalonId into bb
                            from b in bb.DefaultIfEmpty()
                            join c in list2Sao on a.SalonId equals c.SalonId into cc
                            from c in cc.DefaultIfEmpty()
                            join d in list3Sao on a.SalonId equals d.SalonId into dd
                            from d in dd.DefaultIfEmpty()
                            join e in list4Sao on a.SalonId equals e.SalonId into ee
                            from e in ee.DefaultIfEmpty()
                            join sao5 in list5Sao on a.SalonId equals sao5.SalonId into sao55
                            from sao5 in sao55.DefaultIfEmpty()
                            join f in listError on a.SalonId equals f.SalonId into ff
                            from f in ff.DefaultIfEmpty()
                            join g in listTotalScsc0 on a.SalonId equals g.SalonId into gg
                            from g in gg.DefaultIfEmpty()
                            join h in listTotalCurling0 on a.SalonId equals h.SalonId into hh
                            from h in hh.DefaultIfEmpty()
                            join i in totalMoLechScsc on a.SalonId equals i.SalonId into ii
                            from i in ii.DefaultIfEmpty()
                            join j in totalBillMoLechCurling on a.SalonId equals j.SalonId into jj
                            from j in jj.DefaultIfEmpty()
                            join n in listTotalNotImageScsc on a.SalonId equals n.SalonId into nn
                            from n in nn.DefaultIfEmpty()
                            join q in totalBillNotImageCurling on a.SalonId equals q.SalonId into qq
                            from q in qq.DefaultIfEmpty()
                            join r in listFeedbackPoint on a.SalonId equals r.SalonId into rr
                            from r in rr.DefaultIfEmpty()
                            join t in listLongTime on a.SalonId equals t.SalonId into tt
                            from t in tt.DefaultIfEmpty()
                            select new OutReportExperiencePoint
                            {
                                SalonId = a.SalonId,
                                TotalBill = a.TotalBill,
                                RatingTB = (float)((b != null ? b.TotalStar1 : 0) + (c != null ? c.TotalStar2 : 0) * 2 + (d != null ? d.TotalStar3 : 0) * 3 + (e != null ? e.TotalStar4 : 0) * 4 + (sao5 != null ? sao5.TotalStar5 : 0) * 5) / a.TotalBill,
                                TotalMonitorStaff = f != null ? f.TotalError : 0,
                                TotalErorrScsc = g != null ? g.TotalPointScsc0 : 0,
                                TotalErrorScscCurling = h != null ? h.TotalPointCurling0 : 0,
                                ErrorImage = (n != null ? n.TotalNotImageScsc : 0) + (q != null ? q.TotalImageCurling : 0) + (i != null ? i.TotalMoLechScsc: 0) + (j != null ? j.TotalMoLechUon : 0),
                                SalonShortName = a.SalonShortName,
                                Feedback = r != null ? r.TotalFeedback : 0,
                                WaitTime15 = t != null ? t.TotalLongTime : 0,
                                OrderSalon = a.OrderSalon
                            }).OrderBy(r => r.OrderSalon).ToList();
                var total = new OutReportExperiencePoint();

                total.SalonId = 0;
                total.TotalBill = list.Select(r=>r.TotalBill).Sum();
                total.RatingTB = list.Select(r=>r.RatingTB).Average();
                total.TotalMonitorStaff = list.Select(r=>r.TotalMonitorStaff).Sum();
                total.TotalErorrScsc = list.Select(r=>r.TotalErorrScsc).Sum();
                total.TotalErrorScscCurling = list.Select(r=>r.TotalErrorScscCurling).Sum();
                total.ErrorImage = list.Select(r=>r.ErrorImage).Sum();
                total.SalonShortName = "30Shine";
                total.Feedback = list.Select(r=>r.Feedback).Sum();
                total.OrderSalon = 0;
                total.WaitTime15 = list.Select(r=>r.WaitTime15).Sum();

                list.Add(total);
                return Ok(list);
            }
            catch (Exception e)
            {
                return StatusCode(500, "Đã có lỗi xảy ra!");
            }
        }
    }
}