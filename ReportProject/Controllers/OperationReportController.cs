﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using ReportProject.Utils;

namespace ReportProject.Controllers
{
    [Produces("application/json")]
    [Route("api/operation-report")]
    public class OperationReportController : Controller
    {
        private IOperationReportRepo OperationReportRepo { get; set; }
        private ISalonRepo SalonRepo { get; set; }
        private IStatictisReportOperationRepo StatictisReportOperationRepo { get; set; }
        private ILogger<OperationReportController> Logger { get; }
        CultureInfo culture = new CultureInfo("vi-VN");

        public OperationReportController(IOperationReportRepo operationReportRepo, ISalonRepo salonRepo, IStatictisReportOperationRepo statictisReportOperationRepo, ILogger<OperationReportController> logger)
        {
            OperationReportRepo = operationReportRepo;
            SalonRepo = salonRepo;
            StatictisReportOperationRepo = statictisReportOperationRepo;
            Logger = logger;
        }
        //[ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("today")]
        public async Task<IActionResult> GetReportToday([FromQuery] int type, int Id)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                //var dateFrom = Convert.ToDateTime("05-12-2018", culture);
                var dateFrom = DateTime.Today;
                var report = await OperationReportRepo.GetOperationReportToday(type, Id, dateFrom);
                if (!report.dataBodyReport.Any())
                {
                    return NoContent();
                }
                var elapsedMs = watch.ElapsedMilliseconds;
                return Ok(report);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetReportOtherToday([FromQuery] string from, string to, int type, int Id)
        {
            try
            {

                if (DatetimeExtension.StringToDateTime(Constant.DATETIME, from) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME2, from) == null
                    || DatetimeExtension.StringToDateTime(Constant.DATETIME, to) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME2, to) == null)
                {
                    return BadRequest();
                }

                var dateFrom = DateTime.ParseExact(from, @"dd/MM/yyyy", CultureInfo.InvariantCulture); 
                var dateTo = DateTime.ParseExact(to, @"dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(+1);

                var report = await OperationReportRepo.GetByDayOperationReport(dateFrom, dateTo, type, Id);

                if (!report.dataBodyReport.Any())
                {
                    return NoContent();
                }

                return Ok(report);
            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }

        [HttpPost("static-expense")]
        public async Task<IActionResult> PostReportOperation()
        {
            try
            {

                //var dateFrom = Convert.ToDateTime("15-05-2018", culture);
                var dateFrom = DateTime.Today;
                var dateTo = dateFrom;
                await ScheduleRerun(dateFrom, dateTo);
                return Ok(new { message = Constant.SUCCESS });
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }
        }
        [HttpPost("static-expense-rerun")]
        public async Task<IActionResult> PostReporOperationtRerun([FromQuery] string from, string to)
        {
            try
            {
                if (DatetimeExtension.StringToDateTime(Constant.DATETIME, from) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME2, from) == null
                    || DatetimeExtension.StringToDateTime(Constant.DATETIME, to) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME2, to) == null)
                {
                    return BadRequest();
                }

                var dateFrom = DateTime.ParseExact(from, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                var dateTo = DateTime.ParseExact(to, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                await ScheduleRerun(dateFrom, dateTo);
                return Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }
        }
        private async Task<object> ScheduleRerun(DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var from = Convert.ToDateTime(dateFrom, culture);
                var to = Convert.ToDateTime(dateTo, culture);
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = from; date <= to; date = date.AddDays(1))
                    allDates.Add(date);
                var salon = SalonRepo.GetListParam(0, 0);
                IEnumerable<int?> salonIds = salon.Select(a => a.Id).Cast<int?>().ToList();
                var statictisReportOperationOld = await StatictisReportOperationRepo.GetByDayListSalon(salonIds, from, to.AddDays(1));
                StatictisReportOperationRepo.Delete(statictisReportOperationOld);

                var listAdd = new List<StatictisReportOperation>();
                foreach (var workDate in allDates)
                {
                    var report = await OperationReportRepo.GetOperationReportToday(0, 0, workDate);
                    List<StatictisReportOperation> dataReport = report.dataBodyReport.Select(a => new StatictisReportOperation
                    {
                        SalonId = a.salonId,
                        SalonName = a.salonName,
                        TotalIncome = a.totalIncome,
                        TotalServiceInCome = a.totalServiceInCome,
                        TotalProductIncome = a.totalProductIncome,
                        WorktimeStylist = a.worktimeStylist,
                        NumberOfTurns = a.numberOfTurns,
                        NumberCustomerOld = a.numberCustomerOld,
                        NumberBillNotImg = a.numberBillNotImg,
                        TotalStarNumber = a.totalStarNumber,
                        NumberErrorMonitoring = a.numberErrorMonitoring,
                        NumberStylistTimekeeping = a.numberStylistTimekeeping,
                        NumberSkinnerTimekeeping = a.numberSkinnerTimekeeping,
                        NumberStylist = a.numberStylist,
                        NumberCanleBooking = a.numberCanleBooking,
                        NumberDeleteBooking = a.numberDeleteBooking,
                        OvertimeHoursStylist = a.overtimeHoursStylist,
                        NumberSlotBooking = a.numberSlotBooking,
                        NumberBooking = a.numberBooking,
                        NumberBookingBefor = a.numberBookingBefor,
                        NumberBillWaited = a.numberBillWaited,
                        WorkDate = workDate,
                        IsDelete = false,
                        CreateDate = DateTime.Now,
                        NumberBill1Star = a.numberBill1Star,
                        NumberBill2Star = a.numberBill2Star,
                        NumberBill3Star = a.numberBill3Star,
                        NumberBill4Star = a.numberBill4Star,
                        NumberBill5Star = a.numberBill5Star,
                        TotalShineCombo = a.totalShineCombo,
                        TotalKidCombo = a.totalKidCombo,
                        TotalProtein = a.totalProtein,
                        TotalMask = a.totalMask,
                        TotalExFoliation = a.totalExFoliation,
                        TotalGroupUonDuoi = a.totalGroupUonDuoi,
                        TotalGroupColorCombo = a.totalGroupColorCombo
                    }).OrderBy(a => a.SalonId).ToList();
                    listAdd.AddRange(dataReport);
                }

                if (listAdd.Any())
                {
                    StatictisReportOperationRepo.Add(listAdd);
                }

                return new { listAdd };
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}