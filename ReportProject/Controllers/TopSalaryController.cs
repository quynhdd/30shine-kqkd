﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReportProject.Utils;
using Newtonsoft.Json;
using ReportProject.Models.Solution_30shine_Real;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ReportProject.Controllers
{
    [Produces("application/json")]
    [Route("api/top-salary")]
    public class TopSalaryController : Controller
    {
        private Solution_30shineContext context;
        private DbSet<TopSalary> objTopSalary;
        private ILogger<ReportController> Logger { get; }
        CultureInfo culture = new CultureInfo("vi-VN");
        static HttpClient client = new HttpClient();

        public TopSalaryController()
        {
            context = new Solution_30shineContext();
            objTopSalary = context.Set<TopSalary>();
        }
        // GET: /<controller>/
        [HttpGet]
        public IActionResult GetTopSalaryStaff()
        {
            try
            {
                var ListAll = objTopSalary.Where(a => a.IsDelete == false).ToList();
                var listStylist = ListAll.Where(a => a.DepartmentId == 1).Select(a => new GetTopSalary
                {
                    Id = a.Id,
                    StaffId = a.StaffId,
                    StaffName = a.StaffName,
                    DepartmentId = a.DepartmentId,
                    DepartmentName = "Stylist",
                    Month = a.Month,
                    Year = a.Year,
                    SkillLevel = a.SkillLevel,
                    SalonName = a.SalonName,
                    Salary = a.Salary
                }).OrderByDescending(a => a.Salary).OrderByDescending(a => a.Month).OrderByDescending(a => a.Year);
                var listSkinner = ListAll.Where(a => a.DepartmentId == 2).Select(a => new GetTopSalary
                {
                    Id = a.Id,
                    StaffId = a.StaffId,
                    StaffName = a.StaffName,
                    DepartmentId = a.DepartmentId,
                    DepartmentName = "Skinner",
                    Month = a.Month,
                    Year = a.Year,
                    SkillLevel = a.SkillLevel,
                    SalonName = a.SalonName,
                    Salary = a.Salary
                }).OrderByDescending(a => a.Salary).OrderByDescending(a => a.Month).OrderByDescending(a => a.Year);

                var ResulList = listStylist.Union(listSkinner).OrderByDescending(a => a.Salary).OrderByDescending(a => a.Month).OrderByDescending(a => a.Year).ToList();

                if (!ResulList.Any())
                {
                    return NoContent();
                }
                return Ok(ResulList);

            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
    public class GetTopSalary : TopSalary
    {
        public string DepartmentName { get; set; }
    }
}
