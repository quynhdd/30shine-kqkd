﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Implement;
using ReportProject.Respository.Interface;
using ReportProject.Utils;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ReportProject.Controllers
{
    [Produces("application/json")]
    [Route("api/save-staffbilldetail")]
    public class SaveStaffBillServiceDetail : Controller
    {
        private IStaffBillServiceDetailInforRepo StaffBillServiceDetailInforRepo { get; set; }
        private IStaffBillServiceDetailRepo StaffBillServiceDetailRepo { get; set; }
        private ILogger<ReportController> Logger { get; }
        CultureInfo culture = new CultureInfo("vi-VN");
        static HttpClient client = new HttpClient();
        public SaveStaffBillServiceDetail(IStaffBillServiceDetailInforRepo staffBillServiceDetailInforRepo, IStaffBillServiceDetailRepo staffBillServiceDetailRepo, ILogger<ReportController> logger)
        {
            StaffBillServiceDetailInforRepo = staffBillServiceDetailInforRepo;
            StaffBillServiceDetailRepo = staffBillServiceDetailRepo;
            Logger = logger;
        }

        // GET: /<controller>/
        [HttpPost]
        public async Task<IActionResult> StatisticSaveBillServiceDtail()
        {
            try
            {
                DateTime fromdate = DateTime.Today.AddDays(-1);
                DateTime todate = fromdate.AddDays(+1);
                Logger.LogInformation(fromdate + "/" + fromdate.AddDays(+1));
                var StaffBillServiceDetailInforRepoList = StaffBillServiceDetailInforRepo.GetStaffBillServiceDetail(fromdate);
                var StaffBillServiceDetailRepoList = StaffBillServiceDetailRepo.GetByDate(0, fromdate, todate, 0);
                Task.WaitAll(StaffBillServiceDetailInforRepoList, StaffBillServiceDetailRepoList);

                var ListNew = StaffBillServiceDetailInforRepoList.Result;
                var ListOld = StaffBillServiceDetailRepoList.Result;
                #region nghien cu them ve dieu kien
                var ListAdd = //(
                    from a in ListNew
                    where
                    !ListOld.Any
                    (
                        b =>
                        b.BillId == a.BillId
                        && b.ServiceId == a.ServiceId
                        && b.DepartmentId == a.DepartmentId
                        && b.StaffId == a.StaffId
                        && b.StaffLevelId == a.StaffLevelId
                        && b.OvertimeStatusValue == a.OvertimeStatusValue
                        && b.RatingMark == a.RatingMark
                        && b.RatingMoney == a.RatingMoney
                        && b.SalaryCoeficient == a.SalaryCoeficient
                        && b.ServiceCoeficient == b.ServiceCoeficient
                        && b.ServiceIncomeBonus == a.ServiceIncomeBonus
                        //&& b.WorkDate == a.WorkDate
                        && b.Quantity == a.Quantity
                        && b.ConventionPoint == a.ConventionPoint
                        && b.ServiceBonus == a.ServiceBonus
                        && b.BillSalonId == a.BillSalonId
                        )
                    select a
                //).ToList()
                ;

                var ListDelete =
                   // (
                    from a in ListOld
                    where
                    !ListNew.Any
                    (
                        b =>
                        b.BillId == a.BillId
                        && b.ServiceId == a.ServiceId
                        && b.DepartmentId == a.DepartmentId
                        && b.StaffId == a.StaffId
                        && b.StaffLevelId == a.StaffLevelId
                        && b.OvertimeStatusValue == a.OvertimeStatusValue
                        && b.RatingMark == a.RatingMark
                        && b.RatingMoney == a.RatingMoney
                        && b.SalaryCoeficient == a.SalaryCoeficient
                        && b.ServiceCoeficient == b.ServiceCoeficient
                        && b.ServiceIncomeBonus == a.ServiceIncomeBonus
                        //&& b.WorkDate == a.WorkDate
                        && b.Quantity == a.Quantity
                        && b.ConventionPoint == a.ConventionPoint
                        && b.ServiceBonus == a.ServiceBonus
                        && b.BillSalonId == a.BillSalonId
                        )
                    select a
                                    // ).ToList()
                                     ;
                #endregion
                //var ListDelete = null;


                //var staffBillServiceDetailList = StaffBillServiceDetailRepoList.Result;
                //if (ListAdd.Count() == 0)
                //{
                //    return NoContent();
                //}
                StaffBillServiceDetailInforRepo.DeleteAndAdd(ListDelete, ListAdd);

                return Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
            }
        }

        [HttpPost("re-run")]
        public async Task<IActionResult> ReRunStatisticSaveBillServiceDtail([FromQuery] string from)
        {
            try
            {
                DateTime fromdate = DateTime.ParseExact(from, @"d/M/yyyy", CultureInfo.InvariantCulture);
                DateTime todate = fromdate.AddDays(+1);
                Logger.LogInformation(fromdate + "/" + fromdate.AddDays(+1));
                var StaffBillServiceDetailInforRepoList = StaffBillServiceDetailInforRepo.GetStaffBillServiceDetail(fromdate);
                var StaffBillServiceDetailRepoList = StaffBillServiceDetailRepo.GetByDate(0, fromdate, todate, 0);
                Task.WaitAll(StaffBillServiceDetailInforRepoList, StaffBillServiceDetailRepoList);

                var ListNew = StaffBillServiceDetailInforRepoList.Result;
                var ListOld = StaffBillServiceDetailRepoList.Result;
                #region nghien cu them ve dieu kien
                var ListAdd = //(
                    from a in ListNew
                    where
                    !ListOld.Any
                    (
                        b =>
                        b.BillId == a.BillId
                        && b.ServiceId == a.ServiceId
                        && b.DepartmentId == a.DepartmentId
                        && b.StaffId == a.StaffId
                        && b.StaffLevelId == a.StaffLevelId
                        && b.OvertimeStatusValue == a.OvertimeStatusValue
                        && b.RatingMark == a.RatingMark
                        && b.RatingMoney == a.RatingMoney
                        && b.SalaryCoeficient == a.SalaryCoeficient
                        && b.ServiceCoeficient == b.ServiceCoeficient
                        && b.ServiceIncomeBonus == a.ServiceIncomeBonus
                        //&& b.WorkDate == a.WorkDate
                        && b.Quantity == a.Quantity
                        && b.ConventionPoint == a.ConventionPoint
                        && b.ServiceBonus == a.ServiceBonus
                        && b.BillSalonId == a.BillSalonId
                        )
                    select a
                //).ToList()
                ;

                var ListDelete =
                    // (
                    from a in ListOld
                    where
                    !ListNew.Any
                    (
                        b =>
                        b.BillId == a.BillId
                        && b.ServiceId == a.ServiceId
                        && b.DepartmentId == a.DepartmentId
                        && b.StaffId == a.StaffId
                        && b.StaffLevelId == a.StaffLevelId
                        && b.OvertimeStatusValue == a.OvertimeStatusValue
                        && b.RatingMark == a.RatingMark
                        && b.RatingMoney == a.RatingMoney
                        && b.SalaryCoeficient == a.SalaryCoeficient
                        && b.ServiceCoeficient == b.ServiceCoeficient
                        && b.ServiceIncomeBonus == a.ServiceIncomeBonus
                        //&& b.WorkDate == a.WorkDate
                        && b.Quantity == a.Quantity
                        && b.ConventionPoint == a.ConventionPoint
                        && b.ServiceBonus == a.ServiceBonus
                        && b.BillSalonId == a.BillSalonId
                        )
                    select a
                                     // ).ToList()
                                     ;
                #endregion
                //var ListDelete = null;


                //var staffBillServiceDetailList = StaffBillServiceDetailRepoList.Result;
                //if (ListAdd.Count() == 0)
                //{
                //    return NoContent();
                //}
                StaffBillServiceDetailInforRepo.DeleteAndAdd(ListDelete, ListAdd);
                return Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });
                throw e;
            }
        }


    }
}
