﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using ReportProject.Utils;

namespace ReportProject.Controllers
{
    [Produces("application/json")]
    [Route("api/service-salon-type-config")]
    public class ServiceSalonTypeConfigController : Controller
    {
        private ILogger<ServiceSalonTypeConfigController> Logger { get; }
        private IConfigRepo ConfigRepo { get; set; }
        private IServiceSalonConfigRepo ServiceSalonConfigRepo { get; set; }
        private IServiceSalonTypeConfigRepo ServiceSalonTypeConfigRepo { get; set; }
        //constructor
        public ServiceSalonTypeConfigController(ILogger<ServiceSalonTypeConfigController> logger
            , IConfigRepo configRepo
            , IServiceSalonConfigRepo serviceSalonConfigRepo
            , IServiceSalonTypeConfigRepo serviceSalonTypeConfigRepo)
        {
            Logger = logger;
            ConfigRepo = configRepo;
            ServiceSalonConfigRepo = serviceSalonConfigRepo;
            ServiceSalonTypeConfigRepo = serviceSalonTypeConfigRepo;
        }

        /// <summary>
        ///  Insert or update
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> InsertOrUpdate()
        {
            try
            {
                // 1. Get data backup to table 
                var dataServiceSalonConfig = await ServiceSalonConfigRepo.GetList(a => a.IsDelete == false);
                if (dataServiceSalonConfig == null)
                {
                    return NoContent();
                }
                // backup to table BackUpTableServiceSalonConfig
                var list = new List<BackupTableServiceSalonConfig>();
                foreach (var item in dataServiceSalonConfig)
                {
                    var obj = new BackupTableServiceSalonConfig();
                    obj.SalonId = item.SalonId;
                    obj.ServiceId = item.ServiceId;
                    obj.DepartmentId = item.DepartmentId;
                    obj.ServiceCoefficient = item.ServiceCoefficient;
                    obj.ServiceBonus = item.ServiceBonus;
                    obj.CoefficientOvertimeHour = item.CoefficientOvertimeHour;
                    obj.CoefficientOvertimeDay = item.CoefficientOvertimeDay;
                    obj.CreatedTime = item.CreatedTime;
                    obj.ModifiedTime = item.ModifiedTime;
                    obj.IsDelete = item.IsDelete;
                    obj.IsPublish = item.IsPublish;
                    obj.BackupDate = DateTime.Now;
                    list.Add(obj);
                }
                // add list
                ServiceSalonConfigRepo.AddRange(list);
                // save
                await ServiceSalonConfigRepo.SaveChangeAsync();

                // 2. get data
                var listServiceSalonTypeConfig = await ServiceSalonTypeConfigRepo.GetListServiceSalonTypeConfig();

                if (listServiceSalonTypeConfig == null || listServiceSalonTypeConfig.Count() <= 0)
                {
                    return NoContent();
                }
                foreach (var item in listServiceSalonTypeConfig)
                {
                    // get data 
                    var data = dataServiceSalonConfig.Where(a => a.SalonId == item.SalonId
                                                                    && a.DepartmentId == item.Departmentd
                                                                    && a.ServiceId == item.ServiceId)
                                                        .FirstOrDefault();
                    if (data != null)
                    {
                        data.ServiceCoefficient = item.ServiceCoefficient;
                        data.ServiceBonus = item.ServiceBonus;
                        data.CoefficientOvertimeHour = item.CoefficientOvertimeHour;
                        data.CoefficientOvertimeDay = item.CoefficientOvertimeDay;
                        data.ModifiedTime = DateTime.Now;
                        ServiceSalonConfigRepo.Update(data);
                    }
                    else
                    {
                        data = new ServiceSalonConfig
                        {
                            SalonId = item.SalonId,
                            DepartmentId = item.Departmentd,
                            ServiceId = item.ServiceId,
                            ServiceCoefficient = item.ServiceCoefficient,
                            ServiceBonus = item.ServiceBonus,
                            CoefficientOvertimeHour = item.CoefficientOvertimeHour,
                            CoefficientOvertimeDay = item.CoefficientOvertimeDay,
                            CreatedTime = DateTime.Now,
                            IsDelete = false,
                            IsPublish = true
                        };
                        ServiceSalonConfigRepo.Add(data);
                    }
                }
                // save
                await ServiceSalonConfigRepo.SaveChangeAsync();
                // 
                return Ok(new { message = "success" });
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new
                {
                    message = e.Message
                });
            }
        }
    }
}