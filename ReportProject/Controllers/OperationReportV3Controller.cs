﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ReportProject.Models.JsonModels;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Interface;
using ReportProject.Utils;
using static APICheckout.Models.CustomeModel.OutPutModel;
using static ReportProject.Models.JsonModels.BillserviceJson;
using static ReportProject.Models.JsonModels.BookingJson;
using static ReportProject.Models.JsonModels.FlowTimeKeepingJoin;
using static ReportProject.Models.JsonModels.MonitorStaffErrorJson;
using static ReportProject.Models.JsonModels.OperationReportV3Json;

namespace ReportProject.Controllers
{
    [Produces("application/json")]
    [Route("api/operation-report-v3")]
    public class OperationReportV3Controller : Controller
    {
        private ISalonRepo SalonRepo { get; set; }
        private IBillServiceRepo BillServiceRepo { get; set; }
        private IFlowTimeKeepingRepo FlowTimeKeepingRepo { get; set; }
        private IBookingRepo BookingRepo { get; set; }
        private IBookHourRepo BookHourRepo { get; set; }
        private IStaffRepo StaffRepo { get; set; }
        private IFlowServiceRepo FlowServiceRepo { get; set; }
        private IFlowProductRepo FlowProductRepo { get; set; }
        private IMonitorStaffErrorRepo MonitorStaffErrorRepo { get; set; }
        private ITblConfigRepo TblConfigRepo { get; set; }
        private IServiceRepo ServiceRepo { get; set; }
        private IStatictisReportOperationRepo StatictisReportOperationRepo { get; set; }
        private IStoreProcedureRepo StoreProcedureRepo { get; set; }
        private IConfigRepo ConfigRepo { get; set; }
        private ICategoryRepo CategoryRepo { get; set; }
        private IOperationReportStatisticRepo OperationReportStatisticRepo { get; set; }
        private IOperationReportConfigRepo OperationReportConfigRepo { get; set; }
        private Respository.InterfaceV1.ISalonRepo SalonRepoV1 { get; set; }
        private Respository.InterfaceV1.IBillserviceRepo BillServiceRepoV1 { get; set; }
        private Respository.InterfaceV1.IFeedbackPointServiceRepo FeedbackPointServiceRepo { get; set; }
        private Respository.InterfaceV1.IMonitorStaffErrorRepo MonitorStaffErrorRepoV1 { get; set; }
        private Respository.InterfaceV1.IStaffRepo StaffRepoV1 { get; set; }
        private Respository.InterfaceV1.IPermissionSalonAreaRepo PermissionSalonAreaRepo { get; set; }


        private readonly IConfiguration Configuration;
        CultureInfo culture = new CultureInfo("vi-VN");
        private readonly DateTime TODAY = DateTime.Today;
        //private readonly DateTime TODAY = new DateTime(2019, 02, 01);

        public OperationReportV3Controller(ISalonRepo salonRepo,
            IBillServiceRepo billServiceRepo,
            IFlowTimeKeepingRepo flowTimeKeepingRepo,
            IBookingRepo bookingRepo,
            IBookHourRepo bookHourRepo,
            IStaffRepo staffRepo,
            IFlowServiceRepo flowServiceRepo,
            IFlowProductRepo flowProductRepo,
            IMonitorStaffErrorRepo monitorStaffErrorRepo,
            ITblConfigRepo tblConfigRepo,
            IServiceRepo serviceRepo,
            IStatictisReportOperationRepo statictisReportOperationRepo,
            IStoreProcedureRepo storeProcedureRepo,
            IConfigRepo configRepo,
            ICategoryRepo categoryRepo,
            IOperationReportStatisticRepo operationReportStatisticRepo,
            IOperationReportConfigRepo operationReportConfigRepo,
            IConfiguration configuration,
            Respository.InterfaceV1.ISalonRepo salonRepoV1,
            Respository.InterfaceV1.IBillserviceRepo billServiceRepoV1,
            Respository.InterfaceV1.IFeedbackPointServiceRepo feedbackPointServiceRepo,
            Respository.InterfaceV1.IMonitorStaffErrorRepo monitorStaffErrorRepoV1,
            Respository.InterfaceV1.IStaffRepo staffRepoV1,
            Respository.InterfaceV1.IPermissionSalonAreaRepo permissionSalonAreaRepo

            )

        {
            SalonRepo = salonRepo;
            BillServiceRepo = billServiceRepo;
            FlowTimeKeepingRepo = flowTimeKeepingRepo;
            BookingRepo = bookingRepo;
            BookHourRepo = bookHourRepo;
            StaffRepo = staffRepo;
            FlowServiceRepo = flowServiceRepo;
            FlowProductRepo = flowProductRepo;
            MonitorStaffErrorRepo = monitorStaffErrorRepo;
            TblConfigRepo = tblConfigRepo;
            ServiceRepo = serviceRepo;
            StatictisReportOperationRepo = statictisReportOperationRepo;
            StoreProcedureRepo = storeProcedureRepo;
            ConfigRepo = configRepo;
            CategoryRepo = categoryRepo;
            OperationReportStatisticRepo = operationReportStatisticRepo;
            OperationReportConfigRepo = operationReportConfigRepo;
            Configuration = configuration;
            SalonRepoV1 = salonRepoV1;
            BillServiceRepoV1 = billServiceRepoV1;
            FeedbackPointServiceRepo = feedbackPointServiceRepo;
            MonitorStaffErrorRepoV1 = monitorStaffErrorRepoV1;
            StaffRepoV1 = staffRepoV1;
            PermissionSalonAreaRepo = permissionSalonAreaRepo;
        }

        [HttpGet]
        public async Task<IActionResult> GetReportTodayStore([FromQuery] int salonId, int regionId, string dateFrom, string dateTo)
        {
            try
            {
                if ((salonId < 0 && regionId <= 0) || (salonId > 0 && regionId > 0))
                {
                    return BadRequest();
                }

                var from = DatetimeExtension.StringToDateTime(Constant.DATETIME2, dateFrom);
                var to = DatetimeExtension.StringToDateTime(Constant.DATETIME2, dateTo);

                if (from == null || to == null)
                {
                    return BadRequest(new { Message = "Invalid datetime" });
                }

                var salon = new List<TblSalon>();

                if (regionId > 0)
                {
                    salon = (await PermissionSalonAreaRepo.GetList(r => r.IsDelete == false && r.IsActive == true && r.StaffId == regionId))
                        .Select(r => new TblSalon { Id = r.SalonId ?? 0 }).ToList();
                }

                if (salonId >= 0 && regionId <= 0)
                {
                    var tblSalon = await SalonRepo.GetBySalonId(salonId);
                    salon = tblSalon.ToList();
                }

                if (!salon.Any())
                {
                    return NoContent();
                }

                var salonIds = salon.Select(a => a.Id).ToList();

                var returnData = new List<DataReportJson>();

                if (from >= DateTime.Today && to < DateTime.Today.AddDays(1))
                {
                    //var today =
                    //    DateTime.Today;

                    //var dataToday = await GetDataToday(salonId, regionId, salonIds, today, today.AddDays(1));

                    //if (!dataToday.Any())
                    //{
                    //    return NoContent();
                    //}

                    //returnData = dataToday.ToList();

                }
                else
                {
                    var dataPassDay = await GetPassDay(salonId, regionId, from, to.Value.AddDays(1), salonIds);

                    if (dataPassDay == null)
                    {
                        return NoContent();
                    }

                    returnData = dataPassDay.ToList();


                }

                return Ok(returnData);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost("statistic")]
        public async Task<IActionResult> StatisticReport()
        {
            try
            {
                var dateFrom = DateTime.Today;
                var dateTo = dateFrom;
                await Statistic(dateFrom, dateTo);
                return Ok(new { message = Constant.SUCCESS });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost("statistic-rerun")]
        public async Task<IActionResult> StatisticRerun([FromQuery] string from, string to)
        {
            try
            {
                if (DatetimeExtension.StringToDateTime(Constant.DATETIME2, from) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME, from) == null
                || DatetimeExtension.StringToDateTime(Constant.DATETIME2, to) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME, to) == null)
                {
                    return BadRequest();
                }

                var dateFrom = DateTime.ParseExact(from, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                var dateTo = DateTime.ParseExact(to, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                await Statistic(dateFrom, dateTo);
                return Ok(new { Message = "Success!" });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message + "At: " + (new StackTrace(e, true)).GetFrame(0).GetFileLineNumber());
            }
        }

        [HttpGet("sale-target")]
        public async Task<IActionResult> GetSaleTarget([FromQuery] int salonId, int regionId, string dateFrom, string dateTo)
        {
            try
            {
                if ((salonId < 0 && regionId <= 0) || (salonId > 0 && regionId > 0))
                {
                    return BadRequest(new { Message = "Invalid Param!" });
                }

                var from = DatetimeExtension.StringToDateTime(Constant.DATETIME2, dateFrom);
                var to = DatetimeExtension.StringToDateTime(Constant.DATETIME2, dateTo);

                if (from == null || to == null)
                {
                    return BadRequest(new { Message = "Invalid datetime!" });
                }

                if (from.Value.Month != to.Value.Month)
                {
                    return Ok(new { Message = "!" });
                }

                var tblSalon = await SalonRepo.GetBySalonId(salonId);

                if (!tblSalon.Any())
                {
                    return NoContent();
                }

                var saleConfig = await OperationReportConfigRepo.GetSaleTargets(salonId, regionId, from.Value);

                if (saleConfig == null)
                {
                    return NoContent();
                }

                var data = JsonConvert.DeserializeObject<object>(saleConfig.DataJson);

                var returnData = new SaleConfigJson
                {
                    SalonType = saleConfig.SalonType,
                    SaleConfig = data
                };

                return Ok(returnData);
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        [HttpGet("region")]
        public async Task<IActionResult> GetRegion()
        {
            try
            {
                var keys = "region1,region2,region3,region4,region5,region6,region7,region8,region9,region10,region11";


                var config = await ConfigRepo.GetByKeys(keys.Split(",").ToList());

                if (!config.Any())
                {
                    return NoContent();
                }

                return Ok(config.Select(a => new { regionId = Convert.ToInt16(a.Value), regionName = a.Label }).OrderBy(a => a.regionId));
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        [HttpGet("list-asm")]
        public async Task<IActionResult> ListAsm()
        {
            try
            {
                var listStaff = await StaffRepoV1.GetList(r => r.IsDelete == 0 && r.Active == 1 && r.Type == 17);
                if (listStaff.Any())
                {
                    return Ok(listStaff.Select(r => new { Id = r.Id, FullName = r.Fullname }));
                }
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
        }

        private async Task Statistic(DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var from = Convert.ToDateTime(dateFrom, culture);
                var to = Convert.ToDateTime(dateTo, culture);
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = from; date <= to; date = date.AddDays(1))
                    allDates.Add(date);
                var salon = await SalonRepo.GetBySalonId(0);
                var salonIds = salon.Select(a => a.Id).ToList();

                var statistic = await OperationReportStatisticRepo.GetByDay(0, 0, from, to.AddDays(1));

                if (statistic != null)
                {
                    OperationReportStatisticRepo.Delete(statistic);
                }


                var statisticData = new List<OperationReportStatistic>();

                foreach (var workDate in allDates)
                {
                    var dataByDay = await GetDataToday(0, 0, salonIds, workDate, workDate.AddDays(1));

                    var revenue = dataByDay.Select(a => a.Revenue);

                    var data = dataByDay.Select(a => new OperationReportStatistic
                    {
                        SalonId = a.SalonId,
                        TotalBill = a.Customer.TotalBill,
                        BillOldCustomer = a.Customer.OldCustomer,
                        BillNewCustomer = a.Customer.NewCustomer,
                        BookInAdvance = a.Customer.BookInAdvance,
                        CancelBook = a.Customer.CancelBook,
                        BookAll = a.Customer.BookAll,
                        TotalBuyShineMember = a.ShineMember.TotalBuyShineMember,
                        NewCustomerMember = a.ShineMember.BillNewCustomer,
                        OldCustomerMember = a.ShineMember.BillOldCustomer,
                        ShineMemberComeBack = a.ShineMember.ShineMemberComeBack,
                        TotalIncomePerShineMember = a.ShineMember.TotalIncomePerShineMember,
                        TotalStylistWorkTime = a.Productivity.TotalStylistWorkTime,
                        TotalSkinnerWorkTime = a.Productivity.TotalSkinnerWorkTime,
                        AvgRating = a.Quality.AvgRating,
                        Mark1and2 = a.Quality.Mark1and2,
                        StaffError = a.Quality.StaffError,
                        LongWait = a.Quality.Wait,
                        BillWithoutImage = a.Quality.BillWithoutImage,
                        Revenue = JsonConvert.SerializeObject(revenue.SelectMany(c => c.ListService).Where(b => b.SalonId == a.SalonId)),
                        Date = workDate,
                        SpecialCustomer = a.Quality.SpecialCustomer,
                        TotalIncome = a.Revenue.TotalRevenue
                    }).ToList();

                    statisticData.AddRange(data.Where(a => a.TotalBill != null));
                }

                if (statisticData.Any())
                {
                    await OperationReportStatisticRepo.AddAsync(statisticData);
                    await OperationReportStatisticRepo.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private async Task<IEnumerable<DataReportJson>> GetDataToday(int salonId, int regionId, List<int> salonIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {

                //List<int?> salonIds = salonIdsNonNull.Cast<int?>().ToList();

                var configService = Configuration.GetSection("AppConstants:config_key_service").Value;
                var configProduct = Configuration.GetSection("AppConstants:config_key_product").Value;

                var customers = StoreProcedureRepo.GetCustomerInfo(dateFrom, dateTo, salonId, regionId);
                var memberShips = StoreProcedureRepo.GetMemberShipInfo(dateFrom, dateTo, salonId, regionId);
                var productivities = StoreProcedureRepo.GetProductivityInfo(dateFrom, dateTo, salonId, regionId);
                var qualities = StoreProcedureRepo.GetQualityInfo(dateFrom, dateTo, salonId, regionId);


                var revenues = StoreProcedureRepo.GetRevenueInfo(dateFrom, dateTo, salonId, regionId);
                var lngs = StoreProcedureRepo.GetLNGInfo(dateFrom, dateTo, salonId, regionId, salonIds);
                var serviesCategoryIds = ConfigRepo.GetByKey(configService);
                var productCategoryIds = ConfigRepo.GetByKey(configProduct);
                var totalIncomes = StoreProcedureRepo.GetTotalIncome(dateFrom, dateTo, salonId, regionId);
                Task.WaitAll(customers, memberShips, productivities, qualities, revenues, lngs, serviesCategoryIds, serviesCategoryIds, totalIncomes);


                var customer = customers.Result;
                var memberShip = memberShips.Result;
                var productivity = productivities.Result;
                var quality = qualities.Result;
                var revenue = revenues.Result;
                var lng = lngs.Result;
                var totalIncome = totalIncomes.Result;

                var serviceCategoryId = serviesCategoryIds.Result.Value.Split(',').Select(Int32.Parse).ToList();
                var productCategoryId = productCategoryIds.Result.Value.Split(',').Select(Int32.Parse).ToList();
                var categoryIds = serviceCategoryId.Concat(productCategoryId).ToList();
                var returnData = new List<DataReportJson>();
                var listQuantity = await GetQuantity(salonIds, dateFrom, dateTo);

                foreach (var id in salonIds)
                {
                    var salonRevenue = revenue.Where(a => a.SalonId == id).ToList();
                    var revenueIds = salonRevenue.Select(a => a.CategoryId).ToList();
                    var remainCategoryIds = categoryIds.Except(revenueIds);

                    var remainCategory = await CategoryRepo.GetByIdsAsync(remainCategoryIds);

                    var finalRevenue = salonRevenue.Concat(remainCategory).OrderByDescending(a => a.Type).ToList();

                    var productivityFinal = productivity.FirstOrDefault(a => a.SalonId == id);

                    var totalRevenue = new TotalRevenueJson
                    {
                        TotalRevenue = totalIncome.FirstOrDefault(a => a.SalonId == id).TotalIncome,
                        ListService = finalRevenue
                    };

                    var member = memberShip.FirstOrDefault(a => a.SalonId == id);

                    productivityFinal.TotalSalon = salonIds.Count();
                    productivityFinal.TotalDay = dateTo.Subtract(dateFrom).Days;

                    var lngJson = lng.FirstOrDefault(a => a.SalonId == id);

                    if (lngJson == null)
                    {
                        lngJson = new LNGJson
                        {
                            DirectFee = 0,
                            ManageFee = 0,
                            SalonFee = 0
                        };
                    }
                    var qualityResult = new Quality();
                    var qualityObj = quality.FirstOrDefault(a => a.SalonId == id);
                    var qualitySalon = listQuantity.Where(r => r.SalonId == id).FirstOrDefault();

                    qualityResult.AvgRating = qualityObj.AvgRating;
                    qualityResult.BillWithoutImage = qualityObj.BillWithoutImage;
                    qualityResult.ErrorCurling = qualitySalon != null ? qualitySalon.TotalErrorScscCurling : 0;
                    qualityResult.ErrorMissImage = qualitySalon != null ? qualitySalon.ErrorImage : 0;
                    qualityResult.ErrorSCSC = qualitySalon != null ? qualitySalon.TotalErorrScsc : 0;
                    qualityResult.Feedback = qualitySalon != null ? qualitySalon.Feedback : 0;
                    qualityResult.Mark1and2 = qualityObj.Mark1and2;
                    qualityResult.SalonId = qualityObj.SalonId;
                    qualityResult.SpecialCustomer = qualityObj.SpecialCustomer;
                    qualityResult.StaffError = qualitySalon != null ? qualitySalon.TotalMonitorStaff : 0;
                    qualityResult.TNDV = qualityObj.TNDV;
                    qualityResult.Wait = qualityObj.Wait;

                    var data = new DataReportJson
                    {
                        SalonId = id,
                        Customer = customer.FirstOrDefault(a => a.SalonId == id),
                        ShineMember = member,
                        Productivity = productivityFinal,
                        Quality = qualityResult,
                        Revenue = totalRevenue,
                        LNG = lngJson
                    };

                    if (data.Customer.TotalBill != null)
                    {
                        returnData.Add(data);
                    }

                }

                return returnData.OrderBy(a => a.SalonId);

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private async Task<IEnumerable<DataReportJson>> GetPassDay(int salonId, int regionId, DateTime? dateFrom, DateTime? dateTo, List<int> salonIds)
        {
            try
            {
                var listQuantity = await GetQuantity(salonIds, dateFrom.Value, dateTo.Value);
                var statistic = await OperationReportStatisticRepo.GetByDay(salonId, regionId, dateFrom, dateTo);

                if (statistic == null)
                {
                    return null;
                }

                var lng = await StoreProcedureRepo.GetLNGInfo((DateTime)dateFrom, (DateTime)dateTo, salonId, regionId, salonIds);

                var returnData = new List<DataReportJson>();

                var configService = Configuration.GetSection("AppConstants:config_key_service").Value;
                var configProduct = Configuration.GetSection("AppConstants:config_key_product").Value;

                var serviesCategoryIds = await ConfigRepo.GetByKey(configService);
                var productCategoryIds = await ConfigRepo.GetByKey(configProduct);

                var serviceCategoryId = serviesCategoryIds.Value.Split(',').Select(Int32.Parse).ToList();
                var productCategoryId = productCategoryIds.Value.Split(',').Select(Int32.Parse).ToList();

                var categoryIds = serviceCategoryId.Concat(productCategoryId).ToList();
                var listSalon = await SalonRepo.GetList(r => true);
                foreach (var id in salonIds)
                {
                    var salonStatistic = statistic.Where(a => a.SalonId == id).ToList();
                    var lngSalon = lng.Where(a => a.SalonId == id).ToList();
                    var quantitySalon = listQuantity.Where(r => r.SalonId == id).FirstOrDefault();

                    if (salonStatistic != null)
                    {
                        var customer = new CustomerJson
                        {
                            TotalBill = salonStatistic.Sum(a => a.TotalBill),
                            OldCustomer = salonStatistic.Sum(a => a.BillOldCustomer),
                            NewCustomer = salonStatistic.Sum(a => a.BillNewCustomer),
                            BookInAdvance = salonStatistic.Sum(a => a.BookInAdvance),
                            CancelBook = salonStatistic.Sum(a => a.CancelBook),
                            BookAll = salonStatistic.Sum(a => a.BookAll)
                        };

                        var shineMember = new ShineMemberJson
                        {
                            TotalBuyShineMember = salonStatistic.Sum(a => a.TotalBuyShineMember),
                            BillNewCustomer = salonStatistic.Sum(a => a.NewCustomerMember),
                            BillOldCustomer = salonStatistic.Sum(a => a.OldCustomerMember),
                            ShineMemberComeBack = salonStatistic.Sum(a => a.ShineMemberComeBack),
                            TotalIncomePerShineMember = salonStatistic.Sum(a => a.TotalIncomePerShineMember == null ? 0 : a.TotalIncomePerShineMember),
                        };

                        var productivity = new ProductivityJson
                        {
                            TotalSalon = salonIds.Count(),
                            TotalDay = dateTo.Value.Subtract(dateFrom.Value).Days,
                            TotalStylistWorkTime = salonStatistic.Sum(a => a.TotalStylistWorkTime),
                            TotalSkinnerWorkTime = salonStatistic.Sum(a => a.TotalSkinnerWorkTime)
                        };


                        var quality = new Quality
                        {
                            AvgRating = salonStatistic.Sum(a => a.AvgRating == null ? 0 : a.AvgRating) ?? 0,
                            Mark1and2 = salonStatistic.Sum(a => a.Mark1and2 == null ? 0 : a.Mark1and2) ?? 0,
                            StaffError = quantitySalon != null ? quantitySalon.TotalMonitorStaff : 0,
                            Wait = salonStatistic.Sum(a => a.LongWait == null ? 0 : a.LongWait) ?? 0,
                            BillWithoutImage = salonStatistic.Sum(a => a.BillWithoutImage == null ? 0 : a.BillWithoutImage) ?? 0,
                            SpecialCustomer = salonStatistic.Sum(a => a.SpecialCustomer == null ? 0 : a.SpecialCustomer),
                            ErrorCurling = quantitySalon != null ? quantitySalon.TotalErrorScscCurling : 0,
                            ErrorMissImage = quantitySalon != null ? quantitySalon.ErrorImage : 0,
                            ErrorSCSC = quantitySalon != null ? quantitySalon.TotalErorrScsc : 0,
                            Feedback = quantitySalon != null ? quantitySalon.Feedback : 0
                        };

                        var revenueAll = new List<RevenueStatisticJson>();

                        foreach (var item in salonStatistic.Select(a => a.Revenue))
                        {
                            List<RevenueStatisticJson> revenue = JsonConvert.DeserializeObject<List<RevenueStatisticJson>>(item);

                            revenueAll.AddRange(revenue);
                        }

                        var sumRevenue = revenueAll.GroupBy(a => new { a.CategoryId, a.Name, a.Type, a.BillCount, a.Quantity }).Select(a => new RevenueJson
                        {
                            CategoryId = a.Key.CategoryId,
                            Name = a.Key.Name,
                            Income = a.Sum(b => b.Income),
                            Type = a.Key.Type,
                            BillCount = a.Key.BillCount,
                            Quantity = a.Key.Quantity
                        }).ToList();


                        var lngAll = new LNGJson
                        {
                            DirectFee = lngSalon.Sum(a => a.DirectFee),
                            ManageFee = lngSalon.Sum(a => a.ManageFee),
                            SalonFee = lngSalon.Sum(a => a.SalonFee),
                        };


                        var revenueIds = sumRevenue.Select(a => a.CategoryId).ToList();
                        var remainCategoryIds = categoryIds.Except(revenueIds);

                        var remainCategory = await CategoryRepo.GetByIdsAsync(remainCategoryIds);

                        var finalRevenue = sumRevenue.Concat(remainCategory).OrderByDescending(a => a.Type).ToList();

                        var totalRevenue = new TotalRevenueJson
                        {
                            TotalRevenue = salonStatistic.Sum(a => a.TotalIncome == null ? 0 : a.TotalIncome) ?? 0,
                            TotalBill = salonStatistic.Sum(a => a.TotalBill == null ? 0 : a.TotalBill) ?? 0,
                            ListService = finalRevenue
                        };

                        var data = new DataReportJson
                        {
                            ShortName = listSalon.Where(r => r.Id == id).Select(r => r.ShortName).FirstOrDefault(),
                            SalonId = id,
                            Customer = customer,
                            ShineMember = shineMember,
                            Productivity = productivity,
                            Quality = quality,
                            Revenue = totalRevenue,
                            LNG = lngAll
                        };

                        if (data.Customer.TotalBill != 0)
                        {
                            returnData.Add(data);
                        }
                    }
                }

                return returnData;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        private async Task<List<OutReportExperiencePoint>> GetQuantity(List<int> listSalonId, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                // List Id uốn
                var listUonId = new List<int>() { 16 };
                var listBillService = await BillServiceRepoV1.GetListStaffBill(dateFrom, dateTo, listUonId, listSalonId);
                var totalBill = listBillService.GroupBy(r => new { r.SalonId }).Select(r => new
                {
                    SalonId = (int?)r.Key.SalonId
                }).ToList();
                // Loi giam sat
                var listError = await MonitorStaffErrorRepoV1.GetListSalonError(dateFrom, dateTo, listSalonId);
                // loi scsc
                var listTotalScsc0 = listBillService.Where(r => r.PointError == 0 && r.ImageError == false).GroupBy(r => r.SalonId).Select(r => new
                {
                    SalonId = (int?)r.Key,
                    TotalPointScsc0 = (int?)r.Count()
                }).ToList();
                // Loi uon
                var listTotalCurling0 = listBillService.Where(r => r.TotalPointSCSCCurling == 0 && r.ServiceIdIsCurling > 0 && r.IsImageCurling > 0 && r.ImageErrorCurling != true)
                    .GroupBy(r => r.SalonId).Select(r => new
                    {
                        SalonId = (int?)r.Key,
                        TotalPointCurling0 = (int?)r.Count()
                    }).ToList();

                // Tong bill anh uon
                var totalBillNotImageCurling = listBillService.Where(r => r.IsImageCurling != 1 && r.ServiceIdIsCurling > 0)
                    .GroupBy(r => r.SalonId).Select(r => new
                    {
                        SalonId = (int?)r.Key,
                        TotalImageCurling = (int?)r.Count()
                    }).ToList();
                // Tong mo lech uon
                var totalBillMoLechCurling = listBillService.Where(r => r.IsImageCurling == 1 && r.ImageErrorCurling == true)
                    .GroupBy(r => r.SalonId).Select(r => new
                    {
                        SalonId = (int?)r.Key,
                        TotalMoLechUon = (int?)r.Count()
                    }).ToList();
                // Phan hoi khach hang
                var listFeedbackPointRepo = await FeedbackPointServiceRepo.GetList(r => r.IsDelete == false && r.WorkDate >= dateFrom.Date && r.WorkDate <= dateTo.AddDays(-1) && listSalonId.Contains(r.SalonId ?? 0));
                var listFeedbackPoint = listFeedbackPointRepo.GroupBy(r => r.SalonId).Select(r => new
                {
                    SalonId = (int?)r.Key,
                    TotalFeedback = (int?)r.Sum(g => g.FeedbackQuantity)
                }).ToList();
                // bill khong anh scsc
                var listTotalNotImageScsc = listBillService.Where(r => r.IsImages != true).GroupBy(r => r.SalonId).Select(r => new
                {
                    SalonId = (int?)r.Key,
                    TotalNotImageScsc = (int?)r.Count()
                }).ToList();
                // Tong mo lech scsc
                var totalMoLechScsc = listBillService.Where(r => r.IsImages == true && r.ImageError == true)
                    .GroupBy(r => r.SalonId).Select(r => new
                    {
                        SalonId = (int?)r.Key,
                        TotalMoLechScsc = (int?)r.Count()
                    }).ToList();

                var list = (
                            from a in totalBill
                            join f in listError on a.SalonId equals f.SalonId into ff
                            from f in ff.DefaultIfEmpty()
                            join g in listTotalScsc0 on a.SalonId equals g.SalonId into gg
                            from g in gg.DefaultIfEmpty()
                            join h in listTotalCurling0 on a.SalonId equals h.SalonId into hh
                            from h in hh.DefaultIfEmpty()
                            join i in totalMoLechScsc on a.SalonId equals i.SalonId into ii
                            from i in ii.DefaultIfEmpty()
                            join j in totalBillMoLechCurling on a.SalonId equals j.SalonId into jj
                            from j in jj.DefaultIfEmpty()
                            join n in listTotalNotImageScsc on a.SalonId equals n.SalonId into nn
                            from n in nn.DefaultIfEmpty()
                            join q in totalBillNotImageCurling on a.SalonId equals q.SalonId into qq
                            from q in qq.DefaultIfEmpty()
                            join r in listFeedbackPoint on a.SalonId equals r.SalonId into rr
                            from r in rr.DefaultIfEmpty()

                            select new OutReportExperiencePoint
                            {
                                SalonId = a.SalonId,
                                TotalErorrScsc = g != null ? g.TotalPointScsc0 : 0,
                                TotalErrorScscCurling = h != null ? h.TotalPointCurling0 : 0,
                                ErrorImage = (n != null ? n.TotalNotImageScsc : 0) + (q != null ? q.TotalImageCurling : 0) + (i != null ? i.TotalMoLechScsc : 0) + (j != null ? j.TotalMoLechUon : 0),
                                Feedback = r != null ? r.TotalFeedback : 0,
                                TotalMonitorStaff = f != null ? f.TotalError : 0
                            }).OrderBy(r => r.OrderSalon).ToList();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #region Get Today New

        [HttpGet("revenue")]
        public async Task<IActionResult> GetRevenue([FromQuery] int salonId, int regionId, string dateFrom, string dateTo)
        {
            try
            {
                if ((salonId < 0 && regionId <= 0) || (salonId > 0 && regionId > 0))
                {
                    return BadRequest();
                }

                var from = DatetimeExtension.StringToDateTime(Constant.DATETIME2, dateFrom);
                var to = DatetimeExtension.StringToDateTime(Constant.DATETIME2, dateTo);

                if (from == null || to == null)
                {
                    return BadRequest(new { Message = "Invalid datetime" });
                }

                var salon = new List<TblSalon>();

                if (regionId > 0)
                {
                    salon = (await PermissionSalonAreaRepo.GetList(r => r.IsDelete == false && r.IsActive == true && r.StaffId == regionId))
                        .Select(r => new TblSalon { Id = r.SalonId ?? 0 }).ToList();
                }

                if (salonId >= 0 && regionId <= 0)
                {
                    var tblSalon = await SalonRepo.GetBySalonId(salonId);
                    salon = tblSalon.ToList();
                }

                if (!salon.Any())
                {
                    return NoContent();
                }

                var salonIds = salon.Select(a => a.Id).ToList();

                var returnData = new List<DataReportJson>();


                //if (from >= DateTime.Today && to < DateTime.Today.AddDays(1))
                //{
                //var today = DateTime.Today;
                var today = TODAY;

                var dataToday = await GetRevenueInfo(salonId, regionId, salonIds, today, today.AddDays(1));

                if (!dataToday.Any())
                {
                    return NoContent();
                }
                var listSalon = await SalonRepo.GetList(r => true);
                var data = (from a in dataToday
                            join b in listSalon on a.SalonId equals b.Id
                            select new
                            {
                                SalonId = a.SalonId,
                                ShortName = b.ShortName,
                                Revenue = a.Revenue
                            }).ToList();
                return Ok(data);
                //}
                //else
                //{
                //    var dataPassDay = await GetPassDay(salonId, regionId, from, to.Value.AddDays(1), salonIds);

                //    if (dataPassDay == null)
                //    {
                //        return NoContent();
                //    }

                //    returnData = dataPassDay.ToList();

                //    return Ok(returnData);
                //}


            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("productivity")]
        public async Task<IActionResult> GetProductivity([FromQuery] int salonId, int regionId, string dateFrom, string dateTo)
        {
            try
            {
                if ((salonId < 0 && regionId <= 0) || (salonId > 0 && regionId > 0))
                {
                    return BadRequest();
                }

                var from = DatetimeExtension.StringToDateTime(Constant.DATETIME2, dateFrom);
                var to = DatetimeExtension.StringToDateTime(Constant.DATETIME2, dateTo);

                if (from == null || to == null)
                {
                    return BadRequest(new { Message = "Invalid datetime" });
                }

                var salon = new List<TblSalon>();

                if (regionId > 0)
                {
                    salon = (await PermissionSalonAreaRepo.GetList(r => r.IsDelete == false && r.IsActive == true && r.StaffId == regionId))
                        .Select(r => new TblSalon { Id = r.SalonId ?? 0 }).ToList();
                }

                if (salonId >= 0 && regionId <= 0)
                {
                    var tblSalon = await SalonRepo.GetBySalonId(salonId);
                    salon = tblSalon.ToList();
                }

                if (!salon.Any())
                {
                    return NoContent();
                }

                var salonIds = salon.Select(a => a.Id).ToList();

                var returnData = new List<DataReportJson>();

                //if (from >= DateTime.Today && to < DateTime.Today.AddDays(1))
                //{
                var today = TODAY;
                //Convert.ToDateTime("01-02-2019", culture);

                var dataToday = await GetProductivityInfo(salonId, regionId, salonIds, today, today.AddDays(1));

                if (!dataToday.Any())
                {
                    return NoContent();
                }

                return Ok(dataToday);
                //}
                //else
                //{
                //    return BadRequest(new { Message = "Date input must be today!" });
                //}


            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("customer-info")]
        public async Task<IActionResult> GetCustomer([FromQuery] int salonId, int regionId, string dateFrom, string dateTo)
        {
            try
            {
                if ((salonId < 0 && regionId <= 0) || (salonId > 0 && regionId > 0))
                {
                    return BadRequest();
                }

                var from = DatetimeExtension.StringToDateTime(Constant.DATETIME2, dateFrom);
                var to = DatetimeExtension.StringToDateTime(Constant.DATETIME2, dateTo);

                if (from == null || to == null)
                {
                    return BadRequest(new { Message = "Invalid datetime" });
                }

                var salon = new List<TblSalon>();

                if (regionId > 0)
                {
                    salon = (await PermissionSalonAreaRepo.GetList(r => r.IsDelete == false && r.IsActive == true && r.StaffId == regionId))
                       .Select(r => new TblSalon { Id = r.SalonId ?? 0 }).ToList();
                }

                if (salonId >= 0 && regionId <= 0)
                {
                    var tblSalon = await SalonRepo.GetBySalonId(salonId);
                    salon = tblSalon.ToList();
                }

                if (!salon.Any())
                {
                    return NoContent();
                }

                var salonIds = salon.Select(a => a.Id).ToList();

                var returnData = new List<DataReportJson>();

                //if (from >= DateTime.Today && to < DateTime.Today.AddDays(1))
                //{
                var today = TODAY;
                //Convert.ToDateTime("01-02-2019", culture);

                var dataToday = await GetCustomersInfo(salonId, regionId, salonIds, today, today.AddDays(1));

                if (!dataToday.Any())
                {
                    return NoContent();
                }

                return Ok(dataToday);
                //}
                //else
                //{
                //    return BadRequest(new { Message = "Date input must be today!" });
                //}
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("shine-member")]
        public async Task<IActionResult> GetShineMember([FromQuery] int salonId, int regionId, string dateFrom, string dateTo)
        {
            try
            {
                if ((salonId < 0 && regionId <= 0) || (salonId > 0 && regionId > 0))
                {
                    return BadRequest();
                }

                var from = DatetimeExtension.StringToDateTime(Constant.DATETIME2, dateFrom);
                var to = DatetimeExtension.StringToDateTime(Constant.DATETIME2, dateTo);

                if (from == null || to == null)
                {
                    return BadRequest(new { Message = "Invalid datetime" });
                }

                var salon = new List<TblSalon>();

                if (regionId > 0)
                {
                    salon = (await PermissionSalonAreaRepo.GetList(r => r.IsDelete == false && r.IsActive == true && r.StaffId == regionId))
                         .Select(r => new TblSalon { Id = r.SalonId ?? 0 }).ToList();
                }

                if (salonId >= 0 && regionId <= 0)
                {
                    var tblSalon = await SalonRepo.GetBySalonId(salonId);
                    salon = tblSalon.ToList();
                }

                if (!salon.Any())
                {
                    return NoContent();
                }

                var salonIds = salon.Select(a => a.Id).ToList();

                var returnData = new List<DataReportJson>();

                //if (from >= DateTime.Today && to < DateTime.Today.AddDays(1))
                //{
                var today = TODAY;

                var dataToday = await GetMemberShipInfo(salonId, regionId, salonIds, today, today.AddDays(1));

                if (!dataToday.Any())
                {
                    return NoContent();
                }

                return Ok(dataToday);
                //}
                //else
                //{
                //    return BadRequest(new { Message = "Date input must be today!" });
                //}


            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("quality")]
        public async Task<IActionResult> GetQuality([FromQuery] int salonId, int regionId, string dateFrom, string dateTo)
        {
            try
            {
                if ((salonId < 0 && regionId <= 0) || (salonId > 0 && regionId > 0))
                {
                    return BadRequest();
                }

                var from = DatetimeExtension.StringToDateTime(Constant.DATETIME2, dateFrom);
                var to = DatetimeExtension.StringToDateTime(Constant.DATETIME2, dateTo);

                if (from == null || to == null)
                {
                    return BadRequest(new { Message = "Invalid datetime" });
                }

                var salon = new List<TblSalon>();

                if (regionId > 0)
                {
                    salon = (await PermissionSalonAreaRepo.GetList(r => r.IsDelete == false && r.IsActive == true && r.StaffId == regionId))
                         .Select(r => new TblSalon { Id = r.SalonId ?? 0 }).ToList();
                }

                if (salonId >= 0 && regionId <= 0)
                {
                    var tblSalon = await SalonRepo.GetBySalonId(salonId);
                    salon = tblSalon.ToList();
                }

                if (!salon.Any())
                {
                    return NoContent();
                }

                var salonIds = salon.Select(a => a.Id).ToList();

                var returnData = new List<DataReportJson>();

                //if (from >= DateTime.Today && to < DateTime.Today.AddDays(1))
                //{
                var today = TODAY;

                var dataToday = await GetQualityInfo(salonId, regionId, salonIds, today, today.AddDays(1));

                if (!dataToday.Any())
                {
                    return NoContent();
                }

                return Ok(dataToday);
                //}
                //else
                //{
                //    return BadRequest(new { Message = "Date input must be today!" });
                //}


            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("statistic-customer-info")]
        public async Task<IActionResult> PostStatisticCustomerInfo()
        {
            try
            {
                //string dateFrom = "02-01-2019";
                //string dateTo = dateFrom;

                //DateTime from = Convert.ToDateTime(dateFrom, culture);
                //DateTime to = Convert.ToDateTime(dateTo, culture);

                DateTime from = DateTime.Today;
                DateTime to = DateTime.Today;

                await StatisticCustomerInfo(from, to);
                return Ok(new { message = Constant.SUCCESS });

            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPut("statistic-membership-info")]
        public async Task<IActionResult> PutStatisticMembershipInfo()
        {
            try
            {
                //string dateFrom = "02-01-2019";
                //string dateTo = dateFrom;

                //DateTime from = Convert.ToDateTime(dateFrom, culture);
                //DateTime to = Convert.ToDateTime(dateTo, culture);

                DateTime from = DateTime.Today;
                DateTime to = DateTime.Today;

                await StatisticMembershipsInfo(from, to);
                return Ok(new { message = Constant.SUCCESS });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPut("statistic-productivity-info")]
        public async Task<IActionResult> PutStatisticProductivityInfo()
        {
            try
            {
                //string dateFrom = "02-01-2019";
                //string dateTo = dateFrom;

                //DateTime from = Convert.ToDateTime(dateFrom, culture);
                //DateTime to = Convert.ToDateTime(dateTo, culture);

                DateTime from = DateTime.Today;
                DateTime to = DateTime.Today;

                await StatisticProductivityInfo(from, to);
                return Ok(new { message = Constant.SUCCESS });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPut("statistic-revenue-info")]
        public async Task<IActionResult> PutStatisticRevenueInfo()
        {
            try
            {
                //string dateFrom = "02-01-2019";
                //string dateTo = dateFrom;

                //DateTime from = Convert.ToDateTime(dateFrom, culture);
                //DateTime to = Convert.ToDateTime(dateTo, culture);

                DateTime from = DateTime.Today;
                DateTime to = DateTime.Today;

                await StatisticRevenueInfo(from, to);
                return Ok(new { message = Constant.SUCCESS });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPut("statistic-quality-info")]
        public async Task<IActionResult> PutStatisticQualityInfo()
        {
            try
            {

                DateTime from = DateTime.Today;
                DateTime to = DateTime.Today;

                await StatisticQualityInfo(from, to);
                return Ok(new { message = Constant.SUCCESS });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        #region Rerun
        [HttpPost("statistic-customer-info/rerun")]
        public async Task<IActionResult> PostStatisticCustomerInfoRerun([FromQuery] string from, string to)
        {
            try
            {
                if (DatetimeExtension.StringToDateTime(Constant.DATETIME2, from) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME, from) == null
                || DatetimeExtension.StringToDateTime(Constant.DATETIME2, to) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME, to) == null)
                {
                    return BadRequest();
                }

                var dateFrom = DateTime.ParseExact(from, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                var dateTo = DateTime.ParseExact(to, @"dd/MM/yyyy", CultureInfo.InvariantCulture); ;
                await StatisticCustomerInfo(dateFrom, dateTo);
                return Ok(new { Message = "Success!" });

            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPut("statistic-membership-info/rerun")]
        public async Task<IActionResult> PutStatisticMembershipInfoRerun([FromQuery] string from, string to)
        {
            try
            {
                if (DatetimeExtension.StringToDateTime(Constant.DATETIME2, from) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME, from) == null
                || DatetimeExtension.StringToDateTime(Constant.DATETIME2, to) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME, to) == null)
                {
                    return BadRequest();
                }

                var dateFrom = DateTime.ParseExact(from, @"dd/MM/yyyy", CultureInfo.InvariantCulture); ;
                var dateTo = DateTime.ParseExact(to, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                await StatisticMembershipsInfo(dateFrom, dateTo);
                return Ok(new { Message = "Success!" });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPut("statistic-productivity-info/rerun")]
        public async Task<IActionResult> PutStatisticProductivityInfoRerun([FromQuery] string from, string to)
        {
            try
            {
                if (DatetimeExtension.StringToDateTime(Constant.DATETIME2, from) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME, from) == null
                || DatetimeExtension.StringToDateTime(Constant.DATETIME2, to) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME, to) == null)
                {
                    return BadRequest();
                }

                var dateFrom = DateTime.ParseExact(from, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                var dateTo = DateTime.ParseExact(to, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                await StatisticProductivityInfo(dateFrom, dateTo);
                return Ok(new { Message = "Success!" });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPut("statistic-revenue-info/rerun")]
        public async Task<IActionResult> PutStatisticRevenueInfoRerun([FromQuery] string from, string to)
        {
            try
            {
                if (DatetimeExtension.StringToDateTime(Constant.DATETIME2, from) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME, from) == null
                || DatetimeExtension.StringToDateTime(Constant.DATETIME2, to) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME, to) == null)
                {
                    return BadRequest();
                }

                var dateFrom = DateTime.ParseExact(from, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                var dateTo = DateTime.ParseExact(to, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                await StatisticRevenueInfo(dateFrom, dateTo);
                return Ok(new { Message = "Success!" });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPut("statistic-quality-info/rerun")]
        public async Task<IActionResult> PutStatisticQualityInfoRerun([FromQuery] string from, string to)
        {
            try
            {

                if (DatetimeExtension.StringToDateTime(Constant.DATETIME2, from) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME, from) == null
                || DatetimeExtension.StringToDateTime(Constant.DATETIME2, to) == null && DatetimeExtension.StringToDateTime(Constant.DATETIME, to) == null)
                {
                    return BadRequest();
                }

                var dateFrom = DateTime.ParseExact(from, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                var dateTo = DateTime.ParseExact(to, @"dd/MM/yyyy", CultureInfo.InvariantCulture);
                await StatisticQualityInfo(dateFrom, dateTo);
                return Ok(new { Message = "Success!" });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
        #endregion

        private async Task StatisticCustomerInfo(DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var from = Convert.ToDateTime(dateFrom, culture);
                var to = Convert.ToDateTime(dateTo, culture);
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = from; date <= to; date = date.AddDays(1))
                    allDates.Add(date);
                var salon = await SalonRepo.GetBySalonId(0);
                var salonIds = salon.Select(a => a.Id).ToList();

                List<OperationReportStatistic> statisticData = new List<OperationReportStatistic>();

                IEnumerable<OperationReportStatistic> statistics = await OperationReportStatisticRepo.GetByDay(0, 0, dateFrom, dateTo.AddDays(1));

                if (statistics != null)
                {
                    OperationReportStatisticRepo.Delete(statistics);
                }

                foreach (var workDate in allDates)
                {
                    IEnumerable<DataCustomerInfoJson> customerInfo = await GetCustomersInfo(0, 0, salonIds, workDate, workDate.AddDays(1));

                    if (customerInfo.Any())
                    {
                        var data = customerInfo.Select(a => new OperationReportStatistic
                        {
                            SalonId = a.SalonId,
                            TotalBill = a.Customer.TotalBill,
                            BillOldCustomer = a.Customer.OldCustomer,
                            BillNewCustomer = a.Customer.NewCustomer,
                            BookInAdvance = a.Customer.BookInAdvance,
                            CancelBook = a.Customer.CancelBook,
                            BookAll = a.Customer.BookAll,
                            TotalBuyShineMember = 0,
                            NewCustomerMember = 0,
                            OldCustomerMember = 0,
                            ShineMemberComeBack = 0,
                            TotalIncomePerShineMember = 0,
                            TotalStylistWorkTime = 0,
                            TotalSkinnerWorkTime = 0,
                            AvgRating = 0,
                            Mark1and2 = 0,
                            StaffError = 0,
                            LongWait = 0,
                            BillWithoutImage = 0,
                            Revenue = null,
                            Date = workDate,
                            SpecialCustomer = 0,
                            TotalIncome = 0
                        }).ToList();

                        statisticData.AddRange(data.Where(a => a.TotalBill != null));
                    }
                }

                if (statisticData.Any())
                {
                    await OperationReportStatisticRepo.AddAsync(statisticData);
                    await OperationReportStatisticRepo.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        private async Task StatisticMembershipsInfo(DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var from = Convert.ToDateTime(dateFrom, culture);
                var to = Convert.ToDateTime(dateTo, culture);
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = from; date <= to; date = date.AddDays(1))
                    allDates.Add(date);
                var salon = await SalonRepo.GetBySalonId(0);
                var salonIds = salon.Select(a => a.Id).ToList();

                IEnumerable<OperationReportStatistic> statistics = await OperationReportStatisticRepo.GetByDay(0, 0, from, to.AddDays(1));


                foreach (var workDate in allDates)
                {
                    var statistic = statistics.Where(a => a.Date == workDate).ToList();
                    IEnumerable<DataShineMemberJson> membershipInfo = await GetMemberShipInfo(0, 0, salonIds, workDate, workDate.AddDays(1));

                    if (statistic.Any())
                    {
                        foreach (var id in salonIds)
                        {
                            var statisticSalon = statistic.Where(a => a.SalonId == id).FirstOrDefault();
                            DataShineMemberJson info = membershipInfo.Where(a => a.SalonId == id).FirstOrDefault();

                            if (statisticSalon != null && info != null)
                            {
                                statisticSalon.TotalBuyShineMember = info.ShineMember.TotalBuyShineMember;
                                statisticSalon.NewCustomerMember = info.ShineMember.BillNewCustomer;
                                statisticSalon.OldCustomerMember = info.ShineMember.BillOldCustomer;
                                statisticSalon.ShineMemberComeBack = info.ShineMember.ShineMemberComeBack;
                                statisticSalon.TotalIncomePerShineMember = info.ShineMember.TotalIncomePerShineMember;

                                OperationReportStatisticRepo.Update(statisticSalon);
                                await OperationReportStatisticRepo.SaveChangesAsync();
                            }

                        }

                    }

                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        private async Task StatisticProductivityInfo(DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var from = Convert.ToDateTime(dateFrom, culture);
                var to = Convert.ToDateTime(dateTo, culture);
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = from; date <= to; date = date.AddDays(1))
                    allDates.Add(date);
                var salon = await SalonRepo.GetBySalonId(0);
                var salonIds = salon.Select(a => a.Id).ToList();

                var statistics = await OperationReportStatisticRepo.GetByDay(0, 0, from, to.AddDays(1));

                foreach (var workDate in allDates)
                {
                    var statistic = statistics.Where(a => a.Date == workDate).ToList();
                    var productivityInfo = await GetProductivityInfo(0, 0, salonIds, workDate, workDate.AddDays(1));

                    if (statistic.Any())
                    {
                        foreach (var id in salonIds)
                        {
                            var statisticSalon = statistic.FirstOrDefault(a => a.SalonId == id);
                            var info = productivityInfo.FirstOrDefault(a => a.SalonId == id);

                            if (statisticSalon != null && info != null)
                            {
                                statisticSalon.TotalStylistWorkTime = info.Productivity.TotalStylistWorkTime;
                                statisticSalon.TotalSkinnerWorkTime = info.Productivity.TotalSkinnerWorkTime;

                                OperationReportStatisticRepo.Update(statisticSalon);
                                await OperationReportStatisticRepo.SaveChangesAsync();
                            }
                        }


                    }

                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        private async Task StatisticRevenueInfo(DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var from = Convert.ToDateTime(dateFrom, culture);
                var to = Convert.ToDateTime(dateTo, culture);
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = from; date <= to; date = date.AddDays(1))
                    allDates.Add(date);
                var salon = await SalonRepo.GetBySalonId(0);
                var salonIds = salon.Select(a => a.Id).ToList();

                IEnumerable<OperationReportStatistic> statistics = await OperationReportStatisticRepo.GetByDay(0, 0, from, to.AddDays(1));


                foreach (var workDate in allDates)
                {
                    var statistic = statistics.Where(a => a.Date == workDate).ToList();
                    var revenueInfo = await GetRevenueInfo(0, 0, salonIds, workDate, workDate.AddDays(1));

                    if (statistic.Any())
                    {
                        foreach (var id in salonIds)
                        {
                            var statisticSalon = statistic.FirstOrDefault(a => a.SalonId == id);
                            var info = revenueInfo.FirstOrDefault(a => a.SalonId == id);

                            if (statisticSalon != null && info != null)
                            {
                                statisticSalon.Revenue = JsonConvert.SerializeObject(info.Revenue.ListService);
                                statisticSalon.TotalIncome = info.Revenue.TotalRevenue;

                                OperationReportStatisticRepo.Update(statisticSalon);
                                await OperationReportStatisticRepo.SaveChangesAsync();
                            }
                        }
                    }

                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        private async Task StatisticQualityInfo(DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                var from = Convert.ToDateTime(dateFrom, culture);
                var to = Convert.ToDateTime(dateTo, culture);
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = from; date <= to; date = date.AddDays(1))
                    allDates.Add(date);
                var salon = await SalonRepo.GetBySalonId(0);
                var salonIds = salon.Select(a => a.Id).ToList();

                IEnumerable<OperationReportStatistic> statistics = await OperationReportStatisticRepo.GetByDay(0, 0, from, to.AddDays(1));

                foreach (var workDate in allDates)
                {
                    var statistic = statistics.Where(a => a.Date == workDate).ToList();
                    var qualityInfo = await GetQualityInfo(0, 0, salonIds, workDate, workDate.AddDays(1));

                    if (statistic.Any())
                    {
                        foreach (var id in salonIds)
                        {
                            var statisticSalon = statistic.FirstOrDefault(a => a.SalonId == id);
                            var info = qualityInfo.FirstOrDefault(a => a.SalonId == id);

                            if (statisticSalon != null && info != null)
                            {
                                statisticSalon.AvgRating = info.Quality.AvgRating;
                                statisticSalon.Mark1and2 = info.Quality.Mark1and2;
                                statisticSalon.StaffError = info.Quality.StaffError;
                                statisticSalon.LongWait = info.Quality.Wait;
                                statisticSalon.BillWithoutImage = info.Quality.BillWithoutImage;
                                statisticSalon.SpecialCustomer = info.Quality.SpecialCustomer;

                                OperationReportStatisticRepo.Update(statisticSalon);
                                await OperationReportStatisticRepo.SaveChangesAsync();
                            }
                        }

                    }
                }

            }
            catch (Exception e)
            {

                throw e;
            }
        }

        private async Task<IEnumerable<DataRevenueJson>> GetRevenueInfo(int salonId, int regionId, IEnumerable<int> salonIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                string configService = Configuration.GetSection("AppConstants:config_key_service").Value;
                string configProduct = Configuration.GetSection("AppConstants:config_key_product").Value;

                Task<TblConfig> serviesCategoryIds = ConfigRepo.GetByKey(configService);
                Task<TblConfig> productCategoryIds = ConfigRepo.GetByKey(configProduct);
                Task<IEnumerable<RevenueJson>> revenues = StoreProcedureRepo.GetRevenueInfo(dateFrom, dateTo, salonId, regionId);
                Task<IEnumerable<TotalIncomeJson>> totalIncomes = StoreProcedureRepo.GetTotalIncome(dateFrom, dateTo, salonId, regionId);
                Task.WaitAll(serviesCategoryIds, productCategoryIds, revenues, totalIncomes);

                IEnumerable<RevenueJson> revenue = revenues.Result;
                IEnumerable<TotalIncomeJson> totalIncome = totalIncomes.Result;

                List<int> serviceCategoryId = serviesCategoryIds.Result.Value.Split(',').Select(int.Parse).ToList();
                List<int> productCategoryId = productCategoryIds.Result.Value.Split(',').Select(int.Parse).ToList();
                List<int> categoryIds = serviceCategoryId.Concat(productCategoryId).ToList();

                List<DataRevenueJson> returnData = new List<DataRevenueJson>();
                foreach (var id in salonIds)
                {
                    List<RevenueJson> salonRevenue = revenue.Where(a => a.SalonId == id).ToList();
                    var income = totalIncome.FirstOrDefault(a => a.SalonId == id);

                    if (salonRevenue.Any() && income != null)
                    {
                        List<int> revenueIds = salonRevenue.Select(a => a.CategoryId).ToList();
                        IEnumerable<int> remainCategoryIds = categoryIds.Except(revenueIds);

                        IEnumerable<RevenueJson> remainCategory = await CategoryRepo.GetByIdsAsync(remainCategoryIds);

                        List<RevenueJson> finalRevenue = salonRevenue.Concat(remainCategory).OrderByDescending(a => a.Type).ToList();

                        TotalRevenueJson totalRevenue = new TotalRevenueJson
                        {
                            TotalRevenue = income.TotalIncome,
                            TotalBill = salonRevenue.FirstOrDefault().TotalBill,
                            ListService = finalRevenue
                        };

                        DataRevenueJson data = new DataRevenueJson
                        {
                            SalonId = id,
                            Revenue = totalRevenue
                        };

                        returnData.Add(data);
                    }

                }

                return returnData.OrderBy(a => a.SalonId);
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        private async Task<IEnumerable<DataProductivityJson>> GetProductivityInfo(int salonId, int regionId, IEnumerable<int> salonIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                IEnumerable<ProductivityJson> productivity = await StoreProcedureRepo.GetProductivityInfo(dateFrom, dateTo, salonId, regionId);

                List<DataProductivityJson> returnData = new List<DataProductivityJson>();

                foreach (var id in salonIds)
                {

                    ProductivityJson productivityFinal = productivity.FirstOrDefault(a => a.SalonId == id);

                    if (productivityFinal != null)
                    {
                        productivityFinal.TotalSalon = salonIds.Count();
                        productivityFinal.TotalDay = dateTo.Subtract(dateFrom).Days;

                        DataProductivityJson data = new DataProductivityJson
                        {
                            SalonId = id,
                            Productivity = productivityFinal
                        };

                        returnData.Add(data);
                    }
                }
                return returnData.OrderBy(a => a.SalonId);

            }
            catch (Exception e)
            {

                throw e;
            }
        }

        private async Task<IEnumerable<DataCustomerInfoJson>> GetCustomersInfo(int salonId, int regionId, IEnumerable<int> salonIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {

                IEnumerable<CustomerJson> customer = await StoreProcedureRepo.GetCustomerInfo(dateFrom, dateTo, salonId, regionId);

                List<DataCustomerInfoJson> returnData = new List<DataCustomerInfoJson>();

                foreach (var id in salonIds)
                {
                    var customerFinal = customer.FirstOrDefault(a => a.SalonId == id);

                    if (customer != null)
                    {
                        DataCustomerInfoJson data = new DataCustomerInfoJson
                        {
                            SalonId = id,
                            Customer = customerFinal
                        };

                        if (data.Customer.TotalBill != null)
                        {
                            returnData.Add(data);
                        }

                    }
                }

                return returnData.OrderBy(a => a.SalonId);
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        private async Task<IEnumerable<DataShineMemberJson>> GetMemberShipInfo(int salonId, int regionId, IEnumerable<int> salonIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {

                IEnumerable<ShineMemberJson> memberShip = await StoreProcedureRepo.GetMemberShipInfo(dateFrom, dateTo, salonId, regionId);

                List<DataShineMemberJson> returnData = new List<DataShineMemberJson>();

                foreach (var id in salonIds)
                {
                    ShineMemberJson member = memberShip.FirstOrDefault(a => a.SalonId == id);

                    if (member != null)
                    {
                        DataShineMemberJson data = new DataShineMemberJson
                        {
                            SalonId = id,
                            ShineMember = member
                        };

                        returnData.Add(data);
                    }

                }

                return returnData.OrderBy(a => a.SalonId);
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        private async Task<IEnumerable<DataQualityJson>> GetQualityInfo(int salonId, int regionId, List<int> salonIds, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                IEnumerable<QualityJson> quality = await StoreProcedureRepo.GetQualityInfo(dateFrom, dateTo, salonId, regionId);
                var listQuantity = await GetQuantity(salonIds, dateFrom, dateTo);

                List<DataQualityJson> returnData = new List<DataQualityJson>();

                foreach (var id in salonIds)
                {
                    var dataQuality = quality.FirstOrDefault(a => a.SalonId == id);

                    var qualitySalon = listQuantity.Where(r => r.SalonId == id).FirstOrDefault();

                    if (dataQuality != null && qualitySalon != null)
                    {
                        var qualityJson = new Quality();
                        qualityJson.AvgRating = dataQuality.AvgRating;
                        qualityJson.BillWithoutImage = dataQuality.BillWithoutImage;
                        qualityJson.ErrorCurling = qualitySalon != null ? qualitySalon.TotalErrorScscCurling : 0;
                        qualityJson.ErrorMissImage = qualitySalon != null ? qualitySalon.ErrorImage : 0;
                        qualityJson.ErrorSCSC = qualitySalon != null ? qualitySalon.TotalErorrScsc : 0;
                        qualityJson.Feedback = qualitySalon != null ? qualitySalon.Feedback : 0;
                        qualityJson.Mark1and2 = dataQuality.Mark1and2;
                        qualityJson.SalonId = dataQuality.SalonId;
                        qualityJson.SpecialCustomer = dataQuality.SpecialCustomer;
                        qualityJson.StaffError = qualitySalon != null ? qualitySalon.TotalMonitorStaff : 0;
                        qualityJson.TNDV = dataQuality.TNDV;
                        qualityJson.Wait = dataQuality.Wait;

                        DataQualityJson data = new DataQualityJson
                        {
                            SalonId = id,
                            Quality = qualityJson
                        };

                        returnData.Add(data);
                    }
                }


                return returnData.OrderBy(a => a.SalonId);

            }
            catch (Exception e)
            {

                throw e;
            }
        }

        #endregion

    }
}