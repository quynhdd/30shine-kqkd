﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReportProject.Utils;
using Newtonsoft.Json;
using ReportProject.Models.Solution_30shine_Real;
using Microsoft.EntityFrameworkCore;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ReportProject.Controllers
{
    [Produces("application/json")]
    [Route("api/scsc-check")]
    public class SCSCCheckController : Controller
    {
        private Solution_30shineContext context;
        private DbSet<ScscCheckError> objScscCheckError;
        private DbSet<ScscCategory> objScscCategory;
        private DbSet<BillService> objBillService;
        private DbSet<Staff> objStaff;
        private DbSet<ImageData> objImageCurling;
        private DbSet<FlowService> objFlowServices;
        private ILogger<ReportController> Logger { get; }
        CultureInfo culture = new CultureInfo("vi-VN");
        static HttpClient client = new HttpClient();

        public SCSCCheckController()
        {
            context = new Solution_30shineContext();
            objScscCheckError = context.Set<ScscCheckError>();
            objScscCategory = context.Set<ScscCategory>();
            objBillService = context.Set<BillService>();
            objStaff = context.Set<Staff>();
            objImageCurling = context.Set<ImageData>();
            objFlowServices = context.Set<FlowService>();
        }
        // GET: /<controller>/
        [HttpPost]
        public async Task<IActionResult> SaveCheckScsc()
        {
            try
            {
                var dateFrom = DateTime.Now.AddDays(-1);
                var dateTo = dateFrom.AddDays(+1);

                //var dateFrom = new DateTime(2018, 05, 02);
                //var dateTo = dateFrom.AddDays(1);

                var listLevel = new List<int>(new int[] { 2, 6, 7 });
                Random rnd = new Random();

                var listScscCategory = await objScscCategory.Where(a => a.Publish == true && a.IsDelete == false && a.ScscCateIdcate > 0).ToListAsync();
                // lay diểm max của các category
                int maxPoinShapId = listScscCategory.Where(a => a.ScscCateIdcate == 1).Max(m => m.ScscCatePoint ?? 0);
                int maxPoinConnectiveId = listScscCategory.Where(a => a.ScscCateIdcate == 2).Max(m => m.ScscCatePoint ?? 0);
                int maxPoinSharpNess = listScscCategory.Where(a => a.ScscCateIdcate == 3).Max(m => m.ScscCatePoint ?? 0);
                int maxPoinCompletion = listScscCategory.Where(a => a.ScscCateIdcate == 4).Max(m => m.ScscCatePoint ?? 0);
                var listBillId = await (from a in objBillService
                                        join s in objStaff on a.StaffHairdresserId equals s.Id
                                        where
                                        a.Pending == 0 && a.IsDelete == 0 && !string.IsNullOrEmpty(a.ServiceIds) && a.CreatedDate > dateFrom && a.CreatedDate < dateTo && listLevel.Contains(s.SkillLevel ?? 0)
                                        select new
                                        {
                                            billId = a.Id,
                                            isImage = !string.IsNullOrEmpty(a.Images)
                                        })
                                  //.OrderBy(a => a.billId).Take(1)
                                  .ToListAsync();
                var newListIntBill = listBillId.Select(a => a.billId).ToList();
                List<ScscCheckError> listScscCheckOld = objScscCheckError.Where(a => newListIntBill.Contains(a.BillServiceId)).ToList();

                var listImageCurling = await (from a in objFlowServices
                                              join c in objImageCurling on a.BillId equals c.Objid
                                              where a.ServiceId == 16
                                                    && c.ImageAfter != null
                                                    && c.ImageBefore != null
                                                    && c.ImageAfter != ""
                                                    && c.ImageBefore != ""
                                                    && c.Slugkey == "image_bill_curling"
                                                    && newListIntBill.Contains(a.BillId ?? 0)
                                              select a.BillId ?? 0).ToListAsync();
                // Khai bao
                var listScscCheck = new List<ScscCheckError>();

                foreach (var item in listBillId)
                {
                    var scscCheck = new ScscCheckError();
                    var isAdd = false;
                    if (item.isImage)
                    {
                        isAdd = true;
                        //Khai báo tính toán random
                        int TotalPoinSCSC = 0,
                            pointShapId = 0,
                            pointConnectiveId = 0,
                            pointSharpNessId = 0,
                            pointCompletionId = 0,
                            tempPointCheck = 0;
                        TotalPoinSCSC = rnd.Next(5, 8);
                        //TotalPoinSCSC = 5;
                        pointShapId = rnd.Next(1, 3);

                        pointConnectiveId =
                            TotalPoinSCSC != 5 ? rnd.Next(1, 3) : (pointShapId == 2 ? 1 : rnd.Next(1, 3));

                        //if (TotalPoinSCSC == 5)
                        //{
                        //    if (pointShapId == 2)
                        //    {
                        //        pointConnectiveId = 1;
                        //    }
                        //    else
                        //    {
                        //        pointConnectiveId = rnd.Next(1, 3);
                        //    }
                        //}
                        //else
                        //{
                        //    pointConnectiveId = rnd.Next(1, 3);
                        //}

                        tempPointCheck = TotalPoinSCSC - pointShapId - pointConnectiveId;

                        pointSharpNessId = tempPointCheck > 3 ? rnd.Next(2, 4) : rnd.Next(1, tempPointCheck);
                        //if (tempPointCheck > 3)
                        //{
                        //    pointSharpNessId = rnd.Next(2, 4);
                        //}
                        //else
                        //{
                        //    pointSharpNessId = rnd.Next(1, tempPointCheck);
                        //}

                        pointCompletionId = TotalPoinSCSC - pointShapId - pointConnectiveId - pointSharpNessId;

                        scscCheck.BillServiceId = item.billId;
                        scscCheck.ImageError = false;
                        scscCheck.NoteError = string.Empty;
                        scscCheck.ShapeId = ResulCategoryId(listScscCategory, pointShapId, 1);
                        scscCheck.ConnectTiveId = ResulCategoryId(listScscCategory, pointConnectiveId, 2);
                        scscCheck.SharpNessId = ResulCategoryId(listScscCategory, pointSharpNessId, 3);
                        scscCheck.ComPlatetionId = ResulCategoryId(listScscCategory, pointCompletionId, 4);
                        scscCheck.Publish = true;
                        scscCheck.IsDelete = false;
                        scscCheck.CreateDate = DateTime.Now;
                        scscCheck.TotalPointScsc = TotalPoinSCSC;
                        scscCheck.MaxPointShapId = maxPoinShapId;
                        scscCheck.MaxPointConnectiveId = maxPoinConnectiveId;
                        scscCheck.MaxPointSharpNessId = maxPoinSharpNess;
                        scscCheck.MaxPointComPlatetionId = maxPoinCompletion;
                        scscCheck.PointError = TotalPoinSCSC;
                        scscCheck.AutomaticScore = true;
                    }

                    var isImageCurling = listImageCurling.Contains(item.billId);
                    if (isImageCurling)
                    {
                        isAdd = true;
                        scscCheck.AutomaticScore = true;
                        scscCheck.Publish = true;
                        scscCheck.IsDelete = false;
                        scscCheck.CreateDate = DateTime.Now;
                        scscCheck.BillServiceId = item.billId;
                        scscCheck.HairRootId = 26;
                        scscCheck.HairWavesId = 28;
                        scscCheck.HairTipId = 24;
                        scscCheck.TotalPointScsccurling = 3;
                        scscCheck.ImageErrorCurling = false;
                    }

                    if (isAdd)
                    {
                        listScscCheck.Add(scscCheck);
                    }
                }
                if (listScscCheckOld.Any())
                {
                    objScscCheckError.RemoveRange(listScscCheckOld);
                }
                objScscCheckError.AddRange(listScscCheck);
                context.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotic.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message);
                return StatusCode(500, new { message = e.Message });

            }

        }
        private int ResulCategoryId(List<ScscCategory> listCategorySCSC, int pointSCSC, int? ScscCateIdcate)
        {
            try
            {
                int IdCate = listCategorySCSC.FirstOrDefault(a => a.ScscCatePoint == pointSCSC && a.ScscCateIdcate == ScscCateIdcate).IdScscCate;
                return IdCate;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private RandomPoint RandomPointResult(List<ScscCategory> listCategorySCSC)
        {
            try
            {
                Random rnd = new Random();
                //Khai báo tính toán random
                int TotalPoinSCSC = 0, pointShapId = 0, pointConnectiveId = 0, pointSharpNessId = 0, pointCompletionId = 0, tempPointCheck = 0;
                TotalPoinSCSC = rnd.Next(5, 8);
                pointShapId = rnd.Next(1, 3);
                if (TotalPoinSCSC == 5)
                {
                    if (pointShapId == 2)
                    {
                        pointConnectiveId = 1;
                    }
                }
                else
                {
                    pointConnectiveId = rnd.Next(1, 3);
                }

                tempPointCheck = TotalPoinSCSC - pointShapId - pointConnectiveId;

                if (tempPointCheck > 3)
                {
                    pointSharpNessId = rnd.Next(2, 4);
                }
                else
                {
                    pointSharpNessId = rnd.Next(1, tempPointCheck);
                }
                pointCompletionId = TotalPoinSCSC - pointShapId - pointConnectiveId - pointSharpNessId;

                var objRandomPoint = new RandomPoint();
                objRandomPoint.TotalPoinSCSC = TotalPoinSCSC;
                objRandomPoint.pointShapId = ResulCategoryId(listCategorySCSC, pointShapId, 1);
                objRandomPoint.pointConnectiveId = ResulCategoryId(listCategorySCSC, pointConnectiveId, 2);
                objRandomPoint.pointSharpNessId = ResulCategoryId(listCategorySCSC, pointSharpNessId, 3);
                objRandomPoint.pointCompletionId = ResulCategoryId(listCategorySCSC, pointCompletionId, 4);
                return objRandomPoint;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
    public class RandomPoint
    {
        public int TotalPoinSCSC { get; set; }
        public int pointShapId { get; set; }
        public int pointConnectiveId { get; set; }
        public int pointSharpNessId { get; set; }
        public int pointCompletionId { get; set; }
        public int tempPointCheck { get; set; }
    }

}
