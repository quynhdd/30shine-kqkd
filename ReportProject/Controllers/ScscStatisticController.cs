﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APICheckout.Extentions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReportProject.Utils;
using ReportProject.Respository.InterfaceV1;
using static APICheckout.Models.CustomeModel.InputModel;
using System.Globalization;
using ReportProject.Models.Solution_30Shine;
using static APICheckout.Models.CustomeModel.OutPutModel;

namespace ReportProject.Controllers
{
    [Produces("application/json")]
    [Route("api/scsc-statistic")]
    public class ScscStatisticController : Controller
    {
        private ILogger<ScscStatisticController> Logger { get; }
        private IPushNotice PushNotice;
        private IBillserviceRepo BillserviceRepo;
        private ISCSCCheckErrorRepo SCSCCheckErrorRepo;
        private ISalonRepo SalonRepo;
        private IScscCategoryRepo ScscCategoryRepo;
        private IStatisticScscErrorRepo StatisticScscErrorRepo;
        private IStaffRepo StaffRepo;

        public ScscStatisticController(ILogger<ScscStatisticController> logger,
            IPushNotice pushNotice,
            IBillserviceRepo billserviceRepo,
            ISCSCCheckErrorRepo sCSCCheckErrorRepo,
            ISalonRepo salonRepo,
            IScscCategoryRepo scscCategoryRepo,
            IStatisticScscErrorRepo statisticScscErrorRepo,
            IStaffRepo staffRepo)
        {
            Logger = logger;
            PushNotice = pushNotice;
            BillserviceRepo = billserviceRepo;
            SCSCCheckErrorRepo = sCSCCheckErrorRepo;
            SalonRepo = salonRepo;
            ScscCategoryRepo = scscCategoryRepo;
            StatisticScscErrorRepo = statisticScscErrorRepo;
            StaffRepo = staffRepo;
        }
        [HttpPost("auto-static-scsc")]
        public async Task<IActionResult> StaticScscAuto([FromBody] ReqStatisticScsc param)
        {
            try
            {
                if (
                    param == null ||
                    (!string.IsNullOrEmpty(param.FromDate) && string.IsNullOrEmpty(param.ToDate)) ||
                    (string.IsNullOrEmpty(param.FromDate) && !string.IsNullOrEmpty(param.ToDate))
                    )
                {
                    return BadRequest("Data Not Exits!");
                }
                var createdDate = DateTime.Now;
                var fromDate = DateTime.Now.Date.AddDays(-1);
                var toDate = fromDate.AddDays(1);
                var listSalon = await SalonRepo.GetList(r => r.IsDelete == 0 && r.Publish == true && r.IsSalonHoiQuan == false);
                var listSalonId = listSalon.Select(r => r.Id).ToList();
                if (!string.IsNullOrEmpty(param.ToDate) && !string.IsNullOrEmpty(param.FromDate))
                {
                    fromDate = DateTime.ParseExact(param.FromDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                    toDate = DateTime.ParseExact(param.ToDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                    if (fromDate > toDate)
                    {
                        return BadRequest("Ngày không hợp lệ!");
                    }
                }
                if (!string.IsNullOrEmpty(param.ListSalonId))
                {
                    listSalonId = param.ListSalonId.ConvertString().ToList();
                    var isValid = Array.IndexOf(listSalonId.ToArray(), 0);
                    if (isValid >= 0)
                    {
                        return BadRequest("Chuỗi salon không hợp lệ VD: '2,3,4,5'");
                    }

                }
                var listCategoryScsc = await ScscCategoryRepo.GetList(r => r.IsDelete == false && r.Publish == true);
                var listStaff = await StaffRepo.GetList(r => r.IsDelete == 0 && r.Active == 1);

                for (var date = fromDate; date <= toDate; date = date.AddDays(1))
                {
                    //List Statistic toàn hệ thống
                    var listBill = await SCSCCheckErrorRepo.GetListBillScsc(date, date.AddDays(1));
                    // list statistic theo salon
                    var listBillSalon = listBill.Where(r => listSalonId.Contains(r.SalonId ?? 0)).ToList();
                    var listBillIdSalon = listBill.Select(r => r.BillId).ToList();

                    // list statistic theo nhân viên
                    var listBillStaff = (from a in listBill
                                         join b in listStaff on a.StylistId equals b.Id
                                         where listSalonId.Contains(b.SalonId ?? 0) && !listSalonId.Contains(a.SalonId ?? 0)
                                         select a).ToList();

                    var listCalculator = listBillSalon.Union(listBillStaff).ToList();

                    var listStaffSalon = listCalculator.GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new { stylistId = r.Key.StylistId, salonId = r.Key.SalonId }).ToList();

                    var listStatisticScscError = CalculatorScsc(listCalculator, listCategoryScsc, date);
                    // bo du lieu cu.
                    var listStatic = await StatisticScscErrorRepo.GetList(r => r.WorkDate == date.Date);
                    var listStaticOld = (from a in listStatic
                                         join b in listStaffSalon on a.StylistId equals b.stylistId
                                         where a.SalonId == b.salonId
                                         select a).ToList();
                    if (listStaticOld.Any())
                    {
                        foreach (var item in listStaticOld)
                        {
                            StatisticScscErrorRepo.Remove(item);
                        }
                        await StatisticScscErrorRepo.SaveChangeAsync();
                    }
                    // Them du lieu moi.
                    if (listStatisticScscError.Any())
                    {
                        StatisticScscErrorRepo.AddRange(listStatisticScscError);
                        await StatisticScscErrorRepo.SaveChangeAsync();
                    }
                }

                return Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(e, true);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace + "Line: " + trace.GetFrame(0).GetFileLineNumber());
                return StatusCode(500, new { message = e.Message });
            }
        }

        [HttpGet("salon-only-staff")]
        public async Task<IActionResult> GetSalonOnlyStaff([FromQuery] string fromDate, string toDate, int salonId, int stylistId)
        {
            try
            {
                if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                {
                    return BadRequest("Ngày không hợp lệ");
                }
                var fromdate = DateTime.ParseExact(fromDate, @"yyyy/M/d", CultureInfo.InvariantCulture);
                var todate = DateTime.ParseExact(toDate, @"yyyy/M/d", CultureInfo.InvariantCulture);
                var scscStatic = await StatisticScscErrorRepo.GetList(r => r.WorkDate >= fromdate && r.WorkDate <= todate && r.IsDelete == false && (r.StylistId == stylistId || stylistId == 0) && r.SalonId == salonId);
                if (scscStatic.Any())
                {
                    var list = scscStatic.GroupBy(r => new { r.StylistId, r.SalonId }).Select(r => new StatisticScscError
                    {
                        StylistId = r.Key.StylistId,
                        SalonId = r.Key.SalonId,
                        TongKhongAnhScsc = r.Sum(g => g.TongKhongAnhScsc),
                        TongAnhLoiScsc = r.Sum(g => g.TongAnhLoiScsc),
                        TongKhongMoScsc = r.Sum(g => g.TongKhongMoScsc),
                        TongAnhMoScsc = r.Sum(g => g.TongAnhMoScsc),
                        TongLoiScscHoacUon = r.Sum(g => g.TongLoiScscHoacUon),
                        TongDiemScsc = r.Sum(g => g.TongDiemScsc),
                        TongErrorShape = r.Sum(g => g.TongErrorShape),
                        TongErrorComplatetion = r.Sum(g => g.TongErrorComplatetion),
                        TongErrorSharpNess = r.Sum(g => g.TongErrorSharpNess),
                        TongKhongAnhUon = r.Sum(g => g.TongKhongAnhUon),
                        TongBillUon = r.Sum(g => g.TongBillUon),
                        TongAnhUon = r.Sum(g => g.TongAnhUon),
                        TongMoLechUon = r.Sum(g => g.TongMoLechUon),
                        TongLoiUon = r.Sum(g => g.TongLoiUon),
                        TongErrorHairTip = r.Sum(g => g.TongErrorHairTip),
                        TongErrorHairRoots = r.Sum(g => g.TongErrorHairRoots),
                        TongErrorHairWaves = r.Sum(g => g.TongErrorHairWaves),
                        TotalBill = r.Sum(g => g.TotalBill),
                        TongDiemUon = r.Sum(g => g.TongDiemUon),
                        TongMoLechUonScsc = r.Sum(g => g.TongMoLechUonScsc),
                        TongErrorConnective = r.Sum(g => g.TongErrorConnective),
                    }).ToList();

                    var listStaff = await StaffRepo.GetList(r => r.IsDelete == 0 && r.Active == 1);
                    var data = (from a in list
                                join b in listStaff on a.StylistId equals b.Id
                                select new
                                {
                                    FullName = b.Fullname,
                                    StylistId = a.StylistId ?? 0,
                                    TotalBill = a.TotalBill ?? 0,
                                    TotalBillNotImg = a.TongKhongAnhScsc ?? 0,
                                    TotalErrorSCSC = a.TongAnhLoiScsc ?? 0,
                                    TongBillDanhGiaAnhMo = a.TongAnhMoScsc ?? 0,
                                    TotalBillDanhGiaNotImageMoLech = a.TongKhongMoScsc ?? 0,
                                    TotalPointSCSC = a.TongDiemScsc ?? 0,
                                    TongBillNotAnhUon = a.TongKhongAnhUon ?? 0,
                                    TongBillAnhUon = a.TongAnhUon ?? 0,
                                    TongBillAnhMoLech = a.TongMoLechUon ?? 0,
                                    TongBillUonChamLoi = a.TongLoiUon ?? 0,
                                    TotalPointSCSCCurling = a.TongDiemUon ?? 0,
                                    TotalMoLechSCSCUon = a.TongMoLechUonScsc ?? 0,
                                    TotalErrorSCSCUon = a.TongLoiScscHoacUon ?? 0,
                                    Bill_NotImg = (a.TongKhongAnhScsc ?? 0) == 0 ? 0 : Math.Round((double)((double)a.TongKhongAnhScsc / a.TotalBill * 100), 2),
                                    Percent_ErrorKCS = (a.TongLoiScscHoacUon ?? 0) == 0 ? 0 : Math.Round((double)((double)a.TongLoiScscHoacUon / a.TotalBill * 100), 2),
                                    Percent_BillImg_MoLech = (a.TongAnhMoScsc ?? 0) == 0 ? 0 : Math.Round((double)((double)a.TongAnhMoScsc / a.TotalBill * 100), 2),
                                    TotalBill_ChuaDanhGia = (a.TotalBill ?? 0) - (a.TongKhongAnhScsc ?? 0) - (a.TongKhongMoScsc ?? 0) - (a.TongAnhMoScsc ?? 0),
                                    Percent_BillError = Math.Round((double)(((double)(a.TongAnhLoiScsc ?? 0) - a.TongAnhMoScsc) / a.TotalBill * 100), 2),
                                    PointSCSC_TB = ((a.TongKhongMoScsc ?? 0) + a.TongAnhMoScsc) == 0 ? 0 :
                                                    Math.Round((double)((double)(a.TongDiemScsc ?? 0) / ((a.TongKhongMoScsc ?? 0) + a.TongAnhMoScsc)), 2),
                                    ErrorShapeId = a.TongErrorShape ?? 0,
                                    ErrorConnectiveId = a.TongErrorConnective ?? 0,
                                    ErrorSharpNessId = a.TongErrorSharpNess ?? 0,
                                    ErrorComplatetionId = a.TongErrorComplatetion ?? 0,
                                    BillNotImgUon = (a.TongBillUon ?? 0) != 0 ? Math.Round((double)((double)a.TongKhongAnhUon / a.TongBillUon * 100), 2) : 0,
                                    BillNotImgUonMoLech = (a.TongBillUon ?? 0) != 0 ? Math.Round((double)((double)a.TongMoLechUon / a.TongBillUon), 2) : 0,
                                    BillUonChamLoi = (a.TongAnhUon ?? 0) != 0 ? Math.Round((double)((double)a.TongLoiUon / a.TongAnhUon * 100), 2) : 0,
                                    TotalDiemUonTB = (a.TongAnhUon ?? 0) != 0 ? Math.Round((double)((double)a.TongDiemUon / a.TongAnhUon), 2) : 0,
                                    TotalBillUon = a.TongBillUon ?? 0,
                                    Percent_ErrorKCSUon = (a.TongBillUon ?? 0) != 0 ? Math.Round((double)((double)(a.TongLoiUon - a.TongMoLechUon) / a.TongBillUon * 100), 2) : 0,
                                    ErrorHairTip = a.TongErrorHairTip ?? 0,
                                    ErrorHairRoots = a.TongErrorHairRoots ?? 0,
                                    ErrorHairWaves = a.TongErrorHairWaves ?? 0
                                }).OrderByDescending(r => r.StylistId).ToList();
                    //var data =
                    return Ok(data);

                }
                else
                {
                    return Ok();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        [HttpGet("staff-only-staff")]
        public async Task<IActionResult> GetStaffOnlyStaff([FromQuery] string fromDate, string toDate, int salonId, int stylistId)
        {
            try
            {
                if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                {
                    return BadRequest("Ngày không hợp lệ");
                }
                var fromdate = DateTime.ParseExact(fromDate, @"yyyy/M/d", CultureInfo.InvariantCulture);
                var todate = DateTime.ParseExact(toDate, @"yyyy/M/d", CultureInfo.InvariantCulture);
                var scscStatic = await StatisticScscErrorRepo.GetList(r => r.WorkDate >= fromdate && r.WorkDate <= todate && r.IsDelete == false);
                var listStaff = await StaffRepo.GetList(r => r.IsDelete == 0 && r.Active == 1);
                var listSalon = await SalonRepo.GetList(r => r.IsDelete == 0 && r.Publish == true && r.IsSalonHoiQuan == false);
                if (scscStatic.Any())
                {

                    var list = scscStatic.GroupBy(r => new { r.StylistId }).Select(r => new StatisticScscError
                    {
                        StylistId = r.Key.StylistId,
                        TongKhongAnhScsc = r.Sum(g => g.TongKhongAnhScsc),
                        TongAnhLoiScsc = r.Sum(g => g.TongAnhLoiScsc),
                        TongKhongMoScsc = r.Sum(g => g.TongKhongMoScsc),
                        TongAnhMoScsc = r.Sum(g => g.TongAnhMoScsc),
                        TongLoiScscHoacUon = r.Sum(g => g.TongLoiScscHoacUon),
                        TongDiemScsc = r.Sum(g => g.TongDiemScsc),
                        TongErrorShape = r.Sum(g => g.TongErrorShape),
                        TongErrorComplatetion = r.Sum(g => g.TongErrorComplatetion),
                        TongErrorSharpNess = r.Sum(g => g.TongErrorSharpNess),
                        TongKhongAnhUon = r.Sum(g => g.TongKhongAnhUon),
                        TongBillUon = r.Sum(g => g.TongBillUon),
                        TongAnhUon = r.Sum(g => g.TongAnhUon),
                        TongMoLechUon = r.Sum(g => g.TongMoLechUon),
                        TongLoiUon = r.Sum(g => g.TongLoiUon),
                        TongErrorHairTip = r.Sum(g => g.TongErrorHairTip),
                        TongErrorHairRoots = r.Sum(g => g.TongErrorHairRoots),
                        TongErrorHairWaves = r.Sum(g => g.TongErrorHairWaves),
                        TotalBill = r.Sum(g => g.TotalBill),
                        TongDiemUon = r.Sum(g => g.TongDiemUon),
                        TongMoLechUonScsc = r.Sum(g => g.TongMoLechUonScsc),
                        TongErrorConnective = r.Sum(g => g.TongErrorConnective),
                    }).ToList();

                    var data = (from a in list
                                join b in listStaff on a.StylistId equals b.Id
                                join c in listSalon on b.SalonId equals c.Id
                                where (a.StylistId == stylistId || stylistId == 0) &&
                                (b.SalonId == salonId || b.SalonId == 0)
                                select new
                                {
                                    StylistId = a.StylistId ?? 0,
                                    FullName = b.Fullname,
                                    TotalBill = a.TotalBill ?? 0,
                                    TotalBillNotImg = a.TongKhongAnhScsc ?? 0,
                                    TotalErrorSCSC = a.TongAnhLoiScsc ?? 0,
                                    TongBillDanhGiaAnhMo = a.TongAnhMoScsc ?? 0,
                                    TotalBillDanhGiaNotImageMoLech = a.TongKhongMoScsc ?? 0,
                                    TotalPointSCSC = a.TongDiemScsc ?? 0,
                                    TongBillNotAnhUon = a.TongKhongAnhUon ?? 0,
                                    TongBillAnhUon = a.TongAnhUon ?? 0,
                                    TongBillAnhMoLech = a.TongMoLechUon ?? 0,
                                    TongBillUonChamLoi = a.TongLoiUon ?? 0,
                                    TotalPointSCSCCurling = a.TongDiemUon ?? 0,
                                    TotalMoLechSCSCUon = a.TongMoLechUonScsc ?? 0,
                                    TongBillUon = a.TongBillUon ?? 0,
                                    TotalErrorSCSCUon = a.TongLoiScscHoacUon ?? 0,
                                    Bill_NotImg = (a.TongKhongAnhScsc ?? 0) == 0 ? 0 : Math.Round((double)((double)a.TongKhongAnhScsc / a.TotalBill * 100), 2),
                                    Percent_BillError = Math.Round((double)(((double)(a.TongAnhLoiScsc ?? 0) - a.TongAnhMoScsc) / a.TotalBill * 100), 2),
                                    Percent_BillImg_MoLech = (a.TongAnhMoScsc ?? 0) == 0 ? 0 : Math.Round((double)((double)a.TongAnhMoScsc / a.TotalBill * 100), 2),
                                    TotalBill_ChuaDanhGia = (a.TotalBill ?? 0) - (a.TongKhongAnhScsc ?? 0) - (a.TongKhongMoScsc ?? 0) - (a.TongAnhMoScsc ?? 0),
                                    Percent_ErrorKCS = (a.TongLoiScscHoacUon ?? 0) == 0 ? 0 : Math.Round((double)((double)a.TongLoiScscHoacUon / a.TotalBill * 100), 2),
                                    PointSCSC_TB = ((a.TongKhongMoScsc ?? 0) + a.TongAnhMoScsc) == 0 ? 0 :
                                                    Math.Round((double)((double)(a.TongDiemScsc ?? 0) / ((a.TongKhongMoScsc ?? 0) + a.TongAnhMoScsc)), 2),
                                    ErrorShapeId = a.TongErrorShape ?? 0,
                                    ErrorConnectiveId = a.TongErrorConnective ?? 0,
                                    ErrorSharpNessId = a.TongErrorSharpNess ?? 0,
                                    ErrorComplatetionId = a.TongErrorComplatetion ?? 0,
                                    BillNotImgUon = (a.TongBillUon ?? 0) != 0 ? Math.Round((double)((double)a.TongKhongAnhUon / a.TongBillUon * 100), 2) : 0,
                                    BillNotImgUonMoLech = (a.TongBillUon ?? 0) != 0 ? Math.Round((double)((double)a.TongMoLechUon / a.TongBillUon), 2) : 0,
                                    BillUonChamLoi = (a.TongAnhUon ?? 0) != 0 ? Math.Round((double)((double)a.TongLoiUon / a.TongAnhUon * 100), 2) : 0,
                                    TotalDiemUonTB = (a.TongAnhUon ?? 0) != 0 ? Math.Round((double)((double)a.TongDiemUon / a.TongAnhUon), 2) : 0,
                                    TotalBillUon = a.TongBillUon ?? 0,
                                    Percent_ErrorKCSUon = (a.TongBillUon ?? 0) != 0 ? Math.Round((double)((double)(a.TongLoiUon - a.TongMoLechUon) / a.TongBillUon * 100), 2) : 0,
                                    ErrorHairTip = a.TongErrorHairTip ?? 0,
                                    ErrorHairRoots = a.TongErrorHairRoots ?? 0,
                                    ErrorHairWaves = a.TongErrorHairWaves ?? 0
                                }).OrderByDescending(r => r.StylistId).ToList();
                    //var data =
                    return Ok(data);
                }
                else
                {
                    return Ok();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        [HttpGet("salon-all-salon")]
        public async Task<IActionResult> GetSalonAllSalon([FromQuery] string fromDate, string toDate)
        {
            try
            {
                if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                {
                    return BadRequest("Ngày không hợp lệ");
                }
                var fromdate = DateTime.ParseExact(fromDate, @"yyyy/M/d", CultureInfo.InvariantCulture);
                var todate = DateTime.ParseExact(toDate, @"yyyy/M/d", CultureInfo.InvariantCulture);
                var scscStatic = await StatisticScscErrorRepo.GetList(r => r.WorkDate >= fromdate && r.WorkDate <= todate && r.IsDelete == false);
                if (scscStatic.Any())
                {
                    var list = scscStatic.GroupBy(r => new { r.SalonId }).Select(r => new StatisticScscError
                    {
                        SalonId = r.Key.SalonId,
                        TongKhongAnhScsc = r.Sum(g => g.TongKhongAnhScsc),
                        TongAnhLoiScsc = r.Sum(g => g.TongAnhLoiScsc),
                        TongKhongMoScsc = r.Sum(g => g.TongKhongMoScsc),
                        TongAnhMoScsc = r.Sum(g => g.TongAnhMoScsc),
                        TongLoiScscHoacUon = r.Sum(g => g.TongLoiScscHoacUon),
                        TongDiemScsc = r.Sum(g => g.TongDiemScsc),
                        TongErrorShape = r.Sum(g => g.TongErrorShape),
                        TongErrorComplatetion = r.Sum(g => g.TongErrorComplatetion),
                        TongErrorSharpNess = r.Sum(g => g.TongErrorSharpNess),
                        TongKhongAnhUon = r.Sum(g => g.TongKhongAnhUon),
                        TongBillUon = r.Sum(g => g.TongBillUon),
                        TongAnhUon = r.Sum(g => g.TongAnhUon),
                        TongMoLechUon = r.Sum(g => g.TongMoLechUon),
                        TongLoiUon = r.Sum(g => g.TongLoiUon),
                        TongErrorHairTip = r.Sum(g => g.TongErrorHairTip),
                        TongErrorHairRoots = r.Sum(g => g.TongErrorHairRoots),
                        TongErrorHairWaves = r.Sum(g => g.TongErrorHairWaves),
                        TotalBill = r.Sum(g => g.TotalBill),
                        TongDiemUon = r.Sum(g => g.TongDiemUon),
                        TongMoLechUonScsc = r.Sum(g => g.TongMoLechUonScsc),
                        TongErrorConnective = r.Sum(g => g.TongErrorConnective),
                    }).ToList();

                    var listSalon = await SalonRepo.GetList(r => r.IsDelete == 0 && r.IsSalonHoiQuan == false && r.Publish == true);
                    var data = (from a in list
                                join b in listSalon on a.SalonId equals b.Id
                                select new
                                {
                                    SalonId = a.SalonId ?? 0,
                                    ShortName = b.ShortName,
                                    Order = b.Order,
                                    TotalBill = a.TotalBill ?? 0,
                                    TotalBillNotImage = a.TongKhongAnhScsc ?? 0,
                                    TotalBill_DanhGiaNotMolech = a.TongKhongMoScsc ?? 0,
                                    TotalBill_DanhGiaImgMoLech = a.TongAnhMoScsc ?? 0,
                                    TotalErrorSCSC = a.TongAnhLoiScsc ?? 0,
                                    TotalSCSC = a.TongDiemScsc ?? 0,
                                    TongBillNotAnhUon = a.TongKhongAnhUon ?? 0,
                                    TongBillAnhMoLech = a.TongMoLechUon ?? 0,
                                    TongBillUonChamLoi = a.TongLoiUon ?? 0,
                                    TongBillAnhUon = a.TongAnhUon ?? 0,
                                    TotalMoLechSCSCUon = a.TongMoLechUonScsc ?? 0,
                                    TotalErrorSCSCUon = a.TongLoiScscHoacUon ?? 0,
                                    TotalPointSCSCCurling = a.TongDiemUon ?? 0,
                                    Percent_Bill_NotImg = (a.TongKhongAnhScsc ?? 0) == 0 ? 0 : Math.Round((double)((double)a.TongKhongAnhScsc / a.TotalBill * 100), 2),
                                    BillChuaDanhGia = (a.TotalBill ?? 0) - (a.TongKhongAnhScsc ?? 0) - (a.TongKhongMoScsc ?? 0) - (a.TongAnhMoScsc ?? 0),
                                    Percent_BillError = Math.Round((double)(((double)(a.TongAnhLoiScsc ?? 0) - a.TongAnhMoScsc) / a.TotalBill * 100), 2),
                                    Percent_BillImg_MoLech = (a.TongAnhMoScsc ?? 0) == 0 ? 0 : Math.Round((double)((double)a.TongAnhMoScsc / a.TotalBill * 100), 2),
                                    PointSCSC_TB = ((a.TongKhongMoScsc ?? 0) + a.TongAnhMoScsc) == 0 ? 0 :
                                                    Math.Round((double)((double)(a.TongDiemScsc ?? 0) / ((a.TongKhongMoScsc ?? 0) + a.TongAnhMoScsc)), 2),
                                    Percent_ErrorKCS = (a.TongLoiScscHoacUon ?? 0) == 0 ? 0 : Math.Round((double)((double)a.TongLoiScscHoacUon / a.TotalBill * 100), 2),
                                    ErrorShapeId = a.TongErrorShape ?? 0,
                                    ErrorConnective = a.TongErrorConnective ?? 0,
                                    ErrorSharpNess = a.TongErrorSharpNess ?? 0,
                                    ErrorComplatetion = a.TongErrorComplatetion ?? 0,
                                    BillNotImgUon = (a.TongBillUon ?? 0) != 0 ? Math.Round((double)((double)a.TongKhongAnhUon / a.TongBillUon * 100), 2) : 0,
                                    BillNotImgUonMoLech = (a.TongBillUon ?? 0) != 0 ? Math.Round((double)((double)a.TongMoLechUon / a.TongBillUon), 2) : 0,
                                    BillUonChamLoi = (a.TongAnhUon ?? 0) != 0 ? Math.Round((double)((double)a.TongLoiUon / a.TongAnhUon * 100), 2) : 0,
                                    TotalDiemUonTB = (a.TongAnhUon ?? 0) != 0 ? Math.Round((double)((double)a.TongDiemUon / a.TongAnhUon), 2) : 0,
                                    TotalBillUon = a.TongBillUon ?? 0,
                                    Percent_ErrorKCSUon = (a.TongBillUon ?? 0) != 0 ? Math.Round((double)((double)(a.TongLoiUon - a.TongMoLechUon) / a.TongBillUon * 100), 2) : 0,

                                    ErrorHairTip = a.TongErrorHairTip ?? 0,
                                    ErrorHairRoots = a.TongErrorHairRoots ?? 0,
                                    ErrorHairWaves = a.TongErrorHairWaves ?? 0
                                }).OrderBy(r => r.Order).ToList();

                    return Ok(data);
                }
                else
                {
                    return Ok();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        [HttpGet("staff-all-salon")]
        public async Task<IActionResult> GetStaffAllSalon([FromQuery] string fromDate, string toDate)
        {
            try
            {
                if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                {
                    return BadRequest("Ngày không hợp lệ");
                }
                var fromdate = DateTime.ParseExact(fromDate, @"yyyy/M/d", CultureInfo.InvariantCulture);
                var todate = DateTime.ParseExact(toDate, @"yyyy/M/d", CultureInfo.InvariantCulture);
                var scscStatic = await StatisticScscErrorRepo.GetList(r => r.WorkDate >= fromdate && r.WorkDate <= todate && r.IsDelete == false);
                var listStaff = await StaffRepo.GetList(r => r.IsDelete == 0 && r.Active == 1);
                var listSalon = await SalonRepo.GetList(r => r.IsDelete == 0 && r.Publish == true && r.IsSalonHoiQuan == false);

                if (scscStatic.Any())
                {

                    var list = (from a in scscStatic
                                join b in listStaff on a.StylistId equals b.Id
                                select new StatisticScscError
                                {
                                    SalonId = b.SalonId,
                                    TongKhongAnhScsc = a.TongKhongAnhScsc,
                                    TongAnhLoiScsc = a.TongAnhLoiScsc,
                                    TongKhongMoScsc = a.TongKhongMoScsc,
                                    TongAnhMoScsc = a.TongAnhMoScsc,
                                    TongLoiScscHoacUon = a.TongLoiScscHoacUon,
                                    TongDiemScsc = a.TongDiemScsc,
                                    TongErrorShape = a.TongErrorShape,
                                    TongErrorComplatetion = a.TongErrorComplatetion,
                                    TongErrorSharpNess = a.TongErrorSharpNess,
                                    TongKhongAnhUon = a.TongKhongAnhUon,
                                    TongBillUon = a.TongBillUon,
                                    TongAnhUon = a.TongAnhUon,
                                    TongMoLechUon = a.TongMoLechUon,
                                    TongLoiUon = a.TongLoiUon,
                                    TongErrorHairTip = a.TongErrorHairTip,
                                    TongErrorHairRoots = a.TongErrorHairRoots,
                                    TongErrorHairWaves = a.TongErrorHairWaves,
                                    TotalBill = a.TotalBill,
                                    TongDiemUon = a.TongDiemUon,
                                    TongMoLechUonScsc = a.TongMoLechUonScsc,
                                    TongErrorConnective = a.TongErrorConnective,
                                })
                        .GroupBy(r => new { r.SalonId })
                        .Select(r => new StatisticScscError
                        {
                            SalonId = r.Key.SalonId,
                            TongKhongAnhScsc = r.Sum(g => g.TongKhongAnhScsc),
                            TongAnhLoiScsc = r.Sum(g => g.TongAnhLoiScsc),
                            TongKhongMoScsc = r.Sum(g => g.TongKhongMoScsc),
                            TongAnhMoScsc = r.Sum(g => g.TongAnhMoScsc),
                            TongLoiScscHoacUon = r.Sum(g => g.TongLoiScscHoacUon),
                            TongDiemScsc = r.Sum(g => g.TongDiemScsc),
                            TongErrorShape = r.Sum(g => g.TongErrorShape),
                            TongErrorComplatetion = r.Sum(g => g.TongErrorComplatetion),
                            TongErrorSharpNess = r.Sum(g => g.TongErrorSharpNess),
                            TongKhongAnhUon = r.Sum(g => g.TongKhongAnhUon),
                            TongBillUon = r.Sum(g => g.TongBillUon),
                            TongAnhUon = r.Sum(g => g.TongAnhUon),
                            TongMoLechUon = r.Sum(g => g.TongMoLechUon),
                            TongLoiUon = r.Sum(g => g.TongLoiUon),
                            TongErrorHairTip = r.Sum(g => g.TongErrorHairTip),
                            TongErrorHairRoots = r.Sum(g => g.TongErrorHairRoots),
                            TongErrorHairWaves = r.Sum(g => g.TongErrorHairWaves),
                            TotalBill = r.Sum(g => g.TotalBill),
                            TongDiemUon = r.Sum(g => g.TongDiemUon),
                            TongMoLechUonScsc = r.Sum(g => g.TongMoLechUonScsc),
                            TongErrorConnective = r.Sum(g => g.TongErrorConnective),
                        }).ToList();

                    var data = (from a in list
                                join b in listSalon on a.SalonId equals b.Id
                                select new
                                {
                                    SalonId = a.SalonId ?? 0,
                                    ShortName = b.ShortName,
                                    Order = b.Order,
                                    TotalBill = a.TotalBill ?? 0,
                                    TotalBillNotImage = a.TongKhongAnhScsc ?? 0,
                                    TotalBill_DanhGiaNotMolech = a.TongKhongMoScsc ?? 0,
                                    TotalBill_DanhGiaImgMoLech = a.TongAnhMoScsc ?? 0,
                                    TotalErrorSCSC = a.TongAnhLoiScsc ?? 0,
                                    TotalSCSC = a.TongDiemScsc ?? 0,
                                    TongBillNotAnhUon = a.TongKhongAnhUon ?? 0,
                                    TongBillAnhMoLech = a.TongMoLechUon ?? 0,
                                    TongBillUonChamLoi = a.TongLoiUon ?? 0,
                                    TongBillAnhUon = a.TongAnhUon ?? 0,
                                    TotalMoLechSCSCUon = a.TongMoLechUonScsc ?? 0,
                                    TotalErrorSCSCUon = a.TongLoiScscHoacUon ?? 0,
                                    TotalPointSCSCCurling = a.TongDiemUon ?? 0,
                                    Percent_Bill_NotImg = (a.TongKhongAnhScsc ?? 0) == 0 ? 0 : Math.Round((double)((double)a.TongKhongAnhScsc / a.TotalBill * 100), 2),
                                    BillChuaDanhGia = (a.TotalBill ?? 0) - (a.TongKhongAnhScsc ?? 0) - (a.TongKhongMoScsc ?? 0) - (a.TongAnhMoScsc ?? 0),
                                    Percent_BillError = Math.Round((double)(((double)(a.TongAnhLoiScsc ?? 0) - a.TongAnhMoScsc) / a.TotalBill * 100), 2),
                                    Percent_BillImg_MoLech = (a.TongAnhMoScsc ?? 0) == 0 ? 0 : Math.Round((double)((double)a.TongAnhMoScsc / a.TotalBill * 100), 2),
                                    PointSCSC_TB = ((a.TongKhongMoScsc ?? 0) + a.TongAnhMoScsc) == 0 ? 0 :
                                                    Math.Round((double)((double)(a.TongDiemScsc ?? 0) / ((a.TongKhongMoScsc ?? 0) + a.TongAnhMoScsc)), 2),
                                    Percent_ErrorKCS = (a.TongLoiScscHoacUon ?? 0) == 0 ? 0 : Math.Round((double)((double)a.TongLoiScscHoacUon / a.TotalBill * 100), 2),
                                    ErrorShapeId = a.TongErrorShape ?? 0,
                                    ErrorConnective = a.TongErrorConnective ?? 0,
                                    ErrorSharpNess = a.TongErrorSharpNess ?? 0,
                                    ErrorComplatetion = a.TongErrorComplatetion ?? 0,
                                    BillNotImgUon = (a.TongBillUon ?? 0) != 0 ? Math.Round((double)((double)a.TongKhongAnhUon / a.TongBillUon * 100), 2) : 0,
                                    BillNotImgUonMoLech = (a.TongBillUon ?? 0) != 0 ? Math.Round((double)((double)a.TongMoLechUon / a.TongBillUon), 2) : 0,
                                    BillUonChamLoi = (a.TongAnhUon ?? 0) != 0 ? Math.Round((double)((double)a.TongLoiUon / a.TongAnhUon * 100), 2) : 0,
                                    TotalDiemUonTB = (a.TongAnhUon ?? 0) != 0 ? Math.Round((double)((double)a.TongDiemUon / a.TongAnhUon), 2) : 0,
                                    TotalBillUon = a.TongBillUon ?? 0,
                                    Percent_ErrorKCSUon = (a.TongBillUon ?? 0) != 0 ? Math.Round((double)((double)(a.TongLoiUon - a.TongMoLechUon) / a.TongBillUon * 100), 2) : 0,
                                    ErrorHairTip = a.TongErrorHairTip ?? 0,
                                    ErrorHairRoots = a.TongErrorHairRoots ?? 0,
                                    ErrorHairWaves = a.TongErrorHairWaves ?? 0
                                }).OrderBy(r => r.Order).ToList();
                    return Ok(data);
                }
                else
                {
                    return Ok();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        private List<StatisticScscError> CalculatorScsc(List<OutStatisticScsc> listBill, List<ScscCategory> listCategoryScsc, DateTime date)
        {

            try
            {
                // Tổng Bill
                var listTongBill = listBill.GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                {
                    StylistId = r.Key.StylistId,
                    SalonId = r.Key.SalonId,
                    TongBill = r.Count()
                }).ToList();
                // tổng Bill không ảnh
                var listTongBillKhongAnh = listBill.Where(r => r.IsImages == false || r.IsImages == null)
                    .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                    {
                        StylistId = r.Key.StylistId,
                        SalonId = r.Key.SalonId,
                        TongKhongAnhScsc = r.Count()
                    }).ToList();
                // Tong bill anh mo lech
                var listTongBillMoLech = listBill.Where(r => r.ImageError == true)
                    .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                    {
                        StylistId = r.Key.StylistId,
                        SalonId = r.Key.SalonId,
                        TongMoLechScsc = r.Count()
                    }).ToList();

                // Tong khong mo lech
                var listTongKhongMoLech = listBill.Where(r => r.ImageError == false)
                    .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                    {
                        StylistId = r.Key.StylistId,
                        SalonId = r.Key.SalonId,
                        TongKhongMoLech = r.Count()
                    }).ToList();
                // Tong Loi ShapeId
                var listTongShapeId = (from a in listBill
                                       join b in listCategoryScsc on a.ShapeId equals b.IdScscCate
                                       where b.ScscCatePoint == 0
                                       select a)
                                       .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                                       {
                                           StylistId = r.Key.StylistId,
                                           SalonId = r.Key.SalonId,
                                           TongShapeId = r.Count()
                                       }).ToList();
                // Tong loi ConnectTive 
                var listTongConnectTive = (from a in listBill
                                           join b in listCategoryScsc on a.ConnectTiveId equals b.IdScscCate
                                           where b.ScscCatePoint == 0
                                           select a)
                                       .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                                       {
                                           StylistId = r.Key.StylistId,
                                           SalonId = r.Key.SalonId,
                                           TongConnectTiveId = r.Count()
                                       }).ToList();
                // Tong loi SharpNess
                var listTongSharpNess = (from a in listBill
                                         join b in listCategoryScsc on a.SharpNessId equals b.IdScscCate
                                         where b.ScscCatePoint == 0
                                         select a)
                                       .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                                       {
                                           StylistId = r.Key.StylistId,
                                           SalonId = r.Key.SalonId,
                                           TongSharpNessId = r.Count()
                                       }).ToList();
                // Tong loi Complation
                var listTongComplation = (from a in listBill
                                          join b in listCategoryScsc on a.ComplatetionId equals b.IdScscCate
                                          where b.ScscCatePoint == 0
                                          select a)
                                       .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                                       {
                                           StylistId = r.Key.StylistId,
                                           SalonId = r.Key.SalonId,
                                           TongComplationId = r.Count()
                                       }).ToList();
                //Tong diem cham scsc
                var listTongDiemCham = listBill.GroupBy(r => new { r.SalonId, r.StylistId })
                    .Select(r => new
                    {
                        StylistId = r.Key.StylistId,
                        SalonId = r.Key.SalonId,
                        TongDiemScsc = r.Sum(g => g.PointError ?? 0)
                    }).ToList();
                // Tong anh cham loi
                var listTongChamLoi = listBill.Where(r => r.PointError == 0)
                    .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                    {
                        StylistId = r.Key.StylistId,
                        SalonId = r.Key.SalonId,
                        TongLoiScsc = r.Count()
                    }).ToList();
                // Tong bill khong anh uon
                var listKhongAnhUon = listBill.Where(r => r.IsUon == true && r.IsImgUon == false)
                    .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                    {
                        StylistId = r.Key.StylistId,
                        SalonId = r.Key.SalonId,
                        TongKhongAnhUon = r.Count()
                    }).ToList();
                // Tong bill co anh uon
                var listAnhUon = listBill.Where(r => r.IsUon == true && r.IsImgUon == true)
                    .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                    {
                        StylistId = r.Key.StylistId,
                        SalonId = r.Key.SalonId,
                        TongAnhUon = r.Count()
                    }).ToList();
                // Tong bill uon
                var listBillUon = listBill.Where(r => r.IsUon == true)
                    .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                    {
                        StylistId = r.Key.StylistId,
                        SalonId = r.Key.SalonId,
                        TongBillUon = r.Count()
                    }).ToList();
                // Tong bill co anh uon nhung bi cham mo lech
                var listAnhUonMoLech = listBill.Where(r => r.IsUon == true && r.ImageErrorCurling == true && r.IsImgUon == true)
                    .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                    {
                        StylistId = r.Key.StylistId,
                        SalonId = r.Key.SalonId,
                        TongUonMoLech = r.Count()
                    }).ToList();
                // Tong bill cham loi ngon toc
                var listNgonToc = (from a in listBill
                                   join b in listCategoryScsc on a.HairTipId equals b.IdScscCate
                                   where b.ScscCatePoint == 0 && a.IsImgUon == true && a.IsUon == true
                                   select a)
                                   .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                                   {
                                       StylistId = r.Key.StylistId,
                                       SalonId = r.Key.SalonId,
                                       TongNgonToc = r.Count()
                                   }).ToList();
                // tong bill cham loi chan toc
                var listChanToc = (from a in listBill
                                   join b in listCategoryScsc on a.HairRootId equals b.IdScscCate
                                   where b.ScscCatePoint == 0 && a.IsImgUon == true && a.IsUon == true
                                   select a)
                                   .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                                   {
                                       StylistId = r.Key.StylistId,
                                       SalonId = r.Key.SalonId,
                                       TongNgonToc = r.Count()
                                   }).ToList();
                // tong bill cham loi song toc
                var listSongToc = (from a in listBill
                                   join b in listCategoryScsc on a.HairWavesId equals b.IdScscCate
                                   where b.ScscCatePoint == 0 && a.IsImgUon == true && a.IsUon == true
                                   select a)
                                   .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                                   {
                                       StylistId = r.Key.StylistId,
                                       SalonId = r.Key.SalonId,
                                       TongSongToc = r.Count()
                                   }).ToList();
                // tong bill uon cham loi
                var listLoiUon = listBill.Where(r => r.IsUon == true && r.TotalPointSCSCCurling == 0 && r.IsImgUon == true)
                    .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                    {
                        StylistId = r.Key.StylistId,
                        SalonId = r.Key.SalonId,
                        TongLoiUon = r.Count()
                    }).ToList();
                // Tong Diem Uon
                var listTongDiemUon = listBill.Where(r => r.IsUon == true && r.IsImgUon == true)
                    .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                    {
                        StylistId = r.Key.StylistId,
                        SalonId = r.Key.SalonId,
                        TongDiemUon = r.Sum(g => g.TotalPointSCSCCurling ?? 0)
                    }).ToList();
                // Tong bill mo lech uon hoac scsc
                var listMoLechUonScsc = listBill.Where(r => r.ImageError == true || (r.IsUon == true && r.ImageErrorCurling == true))
                    .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                    {
                        StylistId = r.Key.StylistId,
                        SalonId = r.Key.SalonId,
                        TongMoLechUonScsc = r.Count()
                    }).ToList();
                // Tong Loi Uon Hoac Scsc
                var listLoiUonHoacScsc = listBill.Where(r => r.PointError == 0 || (r.IsUon == true && r.TotalPointSCSCCurling == 0))
                     .GroupBy(r => new { r.SalonId, r.StylistId }).Select(r => new
                     {
                         StylistId = r.Key.StylistId,
                         SalonId = r.Key.SalonId,
                         TongLoiUonScsc = r.Count()
                     }).ToList();
                var listStaffSalonBill = listBill.Select(r => new { r.StylistId, r.SalonId }).GroupBy(r => new { r.StylistId, r.SalonId }).Select(r => new
                {
                    StylistId = r.Key.StylistId,
                    SalonId = r.Key.SalonId
                }).ToList();
                var listStatisticScscError = new List<StatisticScscError>();
                foreach (var item in listStaffSalonBill)
                {
                    var record = new StatisticScscError();
                    record.SalonId = item.SalonId;
                    record.StylistId = item.StylistId;
                    record.TongAnhLoiScsc = listTongChamLoi.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongLoiScsc).FirstOrDefault();
                    record.TongAnhMoScsc = listTongBillMoLech.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongMoLechScsc).FirstOrDefault();
                    record.TongAnhUon = listAnhUon.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongAnhUon).FirstOrDefault();
                    record.TongBillUon = listBillUon.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongBillUon).FirstOrDefault();
                    record.TongDiemScsc = listTongDiemCham.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongDiemScsc).FirstOrDefault();
                    record.TongErrorComplatetion = listTongComplation.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongComplationId).FirstOrDefault();
                    record.TongErrorHairRoots = listChanToc.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongNgonToc).FirstOrDefault();
                    record.TongErrorHairTip = listNgonToc.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongNgonToc).FirstOrDefault();
                    record.TongErrorHairWaves = listSongToc.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongSongToc).FirstOrDefault();
                    record.TongErrorShape = listTongShapeId.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongShapeId).FirstOrDefault();
                    record.TongErrorSharpNess = listTongSharpNess.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongSharpNessId).FirstOrDefault();
                    record.TongKhongAnhScsc = listTongBillKhongAnh.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongKhongAnhScsc).FirstOrDefault();
                    record.TongKhongAnhUon = listKhongAnhUon.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongKhongAnhUon).FirstOrDefault();
                    record.TongKhongMoScsc = listTongKhongMoLech.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongKhongMoLech).FirstOrDefault();

                    record.TongLoiScscHoacUon = listLoiUonHoacScsc.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongLoiUonScsc).FirstOrDefault();
                    record.TongLoiUon = listLoiUon.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongLoiUon).FirstOrDefault();
                    record.TongMoLechUon = listAnhUonMoLech.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongUonMoLech).FirstOrDefault();
                    record.TotalBill = listTongBill.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongBill).FirstOrDefault();
                    record.TongDiemUon = listTongDiemUon.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongDiemUon).FirstOrDefault();
                    record.TongMoLechUonScsc = listMoLechUonScsc.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongMoLechUonScsc).FirstOrDefault();
                    record.TongErrorConnective = listTongConnectTive.Where(r => r.StylistId == item.StylistId && r.SalonId == item.SalonId).Select(r => r.TongConnectTiveId).FirstOrDefault();
                    record.WorkDate = date.Date;
                    record.CreatedTime = DateTime.Now;
                    record.IsDelete = false;
                    listStatisticScscError.Add(record);
                }
                return listStatisticScscError;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}





