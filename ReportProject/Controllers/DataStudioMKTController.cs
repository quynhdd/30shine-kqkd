﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ReportProject.Models.JsonModels;
using ReportProject.Respository.Interface;

namespace ReportProject.Controllers
{
    [Produces("application/json")]
    [Route("api/data-studio")]
    public class DataStudioMKTController : Controller
    {
        private readonly IOperationReportStatisticRepo OperationReportStatisticRepo;
        private readonly ISalonRepo SalonRepo;
        private readonly IBookingRepo BookingRepo;
        private readonly IConfiguration Configuration;
        static HttpClient client = new HttpClient();

        public DataStudioMKTController(IOperationReportStatisticRepo operationReportStatisticRepo, ISalonRepo salonRepo, IBookingRepo bookingRepo, IConfiguration configuration)
        {
            OperationReportStatisticRepo = operationReportStatisticRepo;
            SalonRepo = salonRepo;
            BookingRepo = bookingRepo;
            Configuration = configuration;
        }

        [HttpGet]
        public async Task<IActionResult> GetData()
        {
            try
            {
                var date = DateTime.Today.AddDays(-1);

                var report = await OperationReportStatisticRepo.Get(date, DateTime.Today);

                if (!report.Any())
                {
                    return NoContent();
                }

                var salon = await SalonRepo.Get();

                var bookings = await BookingRepo.GetBookings(date);

                foreach (var item in report)
                {
                    var shortName = salon.Where(a => a.Id == item.SalonId).FirstOrDefault().ShortName;

                    var statistic = report.Where(a => a.SalonId == item.SalonId).FirstOrDefault();

                    var income = JsonConvert.DeserializeObject<List<RevenueJson>>(item.Revenue);

                    var totalService = income.Where(a => a.Type == 1).Sum(a => a.Income);
                    var totalProduct = income.Where(a => a.Type == 0).Sum(a => a.Income);

                    var booking = bookings.Where(a => a.SalonId == item.SalonId).ToList();

                    var bookingApp = booking.Where(a => a.Os == 0 || a.Os == 2).Count();
                    var bookingWeb = booking.Where(a => a.Os == 1).Count();

                    var data = new
                    {
                        date = DateTime.Today.ToString("dd/MM/yyyy"),
                        salon = shortName,
                        customer = statistic.TotalBill,
                        comeBackPercent = Math.Round(((double) statistic.BillNewCustomer / (double) statistic.TotalBill), 4),
                        income = statistic.TotalIncome,
                        serviceIncome = totalService,
                        productIncome = totalProduct,
                        bookOnline = booking.Count(),
                        bookApp = bookingApp,
                        bookWeb = bookingWeb

                    };

                    var domainGooglePostAPI = Configuration.GetSection("Excel:MKT").Value;

                    var bookingContent = JsonConvert.SerializeObject(data);
                    var buffer = System.Text.Encoding.UTF8.GetBytes(bookingContent);
                    var byteContent = new ByteArrayContent(buffer);
                    byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    var res = await client.PostAsync(domainGooglePostAPI, byteContent);
                }

                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}