﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APICheckout.Extentions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using ReportProject.Models.Solution_30Shine;
using ReportProject.Models.Solution_30shine_Real;
using ReportProject.Respository.Implement;
using ReportProject.Respository.Interface;
using Swashbuckle.AspNetCore.Swagger;

namespace ReportProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<Solution_30shineContextV1>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("Solution_30shineV1")));
            // Cấu hình cũ.
            services.AddTransient<IAccountingReportRepo, AccountingReportRepo>();//
            services.AddTransient<IBillServiceRepo, BillServiceRepo>();//
            services.AddTransient<IFlowProductRepo, FlowProductRepo>();//
            services.AddTransient<IFlowServiceRepo, FlowServiceRepo>();//
            services.AddTransient<IOperationReportRepo, OperationReportRepo>();//
            services.AddTransient<IReportRepo, ReportRepo>();
            services.AddTransient<ISalonRepo, SalonRepo>();//
            services.AddTransient<ISalonDailyCostRepo, SalonDailyCostRepo>();//
            services.AddTransient<IConfigRepo, ConfigRepo>();//
            services.AddTransient<IStaffBillServiceDetailRepo, StaffBillServiceDetailRepo>();//
            services.AddTransient<IStaffRepo, StaffRepo>();//
            services.AddTransient<IFlowTimeKeepingRepo, FlowTimeKeepingRepo>();//
            services.AddTransient<IBookingRepo, BookingRepo>(); //
            services.AddTransient<IBookingTempRepo, BookingTempRepo>();//
            services.AddTransient<IBookHourRepo, BookHourRepo>();//
            services.AddTransient<IServiceRepo, ServiceRepo>();//
            services.AddTransient<IMonitorStaffErrorRepo, MonitorStaffErrorRepo>();//
            services.AddTransient<IRatingDetailRepo, RatingDetailRepo>();//
            services.AddTransient<IStatictisReportOperationRepo, StatictisReportOperationRepo>();//
            services.AddTransient<ITblConfigRepo, TblConfigRepo>();//
            services.AddTransient<IStaffBillServiceDetailInforRepo, StaffBillServiceDetailInforRepo>();//
            services.AddTransient<IRatingConfigPointRepo, RatingConfigPointRepo>();//
            services.AddTransient<ISalaryConfigRepo, SalaryConfigRepo>();//
            services.AddTransient<IServiceSalonConfigRepo, ServiceSalonConfigRepo>();//
            services.AddTransient<IStoreProcedureRepo, StoreProcedureRepo>();
            services.AddTransient<ICategoryRepo, CategoryRepo>();
            services.AddTransient<IOperationReportStatisticRepo, OperationReportStatisticRepo>();
            services.AddTransient<IOperationReportConfigRepo, OperationReportConfigRepo>();
            services.AddTransient<IBookHourSubRepo, BookHourSubRepo>();
            services.AddTransient<ISalonTypeConfigRepo, SalonTypeConfigRepo>();
            services.AddTransient<IServiceSalonTypeConfigRepo, ServiceSalonTypeConfigRepo>();
            // Cấu hình mới V1
            services.AddTransient<IPushNotice, PushNotice>();
            services.AddTransient<Respository.InterfaceV1.IBillserviceRepo, Respository.ImplementV1.BillserviceRepo>();
            services.AddTransient<Respository.InterfaceV1.ISCSCCheckErrorRepo, Respository.ImplementV1.SCSCCheckErrorRepo>();
            services.AddTransient<Respository.InterfaceV1.ISalonRepo, Respository.ImplementV1.SalonRepo>();
            services.AddTransient<Respository.InterfaceV1.IScscCategoryRepo, Respository.ImplementV1.ScscCategoryRepo>();
            services.AddTransient<Respository.InterfaceV1.IStatisticScscErrorRepo, Respository.ImplementV1.StatisticScscErrorRepo>();
            services.AddTransient<Respository.InterfaceV1.IFeedbackPointServiceRepo, Respository.ImplementV1.FeedbackPointServiceRepo>();
            services.AddTransient<Respository.InterfaceV1.IMonitorStaffErrorRepo, Respository.ImplementV1.MonitorStaffErrorRepo>();
            services.AddTransient<Respository.InterfaceV1.IStaffRepo, Respository.ImplementV1.StaffRepo>();
            services.AddTransient<Respository.InterfaceV1.IOperationReportStatisticRepo, Respository.ImplementV1.OperationReportStatisticRepo>();
            services.AddTransient<Respository.InterfaceV1.IStaffTypeRepo, Respository.ImplementV1.StaffTypeRepo>();
            services.AddTransient<Respository.InterfaceV1.IPermissionSalonAreaRepo, Respository.ImplementV1.PermissionSalonAreaRepo>();
            


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "API Report",
                    Description = "AV Service Team",
                    TermsOfService = "None",
                });
                c.IgnoreObsoleteActions();
                c.IgnoreObsoleteProperties();

                c.CustomSchemaIds((type) => type.FullName);

                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "ReportProject.xml");
                c.IncludeXmlComments(xmlPath);
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AnyOrigin", builder =>
                {
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowCredentials()
                        .AllowAnyHeader();
                });
            });

            services.AddMvc();
            services.AddDirectoryBrowser();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddFile("Logs/myapp-{Date}.txt");

            //session
            //app.UseSession();

            // Enable use files
            app.UseFileServer();
            app.UseDefaultFiles();
            app.UseStaticFiles();


            app.UseCors(builder =>
            {
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
                builder.AllowCredentials();
                builder.AllowAnyOrigin();
            });

            app.UseMvc();

            //app.UseSwagger();
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
            //});

            app.UseSwagger(c =>
            {
                c.RouteTemplate = "report-project/swagger/{documentName}/swagger.json";
            });
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/report-project/swagger/v1/swagger.json", "Sample API");
                c.RoutePrefix = "report-project/swagger";
            });

            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //    // add new
            //    app.UseBrowserLink();

            //    //app.UseSwagger();
            //    app.UseSwagger(c =>
            //    {
            //        c.RouteTemplate = "SampleApi/swagger/{documentName}/swagger.json";
            //    });

            //    //app.UseSwaggerUI(c =>
            //    //{
            //    //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");

            //    //    //c.SwaggerEndpoint("/swagger/index.html/v1/swagger.json", "API V1");
            //    //});

            //    app.UseSwaggerUI(c =>
            //    {
            //        c.SwaggerEndpoint("/SampleApi/swagger/v1/swagger.json", "Sample API");
            //        c.RoutePrefix = "SampleApi/swagger";
            //    });
            //    //end add
            //}
        }
    }
}
