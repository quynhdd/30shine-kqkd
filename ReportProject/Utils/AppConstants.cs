﻿namespace APICheckout.Extentions
{
    public class AppConstants
    {
        public static string APP_ID = "myApp001";
        public static string API_ID = "myAPI001";
        public readonly string[] GROUP_OR_USERNAME = { "sys", "duycs", "admin", "slm", "gsm", "gs" };
        public readonly string[] PERMISSTIONS_BASIC = { "fullControl", "modify", "readExcute", "read", "listContents", "write", "special" };
        public readonly string[] RESOURCE_TABLE = { "table1", "table2", "table3" };
        public readonly string[] RESOURCE_API = { "api1", "api2", "api3" };
        public readonly string[] ACTION = { "add", "remove", "edit", "view" };
        public const string ACTION_READ = "read";
        public const string ACTION_WRITE = "write";
        public const string ACTION_EDIT = "edit";
        public const string ACTION_VIEW = "view";
        public const string ACTION_DEL = "del";


        //notify
        public const bool IS_LOG = false;
        //public const string GROUP_SLACK = "CBM3BGVA4";
        //public const string API_PUSH_NOTIC_TO_SLACK = "https://api-push-notic.30shine.com/api/pushNotice/slack";
        public const string TEAM = "api";
        public const string MODULE_NAME = "30Shine\\API-CheckOut";
        public const string TYPE_ERROR = "Error";
        public const string TYPE_INFO = "Info";
        public const string TYPE_WARNING = "Warning";

        //date
        public const string DATE_FORMAT = "dd-MM-yyyy";


        //
        public const int SALON_NEAR_SLOT = 5;

        /// <summary>
        /// mỹ phẩm sau khi bán
        /// </summary>
        public const int COSMETIC_SELL = 1;

        /// <summary>
        /// category cua membership trong product
        /// </summary>
        public const int CATEGORY_MEMBER_SHIP = 98;

        //
        public const int PRODUCT_MEMBER_TYPE_MONTH = 1;
        public const int PRODUCT_MEMBER_TYPE_DAY = 2;
    }
}
