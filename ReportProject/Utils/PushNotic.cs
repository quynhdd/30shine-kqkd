﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ReportProject.Utils;

namespace ReportProject.Utils
{
    public static class PushNotic
    {
        public static void PushErrorToSlack(string className, string methodName, string messageError)
        {
            try
            {
                string uri = Constant.API_PUSH_NOTIC_TO_SLACK;
                var content = new
                {
                    list_group_id = new[] { Constant.GROUP_SLACK },
                    team = Constant.TEAM,
                    message = "Error: " + messageError + "__ClassName: " + className + "__MethodName: " + methodName,
                    module_name = Constant.MODULE_NAME
                };
                new System.Net.Http.HttpClient().PostAsync(uri, new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json"));
            }
            catch (Exception e) { throw e; }
        }

        public static void PushInfoToSlack(string message)
        {
            string uri = Constant.API_PUSH_NOTIC_TO_SLACK;
            var content = new
            {
                //list_group_id = new[] { },
                team = Constant.TEAM,
                message = "Info: " + message,
                module_name = Constant.MODULE_NAME
            };
            new System.Net.Http.HttpClient().PostAsync(uri, new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json"));
        }
    }
}
