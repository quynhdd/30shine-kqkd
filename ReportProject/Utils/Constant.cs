﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Edm.Expressions;

namespace ReportProject.Utils
{
    public static class Constant
    {
        #region DateTime Format
        public const string DATETIME = "d-M-yyyy";
        public const string DATETIME2 = "d/M/yyyy";
        #endregion


        #region Variable
        public const int ALL_SALON_ID = 0;
        public const int SALONTEST = 24;
        public const int ALL_TYPE_STAFF_SALARY = 411;
        public const string BU_LUONG = "bu_luong";
        public const string BAO_HIEM = "bao_hiem";
        public const string KHAC = "khac";
        public const string XU_PHAT = "xu_phat";
        public const string UNG_LUONG = "ung_luong";
        public const string LUONG_5C = "luong_5c";
        public const string THUE_THU_NHAP_CA_NHAN = "thue_thu_nhap_ca_nhan";
        public const string EMAIL_KEY = "email_report";
        public const string PRE_EMAIL_KEY = "pre_email_report";
        public const string KEY_SERVICE_FOLLOW = "service_config";

        public const int ADD_DAY = 8;

        public const int GIFT_CARD_TO_CUSTOMER = 86;
        public const int SHINE_COMBO = 53;
        public const int KID_COMBO = 74;
        public const int PROTEIN = 69;
        public const int CURLING = 16;
        public const int STRAIGHTEN = 75;
        public const int HAIR_DYED = 14;
        public const int HAIR_BLEACH = 17;
        public const int HAIR_BLEACH_X2 = 24;
        public const int MASK = 90;
        public const int EXFOLIATION = 91;


        public const int RAT_HAI_LONG = 3;
        public const int HAI_LONG = 2;
        public const int KHONG_HAI_LONG = 1;

        public const int STYLIST = 1;
        public const int SKINNER = 2;

        public const int NOT_OVERTIME = 0;
        public const int OVERTIME_HOUR = 1;
        public const int OVERTIME_DAY = 2;

        public const int NOT_DISCOUNT = 0;

        public const int PRICE_COMPARE = 3000000;

        public const int TIME_WAITED = 16;

        public const string RATING_CONVENTION_RHL = "Rất hài lòng";
        public const string RATING_CONVENTION_HL = "Hài lòng";
        public const string RATING_CONVENTION_KHL = "Chưa hài lòng";

        public const int COEFICIENT_RATING_4_SHINECOMBO = 4;

        public const string SHINE_COMBO_100K = "Shine Combo 100k";
        #endregion


        #region Message
        public const string UPDATE = "Update!";
        public const string SUCCESS = "Success!";
        #endregion

        #region HOST API
        public const string APIINVENTORY = "http://api-inventory.30shine.com/";
        public const string APISALARY = "https://apifinancial.30shine.com/";
        public const string APIPUSHNOTIC = "https://api-push-notic.30shine.com/";
        #endregion

        #region Slack
        public const string GROUP_SLACK = "CA9L6T0JH";
        public const string API_PUSH_NOTIC_TO_SLACK = "https://api-push-notic.30shine.com/api/pushNotice/slack";
        public const string TEAM = "api";
        public const string MODULE_NAME = "30Shine\\API_Report";
        #endregion

        #region uri
        public const string uri_PUSH_NOTIC_EMAIL = "api/pushNotice/email";
        #endregion

    }
}
