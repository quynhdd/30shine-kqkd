﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Utils
{
    public static class FormatExtension
    {
        public static string FormatNumber(double? num)
        {
            try
            {
                var formatNum = string.Format("{0:#,0.##}", num);
                return formatNum;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static IEnumerable<int> ConvertString(this string data)
        {
            return data.Split(",").Select(a => int.TryParse(a.Trim(), out var integer) ? integer : 0);
        }
    }
}
