﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportProject.Utils
{
    public class DatetimeExtension
    {
        public static List<DateTime?> ListStringToDateTime(string DatetimeFormat, params string[] multiString)
        {
            List<DateTime?> list = new List<DateTime?>();
            foreach (var item in multiString)
            {
                var dateTime = StringToDateTime(DatetimeFormat, item);
                if (dateTime == null)
                {
                    return null;
                }
                list.Add(dateTime);
            }
            return list;
        }

        public static DateTime? StringToDateTime(string DatetimeFormat, string strDatetime)
        {
            if (DateTime.TryParseExact(strDatetime, DatetimeFormat, System.Globalization.CultureInfo.InvariantCulture,
                System.Globalization.DateTimeStyles.NoCurrentDateDefault, out var dateTime))
            {
                return dateTime;
            }
            return null;
        }

        public static long ToUnixTimeSeconds(DateTime? dateTime)
        {
            var date = dateTime ?? new DateTime(1970, 1, 1);
            var epochTicks = new TimeSpan(new DateTime(1970, 1, 1).Ticks);
            var unixTicks = new TimeSpan(date.Ticks) - epochTicks;
            var unixTime = unixTicks.TotalSeconds;
            return (long)unixTime;
        }

        public static long ToUnixTimeSeconds(string datetimeFormat, string dateTime)
        {
            if (dateTime == null) return 0;
            var mDateTime = StringToDateTime(datetimeFormat, dateTime);
            return mDateTime == null ? 0 : ToUnixTimeSeconds(mDateTime);
        }

        public static long GetCurrentUnixTimeStamp()
        {
            return DateTimeOffset.Now.ToUnixTimeSeconds();
        }

        public static string DateTimeToString(string dateTimeFormat, DateTime dateTime)
        {
            return string.IsNullOrEmpty(dateTimeFormat) ? null : dateTime.ToString(dateTimeFormat);
        }

        public static string DateTimeToString(string dateTimeFormat, DateTime? dateTime)
        {
            if (dateTime == null || string.IsNullOrEmpty(dateTimeFormat))
            {
                return null;
            }

            var date = (DateTime)dateTime;

            return DateTimeToString(dateTimeFormat, dateTime);
        }

        public static DateTime? UnixTimeStampToDateTime(long unixTimeStamp)
        {
            if (unixTimeStamp == 0)
            {
                return null;
            }
            var dateTime = new DateTime(1970, 1, 1);
            dateTime = dateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dateTime;
        }

        public static string UnixTimeStampToDateTime(string dateTimeFormat, long unixTimeStamp)
        {
            var dateTime = UnixTimeStampToDateTime(unixTimeStamp);
            return dateTime == null ? null : DateTimeToString(dateTimeFormat, dateTime);
        }
    }
}
